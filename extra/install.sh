#!/bin/sh
INSTALL_DIR="/opt/pasteque"
DIR=$(pwd)

rm -rf $INSTALL_DIR
mv $DIR $INSTALL_DIR
cp -f $INSTALL_DIR/pasteque /usr/bin/
chmod 755 /usr/bin/pasteque
cp -f $INSTALL_DIR/pasteque.desktop /usr/share/applications/
rm -f /usr/share/icons/pasteque.png
ln -s $INSTALL_DIR/pasteque-logo-128.png /usr/share/icons/pasteque.png
