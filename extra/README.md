Pasteque Desktop 8.24
=====================

Pasteque Desktop is the in-store POS software to register sales for desktop and
laptop computers. It connects to Pasteque API retrieve data and store sales.


Requirements
------------

Pasteque Desktop requires Java 1.7. and isn't compatible with Java 9


Launch Pasteque Desktop
-----------------------

### For Windows

Run `start.bat` to start Pasteque Desktop. You will be prompt your credentials
to connect to your account on the API.

You may run `configure.bat` to directly open the configuration screen.

### For Linux

Run `start.sh` to start Pasteque Desktop. You will be prompt your credentials
to connect to your account on the API.

You may run `configure.sh` to directly open the configuration screen.


Licensing
---------

Pasteque is licensed under GNU General Public Licence version 3 or (at your
option) any later version.

See `licensing/gpl-3.0.txt` for the whole licence text.

Pasteque includes third party dependencies, see
`licensing/Openbravo POS notice.txt` for the detail of it.
