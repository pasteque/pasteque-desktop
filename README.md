Pasteque Desktop is the in-store POS client to register sales for desktop and
laptop computers. It connects to Pasteque API retrieve data and store sales.

For a quick look at available tasks for compiling, testing and making releases
use `ant -p` to list tasks once you got familiar with the build process
described in this file.


Compile and test Pasteque Desktop
=================================


Requirements
------------

Pasteque Desktop requires Java 1.6. and isn't compatible with Java 9

The compilation process requires ant, unit testing requires JUnit.


Build
-----

Compile the source files with `ant build`. The generated files are stored
under `build/bin`.

You can then start with `./build/bin/start.sh` for Linux or run
`build/bin/start.bat` for Windows.

Once build is called, you can do a quick-compile with `ant debug` instead of
build. Only modified source code files are recompiled, other resources like
translation files and images are not updated.


Test
----

Running the unit tests requires JUnit. The tests are created for JUnit 4.

* Download JUnit 4 (https://search.maven.org/search?q=g:junit%20AND%20a:junit)
* Download hamcrest (https://search.maven.org/artifact/org.hamcrest/hamcrest-core/1.3/jar)

Install them by putting both jar in JUnit path for Pasteque. By default ant
will look for them under `junit` in your home folder. You can create a
`local.properties` file in the base directory of Pasteque and put your folder
into `junit.path`. For example `junit.path=${user.home}/.local/lib/junit`.

Then, `ant test` will compile the test classes and run them. `ant dtest` can
help when writing tests to quickly recompile tests once `ant test` is called.

All the test tasks doesn't recompile Pasteque. When making changes, don't forget
to call `ant debug` before running the tests again with `ant dtest`.


Creating releases
=================

The task `ant installer` generates all the installation files, based upon
the distribution files and configuration stored into the `installer` directory.
It eases the installation by providing automatic processes.

Generating these installers requires multiple tools that must be installed
and linked into the project files. These are not required for development
and debugging, but are generated from the compiled distribution files.


Multi-OS installer
------------------

IzPack is a tool to create installers that runs with Java. This installer is
compatible with all systems, providing Java is already installed.

This installer is made with the version 5 of IzPack (http://izpack.org/).

Before generating the jar for the installer, follow these installation steps:

* Download IzPack and unpack it anywhere you want.
* Create the file `local.properties` in the base directory of Pasteque
  along the `build.xml` file.
* Add the property `izpack.path` in that file pointing to the IsPack directory.
  For example `izpack.path=${user.home}/.local/lib/IzPack`

The default configuration (without localpath.properties) checks for IzPack in
your home directory.

Once done, after building at least once with `ant build` and
`ant build-wrapexe64`, run `ant izpack` to build the installer jar.


Windows installer
-----------------

Launch4j is a cross-platform tool to generate an executable for a jar, and
checks if Java is available on the system. The Windows executable is built
by wrapping the installation jar from IzPack into an executable.

The configuration file was written for Launch4j 3.50, you can get it from
https://launch4j.sourceforge.net.

Before wrapping the installation jar into an executable, follow these
installation steps:

* Download Launch4j ant unpack it anywhere you want.
* Create the file `local.properties` in the base directory of Pasteque
  along the `build.xml` file.
* Add the property `launch4j.path` in that file pointing to the Launch4j directory.
  For example `launch4j.path=${user.home}/.local/lib/launch4j`

The default configuration (without localpath.properties) checks for launch4j in
your home directory.

Once done, generate the installation jar with `ant izpack` (see Multi-OS
installer above), then run `ant izpack-wrapexe` to create an executable that
includes the installation jar.


Licensing
=========

Pasteque is licensed under GNU General Public Licence version 3 or (at your
option) any later version.

See `licensing/gpl-3.0.txt` for the whole licence text.

Pasteque includes third party dependencies, see
`licensing/Openbravo POS notice.txt` for the detail of it.

Sounds:
- soft_error.aiff: Public domain, original by His Self (https://soundbible.com/1815-A-Tone.html)
