//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.instance;

import javax.swing.JFrame;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class InstanceManagerTest
{
    private InstanceManager manager1;
    private InstanceManager manager2;

    @Before
    public void setUp() {
        this.manager1 = new InstanceManager();
        this.manager2 = new InstanceManager();
    }

    @After
    public void tearDown() {
        if (this.manager1.isRegistered()) {
            this.manager1.clear();
        }
        if (this.manager2.isRegistered()) {
            this.manager2.clear();
        }
    }

    @Test
    public void testRecallNull() {
        Assert.assertNull(manager1.restoreInstance());
    }

    @Test
    public void testRegisterRecall() {
        Assert.assertTrue(manager1.registerStartingApp());
        Assert.assertEquals(manager1.getInstanceId(), manager1.restoreInstance());
    }

    /** Test restoring an instance from its own manager. */
    @Test
    public void testRegisterReadyRecall() {
        Assert.assertTrue(manager1.registerStartingApp());
        Assert.assertTrue(manager1.instanceReady(new JFrame()));
        Assert.assertEquals(manager1.getInstanceId(), manager1.restoreInstance());
    }

    /** Test restoring an instance form an other manager. */
    @Test
    public void testRecallOther() {
        Assert.assertTrue(manager1.registerStartingApp());
        Assert.assertEquals(manager1.getInstanceId(), manager2.restoreInstance());
    }
}
