//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.util;

import org.junit.Assert;
import org.junit.Test;

public class PriceTest
{
    private static final double DPREC = 0.0000000000000001;

    @Test
    public void testAdd() {
        // 1.14d + 1.13d = 2.2699999999999996
        Assert.assertNotEquals(2.27d, 1.14d + 1.13d, DPREC);
        Assert.assertEquals(2.27d, Price.add(1.14d, 1.13d), DPREC);
    }

    @Test
    public void testRoundDiff() {
        Assert.assertEquals(0.00345d, Price.roundDiff(1.12345d), DPREC);
        Assert.assertEquals(-0.00300d, Price.roundDiff(1.12700d), DPREC);
    }

    @Test
    public void testInQuantity() {
        Assert.assertEquals(1.2d, Price.inQuantity(1.0d, 1.2d), DPREC);
        Assert.assertEquals(1.25d, Price.inQuantity(1.0d, 1.253), DPREC);
        Assert.assertEquals(1.26d, Price.inQuantity(1.0d, 1.257), DPREC);
    }

    @Test
    public void testDiscount() {
        Assert.assertEquals(0.8d, Price.discount(1.0d, 0.2d), DPREC);
        Assert.assertEquals(0.75d, Price.discount(1.0d, 0.253d), DPREC);
        Assert.assertEquals(0.74d, Price.discount(1.0d, 0.257d), DPREC);
    }

    @Test
    public void testTaxFromTaxedPrices() {
        testUntax();
        testExtractTax();
    }

    /* Ran in testTaxFromTaxedPrices */
    public void testUntax() {
        Assert.assertEquals(1.0d, Price.untax(1.2d, 0.2d), DPREC);
        // 1.0 * (1-0.055) = 0.0945
        Assert.assertEquals(0.95d, Price.untax(1.0d, 0.055d), DPREC);
        // 0.89 * (1-0.055) = 0.84105
        Assert.assertEquals(0.84d, Price.untax(0.89d, 0.055d), DPREC);
    }

    /* Ran in testTaxFromTaxedPrices */
    public void testExtractTax() {
        Assert.assertEquals(0.2d, Price.extractTax(1.2d, 0.2d), DPREC);
        Assert.assertEquals(0.05d, Price.extractTax(1.0d, 0.055d), DPREC);
        Assert.assertEquals(0.05d, Price.extractTax(0.89d, 0.055d), DPREC);
    }

    @Test
    public void testTaxFromUntaxedPrices() {
        testGetTax();
    }

    /* Ran in testTaxFromUntaxedPrices */
    public void testGetTax() {
        Assert.assertEquals(0.2d, Price.getTax(1.0d, 0.2d), DPREC);
        // 1.12345 * 0.2 = 0.22469
        Assert.assertEquals(0.22d, Price.getTax(1.12345d, 0.2d), DPREC);
        // 1.14 * 0.2 = 0.228
        Assert.assertEquals(0.23d, Price.getTax(1.14d, 0.2d), DPREC);
    }

    /* Ran in testTaxFromUntaxedPrices */
    public void testAddTax() {
        Assert.assertEquals(1.2d, Price.addTax(1.0d, 0.2d), DPREC);
        Assert.assertEquals(1.34d, Price.addTax(1.12345d, 0.2d), DPREC);
        Assert.assertEquals(1.37d, Price.addTax(1.14d, 0.2d), DPREC);
    }
}
