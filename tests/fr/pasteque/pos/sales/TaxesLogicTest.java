//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.ticket.TicketTaxInfo;
import fr.pasteque.pos.util.Price;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TaxesLogicTest
{
    private static final double DPREC = 0.0000000000000001;

    private static List<TaxInfo> taxes;
    private TaxesLogic logic;

    @BeforeClass
    public static void setUpBeforeClass() {
        taxes = new ArrayList<TaxInfo>();
        taxes.add(new TaxInfo("1", "T1", null, null, null, null,
                0.1, false, 1));
        taxes.add(new TaxInfo("2", "T2", null, null, null, null,
                0.2, false, 2));
        taxes.add(new TaxInfo("3", "T3", null, null, null, null,
                0.055, false, 3));
    }

    @Before
    public void setUp() {
        this.logic = new TaxesLogic(taxes);
    }

    @Test
    /**
     * Basic test without any rounding issues.
     */
    public void testCalculateTaxesOneLine() {
        TicketInfo tkt = new TicketInfo();
        // 1.1 at 10% tax
        tkt.addLine(new TicketLineInfo(null, 1.0, 1.1, taxes.get(0),
                true, new Properties()));
        // 1.2 at 20% tax
        tkt.addLine(new TicketLineInfo(null, 1.0, 1.2, taxes.get(1),
                true, new Properties()));
        this.logic.calculateTaxes(tkt);
        List<TicketTaxInfo> tktTaxes = tkt.getTaxes();
        Assert.assertEquals("Missing tax info", 2, tktTaxes.size());
        double subtotal = 0.0;
        double tax = 0.0;
        for (TicketTaxInfo t : tktTaxes) {
            subtotal = Price.add(subtotal, t.getSubTotal());
            tax = Price.add(tax, t.getTaxAmount());
        };
        Assert.assertEquals("Tax computation mismatch",
                tkt.getTotal(), Price.add(subtotal, tax), 0.00001);
    }

    @Test
    /*
     * Test spreading rounding error in taxes with a discount
     * that requires to add 0.01 somewhere.
     */
    public void testCalculateTaxesDiscountPositive() {
        // Order with a 14.8% discount
        TicketInfo tkt = new TicketInfo();
        tkt.setDiscountRate(0.148);
        // 5,34 (=4,55 with discount, 0.00032 rounding approximation)
        tkt.addLine(new TicketLineInfo(null, 1.0, 5.34, taxes.get(1),
                true, new Properties()));
        // 8.50 (=7.24 with discount, 0.00200 rounding approximation)
        tkt.addLine(new TicketLineInfo(null, 1.0, 8.50, taxes.get(0),
                true, new Properties()));
        // 4.23 (=3,60 with discount, 0.00396 rounding approximation)
        // will become 3.61 to match total
        tkt.addLine(new TicketLineInfo(null, 1.0, 4.23, taxes.get(2),
                true, new Properties()));
        // Total=18.07, 15.40 after discount
        // Sum of each rounded subtotal after discount=15.39
        this.logic.calculateTaxes(tkt);
        List<TicketTaxInfo> tktTaxes = tkt.getTaxes();
        Assert.assertEquals("Missing tax info", 3, tktTaxes.size());
        for (TicketTaxInfo t : tktTaxes) {
            if (t.getTax().getId().equals(taxes.get(1).getId())) {
                Assert.assertEquals(3.79d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.76d, t.getTaxAmount(), DPREC);
            } else if (t.getTax().getId().equals(taxes.get(0).getId())) {
                Assert.assertEquals(6.58d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.66d, t.getTaxAmount(), DPREC);
            } else {
                Assert.assertEquals(3.42d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.19d, t.getTaxAmount(), DPREC);
            }
        }
    }

    @Test
    /*
     * Test spreading rounding error in taxes with a discount
     * that requires to subtract 0.01 somewhere.
     */
    public void testCalculateTaxesDiscountNegative() {
        // Order with a 14% discount
        TicketInfo tkt = new TicketInfo();
        tkt.setDiscountRate(0.14);
        // 5,32 (=4,58 with discount, -0.00480 rounding approximation)
        // will become 4.57 to match total
        tkt.addLine(new TicketLineInfo(null, 1.0, 5.32, taxes.get(1),
                true, new Properties()));
        // 4.02 (=3.46  with discount, -0.00428 rounding approximation)
        tkt.addLine(new TicketLineInfo(null, 1.0, 4.02, taxes.get(0),
                true, new Properties()));
        // 4.22 (=3,63 with discount, -0.00080 rounding approximation)
        tkt.addLine(new TicketLineInfo(null, 1.0, 4.22, taxes.get(2),
                true, new Properties()));
        // Total=13.56, 11.66 after discount
        // Sum of each rounded subtotal after discount=11.67
        this.logic.calculateTaxes(tkt);
        List<TicketTaxInfo> tktTaxes = tkt.getTaxes();
        Assert.assertEquals("Missing tax info", 3, tktTaxes.size());
        for (TicketTaxInfo t : tktTaxes) {
            if (t.getTax().getId().equals(taxes.get(1).getId())) {
                Assert.assertEquals(3.81d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.76d, t.getTaxAmount(), DPREC);
            } else if (t.getTax().getId().equals(taxes.get(0).getId())) {
                Assert.assertEquals(3.15d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.31d, t.getTaxAmount(), DPREC);
            } else {
                Assert.assertEquals(3.44d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.19d, t.getTaxAmount(), DPREC);
            }
        }
    }

    @Test
    /*
     * Test spreading rounding error in taxes with a discount
     * that requires to subtract 0.01 somewhere with an upper rounding.
     */
    public void testCalculateTaxesDiscountNegativeMixed() {
        // Order with a 14% discount
        TicketInfo tkt = new TicketInfo();
        tkt.setDiscountRate(0.14);
        // 5,32 (=4,58 with discount, -0.00480 rounding approximation)
        // will become 4.57 to match total
        tkt.addLine(new TicketLineInfo(null, 1.0, 5.32, taxes.get(1),
                true, new Properties()));
        // 4.14 (=3.56  with discount, 0.00004 rounding approximation)
        tkt.addLine(new TicketLineInfo(null, 1.0, 4.14, taxes.get(0),
                true, new Properties()));
        // 4.22 (=3,63 with discount, -0.00456 rounding approximation)
        tkt.addLine(new TicketLineInfo(null, 1.0, 4.22, taxes.get(2),
                true, new Properties()));
        // Total=13.68, 11.76 after discount
        // Sum of each rounded subtotal after discount=11.77
        this.logic.calculateTaxes(tkt);
        List<TicketTaxInfo> tktTaxes = tkt.getTaxes();
        Assert.assertEquals("Missing tax info", 3, tktTaxes.size());
        for (TicketTaxInfo t : tktTaxes) {
            if (t.getTax().getId().equals(taxes.get(1).getId())) {
                Assert.assertEquals(3.81d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.76d, t.getTaxAmount(), DPREC);
            } else if (t.getTax().getId().equals(taxes.get(0).getId())) {
                Assert.assertEquals(3.24d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.32d, t.getTaxAmount(), DPREC);
            } else {
                Assert.assertEquals(3.44d, t.getSubTotal(), DPREC);
                Assert.assertEquals(0.19d, t.getTaxAmount(), DPREC);
            }
        }
    }
}
