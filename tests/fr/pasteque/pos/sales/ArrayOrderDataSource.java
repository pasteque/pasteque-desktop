//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.datasource.OrderDataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Test class that stores orders in memory. */
public class ArrayOrderDataSource implements OrderDataSource
{
    private Map<String, SharedTicketInfo> source;

    public ArrayOrderDataSource() {
        this.source = new HashMap<String, SharedTicketInfo>();
    }

    public SharedTicketInfo getOrder(String id) throws BasicException {
        return this.source.get(id);
    }
    public List<SharedTicketInfo> getOrderList() throws BasicException {
        java.util.ArrayList<SharedTicketInfo> l = new ArrayList<SharedTicketInfo>();
        for (SharedTicketInfo o : this.source.values()) {
            l.add(o);
        }
        return l;
    }
    public void saveOrder(SharedTicketInfo order) throws BasicException {
        this.source.put(order.getId(), order);
    }

    public void deleteOrder(final String id) throws BasicException {
        this.source.remove(id);
    }
}
