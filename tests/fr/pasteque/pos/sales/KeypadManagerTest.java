//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.sales.KeypadManager;
import fr.pasteque.pos.sales.KeypadManagerListener;
import fr.pasteque.pos.widgets.JNumberKeys;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JLabel;

public class KeypadManagerTest implements KeypadManagerListener
{
    private JNumberKeys keypad;
    private JLabel inputLabel;
    private JButton okButton;
    private KeypadManager manager;

    private boolean emptyReceived;
    private boolean incorrectQuantityReceived;
    private String customerCardReceived;
    private double quantityReceived;
    private String barcodeReceived;

    @BeforeClass
    public static void setUpBeforeClass() {
        AppConfig cfg = new AppConfig();
        cfg.load();
    }

    @Before
    public void setUp() {
        this.keypad = new JNumberKeys();
        this.inputLabel = new JLabel();
        this.okButton = new JButton();
        this.manager = new KeypadManager(keypad, inputLabel, okButton, this);
        this.emptyReceived = false;
        this.incorrectQuantityReceived = false;
        this.customerCardReceived = null;
        this.quantityReceived = 0.0;
        this.barcodeReceived = null;
    }

    @Test
    public void testInputChar() {
        this.manager.inputChar('a');
        Assert.assertEquals("a", this.manager.getInput());
        this.manager.inputChar(KeypadManager.KEY_BACK);
        Assert.assertEquals("", this.manager.getInput());
        this.manager.inputChar(KeypadManager.KEY_BACK);
        Assert.assertEquals("", this.manager.getInput());
        this.manager.inputChar('a');
        this.manager.inputChar('b');
        Assert.assertEquals("ab", this.manager.getInput());
        this.manager.reset();
        Assert.assertEquals("", this.manager.getInput());
        this.manager.inputChar('-');
        Assert.assertEquals("-", this.manager.getInput());
        this.manager.inputChar('-');
        Assert.assertEquals("-", this.manager.getInput());
        this.manager.inputChar('+');
        Assert.assertEquals("", this.manager.getInput());
    }

    @Test
    public void testEmptyInput() {
        this.manager.processInput();
        Assert.assertEquals(true, this.emptyReceived);
    }

    @Test
    public void testBarcodeInput() {
        this.manager.inputChar('a');
        this.manager.processInput();
        Assert.assertEquals("a", this.barcodeReceived);
        Assert.assertEquals(1.0, this.quantityReceived, 0.01);
    }

    @Test
    public void testQtyBarcodeInput() {
        this.manager.inputChar('2');
        this.manager.inputChar('.');
        this.manager.inputChar('4');
        this.manager.inputChar('*');
        this.manager.inputChar('a');
        this.manager.processInput();
        Assert.assertEquals(2.4, this.quantityReceived, 0.01);
        Assert.assertEquals("a", this.barcodeReceived);
    }

    @Test
    public void testNegQtyBarcodeInput() {
        this.manager.inputChar('-');
        this.manager.inputChar('*');
        this.manager.inputChar('a');
        this.manager.processInput();
        Assert.assertEquals(-1.0, this.quantityReceived, 0.01);
        Assert.assertEquals("a", this.barcodeReceived);
    }

    @Test
    public void testSpecialQtyBarcodeInput() {
        this.manager.inputChar('2');
        this.manager.inputChar('*');
        this.manager.inputChar('2');
        this.manager.inputChar('1');
        this.manager.inputChar('0');
        this.manager.inputChar('a');
        this.manager.inputChar('b');
        this.manager.inputChar('c');
        this.manager.inputChar('d');
        this.manager.inputChar('3');
        this.manager.inputChar('4');
        this.manager.inputChar('5');
        this.manager.inputChar('6');
        this.manager.inputChar('7');
        this.manager.inputChar('8');
        this.manager.processInput();
        Assert.assertEquals(3456.78, this.quantityReceived, 0.0001);
        Assert.assertEquals("210abcd", this.barcodeReceived);
    }

    @Test
    public void testCustomerInput() {
        this.manager.inputChar('c');
        this.manager.inputChar('2');
        this.manager.processInput();
        Assert.assertEquals("c2", this.customerCardReceived);
    }

    @Test
    public void testNumberInput() {
        this.manager.inputChar('3');
        this.manager.inputChar('.');
        this.manager.inputChar('1');
        Assert.assertEquals(3.1, this.manager.getInputNumber(), 0.01);
    }

    @Test
    public void testNumberInputNull() {
        Assert.assertEquals(null, this.manager.getInputNumber());
        this.manager.inputChar(' ');
        Assert.assertEquals(null, this.manager.getInputNumber());
    }

    @Test
    public void testNumberMinusInput() {
        this.manager.inputChar('-');
        Assert.assertEquals(-1.0, this.manager.getInputNumber(), 0.01);
    }

    @Test
    public void testNaNNumberInput() {
        this.manager.inputChar('a');
        boolean exceptionThrown = false;
        try {
            this.manager.getInputNumber();
        } catch (NumberFormatException e) {
            exceptionThrown = true;
        }
        Assert.assertEquals(true, exceptionThrown);
    }

    @Test
    public void testCorruptedLabel() {
        this.inputLabel.setText("a*abcd");
        this.manager.processInput();
        Assert.assertEquals(true, this.incorrectQuantityReceived);
    }

    @Override
    public void onIncorrectQuantity(KeypadManager manager) {
        this.incorrectQuantityReceived = true;
    }

    @Override
    public void onCustomerInput(KeypadManager manager, String customerCard) {
        this.customerCardReceived = customerCard;
    }

    @Override
    public void onScaledBarcodeInput(KeypadManager manager,
            double quantity, String barcode) {
        this.quantityReceived = quantity;
        this.barcodeReceived = barcode;
    }

    @Override
    public void onEmptyInput(KeypadManager manager) {
        this.emptyReceived = true;
    }

    @Override
    public void onOtherInput(KeypadManager manager,
            double quantity, String barcode) {
        this.quantityReceived = quantity;
        this.barcodeReceived = barcode;
    }

    @Override
    public void onCheckout(KeypadManager manager) {
        // Maybe it should be removed some day.
    }
}
