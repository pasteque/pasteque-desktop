//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.datasource.CustomerDataSource;
import fr.pasteque.pos.inventory.TaxCategoryInfo;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TariffAreaPrice;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class OrderManagerTest implements CatalogDataSource, CustomerDataSource
{
    private static CategoryInfo cat;
    private static ProductInfoExt prdA;
    private static ProductInfoExt prdB;
    private static TaxInfo tax;
    private static TariffInfo tariffArea;

    private OrderManager manager;

    /* Data source functions */
    public ProductInfoExt getProductInfo(String id) throws BasicException {
        if (prdA.getID().equals(id)) {
            return prdA;
        }
        if (prdB.getID().equals(id)) {
            return prdB;
        }
        return null;
    }
    public ProductInfoExt getProductInfoByCode(String sCode) throws BasicException {
        if (prdA.getCode().equals(sCode)) {
            return prdA;
        }
        if (prdB.getCode().equals(sCode)) {
            return prdB;
        }
        return null;
    }
    public List<ProductInfoExt> searchProducts(String label, String reference) throws BasicException {
        return null;
    }
    public List<CategoryInfo> getRootCategories() throws BasicException {
        return null;
    }
    public CategoryInfo getCategory(String id) throws BasicException {
        if (cat.getID().equals(id)) {
            return cat;
        }
        return null;
    }
    public List<CategoryInfo> getSubcategories(String category) throws BasicException {
        return null;
    }
    public List<CategoryInfo> getCategories() throws BasicException {
        return null;
    }
    public List<SubgroupInfo> getSubgroups(String composition) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> getSubgroupCatalog(Integer subgroup) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> getProductCatalog(String category) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> getProductComments(String id) throws BasicException {
        return null;
    }
    public List<TaxInfo> getTaxList() throws BasicException {
        ArrayList<TaxInfo> list = new ArrayList<TaxInfo>();
        list.add(tax);
        return list;
    }
    public TaxInfo getTax(String taxId) throws BasicException {
        if (tax.getId().equals(taxId)) {
            return tax;
        }
        return null;
    }
    public List<TaxCategoryInfo> getTaxCategoriesList() throws BasicException {
        return null;
    }
    public List<CurrencyInfo> getCurrenciesList() throws BasicException {
        return null;
    }
    public CurrencyInfo getCurrency(int currencyId) throws BasicException {
        return null;
    }
    public CurrencyInfo getMainCurrency() throws BasicException {
        return null;
    }
    public TariffInfo getTariffArea(int id) throws BasicException {
        if (id == tariffArea.getID()) {
            return tariffArea;
        } else {
            return null;
        }
    }
    public TariffAreaPrice getTariffAreaPrice(int tariffAreaId, String productId) throws BasicException {
        if (tariffAreaId == tariffArea.getID()) {
            if (productId.equals(prdA.getID())) {
                return new TariffAreaPrice(productId, 0.8, null);
            }
        }
        return null;
    }
    public List<TariffInfo> getTariffAreaList() throws BasicException {
        List<TariffInfo> list = new ArrayList<TariffInfo>();
        list.add(tariffArea);
        return list;
    }
    public CustomerInfoExt getCustomer(String id) throws BasicException {
        return null;
    }
    public CustomerInfoExt getCustomerByCard(String card) throws BasicException {
        return null;
    }
    public List<CustomerInfoExt> getTop10CustomerList() throws BasicException {
        return new ArrayList<CustomerInfoExt>();
    }
    public List<CustomerInfoExt> searchCustomers(String search) throws BasicException {
        return new ArrayList<CustomerInfoExt>();
    }
    public DiscountProfile getDiscountProfile(int id) throws BasicException {
        return new DiscountProfile(1, 0.1);
    }
    public List<DiscountProfile> getDiscountProfiles() throws BasicException {
        DiscountProfile dp = new DiscountProfile(1, 0.1);
        List<DiscountProfile> list = new ArrayList<DiscountProfile>();
        list.add(dp);
        return list;
    }


    @BeforeClass
    public static void setUpBeforeClass() {
        tax = new TaxInfo("1", "tax", null, null, null, null, 0.2, false, 1);
        cat = new CategoryInfo("1", "cat", "Category", null);
        tariffArea = new TariffInfo(1, "Tariff area");
    }

    @Before
    public void setUp() {
        try {
            this.manager = new OrderManager(this, this);
        } catch (BasicException e) {
            e.printStackTrace();
        }
        prdA = new ProductInfoExt();
        prdA.setTaxId(tax.getId());
        prdA.setCategoryID(cat.getID());
        prdA.setCode("prdACode");
        prdA.setID("1");
        prdA.setName("prdA");
        prdA.setPriceSell(1.0);
        prdA.setPriceSellTax(1.2);
        prdA.setReference("refA");
        prdB = new ProductInfoExt();
        prdB.setTaxId(tax.getId());
        prdB.setCategoryID(cat.getID());
        prdB.setCode("prdBCode");
        prdB.setID("2");
        prdB.setName("prdB");
        prdB.setPriceSell(2.0);
        prdB.setPriceSellTax(2.4);
        prdB.setReference("refB");
    }

    @Test
    /** Check that it doesn't crash. */
    public void testNullOrder() throws BasicException {
        TicketLineInfo l = new TicketLineInfo(prdA, 1.0, prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        assertEquals(OrderManager.Ret.Err(), this.manager.addLine(l));
        assertEquals(OrderManager.Ret.Err(), this.manager.addProduct(prdA, 1.0));
        assertEquals(OrderManager.Ret.Err(), this.manager.addSubline(l, 0));
        assertEquals(OrderManager.Ret.Err(), this.manager.addSubproduct(prdA, 0));
        assertEquals(OrderManager.Ret.Err(), this.manager.adjustLineQuantity(0, 1.5));
        this.manager.assignCustomer(new CustomerInfoExt("1"));
        assertEquals(null, this.manager.getOrder());
        assertEquals(null, this.manager.getOrderExtra());
        assertEquals(OrderManager.Ret.Err(), this.manager.removeLine(0));
        this.manager.replaceLine(0, l);
        this.manager.setCustomersCount(1);
        this.manager.setTariffArea(this.tariffArea);
    }

    @Test
    public void testAddLineNoStack() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.setArticleStacking(false);
        OrderManager.Ret r = this.manager.addLine(l1);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 0, 1), r);
        r = this.manager.addLine(l2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 1, 1), r);
        assertEquals(2, this.manager.getOrderContent().getLinesCount());
    }

    @Test
    public void testAddLineStack() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.setArticleStacking(true);
        OrderManager.Ret r = this.manager.addLine(l1);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 0, 1), r);
        r = this.manager.addLine(l2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 1), r);
        assertEquals(2.0, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
    }

    @Test
    public void testAddLineStackRemove() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdA, -1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.setArticleStacking(true);
        this.manager.addLine(l1);
        OrderManager.Ret r = this.manager.addLine(l2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 1), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
    }

    @Test
    public void testAddLineCustomStack() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0, 10.0, tax, true,
                new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdA, 1.0, 10.0, tax, true,
                new java.util.Properties());
        TicketLineInfo l3 = new TicketLineInfo(prdB, 1.0, 1.25, tax, true,
                new java.util.Properties());
        TicketLineInfo l4 = new TicketLineInfo(prdB, 1.0, 1.15, tax, true,
                new java.util.Properties());
        this.manager.setArticleStacking(true);
        OrderManager.Ret r = this.manager.addLine(l1);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 0, 1), r);
        r = this.manager.addLine(l3);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 1, 1), r);
        r = this.manager.addLine(l2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 1), r);
        r = this.manager.addLine(l4);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 2, 1), r);
        assertEquals(2.0, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
        assertEquals(1.0, this.manager.getOrderContent().getLine(1).getMultiply(), 0.01);
        assertEquals(1.0, this.manager.getOrderContent().getLine(2).getMultiply(), 0.01);
        assertEquals(3, this.manager.getOrderContent().getLinesCount());
    }

    @Test
    public void testAddProductNoStack() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        this.manager.setArticleStacking(false);
        OrderManager.Ret r = this.manager.addProduct(prdA, 1.0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 0, 1), r);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
        r = this.manager.addProduct(prdA, 1.2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 1, 1), r);
        assertEquals(2, this.manager.getOrderContent().getLinesCount());
        assertEquals(1.2, this.manager.getOrderContent().getLine(1).getMultiply(), 0.01);
    }

    @Test
    public void testAddProductStack() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        this.manager.setArticleStacking(true);
        OrderManager.Ret r = this.manager.addProduct(prdA, 1.0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 0, 1), r);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
        r = this.manager.addProduct(prdA, 1.2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 1), r);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
        assertEquals(2.2, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
    }

    @Test
    public void testAddSublineNone() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l = new TicketLineInfo(prdA, 1.0, prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        l.setSubproduct(true);
        OrderManager.Ret r = this.manager.addLine(l);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ERR, -1, -1), r);
    }

    @Test
    public void testAddSublineNotSub() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo main = new TicketLineInfo(prdA, 3.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo aux = new TicketLineInfo(prdA, 1.0, prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        aux.setSubproduct(false);
        this.manager.addLine(main);
        OrderManager.Ret r = this.manager.addSubline(aux, 0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ERR, -1, -1), r);
    }

    @Test
    public void testAddSubline() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 3.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdA, 2.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo aux1 = new TicketLineInfo(prdB, 1.1,
                prdB.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo aux2 = new TicketLineInfo(prdB, 1.1,
                prdB.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo aux3 = new TicketLineInfo(prdB, 1.1,
                prdB.getPriceSellTax(),
                tax, true, new java.util.Properties());
        aux1.setSubproduct(true);
        aux2.setSubproduct(true);
        aux3.setSubproduct(true);
        this.manager.setArticleStacking(false);
        this.manager.addLine(l1);
        this.manager.addLine(l2);
        assertEquals(2, this.manager.getOrderContent().getLinesCount());
        this.manager.setArticleStacking(true); // Check that aux don't stack
        OrderManager.Ret r = this.manager.addSubline(aux1, 0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 1, 1), r);
        r = this.manager.addSubline(aux2, 1);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ERR, -1, -1), r);
        r = this.manager.addSubline(aux2, 0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 2, 1), r);
        r = this.manager.addSubline(aux3, 3);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 4, 1), r);
        assertEquals(5, this.manager.getOrderContent().getLinesCount());
        assertEquals(3.0, this.manager.getOrderContent().getLine(1).getMultiply(), 0.01);
        assertEquals(true, this.manager.getOrderContent().getLine(1).isSubproduct());
        assertEquals(3.0, this.manager.getOrderContent().getLine(2).getMultiply(), 0.01);
        assertEquals(true, this.manager.getOrderContent().getLine(2).isSubproduct());
        assertEquals(2.0, this.manager.getOrderContent().getLine(4).getMultiply(), 0.01);
        assertEquals(true, this.manager.getOrderContent().getLine(4).isSubproduct());
    }

    @Test
    public void testAddSubproduct() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 3.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdA, 2.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.setArticleStacking(false);
        this.manager.addLine(l1);
        this.manager.addLine(l2);
        assertEquals(2, this.manager.getOrderContent().getLinesCount());
        this.manager.setArticleStacking(true); // Check that aux don't stack
        OrderManager.Ret r = this.manager.addSubproduct(prdA, 0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 1, 1), r);
        r = this.manager.addSubproduct(prdA, 1);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ERR, -1, -1), r);
        r = this.manager.addSubproduct(prdB, 0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 2, 1), r);
        r = this.manager.addSubproduct(prdB, 3);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ADD, 4, 1), r);
        assertEquals(5, this.manager.getOrderContent().getLinesCount());
        assertEquals(3.0, this.manager.getOrderContent().getLine(1).getMultiply(), 0.01);
        assertEquals(true, this.manager.getOrderContent().getLine(1).isSubproduct());
        assertEquals(3.0, this.manager.getOrderContent().getLine(2).getMultiply(), 0.01);
        assertEquals(true, this.manager.getOrderContent().getLine(2).isSubproduct());
        assertEquals(2.0, this.manager.getOrderContent().getLine(4).getMultiply(), 0.01);
        assertEquals(true, this.manager.getOrderContent().getLine(4).isSubproduct());
   }

    @Test
    public void testAddSubproductNotSub() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo main = new TicketLineInfo(prdA, 3.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        TicketLineInfo aux = new TicketLineInfo(prdA, 1.0, prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        aux.setSubproduct(true);
        this.manager.addLine(main);
        this.manager.addSubline(aux, 0);
        assertEquals(2, this.manager.getOrderContent().getLinesCount());
        OrderManager.Ret r = this.manager.addSubproduct(prdB, 1);
        assertEquals(new OrderManager.Ret(OrderManager.OP_ERR, -1, -1), r);
    }

    @Test
    public void testAdjustLineQuantityRegular() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
        OrderManager.Ret r = this.manager.adjustLineQuantity(0, 0.5);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 1), r);
        assertEquals(1.5, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
        r = this.manager.adjustLineQuantity(0, -0.2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 1), r);
        assertEquals(1.3, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
    }

    @Test
    public void testAdjustLineQuantitySubRegular() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        this.manager.addSubproduct(prdB, 0);
        assertEquals(2, this.manager.getOrderContent().getLinesCount());
        OrderManager.Ret r = this.manager.adjustLineQuantity(1, 0.5);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 2), r);
        assertEquals(1.5, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
        assertEquals(1.5, this.manager.getOrderContent().getLine(1).getMultiply(), 0.01);
        r = this.manager.adjustLineQuantity(1, -0.2);
        assertEquals(new OrderManager.Ret(OrderManager.OP_UPD, 0, 2), r);
        assertEquals(1.3, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
        assertEquals(1.3, this.manager.getOrderContent().getLine(1).getMultiply(), 0.01);
    }

    @Test
    public void testAdjustLineQuantitySwitchZero() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        // -1.0 to more than 0.0
        TicketLineInfo l = new TicketLineInfo(prdA, -1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        OrderManager.Ret r = this.manager.adjustLineQuantity(0, 1.5);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 1), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
        // -1.0 to 0.0
        l = new TicketLineInfo(prdA, -1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        r = this.manager.adjustLineQuantity(0, 1.0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 1), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
        // 1.0 to less than 0.0
        l = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        r = this.manager.adjustLineQuantity(0, -1.5);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 1), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
        // 1.0 to 0.0
        l = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        r = this.manager.adjustLineQuantity(0, -1.0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 1), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
    }

    @Test
    public void testAdjustLineQuantitySubSwitchZero() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        // -1.0 to more than 0.0
        TicketLineInfo l = new TicketLineInfo(prdA, -1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        this.manager.addSubproduct(prdB, 0);
        OrderManager.Ret r = this.manager.adjustLineQuantity(1, 1.5);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 2), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
        // -1.0 to 0.0
        l = new TicketLineInfo(prdA, -1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        this.manager.addSubproduct(prdB, 0);
        r = this.manager.adjustLineQuantity(1, 1.0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 2), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
        // 1.0 to less than 0.0
        l = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        this.manager.addSubproduct(prdB, 0);
        r = this.manager.adjustLineQuantity(1, -1.5);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 2), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
        // 1.0 to 0.0
        l = new TicketLineInfo(prdA, 1.0,
                prdA.getPriceSellTax(),
                tax, true, new java.util.Properties());
        this.manager.addLine(l);
        this.manager.addSubproduct(prdB, 0);
        r = this.manager.adjustLineQuantity(1, -1.0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 2), r);
        assertEquals(0, this.manager.getOrderContent().getLinesCount());
    }

    @Test
    public void testRemoveLine() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0, 10.0, tax, true,
                new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdB, 2.0, 12.0, tax, true,
                new java.util.Properties());
        this.manager.addLine(l1);
        this.manager.addLine(l2);
        OrderManager.Ret r = this.manager.removeLine(0);
        assertEquals(new OrderManager.Ret(OrderManager.OP_RM, 0, 1), r);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
        assertEquals(2.0, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
    }

    @Test
    public void testReplaceLine() {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0, 10.0, tax, true,
                new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdB, 2.0, 12.0, tax, true,
                new java.util.Properties());
        this.manager.addLine(l1);
        this.manager.replaceLine(0, l2);
        assertEquals(1, this.manager.getOrderContent().getLinesCount());
        assertEquals(2.0, this.manager.getOrderContent().getLine(0).getMultiply(), 0.01);
    }

    @Test
    public void testSetTariffArea() throws BasicException {
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0, 10.0, tax, true,
                new java.util.Properties());
        TicketLineInfo l2 = new TicketLineInfo(prdB, 2.0, 12.0, tax, true,
                new java.util.Properties());
        TicketLineInfo l3 = new TicketLineInfo(prdA, 2.0, 10.0, tax, true,
                new java.util.Properties());
        this.manager.addLine(l1);
        this.manager.addLine(l2);
        this.manager.setTariffArea(tariffArea);
        assertEquals(0.8, this.manager.getOrderContent().getLine(0).getPrice(), 0.01);
        // TODO: tariff area doesn't keep custom price, this will fail when it does
        assertEquals(2.0, this.manager.getOrderContent().getLine(1).getPrice(), 0.01);
        this.manager.setArticleStacking(false);
        this.manager.addLine(l3);
        assertEquals(0.8, this.manager.getOrderContent().getLine(2).getPrice(), 0.01);
        this.manager.setTariffArea(null);
        assertEquals(1.0, this.manager.getOrderContent().getLine(0).getPrice(), 0.01);
        assertEquals(2.0, this.manager.getOrderContent().getLine(1).getPrice(), 0.01);
        assertEquals(1.0, this.manager.getOrderContent().getLine(2).getPrice(), 0.01);
    }

    @Test
    public void testAssignCustomerProfile() throws BasicException {
        CustomerInfoExt cust = new CustomerInfoExt("1");
        cust.setDiscountProfileId(1);
        cust.setTariffAreaId(tariffArea.getID());
        SharedTicketInfo o = new SharedTicketInfo("1", new TicketInfo());
        this.manager.setOrder(o);
        TicketLineInfo l1 = new TicketLineInfo(prdA, 1.0, 10.0, tax, true,
                new java.util.Properties());
        this.manager.addLine(l1);
        this.manager.assignCustomer(cust);
        assertEquals(0.8, this.manager.getOrderContent().getLine(0).getPrice(), 0.01);
        assertEquals(Integer.valueOf(1), this.manager.getOrderContent().getDiscountProfileId());
        assertEquals(this.getDiscountProfile(1).getRate(),
                this.manager.getOrderContent().getDiscountRate(), 0.01);
    }
}
