//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.datasource.CustomerDataSource;
import fr.pasteque.pos.datasource.OrderDataSource;
import fr.pasteque.pos.inventory.TaxCategoryInfo;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TariffAreaPrice;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SalesControllerTest implements CatalogDataSource, CustomerDataSource
{
    private static CategoryInfo cat;
    private static ProductInfoExt prdA;
    private static ProductInfoExt prdB;
    private static TaxInfo tax;
    private static TariffInfo tariffArea;

    private SalesController controller;
    private OrderDataSource orderDs;

    /* Data source functions */
    public ProductInfoExt getProductInfo(String id) throws BasicException {
        if (prdA.getID().equals(id)) {
            return prdA;
        }
        if (prdB.getID().equals(id)) {
            return prdB;
        }
        return null;
    }
    public ProductInfoExt getProductInfoByCode(String sCode) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> searchProducts(String label, String reference) throws BasicException {
        return null;
    }
    public List<CategoryInfo> getRootCategories() throws BasicException {
        return null;
    }
    public CategoryInfo getCategory(String id) throws BasicException {
        if (cat.getID().equals(id)) {
            return cat;
        }
        return null;
    }
    public List<CategoryInfo> getSubcategories(String category) throws BasicException {
        return null;
    }
    public List<CategoryInfo> getCategories() throws BasicException {
        return null;
    }
    public List<SubgroupInfo> getSubgroups(String composition) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> getSubgroupCatalog(Integer subgroup) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> getProductCatalog(String category) throws BasicException {
        return null;
    }
    public List<ProductInfoExt> getProductComments(String id) throws BasicException {
        return null;
    }
    public List<TaxInfo> getTaxList() throws BasicException {
        ArrayList<TaxInfo> list = new ArrayList<TaxInfo>();
        list.add(tax);
        return list;
    }
    public TaxInfo getTax(String taxId) throws BasicException {
        return null;
    }
    public List<TaxCategoryInfo> getTaxCategoriesList() throws BasicException {
        return null;
    }
    public List<CurrencyInfo> getCurrenciesList() throws BasicException {
        return null;
    }
    public CurrencyInfo getCurrency(int currencyId) throws BasicException {
        return null;
    }
    public CurrencyInfo getMainCurrency() throws BasicException {
        return null;
    }
    public TariffInfo getTariffArea(int id) throws BasicException {
        return null;
    }
    public TariffAreaPrice getTariffAreaPrice(int tariffAreaId, String productId) throws BasicException {
        return null;
    }
    public List<TariffInfo> getTariffAreaList() throws BasicException {
        return new ArrayList<TariffInfo>();
    }
    public CustomerInfoExt getCustomer(String id) throws BasicException {
        return null;
    }
    public CustomerInfoExt getCustomerByCard(String card) throws BasicException {
        return null;
    }
    public List<CustomerInfoExt> getTop10CustomerList() throws BasicException {
        return new ArrayList<CustomerInfoExt>();
    }
    public List<CustomerInfoExt> searchCustomers(String search) throws BasicException {
        return new ArrayList<CustomerInfoExt>();
    }
    public DiscountProfile getDiscountProfile(int id) throws BasicException {
        return null;
    }
    public List<DiscountProfile> getDiscountProfiles() throws BasicException {
        return new ArrayList<DiscountProfile>();
    }

    protected void addLine(SharedTicketInfo order, ProductInfoExt product,
            TaxInfo tax, double quantity) {
        order.getTicket().addLine(new TicketLineInfo(product, quantity,
            product.getPriceSellTax(), tax, true,
            (java.util.Properties) (product.getProperties().clone())));
    }

    @BeforeClass
    public static void setUpBeforeClass() {
        tax = new TaxInfo("1", "tax", null, null, null, null, 0.2, false, 1);
        cat = new CategoryInfo("1", "cat", "Category", null);
        prdA = new ProductInfoExt();
        prdA.setTaxId(tax.getId());
        prdA.setCategoryID(cat.getID());
        prdA.setCode("prdACode");
        prdA.setID("1");
        prdA.setName("prdA");
        prdA.setPriceSell(1.0);
        prdA.setPriceSellTax(1.2);
        prdA.setReference("refA");
        prdB = new ProductInfoExt();
        prdB.setTaxId(tax.getId());
        prdB.setCategoryID(cat.getID());
        prdB.setCode("prdBCode");
        prdB.setID("2");
        prdB.setName("prdB");
        prdB.setPriceSell(2.0);
        prdB.setPriceSellTax(2.4);
        prdB.setReference("refB");
    }

    @Before
    public void setUp() {
        try {
            this.orderDs = new ArrayOrderDataSource();
            this.controller = new SalesController(new OrderManager(this, this), this.orderDs, this);
        } catch (BasicException e) {
            e.printStackTrace();
        }
    }

    @Test
    /** Check that it doesn't crash. */
    public void testHasPendingOrder() throws BasicException {
        // No orders
        assertFalse(this.controller.hasPendingOrders());
        // One order, no selection
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.orderDs.saveOrder(order);
        assertTrue(this.controller.hasPendingOrders());
        // One order, selected
        this.controller.setOrder(order, null);
        assertFalse(this.controller.hasPendingOrders());
        // Two orders, one selected
        SharedTicketInfo order2 = new SharedTicketInfo("2", new TicketInfo());
        this.orderDs.saveOrder(order2);
        assertTrue(this.controller.hasPendingOrders());
    }

    @Test /* Test saving a non-empty order on switch */
    public void testSetOrderSave() throws BasicException {
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        order.getTicket().setCustomersCount(1);
        this.controller.setOrder(order, null);
        this.controller.setOrder(null, null);
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        assertEquals("1", orders.get(0).getId());
    }

    @Test /* Tes discarding an empty order on switch */
    public void testSetOrderDropEmpty() throws BasicException {
        // Create a non-empty order and persist it
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        order.getTicket().setCustomersCount(1);
        this.controller.setOrder(order, null);
        this.controller.saveCurrentOrder();
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        // Empty that order and switch
        order.getTicket().setCustomersCount(0);
        this.controller.setOrder(null, null);
        orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(0, orders.size());
    }

    @Test /* Test merging from null to empty */
    public void testMergeOrderIntoFromNull() throws BasicException {
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.controller.mergeOrderInto(order, null);
        assertEquals("1", this.controller.getCurrentOrder().getId());
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        assertEquals("1", orders.get(0).getId());
    }

    @Test /* Test merging from empty to null */
    public void testMergeOrderIntoToNull() throws BasicException {
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.controller.setOrder(order, null);
        this.controller.saveCurrentOrder();
        this.controller.mergeOrderInto(order, null);
        assertEquals("1", this.controller.getCurrentOrder().getId());
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        assertEquals("1", orders.get(0).getId());
    }

    @Test /* Test merging from empty to content */
    public void testMergeOrderIntoToContent() throws BasicException {
        // Setup source order
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.controller.setOrder(order, null);
        this.controller.saveCurrentOrder();
        // Setup dest order
        SharedTicketInfo order2 = new SharedTicketInfo("2", new TicketInfo());
        CustomerInfoExt cust2 = new CustomerInfoExt("order");
        order2.getTicket().setCustomer(cust2);
        order2.getTicket().setCustomersCount(1);
        addLine(order2, prdA, tax, 1.0);
        // Merge and check results
        this.controller.mergeOrderInto(order2, "extra");
        assertEquals("2", this.controller.getCurrentOrder().getId());
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        assertEquals("2", orders.get(0).getId());
        SharedTicketInfo current = this.controller.getCurrentOrder();
        assertEquals(cust2.getId(), current.getTicket().getCustomer().getId());
        assertEquals(Integer.valueOf(1), current.getTicket().getCustomersCount());
        assertEquals(1, current.getTicket().getLinesCount());
        assertEquals(prdA.getID(), current.getTicket().getLine(0).getProductID());
        assertEquals(1.0, current.getTicket().getLine(0).getMultiply(), 0.00001);
        assertEquals("extra", this.controller.getOrderManager().getOrderExtra());
    }

    @Test /* Test merging from content to empty */
    public void testMergeOrderIntoEmpty() throws BasicException {
        // Setup source order
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        CustomerInfoExt cust = new CustomerInfoExt("order");
        order.getTicket().setCustomer(cust);
        order.getTicket().setCustomersCount(1);
        addLine(order, prdA, tax, 1.0);
        this.controller.setOrder(order, "extra");
        this.controller.saveCurrentOrder();
        // Setup dest order
        SharedTicketInfo order2 = new SharedTicketInfo("2", new TicketInfo());
        // Merge and check results
        this.controller.mergeOrderInto(order2, null);
        assertEquals("2", this.controller.getCurrentOrder().getId());
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        assertEquals("2", orders.get(0).getId());
        SharedTicketInfo current = this.controller.getCurrentOrder();
        assertEquals(cust.getId(), current.getTicket().getCustomer().getId());
        assertEquals(Integer.valueOf(1), current.getTicket().getCustomersCount());
        assertEquals(1, current.getTicket().getLinesCount());
        assertEquals(prdA.getID(), current.getTicket().getLine(0).getProductID());
        assertEquals(1.0, current.getTicket().getLine(0).getMultiply(), 0.00001);
        assertEquals("extra", this.controller.getOrderManager().getOrderExtra());
    }

    @Test /* Test merging from content to another content */
    public void testMergeOrderBoth() throws BasicException {
        // Setup source order
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        CustomerInfoExt cust = new CustomerInfoExt("order");
        order.getTicket().setCustomer(cust);
        order.getTicket().setCustomersCount(1);
        addLine(order, prdA, tax, 1.0);
        this.controller.setOrder(order, "extra");
        this.controller.saveCurrentOrder();
        // Setup dest order
        SharedTicketInfo order2 = new SharedTicketInfo("2", new TicketInfo());
        CustomerInfoExt cust2 = new CustomerInfoExt("order2");
        order2.getTicket().setCustomer(cust2);
        order2.getTicket().setCustomersCount(2);
        addLine(order2, prdB, tax, 2.0);
        // Merge and check results
        this.controller.mergeOrderInto(order2, "extra2");
        List<SharedTicketInfo> orders = this.controller.getOrderDataSource().getOrderList();
        assertEquals(1, orders.size());
        assertEquals("2", orders.get(0).getId());
        SharedTicketInfo current = this.controller.getCurrentOrder();
        assertEquals("2", current.getId());
        assertEquals(cust2.getId(), current.getTicket().getCustomer().getId());
        assertEquals(Integer.valueOf(3), current.getTicket().getCustomersCount());
        assertEquals(2, current.getTicket().getLinesCount());
        assertEquals(prdB.getID(), current.getTicket().getLine(0).getProductID());
        assertEquals(2.0, current.getTicket().getLine(0).getMultiply(), 0.00001);
        assertEquals(prdA.getID(), current.getTicket().getLine(1).getProductID());
        assertEquals(1.0, current.getTicket().getLine(1).getMultiply(), 0.00001);
        assertEquals("extra2", this.controller.getOrderManager().getOrderExtra());
    }

    @Test
    public void testRefreshCustomer() throws BasicException {
        CustomerInfoExt orderCust = new CustomerInfoExt("order");
        CustomerInfoExt other = new CustomerInfoExt("1");
        CustomerInfoExt refresh = new CustomerInfoExt("order");
        refresh.setName("Refreshed");
        // No order refresh
        assertFalse(this.controller.refreshCustomer(refresh));
        // No customer refresh
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.controller.setOrder(order, null);
        assertFalse(this.controller.refreshCustomer(refresh));
        // Set customer
        order.getTicket().setCustomer(orderCust);
        // Refresh to null
        assertFalse(this.controller.refreshCustomer(null));
        // Wrong customer refresh
        assertFalse(this.controller.refreshCustomer(other));
        assertEquals(null, this.controller.getCurrentOrder().getTicket().getCustomer().getName());
        // Refresh
        assertTrue(this.controller.refreshCustomer(refresh));
        assertEquals("Refreshed", this.controller.getCurrentOrder().getTicket().getCustomer().getName());
    }

    @Test
    public void testSaveOrder() throws BasicException {
        // Save null
        this.controller.saveCurrentOrder();
        assertEquals(0, this.orderDs.getOrderList().size());
        // Save
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.controller.setOrder(order, null);
        this.controller.saveCurrentOrder();
        assertEquals(1, this.orderDs.getOrderList().size());
    }

    @Test
    public void testDeleteOrder() throws BasicException {
        // Delete null
        this.controller.deleteCurrentOrder();
        // Save and delete
        SharedTicketInfo order = new SharedTicketInfo("1", new TicketInfo());
        this.controller.setOrder(order, null);
        this.controller.saveCurrentOrder();
        this.controller.deleteCurrentOrder();
        assertEquals(0, this.orderDs.getOrderList().size());
    }
}
