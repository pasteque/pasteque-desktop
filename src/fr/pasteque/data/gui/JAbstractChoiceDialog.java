//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import java.awt.Component;

/**
 * Basic implementation of an dialog to select or enter something.
 * Subclasses must set GUI and pick the selected choice either by overriding
 * confirm() or calling setChoice() upon selection.
 */
public abstract class JAbstractChoiceDialog<T> extends JAbstractDialog
implements ShortcutListener
{
    private static final long serialVersionUID = -2689036285908658414L;

    /**
     * The choice previously set when updating something. It is set from
     * the constructor and is picked when cancelling.
     */
    protected T originalChoice;
    /**
     * The choice the user entered. Subclasses must set this value
     * at least at confirmation time.
     */
    protected T choice;

    /**
     * Create a new dialog.
     * @param parent Parent component, {@link JAbstractDialog#JAbstractDialog(Component)}
     * @param originalChoice The choice to keep when the dialog is canceled.
     */
    public JAbstractChoiceDialog(Component parent, T originalChoice) {
        super(parent);
        this.originalChoice = originalChoice;
    }

    /**
     * Assign shortcut listeners. Add itself to the parent listeners.
     * Overriding shortcutPressed will erase the default confirm shortcut.
     */
    protected void setShortcuts() {
        super.setShortcuts();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
    }

    protected void cleanShortcuts() {
        super.cleanShortcuts();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
    }

    /**
     * Call confirm() when pressing GENERAL_VALIDATE.
     */
    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                this.confirm();
                return true;
            default: // Ignore
                return false;
        }
    }
    /**
     * Close the dialog. Subclasses may override this method to set
     * the current choice.
     */
    protected void confirm() {
        super.close();
    }

    /**
     * Close the dialog and reset choice to the original choice.
     */
    protected void close() {
        this.choice = this.originalChoice;
        super.close();
    }

    /**
     * Get the choice that was preselected before the dialog was shown.
     * @return The choice that was passed to the constructor.
     */
    public T getOriginalChoice() {
        return this.originalChoice;
    }

    /**
     * Get the choice the user made. It can be called after the dialog is shown
     * to pick the user's choice. If the dialog is canceled, it will return
     * the original choice, if it is confirmed, it will return the new choice.
     */
    public T getChoice() {
        return this.choice;
    }

    /**
     * Change the actual choice. Subclasses must call this either when the
     * selection is changed or when the dialog is confirmed.
     */
    protected void setChoice(T choice) {
        this.choice = choice;
    }
}
