//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;

/**
 * Superclass for dialogs to ease an uniformize dialog creation.
 * It automatically sets the dialog size and position and assign
 * the GENERAL_CANCEL shortcut to close the dialog and blocks
 * GENERAL_VALIDATE.
 */
public abstract class JAbstractDialog extends javax.swing.JDialog
{
    private static final long serialVersionUID = -6884731683195046146L;
    /** The usual text width for popup messages in `em`. */
    public static final int COMMON_WIDTH_EM = 20;

    protected ShortcutListener closeListener;
    protected double minWidthRatio;
    protected double minHeightRatio;

    /**
     * Create a new dialog attached to the window of its parent.
     * @param parent The parent component to retreive the window from.
     * If it is not a window itself, it will seek up in the component tree
     * to find the first window.
     */
    public JAbstractDialog(Component parent) {        
        super(getWindow(parent), Dialog.ModalityType.APPLICATION_MODAL);
        this.applyComponentOrientation(parent.getComponentOrientation());
        this.commonSetup();
    }

    /**
     * Get the first window up in the component tree starting from parent.
     * @param parent The first parent component of this dialog
     * @return The first window found going up in the component tree starting
     * from parent. Or a new one if none are found.
     */
    protected static Window getWindow(Component parent) {
        if (parent == null) {
            return (Window) new javax.swing.JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window) parent;
        } else {
            return getWindow(parent.getParent());
        }
    }

    private void commonSetup() {
        this.initComponents();
        // Bind close window to close
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                JAbstractDialog.this.close();
            }
        });
        // Finalize popup size and position
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        this.setMinimumSize(new java.awt.Dimension(
                (int)Math.round(screenSize.width * this.minWidthRatio),
                (int)Math.round(screenSize.height * this.minHeightRatio)));
        this.pack();
        this.setLocationRelativeTo(getOwner());
    }

    /**
     * Assign shortcut listeners. A close listener is already registered,
     * subclasses can extend this method to add more listeners.
     * It is automatically called from open().
     */
    protected void setShortcuts() {
        // Set close shortcut
        this.closeListener = new CloseShortcutListener();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL,
                this.closeListener);
    }

    /**
     * Counterpart method of setShortcuts to remove them.
     * It is automatically called from close().
     */
    protected void cleanShortcuts() {
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.removeListener(this.rootPane, ShortcutSections.GENERAL,
                this.closeListener);
    }

    /**
     * Show the dialog and assign shortcuts. Use this method
     * instead of setVisibility(true) to register shortcuts.
     */
    public void open() {
        this.setShortcuts();
        this.setVisible(true);
    }

    /**
     * Close and cleanup the dialog.
     */
    protected void close() {
        this.cleanShortcuts();
        this.setVisible(false);
        this.dispose();
    }

    private class CloseShortcutListener implements ShortcutListener {
        @Override // from ShortcutListener
        public boolean shortcutPressed(Shortcuts s) {
            switch (s) {
                case GENERAL_CANCEL:
                    JAbstractDialog.this.close();
                    return true;
                case GENERAL_VALIDATE:
                    // Prevent propagation
                    return true;
                default: // Ignore
                    return false;
            }
        }
    }

    /**
     * Generate the UI components. It is automatically called
     * from the constructor. The position and size of the dialog
     * is automatically set after that.
     */
    protected abstract void initComponents();

    /**
     * Set the minimum width of the popup, from screen size.
     * @param ratio The fraction size of the screen. Set to 0.0 to only
     * fit from content. The value is clamped between 0.0 and 1.0.
     */
    protected void setMinWidthRatio(double ratio) {
        this.minWidthRatio = Math.max(0.0, ratio);
        this.minWidthRatio = Math.min(1.0, this.minWidthRatio);
    }

    /**
     * Set the minimum height of the popup, from screen size.
     * @param ratio The fraction size of the screen. Set to 0.0 to only
     * fit from content. The value is clamped between 0.0 and 1.0.
     */
    protected void setMinHeightRatio(double ratio) {
        this.minHeightRatio = Math.max(0.0, ratio);
        this.minHeightRatio = Math.min(1.0, this.minHeightRatio);
    }
}
