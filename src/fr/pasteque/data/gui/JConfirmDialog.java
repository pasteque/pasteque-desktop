//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.util.Alarm;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;

/**
 * Confirmation dialog, with default set to cancel.
 * Use {@link #showConfirm(parent, title, message)} for a yes/no dialog
 * and {@link #showAcceptAction(parent, title, message)} for a yes/no/cancel
 * dialog.
 */
public class JConfirmDialog extends JAbstractDialog implements ShortcutListener
{
    private static final long serialVersionUID = 1895578862131435948L;

    /** Type for an Ok/Cancel confirmation popup. */
    public static final int TYPE_YES_NO = 0;
    /** Type for a Yes/No/Cancel confirmation popup. */
    public static final int TYPE_YES_NO_CANCEL = 1;

    private int type;
    private String title;
    private String message;
    private Boolean choice;
    protected Alarm sound;

    /**
     * Display the confirmation dialog and return the choice.
     * Default value is No.
     * @param parent The component to attach the popup to.
     * @param title The title of the popup.
     * @param message The message of the popup.
     * @return True when the user confirms, false otherwise.
     */
    public static boolean showConfirm(Component parent,
            String title, String message) {
        JConfirmDialog dialog = new JConfirmDialog(parent, title, message,
                TYPE_YES_NO);
        dialog.open();
        return dialog.confirmed();
    }

    /**
     * Ask to run an action, don't run or cancel (yes/no/cancel).
     * Default value is Cancel.
     * @return Boolean.TRUE when the user confirmed (Yes),
     * Boolean.FALSE when not confirmed (No), null when canceled (Cancel).
     */
    public static Boolean showAcceptAction(Component parent,
            String title, String message) {
        JConfirmDialog dialog = new JConfirmDialog(parent, title, message,
                TYPE_YES_NO_CANCEL);
        dialog.open();
        return dialog.getChoice();
    }

    /**
     * Create a confirmation dialog with given title and message,
     * with default to cancel.
     * @param parent The component to attach the popup to.
     * @param title The title of the popup.
     * @param message The message of the popup.
     * @param type The popup type (see constants).
     */
    public JConfirmDialog(Component parent, String title, String message,
            int type) {
        super(parent);
        this.title = title;
        this.message = message;
        this.type = type;
        if (this.type == TYPE_YES_NO) {
            this.choice = Boolean.FALSE;
        } else {
            this.choice = null;
        }
        this.setTitle(this.title);
        this.messageLbl.setText(message);
        this.pack();
        this.setLocationRelativeTo(getOwner());
        this.sound = new Alarm("audio/soft_error.aiff");
    }

    @Override // from JAbstractDialog
    public void open() {
        if (this.sound != null) {
            this.sound.soundplay();
        }
        if (this.type == TYPE_YES_NO) {
            jcmdCancel.setVisible(false);
            jcmdNo.requestFocus();
        } else {
            jcmdCancel.setVisible(true);
            jcmdCancel.requestFocus();
        }
        super.open();
    }

    protected void setShortcuts() {
        super.setShortcuts();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
    }

    protected void cleanShortcuts() {
        super.cleanShortcuts();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
    }

    /**
     * Press button when pressing GENERAL_VALIDATE.
     */
    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                if (this.jcmdNo.isFocusOwner()) {
                    this.jcmdNo.doClick(Shortcuts.CLICK_TIME);
                } else if (this.jcmdYes.isFocusOwner()) {
                    this.jcmdYes.doClick(Shortcuts.CLICK_TIME);
                } // else block further propagation
                return true;
            default: // Ignore
                return false;
        }
    }
    /**
     * Check wether the user confirmed or not. Until a button is clicked,
     * this value is the default one.
     */
    public boolean confirmed() {
        return Boolean.TRUE.equals(this.choice);
    }

    /**
     * Get user's choice. Until a button is clicked, this value is the default
     * one.
     * @return The user's choice, or the default value if no choice was
     * explicitely choosen yes.
     */
    public Boolean getChoice() {
        return this.choice;
    }

    protected void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        messageLbl = WidgetsBuilder.createWrapLabel(this.message,
                WidgetsBuilder.SIZE_MEDIUM, COMMON_WIDTH_EM);
        int marginInset = 10;
        jcmdYes = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.Yes"),
                WidgetsBuilder.SIZE_MEDIUM);
        jcmdNo = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.No"),
                WidgetsBuilder.SIZE_MEDIUM);
        jcmdCancel = WidgetsBuilder.createButton(
                null,
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jcmdYes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcmdYesActionPerformed(evt);
            }
        });
        jcmdNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcmdNoActionPerformed(evt);
            }
        });
        jcmdCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcmdCancelActionPerformed(evt);
            }
        });

        this.setLayout(new GridBagLayout());
        GridBagConstraints c;

        // Confirm message
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 0;
        c.insets = new java.awt.Insets(btnSpacing, btnSpacing, 0, btnSpacing);
        this.add(messageLbl, c);
        c = new GridBagConstraints();

        // Ok/cancel buttons
        JPanel buttonsContainer = new JPanel();
        buttonsContainer.setLayout(new FlowLayout(FlowLayout.CENTER,
                marginInset, btnSpacing));
        buttonsContainer.add(jcmdYes);
        buttonsContainer.add(jcmdNo);
        buttonsContainer.add(jcmdCancel);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 1; c.gridwidth = 2;
        c.anchor = GridBagConstraints.EAST;
        c.insets = new java.awt.Insets(btnSpacing, 0, 0, 0);
        this.add(buttonsContainer, c);
    }

    private void jcmdYesActionPerformed(java.awt.event.ActionEvent evt) {
        this.choice = Boolean.TRUE;
        this.close();
    }

    private void jcmdNoActionPerformed(java.awt.event.ActionEvent evt) {
        this.choice = Boolean.FALSE;
        this.close();
    }

    private void jcmdCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.choice = null;
        this.close();
    }

    private JWrapLabel messageLbl;
    private javax.swing.JButton jcmdYes;
    private javax.swing.JButton jcmdNo;
    private javax.swing.JButton jcmdCancel;
}
