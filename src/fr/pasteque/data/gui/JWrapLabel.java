//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import javax.swing.JTextArea;

/**
 * A label with word wrap built upon JTextArea.
 */
public class JWrapLabel extends JTextArea
{
    private static final long serialVersionUID = -4604331431913864269L;

    /**
     * Create an empty JWrapLabel. Use {@link #setText(String)} and
     * {@link #setColumns(int)} to configure it later.
     */
    public JWrapLabel() {
        super();
        this.setupStyle(0);
    }

    /**
     * Create and set text and width.
     * @param text The initial text.
     * @param em The width of the widget, roughly the number of `m`.
     */
    public JWrapLabel(String text, int em) {
        super(text);
        this.setupStyle(em);
    }

    /**
     * Make the JTextArea look like a JLabel with word wrap.
     * @param em The parameter of {@link #setColumns(int)}.
     */
    private void setupStyle(int em) {
        this.setBackground(null);
        this.setBorder(null);
        this.setEditable(false);
        this.setFocusable(false);
        this.setLineWrap(true);
        this.setWrapStyleWord(true);
        this.setColumns(em);
    }
}
