//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import java.awt.*;
import javax.swing.*;
import fr.pasteque.data.loader.LocalRes;
import fr.pasteque.pos.util.Alarm;

/**
 * Information message to display log to the user.
 * Paired with JMessageDialog for the actual display.
 */
public class MessageInf
{
    /** Word code for a critical error. */
    public final static int SGN_DANGER = 0xFF000000; // Death or serious injury will occur
    /** Word code for a recoverable error. */
    public final static int SGN_WARNING = 0xFE000000; // Death or serious injury may occur
    /** Word code for a potential error. */
    public final static int SGN_CAUTION = 0xFD000000; // Minor or moderate injury may occur
    /** Word code for a generic information. */
    public final static int SGN_NOTICE = 0xFC000000; // Damage to property may occur
    /** Word code for an important information. */
    public final static int SGN_IMPORTANT = 0xFA000000; // Operating or maintenance instructions or additional information
    /** Word code for to notice a successful operation. */
    public final static int SGN_SUCCESS = 0xFB000000;

    /** Default word code with no actual meaning. */
    public final static int CLS_GENERIC = 0x00000000;


    /** Word code */
    private int m_iMsgNumber; // = SIGNAL_WORD (0xFF000000) | ERROR_CLASS (0x00FF0000) | ERROR_CODE (0x0000FFFF)
    /** Message to display */
    private String m_sHazard;
    /** The object that triggered this message. */
    private Object m_eCause;

    /**
     * Creates a message with a type and a cause.
     * @param iSignalWord The message word code type (see constants).
     * @param sHazard The actual message.
     * @param e The object that caused the message to show.
     */
    public MessageInf(int iSignalWord, String sHazard, Object e) {
        m_iMsgNumber = iSignalWord | CLS_GENERIC;
        m_sHazard = sHazard;
        m_eCause = e;
    }

    /**
     * Create a message with a type.
     * @param iSignalWord The message word code type (see constants).
     * @param sHazard The actual message.
     */
    public MessageInf(int iSignalWord, String sHazard) {
        this (iSignalWord, sHazard, null);
    }

    /**
     * Create a warning message from an error.
     * @param e The error thrown.
     */
    public MessageInf(Throwable e) {
        this(SGN_WARNING, e.getLocalizedMessage(), e);
    }

    /**
     * Pass this message to a JMessageDialog and show it.
     * Shortcut for JMessageDialog.showMessage(parent, this).
     * @param parent The parent component the popup will be attached to.
     */
    public void show(Component parent) {
        JMessageDialog.showMessage(parent, this);
    }

    public Object getCause() {
        return m_eCause;
    }

    public int getSignalWord() {
        return m_iMsgNumber & 0xFF000000;
    }

    public Icon getSignalWordIcon() {
        int iSignalWord = getSignalWord();
        if (iSignalWord == SGN_DANGER) {
            return UIManager.getIcon("OptionPane.errorIcon");
        } else if (iSignalWord == SGN_WARNING) {
            return UIManager.getIcon("OptionPane.warningIcon");
       } else if (iSignalWord == SGN_CAUTION) {
            return UIManager.getIcon("OptionPane.warningIcon");
        } else if (iSignalWord == SGN_NOTICE) {
            return UIManager.getIcon("OptionPane.informationIcon");
        } else if (iSignalWord == SGN_IMPORTANT) {
            return UIManager.getIcon("OptionPane.informationIcon");
        } else if (iSignalWord == SGN_SUCCESS) {
            return UIManager.getIcon("OptionPane.informationIcon");
        } else {
            return UIManager.getIcon("OptionPane.questionIcon");
        }
    }

    public String getErrorCodeMsg() {
        StringBuffer sb = new StringBuffer();       
        int iSignalWord = getSignalWord();
        if (iSignalWord == SGN_DANGER) {
            sb.append("DNG_");
        } else if (iSignalWord == SGN_WARNING) {
            sb.append("WRN_");
        } else if (iSignalWord == SGN_CAUTION) {
            sb.append("CAU_");
        } else if (iSignalWord == SGN_NOTICE) {
            sb.append("NOT_");
        } else if (iSignalWord == SGN_IMPORTANT) {
            sb.append("IMP_");
        } else if (iSignalWord == SGN_SUCCESS) {
            sb.append("INF_");
        } else {
            sb.append("UNK_");
        }
        sb.append(toHex((m_iMsgNumber & 0x00FF0000) >> 16, 2));
        sb.append('_');
        sb.append(toHex(m_iMsgNumber & 0x0000FFFF, 4));
        return sb.toString();
    }

    private String toHex(int i, int iChars) {
        String s = Integer.toHexString(i);
        return s.length() >= iChars ? s : fillString(iChars - s.length()) + s;
    }

    private String fillString(int iChars) {
        char[] aStr = new char[iChars];
        for (int i = 0; i < aStr.length; i++) {
            aStr[i] = '0';
        }
        return new String(aStr);
    }

    public String getMessageMsg() {
        StringBuffer sb = new StringBuffer();     
        int iSignalWord = getSignalWord();
        if (iSignalWord == SGN_DANGER) {
            sb.append(LocalRes.getIntString("sgn.danger"));
        } else if (iSignalWord == SGN_WARNING) {
            sb.append(LocalRes.getIntString("sgn.warning"));
        } else if (iSignalWord == SGN_CAUTION) {
            sb.append(LocalRes.getIntString("sgn.caution"));
        } else if (iSignalWord == SGN_NOTICE) {
            sb.append(LocalRes.getIntString("sgn.notice"));
        } else if (iSignalWord == SGN_IMPORTANT) {
            sb.append(LocalRes.getIntString("sgn.important"));
        } else if (iSignalWord == SGN_SUCCESS) {
            sb.append(LocalRes.getIntString("sgn.success"));
        } else {
            sb.append(LocalRes.getIntString("sgn.unknown"));
        }
        sb.append(m_sHazard);
        return sb.toString();
    }

    /**
     * Get a sound to play when the message is shown.
     * @return An Alarm with different notification sound according
     * to the severity of the message, or even null.
     */
    public Alarm getMessageAlarm() {
        int iSignalWord = getSignalWord();
        switch (iSignalWord) {
            case SGN_IMPORTANT:
            case SGN_DANGER:
            case SGN_WARNING:
            case SGN_CAUTION:
                return new Alarm("audio/error.aiff");
            case SGN_NOTICE:
                return new Alarm("audio/soft_error.aiff");
            default:
                return null;
        }
    }    
}
