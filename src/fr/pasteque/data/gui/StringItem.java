//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

/**
 * Text item for gui components to uses a different label than its value.
 */
public class StringItem<E>
{
    protected E item;
    protected String label;

    /**
     * Create an item with toString as its label.
     */
    public StringItem(E item) {
        this(item, item == null ? "" : item.toString());
    }

    /**
     * Create an item with an explicitely set label.
     */
    public StringItem(E item, String label) {
        this.item = item;
        this.label = label;
    }

    public E getItem() {
        return this.item;
    }

    @Override
    public String toString() {
        return this.label;
    }

    /**
     * Check for equality, either directly or with item.
     * @return True if o.equals(this.item) or o.item.equals(this.item).
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return this.item == null;
        }
        if (this.item != null && this.item.equals(o)) {
            return true;
        }
        if (o instanceof StringItem) {
            StringItem<?> cast = (StringItem<?>) o;
            return this.equals(cast.item);
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (this.item == null) {
            return 0;
        }
        return this.item.hashCode();
    }
}
