//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

/**
 * Extension of javax.swing.JComboBox to cast items correctly.
 * Use {@link #getSelectedValue} and {@link #setSelectedValue} instead
 * of getSelectedItem and setSelectedItem for the actual item.
 */
public class JComboBoxVal<E> extends javax.swing.JComboBox<StringItem<E>>
{
    private static final long serialVersionUID = 854346422516266875L;

    /**
     * Create a combo box with a predefined list of values.
     * Same as creating an empty JComboBoxVal and calling
     * addItems.
     */
    @SafeVarargs
    public JComboBoxVal(StringItem<E>... items) {
        super();
        this.addItems(items);
    }

    /**
     * Shortcut for addItem(new StringItem(value, display))
     */
    public void addItem(E value, String display) {
        this.addItem(new StringItem<E>(value, display));
    }

    /**
     * Utility function to add multiple items in a row.
     */
    @SafeVarargs
    public final void addItems(StringItem<E>... items) {
        for (StringItem<E> i : items) {
            this.addItem(i);
        }
    }

    /**
     * Add all elements of a list using toString as their label.
     */
    public void addItems(java.util.List<E> items) {
        for (E i : items) {
            this.addItem(new StringItem<E>(i));
        }
    }

    /**
     * Get the currently selected value. Use this instead of getSelectedItem
     * to cast the returned object correctly.
     * @return The selected value, null if no selection or a custom entry.
     */
    public E getSelectedValue() {
        int index = this.getSelectedIndex();
        if (index == -1) {
            return null;
        }
        return this.getItemAt(index).getItem();
    }

    /**
     * Get the selected item. Use this instead of setSelectedItem
     * to avoid casting issues.
     */
    public void setSelectedValue(E value) {
        if (value == null) {
            this.setSelectedItem(null);
        } else {
            this.setSelectedItem(new StringItem<E>(value, ""));
        }
    }
}
