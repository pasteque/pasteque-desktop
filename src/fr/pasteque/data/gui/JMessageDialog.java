//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.loader.LocalRes;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.Alarm;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

/**
 * Popup to show an information message with optional details.
 * @author  adrian
 */
public class JMessageDialog extends JAbstractDialog
{
    private static final long serialVersionUID = -7292502016191030929L;

    public JMessageDialog(Component parent) {
        super(parent);
    }

    /**
     * All-in-one function to create and display the dialog.
     */
    public static void showMessage(Component parent, MessageInf inf) {
        JMessageDialog myMsg = new JMessageDialog(parent);
        myMsg.getRootPane().setDefaultButton(myMsg.jcmdOK);

        myMsg.jlblIcon.setIcon(inf.getSignalWordIcon());
        myMsg.jlblErrorCode.setText(inf.getErrorCodeMsg());
        myMsg.jlblMessage.setText(inf.getMessageMsg());

        // Capturamos el texto de la excepcion...
        if (inf.getCause() == null) {
            myMsg.jtxtException.setText(null);
        } else {
            StringBuffer sb = new StringBuffer();

            if (inf.getCause() instanceof Throwable) {
                Throwable t = (Throwable) inf.getCause();
                t.printStackTrace();
                while (t != null) {
                    sb.append(t.getClass().getName());
                    sb.append(": \n");
                    sb.append(t.getMessage());
                    sb.append("\n\n");
                    t = t.getCause();
                }
            } else if (inf.getCause() instanceof Throwable[]) {
                Throwable[] m_aExceptions = (Throwable[]) inf.getCause();
                for (int i = 0; i < m_aExceptions.length; i++) {
                    m_aExceptions[i].printStackTrace();
                    sb.append(m_aExceptions[i].getClass().getName());
                    sb.append(": \n");
                    sb.append(m_aExceptions[i].getMessage());
                    sb.append("\n\n");
                }
            } else if (inf.getCause() instanceof Object[]) {
                Object [] m_aObjects = (Object []) inf.getCause();
                for (int i = 0; i < m_aObjects.length; i++) {
                    sb.append(m_aObjects[i].toString());
                    sb.append("\n\n");
                }
            } else if (inf.getCause() instanceof String) {
                sb.append(inf.getCause().toString());
            } else {
                sb.append(inf.getCause().getClass().getName());
                sb.append(": \n");
                sb.append(inf.getCause().toString());
            }
            myMsg.jtxtException.setText(sb.toString());
        }
        myMsg.jtxtException.setCaretPosition(0);

        myMsg.pack();
        myMsg.setLocationRelativeTo(myMsg.getOwner());

        myMsg.alarm = inf.getMessageAlarm();

        myMsg.open();
    }

    /**
     * All-in-one function to create and display a simple information dialog.
     * The execution is blocked until the popup is dismissed.
     * @param parent Parent component to attach the popup to.
     * @param title Popup title.
     * @param message Message to display.
     */
    public static void showMessage(Component parent, String title, String message) {
        JMessageDialog myMsg = new JMessageDialog(parent);
        myMsg.getRootPane().setDefaultButton(myMsg.jcmdOK);
        myMsg.setTitle(title);
        myMsg.jlblErrorCode.setText("");
        myMsg.jlblIcon.setIcon(javax.swing.UIManager.getIcon("OptionPane.informationIcon"));
        myMsg.jlblMessage.setText(message);
        myMsg.pack();
        myMsg.setLocationRelativeTo(myMsg.getOwner());
        myMsg.open();
    }

    @Override // from JAbstractDialog
    public void open() {
        if (this.alarm != null) {
            this.alarm.soundplay();
        }
        this.jlblErrorCode.setVisible(!this.jlblErrorCode.getText().isEmpty());
        this.jscrException.setVisible(!this.jtxtException.getText().isEmpty());
        super.open();
    }

    protected void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;
        jlblErrorCode = WidgetsBuilder.createLabel();
        jlblMessage = WidgetsBuilder.createWrapLabel("jlblMessage",
                WidgetsBuilder.SIZE_MEDIUM, COMMON_WIDTH_EM - 2);
        jscrException = new javax.swing.JScrollPane();
        jtxtException = new javax.swing.JTextArea();
        jlblIcon = new javax.swing.JLabel();
        jcmdOK = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.tr("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);

        setTitle(LocalRes.getIntString("title.message")); // NOI18N
        setResizable(false);

        javax.swing.JPanel container = new javax.swing.JPanel();
        this.getContentPane().add(container);
        container.setLayout(new GridBagLayout());
        GridBagConstraints c = null;
        container.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));

        // Icon
        jlblIcon.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jlblIcon.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 0; c.gridheight = 3;
        c.anchor = GridBagConstraints.NORTH;
        container.add(jlblIcon, c);

        // Text
        jlblErrorCode.setFont(jlblErrorCode.getFont().deriveFont(jlblErrorCode.getFont().getStyle() & ~java.awt.Font.BOLD, jlblErrorCode.getFont().getSize()-2));
        jlblErrorCode.setText("jlblErrorCode");
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 0; c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        container.add(jlblErrorCode, c);

        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 1; c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        container.add(jlblMessage, c);

        jscrException.setAlignmentX(0.0F);
        jtxtException.setEditable(false);
        jscrException.setViewportView(jtxtException);
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 2; c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        container.add(jscrException, c);
        

        // Button line
        jcmdOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcmdOKActionPerformed(evt);
            }
        });
        javax.swing.JPanel buttonsContainer = new javax.swing.JPanel();
        buttonsContainer.setLayout(new FlowLayout(FlowLayout.CENTER,
                marginInset, btnSpacing));
        buttonsContainer.add(jcmdOK);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 3; c.gridwidth = 2;
        c.anchor = GridBagConstraints.EAST;
        c.insets = new java.awt.Insets(btnSpacing, 0, 0, 0);
        container.add(buttonsContainer, c);
    }

    private void jcmdOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcmdOKActionPerformed
        this.close();
    }//GEN-LAST:event_jcmdOKActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jcmdOK;
    private javax.swing.JLabel jlblErrorCode;
    private javax.swing.JLabel jlblIcon;
    private JWrapLabel jlblMessage;
    private javax.swing.JScrollPane jscrException;
    private javax.swing.JTextArea jtxtException;
    // BEEP
    private Alarm alarm;
    // End of variables declaration//GEN-END:variables

}
