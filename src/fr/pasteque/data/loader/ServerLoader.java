//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.loader;

import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.util.AltEncrypter;
import fr.pasteque.pos.util.URLBinGetter;
import fr.pasteque.pos.util.URLTextGetter;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * API request sender.
 * Manages URL, login and JWT token.
 */
public class ServerLoader
{
    private static Logger logger = Logger.getLogger("fr.pasteque.data.loader.ServerLoader");

    /** Shared login JWT to resend at each call. */
    private static String lastToken = null;
    /** Shared token user to check if it is the same. */
    private static String lastTokenUser = null;

    private static String getLastToken(String user) {
        if (ServerLoader.lastToken != null
                && user != null && user.equals(ServerLoader.lastTokenUser)) {
            return ServerLoader.lastToken;
        } else {
            return null;
        }
    }

    private String url;
    private String user;
    private String password;

    private void preformatUrl() {
        if (!this.url.startsWith("http")) {
            this.url = "http://" + this.url;
        }
        if (!this.url.endsWith("/")) {
            this.url += "/";
        }
    }

    /** Create from AppConfig */
    public ServerLoader() {
        String url = AppConfig.loadedInstance.getProperty("server.backoffice");
        String user = AppConfig.loadedInstance.getProperty("db.user");
        String password = AppConfig.loadedInstance.getProperty("db.password");
        if (password != null && password.startsWith("crypt:")) {
            // the password is encrypted
            AltEncrypter cypher = new AltEncrypter("cypherkey" + user);
            password = cypher.decrypt(password.substring(6));
        }
        this.url = url;
        this.preformatUrl();
        this.user = user;
        this.password = password;
    }

    private Map<String, String> params(String token,
            String... params) {
        Map<String, String> ret = new HashMap<String, String>();
        ret.put("Token", token);
        for (int i = 0; i < params.length; i+= 2) {
            if (params[i + 1] != null) {
                String key = params[i];
                String value = params[i + 1];
                ret.put(key, value);
            }
        }
        return ret;
    }

    /** Inner call to get a JWT from scratch. */
    private Response login()
        throws SocketTimeoutException, URLTextGetter.ServerException,
               IOException {
        logger.log(Level.INFO, "Logging in " + this.url + "api/login");
        JSONObject resp = URLTextGetter.getText(this.url + "api/login",
                null, this.params("", "user", this.user,
                        "password", this.password));
        logger.log(Level.INFO, "Server response: " + resp);
        try {
            Response r = new Response(resp);
            if (r.getToken() != null) {
                ServerLoader.lastToken = r.getToken();
                ServerLoader.lastTokenUser = this.user;
            }
            return r;
        } catch (JSONException e) {
            throw new URLTextGetter.ServerException(e.getMessage());
        }
    }

    /**
     * Check token from response, refresh if accepted.
     * @return True if the token was accepted and refreshed,
     * false when it was rejected and discarded. A new one will be fetched
     * with getToken.
     */
    private boolean checkAndRelog(Response r) {
        if (r == null) {
            // Dafuk?
            logger.log(Level.WARNING, "Received a null server Response.");
            ServerLoader.lastToken = null;
            ServerLoader.lastTokenUser = null;
            return false;
        }
        try {
            if (Response.ERR_CODE_NOT_LOGGED.equals(r.getErrorCode())) {
                // The token was rejected, discard it to get a new one.
                ServerLoader.lastToken = null;
                ServerLoader.lastTokenUser = null;
                return false;
            }
        } catch (JSONException e) {
            // This is not an error
        }
        ServerLoader.lastToken = r.getToken();
        return true;
    }

    /** Get the last valid token or request a new one. */
    private String getToken()
        throws SocketTimeoutException, URLTextGetter.ServerException,
               IOException {
        String token = ServerLoader.getLastToken(this.user);
        if (token == null) {
            Response login = this.login();
            if (!Response.STATUS_OK.equals(login.getStatus())) {
                return null;
            }
            token = login.getToken();
        }
        return token;
    }

    /*
     * Low level request, with all parameters.
     * @param method Http method (GET, POST...).
     * @param target API url without the base url.
     * @param body Data to send in json format.
     * @return Server response
     * @throw SocketTimeoutException When the connexion timed out.
     * @throw URLTextGetter.ServerException See URLTextGetter.
     * @throw IOException
     */
    public Response request(String method, String target, JSONObject body)
            throws SocketTimeoutException, URLTextGetter.ServerException,
                IOException {
        String token = this.getToken();
        if (token == null) {
            throw new URLTextGetter.ServerException("Unable to get auth token");
        }
        String strBody = body.toString();
        logger.log(Level.INFO, "Requesting " + method + " " + this.url + target
                + " - " + strBody);
        JSONObject resp = URLTextGetter.getText(this.url + target, this.params(token),
                strBody, method, URLTextGetter.TYPE_JSON);
        logger.log(Level.INFO, "Server response: " + resp);
        try {
            Response r = new Response(resp);
            if (!this.checkAndRelog(r)) { // Retry with new token
                return this.request(method, target, body);
            }
            return r;
        } catch (JSONException e) {
            throw new URLTextGetter.ServerException(e.getMessage());
        }
    }

    /*
     * GET from API.
     * @param target API url without base url.
     * @param params List of parameters with key and value as two following
     * arguments.
     */
    public Response read(String target, String... params)
        throws SocketTimeoutException, URLTextGetter.ServerException,
               IOException {
        String token = this.getToken();
        if (token == null) {
            throw new URLTextGetter.ServerException("Unable to get auth token");
        }
        logger.log(Level.INFO, "Reading " + this.url + target);
        JSONObject resp = URLTextGetter.getText(this.url + target,
                this.params(token, params));
        logger.log(Level.INFO, "Server response: " + resp);
        try {
            Response r = new Response(resp);
            if (!this.checkAndRelog(r)) { // Retry with new token
                return this.read(target, params);
            }
            return r;
        } catch (JSONException e) {
            throw new URLTextGetter.ServerException(e.getMessage());
        }
    }

    /**
     * POST to API.
     * @param target API url without base url.
     * @param params List of parameters with key and value as two following
     * arguments.
     */
    public Response write(String target, String... params)
        throws SocketTimeoutException, URLTextGetter.ServerException,
               IOException {
        String token = this.getToken();
        if (token == null) {
            throw new URLTextGetter.ServerException("Unable to get auth token");
        }
        logger.log(Level.INFO, "Writing " + target + " "
                + Arrays.deepToString(params));
        JSONObject resp = URLTextGetter.getText(this.url + target, null,
                this.params(token, params));
        logger.log(Level.INFO, "Server response: " + resp);
        try {
            Response r = new Response(resp);
            if (!this.checkAndRelog(r)) { // Retry with new token
                return this.write(target, params);
            }
            return r;
        } catch (JSONException e) {
            throw new URLTextGetter.ServerException(e.getMessage());
        }
    }

    public byte[] readImage(String model, String id)
        throws SocketTimeoutException, URLBinGetter.ServerException,
               IOException {
        String token = null;
        try {
            token = this.getToken();
        } catch (URLTextGetter.ServerException e) {
            throw new URLBinGetter.ServerException(e.getMessage());
        }
        if (token == null) {
            throw new URLBinGetter.ServerException("Unable to get auth token");
        }
        String target = "api/image/" + model + "/" + id;
        return URLBinGetter.getBinary(this.url + target,
                this.params(token));
    }

    /**
     * A response from the API.
     */
    public class Response {
        public static final String STATUS_OK = "ok";
        public static final String STATUS_REJECTED = "rej";
        public static final String STATUS_ERROR = "err";

        public static final String ERR_CODE_NOT_LOGGED = "Not logged";

        private String status;
        private JSONObject response;
        private String token;

        /** Create a Response from a URLTextGetter content. */
        public Response(JSONObject content) {
            this.status = content.getString("status");
            this.response = content;
            if (Response.STATUS_OK.equals(this.status) && content.has("token")) {
                this.token = content.getString("token");
            }
        }

        public String getStatus() {
            return this.status;
        }

        public JSONObject getResponse() {
            return this.response;
        }

        public String getToken() {
            return this.token;
        }

        public JSONObject getObjContent() {
            try {
                return this.response.getJSONObject("content");
            } catch (JSONException e) {
                return null;
            }
        }

        public JSONArray getArrayContent() {
            try {
                return this.response.getJSONArray("content");
            } catch (JSONException e) {
                return null;
            }
        }

        /** Get error code for rejected or error response. */
        public String getErrorCode() {
            try {
                return this.response.getString("content");
            } catch (JSONException e) {
                return null;
            } catch (NullPointerException e) {
                return null;
            }
        }
    }
}
