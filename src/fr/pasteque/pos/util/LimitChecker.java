package fr.pasteque.pos.util;

import fr.pasteque.pos.forms.AppConfig;

/**
 * Utility class to restrict input to an acceptable range.
 */
public class LimitChecker
{
    private static LimitChecker instance;

    private Double minValue = null;
    private Double maxValue = null;

    /**
     * Create a limit checker initialized from the application config.
     * The limit are set from "ui.editornumber.maxvalue" with positive
     * and negative sign.
     */
    public LimitChecker() {
        AppConfig cfg = AppConfig.loadedInstance;
        this.minValue = -cfg.getNumber("ui.editornumber.maxvalue");
        this.maxValue = cfg.getNumber("ui.editornumber.maxvalue");
    }

    /**
     * Get the application wide instance, create it when not initialized. */
    public static LimitChecker getInstance() {
        if (instance == null) {
            return instance = new LimitChecker();
        } else {
            return instance;
        }
    }

    public Double getMaxValue() {
        return this.maxValue;
    }

    /**
     * Set the maximum accepted value.
     * @param maxValue The maximum accepted value.
     * Set to null to remove the limit.
     */
    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public Double getMinValue() {
        return this.minValue;
    }

    /**
     * Set the minimum accepted value.
     * @param minValue The minimum accepted value.
     * Set to null to remove the limit.
     */
    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    /**
     * Set minimum and maximum accepted values.
     * @param minValue The minimum accepted value.
     * Set to null to remove the limit.
     * @param maxValue The maximum accepted value.
     * Set to null to remove the limit.
     */
    public void setBounds(Double minValue, Double maxValue) {
        this.setMinValue(minValue);
        this.setMaxValue(maxValue);
    }

    /**
     * Check if the value stays within the limits.
     * @param d The value to check.
     * @return True when the value is within the accepted range.
     * False when the value is out of the accepted range.
     */
    public boolean checkLimits(double d) {
        if ((this.maxValue != null && d > this.maxValue)
                || (this.minValue != null && d < this.minValue)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if the value stays within the limits.
     * @param text The string representation of the value to check.
     * @return True if the text is null, empty or cannot be read as a number,
     * or when its value is within the accepted range.
     * False when the value is out of the accepted range.
     */
    public boolean checkLimits(String text) {
        if (text == null || text.equals("")) {
            return true;
        } else {
            try {
                double d = Double.parseDouble(text);
                return this.checkLimits(d);
            } catch (NumberFormatException e) {
                return true;
            }
        }
    }
}
