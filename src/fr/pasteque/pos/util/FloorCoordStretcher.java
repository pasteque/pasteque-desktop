//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.util;

/**
 * Utility class to recompute coordinates of place buttons inside a floor panel
 * according to the size of the container.
 */
public class FloorCoordStretcher
{
    private int buttonWidth;
    private int buttonHeight;
    /** Minimum x position of a table to crop unused space. */
    private int xMin;
    /** Maximum x position of a table to crop unused space. */
    private int xMax;
    /** Minimum y position of a table to crop unused space. */
    private int yMin;
    /** Maximum y position of a table to crop unused space. */
    private int yMax;
    private int containerWidth;
    private int containerHeight;
    /** The minimum margin for the container, top, right, bottom, left. */
    private int[] margin;
    /** The actual margin to add to center the floor in its container. */
    private int marginLeft;
    /** The actual margin to add to center the floor in its container. */
    private int marginTop;
    private float coordFactor;
    /** Whether the size and margins should be recomputed or not. */
    private boolean dirty;

    /**
     * Create a stretcher to compute coordinate with the given GUI settings.
     * @param buttonWidth The width in pixel of a button.
     * @param buttonHeight The height in pixel of a button.
     * @param margin The minimum margin between the border of the container
     * and a button.
     */
    public FloorCoordStretcher(int buttonWidth, int buttonHeight, int margin) {
        this(buttonWidth, buttonHeight, margin, margin, margin, margin);
    }

    public FloorCoordStretcher(int buttonWidth, int buttonHeight, int marginTop,
            int marginRight, int marginBottom, int marginLeft) {
        this.buttonWidth = buttonWidth;
        this.buttonHeight = buttonHeight;
        this.margin = new int[] {marginTop, marginRight, marginBottom, marginLeft};
        this.xMin = Integer.MAX_VALUE;
        this.xMax = Integer.MIN_VALUE;
        this.yMin = Integer.MAX_VALUE;
        this.yMax = Integer.MIN_VALUE;
        this.coordFactor = 1.0f;
    }

    /**
     * Extends the local coordinates to contains the given location.
     * The coordinates will be recomputed by removing the unused space
     * on the edges.
     * @param x The x coordinate of the location. It should match the left side
     * of a button.
     * @param y The y coordinate of the location. It should match the top side
     * of a button.
     */
    public void extendBoundsFor(int x, int y) {
        if (x < this.xMin) {
            this.xMin = x;
            this.dirty = true;
        }
        if (x > this.xMax) {
            this.xMax = x;
            this.dirty = true;
        }
        if (y < this.yMin) {
            this.yMin = y;
            this.dirty = true;
        }
        if (y > this.yMax) {
            this.yMax = y;
            this.dirty = true;
        }
    }

    /**
     * Indicate the size of the container. The content will be stretched
     * to fit in this size.
     * @param width The width in pixel of the container, including margin.
     * @param height The height in pixel of the container, including margin.
     */
    public void setContainerSize(int width, int height) {
        this.containerWidth = width;
        this.containerHeight = height;
        this.dirty = true;
    }

    /**
     * Recompute the stretching factor to fill the container while respecting
     * the aspect ratio.
     */
    private void recomputeRatio() {
        int contentWidth = this.containerWidth - this.margin[3] - this.margin[1];
        int contentHeight = this.containerHeight - this.margin[0] - this.margin[2];
        int floorWidth = this.xMax - this.xMin;
        int floorHeight = this.yMax - this.yMin;
        this.coordFactor = Math.min(
                Float.valueOf(contentWidth - this.buttonWidth) / floorWidth,
                Float.valueOf(contentHeight - this.buttonHeight) / floorHeight);
        this.marginLeft = Double.valueOf(Math.floor((contentWidth - (floorWidth * coordFactor + this.buttonWidth)) / 2)).intValue() + this.margin[3];
        this.marginTop = Double.valueOf(Math.floor((contentHeight - (floorHeight * coordFactor + this.buttonHeight)) / 2)).intValue() + this.margin[0];
    }

    /**
     * Compute new coordinates to use most space inside the container.
     * @param x The x coordinate. Often the coordinate of a Place.
     * @param y The y coordinate. Often the coordiante of a Place.
     * @return The new stretched coordinates
     */
    public Point stretchCoord(int x, int y) {
        if (this.containerWidth == 0 || this.containerHeight == 0) {
            return new Point(x, y);
        }
        if (this.dirty) {
            this.recomputeRatio();
            this.dirty = false;
        }
        int localX = x - this.xMin;
        int localY = y - this.yMin;
        int posX = Double.valueOf(Math.floor(localX * this.coordFactor)).intValue() + this.marginLeft;
        int posY = Double.valueOf(Math.floor(localY * this.coordFactor)).intValue() + this.marginTop;
        return new Point(posX, posY);
    }

    /**
     * A coordinate couple.
     */
    public class Point {
        /** The X coordinate. */
        public int x;
        /** The Y coordinate. */
        public int y;
        /**
         * Create a new Point
         * @param x The x coordinate.
         * @param y The y coordinate.
         */
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
