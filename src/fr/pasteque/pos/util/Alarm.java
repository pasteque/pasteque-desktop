package fr.pasteque.pos.util;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Alarm
{
    protected Clip clip;
    boolean canPlay;

    /**
     * Create an alarm for the given relative filename.
     * @param wavefile Path to filename, relative to the build path
     * set in 'dirname.path' from command line when starting Pasteque,
     * or current directory when not set.
     */
    public Alarm(String wavefile){
        String base = System.getProperty("dirname.path");
        if (base == null) {
            base = ".";
        }
        try {
            // Open an audio input stream.
            File soundFile = new File(base + "/" + wavefile);
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
            AudioFormat format = audioIn.getFormat();
            // Get a sound clip resource.
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            this.clip = (Clip) AudioSystem.getLine(info);
            // Open audio clip and load samples from the audio input stream.
            this.clip.open(audioIn);
            this.canPlay = true;
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void soundplay()  {
        if (this.canPlay) {
            this.clip.stop();
            this.clip.setMicrosecondPosition(0);
            this.clip.start();
        }
    }
}


