//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.util;

import java.awt.Component;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import fr.pasteque.beans.JPasswordDialog;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;

/**
 * Utility to manage hashed passwords and request password changes
 * from the GUI.
 */
public class Hashcypher
{
    /** Creates a new instance of Hashcypher */
    private Hashcypher() {
    }

    /**
     * Check a password against a hashed password.
     * @param sPassword The clear password to check
     * @param sHashPassword The hashed version of the password, created with
     * {@link #hashString(String)}.
     * @return True When they match, false otherwise.
     */
    public static boolean authenticate(String sPassword, String sHashPassword) {
        if (sHashPassword == null || sHashPassword.equals("") || sHashPassword.startsWith("empty:")) {
            return sPassword == null || sPassword.equals("");
        } else if (sHashPassword.startsWith("sha1:")) {
            String hash = hashString(sPassword).toLowerCase();
            return sHashPassword.toLowerCase().equals(hash);
        } else if (sHashPassword.startsWith("plain:")) {
            return sHashPassword.equals("plain:" + sPassword);
        } else {
            return sHashPassword.equals(sPassword);
        }
    }

    /**
     * Create a hashed version of a password.
     * @param sPassword The password to hash.
     * @return A hashed version prefixed by the hashing method.
     */
    public static String hashString(String sPassword) {

        if (sPassword == null || sPassword.equals("")) {
            return "empty:";
        } else {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                md.update(sPassword.getBytes("UTF-8"));
                byte[] res = md.digest();
                return "sha1:" + StringUtils.byte2hex(res);
            } catch (NoSuchAlgorithmException e) {
                return "plain:" + sPassword;
            } catch (UnsupportedEncodingException e) {
                return "plain:" + sPassword;
            }
        }
    }

    /**
     * Show popups to ask for a new password twice.
     * @param parent The parent to attach the popups to.
     * @return The new password when entered twice,
     * null when they mismatch.
     */
    public static String changePassword(Component parent) {
        // Show the changePassword dialogs but do not check the old password
        String sPassword = JPasswordDialog.showEditPassword(parent,
                AppLocal.getIntString("Label.Password"),
                AppLocal.getIntString("label.passwordnew"),
                ImageLoader.readImageIcon("menu_password.png"));
        if (sPassword != null) {
            String sPassword2 = JPasswordDialog.showEditPassword(parent,
                    AppLocal.getIntString("Label.Password"),
                    AppLocal.getIntString("label.passwordrepeat"),
                    ImageLoader.readImageIcon("menu_password.png"));
            if (sPassword2 != null) {
                if (sPassword.equals(sPassword2)) {
                    return  sPassword;
                } else {
                    JMessageDialog.showMessage(parent,
                            AppLocal.tr("message.title"),
                            AppLocal.tr("message.changepassworddistinct"));
                }
            }
        }
        return null;
    }

    /**
     * Open a popup to ask to change the user's password.
     * The current password is asked, then the new password is asked twice.
     * @param parent The parent to attach the popups to.
     * @param sOldPassword The current user's password.
     * @return {New password, new password confirmation}, or null if
     * the current password is incorrect.
     */
    public static String[] changePassword(Component parent, String sOldPassword) {
        String sPassword = JPasswordDialog.showEditPassword(parent,
                AppLocal.getIntString("Label.Password"),
                AppLocal.getIntString("label.passwordold"),
                ImageLoader.readImageIcon("menu_password.png"));
        if (sPassword != null) {
            if (Hashcypher.authenticate(sPassword, sOldPassword)) {
                return new String[] {sPassword, changePassword(parent)};
            } else {
                JMessageDialog.showMessage(parent,
                        AppLocal.tr("message.title"),
                        AppLocal.tr("message.BadPassword")
                );
           }
        }
        return null;
    }
}
