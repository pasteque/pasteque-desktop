//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.util;

/** Utility class to make operation on prices.
 * Prices should be computed as follow:
 * Base price (with or without tax) is multiplied by quantity.
 * The discount is then applied.
 * Taxes must be computed only once on the final sums, either by
 * extracting the amount from the taxed result, or computing the
 * tax amount from the untaxed result.
 * All taxed prices and tax amounts are rounded to 2 decimals.
 * Untaxed prices may be rounded to 5 decimal as long as they are informative.
 * Informative meaning it is not reported directly in accounting.
 * The final untaxed price is rounded to 2 decimals. */
public class Price
{
    private Price() {}

    /** Round a value to 2 decimals. */
    private static double round2(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    private static double round5(double value) {
        return Math.round(value * 100000.0) / 100000.0;
    }

    /** Multiply a base price by quantity to get a rounded price.
     * This is the first operation to do. It gives the base on 2 decimals
     * that can be used to compute discount on. */
    public static double inQuantity(double unitPrice, double quantity) {
        return Price.round2(unitPrice * quantity);
    }

    /** Apply a discount to a price in quantity.
     * It is rounded with 2 decimals.
     * This is the last operation to do. It gives the final value on 2 decimals
     * that can be used to add or remove tax from. */
    public static double discount(double price, double discountRate) {
        return Price.round2(price * (1.0 - discountRate));
    }

    /** Get a one digit rounding increment or decrement.
     * This is used to check where a rounding precision could be reported.
     * @param original The unrounded value.
     * @return Difference between the 5 digits rounding and 2 digits rounding.
     * For example roundDiff(0.541) returns 0.001, because the exact value
     * would be 0.54100 and would be rounded to 0.54.
     */
    public static double roundDiff(double original) {
        double roundedVal = Price.round2(original);
        double exactVal = Price.round5(original);
        return Price.round5(exactVal - roundedVal);
    }

    /** Extract the final tax amount from a taxed final price.
     * It is done by computing untaxed price (on 2 decimals)  and substracting
     * it from the final taxed price (which is also on 2 decimals).
     * @param finalTaxedPrice The final taxed price. There shouldn't be any
     * other operation on it and it must be rounded on 2 decimals.
     * @param taxRate The tax rate included in the price. */
    public static double extractTax(double finalTaxedPrice, double taxRate) {
        return Price.round2(finalTaxedPrice
                - Price.untax(finalTaxedPrice, taxRate));
    }
    /** Get the final untaxed price from a final taxed price and a tax rate.
     * @param finalTaxedPrice The final taxed price. There shouldn't be any
     * other operation on it and it must be rounded on 2 decimals.
     * @param taxRate The tax rate included in the price. */
    public static double untax(double finalTaxedPrice, double taxRate) {
        return Price.round2(finalTaxedPrice / (1.0 + taxRate));
    }
    /** Get the final tax amount from a final untaxed price and a tax rate.
     * @param finalUntaxedPrice The final untaxed price. There shouldn't be any
     * other operation on it and it must be rounded on 2 decimals.
     * @param taxRate The tax rate to include in the price. */
    public static double getTax(double finalUntaxedPrice, double taxRate) {
        return Price.round2(finalUntaxedPrice * (taxRate));
    }
    /** Get the final taxed price from a final untaxed price and a tax rate.
     * It is done by computing the tax amount (on 2 decimals) and adding it
     * to the final untaxed price (which is also on 2 decimals).
     * @param finalUntaxedPrice The final untaxed price. There shouldn't be any
     * other operation on it and it must be rounded on 2 decimals.
     * @param taxRate The tax rate to include in the price. */
    public static double addTax(double finalUntaxedPrice, double taxRate) {
        return Price.round2(finalUntaxedPrice
                + Price.getTax(finalUntaxedPrice, taxRate));
    }

    /** Add a final price to an other. Both must be rounded on 2 decimals. */
    public static double add(double price1, double price2) {
        return Price.round2(price1 + price2);
    }
}
