//    POS-Tech
//
//    Copyright (C) 2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.util;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.pasteque.data.loader.ServerLoader;

public class URLTextGetter
{
    public static final String TYPE_JSON = "application/json";
    public static final String TYPE_URLENCODED = "application/x-www-form-urlencoded";

    public static final int DEFAULT_TIMEOUT = 30000;
    /** Request timeout in milliseconds. It is approximatively 1/3 for connexion
     * and 2/3 for reading. */
    private static int timeout = DEFAULT_TIMEOUT;

    public static void setTimeout(int t) {
         timeout = t;
    }

    public static JSONObject getText(final String url,
            final Map<String, String> getParams)
        throws SocketTimeoutException, IOException, ServerException {
        return getText(url, getParams, (String) null, "GET", null);
    }

    public static JSONObject getText(final String url,
            final Map<String, String> getParams,
            final Map<String, String> postParams)
        throws SocketTimeoutException, IOException, ServerException {
        if (postParams != null) {
            return URLTextGetter.getText(url, getParams, postParams, "POST");
        } else {
            return URLTextGetter.getText(url, getParams, postParams, "GET");
        }
    }

    public static JSONObject getText(final String url,
            final Map<String, String> getParams,
            final Map<String, String> postParams,
            final String method)
        throws SocketTimeoutException, IOException, ServerException {
        // Format POST
        String postBody = "";
        for (String key : postParams.keySet()) {
            postBody += URLEncoder.encode(key, "utf-8") + "="
                    + URLEncoder.encode(postParams.get(key), "utf-8")
                    + "&";
        }
        if (postBody.endsWith("&")) {
            postBody = postBody.substring(0, postBody.length() - 1);
        }
        return URLTextGetter.getText(url, getParams, postBody, method, URLTextGetter.TYPE_URLENCODED);
    }

    public static JSONObject getText(final String url,
            final Map<String, String> getParams,
            final String postBody,
            final String method, final String contentType)
        throws SocketTimeoutException, IOException, ServerException {
        // Format url
        String fullUrl = url;
        if (getParams != null && getParams.size() > 0) {
            fullUrl += "?";
            for (String param : getParams.keySet()) {
                fullUrl += URLEncoder.encode(param, "utf-8") + "="
                        + URLEncoder.encode(getParams.get(param), "utf-8") + "&";
            }
        }
        if (fullUrl.endsWith("&")) {
            fullUrl = fullUrl.substring(0, fullUrl.length() - 1);
        }
        // Init connection
        URL finalURL = new URL(fullUrl);
        HttpURLConnection conn = (HttpURLConnection) finalURL.openConnection();
        conn.setConnectTimeout(timeout / 3);
        conn.setReadTimeout((timeout / 3) * 2);
        conn.setRequestMethod(method);
        if (postBody != null && !"".equals(postBody)) {
            byte[] bytes = postBody.getBytes(java.nio.charset.StandardCharsets.UTF_8);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Length",
                    String.valueOf(bytes.length));
            conn.setRequestProperty("Content-Type", contentType);
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.write(bytes, 0, bytes.length);
            os.close ();
        }
        // GO!
        conn.connect();
        int code = conn.getResponseCode();
        String codeMsg = conn.getResponseMessage();
        String token = conn.getHeaderField("token");
        InputStream in = null;
        // Convert the response to the good ol' JSON format
        JSONObject resp = new JSONObject();
        switch (code / 100) {
        case 2:
            in = conn.getInputStream();
            resp.put("status", ServerLoader.Response.STATUS_OK);
            break;
        case 4:
            in = conn.getErrorStream(); // because Java.
            resp.put("status", ServerLoader.Response.STATUS_REJECTED);
            break;
        case 5:
        default:
            in = conn.getErrorStream();
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String content = s.hasNext() ? s.next() : "";
            conn.disconnect();
            throw new ServerException("Server not available: status " + code + ", message " + content);
        }
        // When there is no content sent, the input stream is null.
        if (ServerLoader.Response.ERR_CODE_NOT_LOGGED.equals(codeMsg)) {
            // Not logged case
            resp.put("status", ServerLoader.Response.STATUS_REJECTED);
            resp.put("content", ServerLoader.Response.ERR_CODE_NOT_LOGGED);
        } else if (in != null) {
            // Use a simple hack with Scanner to get the content of a whole InputStream...
            Scanner s = new Scanner(in).useDelimiter("\\A");
            String content = s.hasNext() ? s.next() : "";
            s.close();
            conn.disconnect();
            if (token != null && !"".equals(token)) {
                resp.put("token", token);
            }
            try {
                resp.put("content", new JSONObject(content));
            } catch (JSONException e) {
                try {
                    resp.put("content", new JSONArray(content));
                } catch (JSONException ee) {
                    resp.put("content", content);
                }
            }
        } else {
            resp.put("content", conn.getResponseMessage());
            // Do something else when there is no content
        }
        return resp;
    }

    public static class ServerException extends Exception
    {
        private static final long serialVersionUID = -4469267302877049485L;
        public ServerException(String message) {
            super(message);
        }
        public ServerException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
