//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.pos.forms.AppConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OptionsCache
{
    private static final Logger logger = Logger.getLogger("fr.pasteque.pos.caching.RolesCache");

    private static String path() {
        return AppConfig.loadedInstance.getDataDir() + "/options.cache";
    }
    private OptionsCache() {}

    public static Properties load() throws IOException {
        File cache = new File(path());
        logger.log(Level.INFO, "Reading options from " + cache.getAbsolutePath());
        if (cache.exists()) {
            FileInputStream fis = new FileInputStream(cache);
            Properties options = new Properties();
            options.load(fis);
            fis.close();
            logger.log(Level.INFO, "Read " + options.stringPropertyNames().size() + " options");
            return options;
        } else {
            return null;
        }
    }

    public static void save(Properties options) throws IOException {
        File cache = new File(path());
        logger.log(Level.INFO, "Saving " + options.stringPropertyNames().size() + " options in "
                + cache.getAbsoluteFile());
        if (!cache.exists()) {
            cache.createNewFile();
            logger.log(Level.INFO, "Created cache file "
                    + cache.getAbsoluteFile());
        }
        FileOutputStream fos = new FileOutputStream(cache);
        options.store(fos, "Options");
        fos.flush();
        fos.close();
    }
}
