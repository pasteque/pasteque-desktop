//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.ticket.CashSession;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CashSessionsCache
{
    private static PreparedStatement cashSession;

    private CashSessionsCache() {}

    private static void init() throws SQLException {
        // Get all sessions because for now there are always only one.
        // When it will be possible to cache closed session waiting for
        // server there would be more.
        cashSession = LocalDB.prepare("SELECT data FROM cashSessions "
                + "WHERE cashRegisterId = ? AND sequence = ?");
    }

    private static List<CashSession> readCashSessionResult(ResultSet rs)
        throws BasicException {
        try {
            List<CashSession> crs = new ArrayList<CashSession>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                CashSession cr = (CashSession) os.readObject();
                crs.add(cr);
            }
            return crs;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }

    /** Clear and replace cash registers. */
    public static void saveCashSession(CashSession session)
        throws BasicException {
        try {
            // Truncate and replace to keep only one
            // (until multiple is implemented).
            LocalDB.execute("TRUNCATE TABLE cashSessions");
            PreparedStatement stmt = LocalDB.prepare("INSERT INTO "
                    + "cashSessions (cashRegisterId, sequence, data) "
                    + "VALUES (?, ?, ?)");
            stmt.setInt(1, session.getCashRegisterId());
            stmt.setInt(2, session.getSequence());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(session);
            stmt.setBytes(3, bos.toByteArray());
            os.close();
            stmt.execute();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    public static CashSession getCurrentCashSession() throws BasicException {
        try {
            if (cashSession == null) {
                init();
            }
            PreparedStatement stmt = LocalDB.prepare(
                    "SELECT data FROM cashSessions "
                    + "ORDER BY sequence DESC LIMIT 1");
            ResultSet rs = stmt.executeQuery();
            List<CashSession> crs = readCashSessionResult(rs);
            if (crs.size() > 0) {
                return crs.get(0);
            }
            return null;
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static CashSession getCashSession(int cashSessionId,
            int sequence) throws BasicException {
        try {
            if (cashSession == null) {
                init();
            }
            cashSession.clearParameters();
            cashSession.setInt(1, cashSessionId);
            cashSession.setInt(2, sequence);
            ResultSet rs = cashSession.executeQuery();
            List<CashSession> crs = readCashSessionResult(rs);
            if (crs.size() > 0) {
                return crs.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
}
