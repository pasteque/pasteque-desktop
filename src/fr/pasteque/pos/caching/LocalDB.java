//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.pos.forms.AppConfig;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Local database cache. Not intended for multithreading or multiuser. */
public class LocalDB
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.LocalDB");

    private static Connection conn = null;

    private static String path() {
        return AppConfig.loadedInstance.getDataDir() + "/db_cache";
    }

    private LocalDB() {}

    private static Connection getConnection() throws SQLException {
        if (conn == null) {
            try {
                Class.forName("org.h2.Driver");
                String url = "jdbc:h2:" + path();
                conn = DriverManager.getConnection(url, "pasteque", "");
                logger.log(Level.INFO, "Opened H2 Database");
            } catch (ClassNotFoundException e) {
                // Should never happen
                e.printStackTrace();
            }
        }
        return conn;
    }

    private static void initDBv1(Connection c) throws SQLException {
        Statement stmt = c.createStatement();
        stmt.execute("CREATE TABLE meta (version INTEGER)");
        stmt.execute("INSERT INTO meta (version) VALUES (1)");
        stmt.execute("CREATE TABLE cashRegisters ("
                + "id INTEGER(255), data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE cashSessions ("
                + "cashRegisterId INTEGER(255), sequence INTEGER(255), "
                + "data BINARY(500000), "
                + "PRIMARY KEY (cashRegisterId, sequence))");
        stmt.execute("CREATE TABLE products ("
                + "id VARCHAR(255), ref VARCHAR(255), label VARCHAR(255), "
                + "barcode VARCHAR(255), categoryId VARCHAR(255), "
                + "dispOrder INTEGER(255), data BINARY(5000000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE categories ("
                + "id VARCHAR(255), label VARCHAR(255), "
                + "parentId VARCHAR(255), "
                + "dispOrder INTEGER(255), data BINARY(5000000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE taxCats ("
                + "id VARCHAR(255), data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE taxes ("
                + "id VARCHAR(255), taxCatId VARCHAR(255), "
                + "data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE currencies ("
                + "id INTEGER(255), main BOOLEAN, data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE customers ("
                + "id VARCHAR(255), number INTEGER(255), key VARCHAR(255), "
                + "name VARCHAR(255), card VARCHAR(255), "
                + "data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE customerRanking ("
                + "id VARCHAR(255), rank INTEGER(255), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE tariffAreas ("
                + "id INTEGER(255), data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE tariffAreaPrices ("
                + "areaId INTEGER(255), prdId VARCHAR(255), price DOUBLE, "
                + "PRIMARY KEY (areaId, prdId))");
        stmt.execute("CREATE TABLE subgroups ("
                + "id INTEGER(255), compositionId VARCHAR(255), "
                + "dispOrder INTEGER(255), data BINARY (500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE subgroupProds ("
                + "groupId INTEGER(255), prdId VARCHAR(255), "
                + "dispOrder INTEGER(255), "
                + "PRIMARY KEY (groupId, prdId))");
        stmt.execute("CREATE TABLE sharedTickets ("
                + "id VARCHAR(255), data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE sharedTicketQueue ("
                + "id VARCHAR(255), operation INTEGER(255), "
                + "data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE ticketQueue ("
                + "ticketId VARCHAR(255), data BINARY(500000), "
                + "PRIMARY KEY (ticketId))");
        stmt.close();
    }

    private static void initDBv2(Connection c) throws SQLException {
        Statement stmt = c.createStatement();
        stmt.execute("CREATE TABLE floors ("
                + "id VARCHAR(255), data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("CREATE TABLE places ("
                + "id VARCHAR(255), floorId VARCHAR(255), "
                + "data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("UPDATE meta SET version = 2");
    }

    private static void initDBv3(Connection c) throws SQLException {
        Statement stmt = c.createStatement();
        stmt.execute("CREATE TABLE cashMovements ("
                + "cashRegisterId INTEGER(255), sequence INTEGER(255), "
                + "date BIGINT, data BINARY(500000), "
                + "PRIMARY KEY (cashRegisterId, sequence, date))");
        stmt.execute("UPDATE meta SET version = 3");
    }

    private static void initDBv4(Connection c) throws SQLException {
        Statement stmt = c.createStatement();
        stmt.execute("CREATE TABLE paymentModes ("
                + "id INTEGER(255), dispOrder INTEGER(255), "
                + "data BINARY(500000), "
                + "PRIMARY KEY (id))");
        stmt.execute("UPDATE meta SET version = 4");
    }

    private static void initDBv5(Connection c) throws SQLException {
        Statement stmt = c.createStatement();
        stmt.execute("DROP TABLE meta");
        stmt.execute("CREATE TABLE meta ("
                + "key VARCHAR(255), value VARCHAR(255), "
                + "PRIMARY KEY (key))");
        stmt.execute("INSERT INTO meta (key, value) VALUES "
                + "('dbVersion', '5'), "
                + "('syncDate', NULL)");
    }

    private static void initDBv6(Connection c) throws SQLException {
        Statement stmt = c.createStatement();
        stmt.execute("DROP TABLE tariffAreaPrices");
        stmt.execute("CREATE TABLE tariffAreaPrices ("
                + "areaId INTEGER(255), prdId VARCHAR(255), price DOUBLE, "
                + "taxId INTEGER(255), "
                + "PRIMARY KEY (areaId, prdId))");
        stmt.execute("UPDATE meta SET value = '6' WHERE key = 'dbVersion'");
        stmt.execute("UPDATE meta SET value = NULL WHERE key = 'syncDate'");
    }

    private static boolean install(int fromVersion, Connection c)
        throws SQLException {
        switch (fromVersion) {
        case 0: LocalDB.initDBv1(c);
        case 1: LocalDB.initDBv2(c);
        case 2: LocalDB.initDBv3(c);
        case 3: LocalDB.initDBv4(c);
        case 4: LocalDB.initDBv5(c);
        case 5: LocalDB.initDBv6(c);
            return true;
        }
        return false;
    }

    public static void init() throws SQLException {
        Connection c = getConnection();
        Statement stmt = c.createStatement();
        int version = 0;
        try {
            ResultSet rs = stmt.executeQuery("SELECT value FROM meta WHERE key = 'dbVersion'");
            if (rs.next()) {
                version = Integer.parseInt(rs.getString("value"));
            }
        } catch (SQLException e) {
            // Pre-5 local db version
            try {
                ResultSet rs = stmt.executeQuery("SELECT version FROM meta");
                if (rs.next()) {
                    version = rs.getInt("version");
                }
            } catch (SQLException ee) {
                // Database not found
            }
        }
        try {
            if (LocalDB.install(version, c)) {
                logger.log(Level.INFO, "Initialized database from version " + version);
            }
        } catch (SQLException e) {
            // The local DB may be screwed up. Delete it and retry
            try {
                logger.log(Level.WARNING, "Probably corrupted local cache" ,e);
                LocalDB.close();
                if (!new File(path() + ".h2.db").delete()) {
                    logger.log(Level.WARNING, "Couldn't delete local cache");
                }
                c = LocalDB.getConnection();
                // SQLException thrown on script error
                LocalDB.install(version, c);
            } catch (SecurityException ee) {
                // Too bad, we cannot delete the file
                logger.log(Level.SEVERE, "Unable to delete local cache db", ee);
            }
        }
    }

    public static void close() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                // Can't we get anything from it...
                e.printStackTrace();
            }
            conn = null;
        }
    }

    public static PreparedStatement prepare(String sql) throws SQLException {
        Connection c = getConnection();
        return c.prepareStatement(sql);
    }

    public static void execute(String sql) throws SQLException {
        Connection c = getConnection();
        Statement stmt = c.createStatement();
        stmt.execute(sql);
    }

    public static Date getSyncDate() {
        try {
            Connection c = getConnection();
            Statement stmt = c.createStatement();
            DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
            ResultSet rs = stmt.executeQuery("SELECT value FROM meta WHERE key = 'syncDate'");
            if (rs.next()) {
                String strDate = rs.getString("value");
                if (strDate == null) {
                    return null;
                }
                try {
                    return df.parse(strDate);
                } catch (ParseException e) {
                    return null;
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setSyncDate(Date syncDate) {
        try {
            Connection c = getConnection();
            PreparedStatement stmt = c.prepareStatement("UPDATE meta SET value = ? WHERE key = 'syncDate'");
            if (syncDate == null) {
                stmt.setNull(1, java.sql.Types.VARCHAR);
            } else {
                DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
                stmt.setString(1, df.format(syncDate));
            }
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
