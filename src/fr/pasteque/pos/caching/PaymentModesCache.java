//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ImageUtils;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.payment.PaymentMode;
import fr.pasteque.pos.payment.PaymentModeValue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentModesCache
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.PaymentModesCache");
    private static final String PM_IMG_DIR = "pmImgs";

    private static PreparedStatement paymentModes;
    private static PreparedStatement paymentMode;

    private PaymentModesCache() {}

    private static void init() throws SQLException {
        paymentModes = LocalDB.prepare("SELECT data FROM paymentModes order by dispOrder asc");
        paymentMode = LocalDB.prepare("SELECT data FROM paymentModes "
                + "WHERE id = ?");
    }

    private static List<PaymentMode> readPaymentModesResult(ResultSet rs)
        throws BasicException {
        try {
            List<PaymentMode> pms = new ArrayList<PaymentMode>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                PaymentMode pm = (PaymentMode) os.readObject();
                // Check for image
                try {
                    String path = AppConfig.loadedInstance.getDataDir()
                            + "/" + PM_IMG_DIR + "/" + pm.getId();
                    File f = new File(path);
                    if (f.exists()) {
                        FileInputStream fis = new FileInputStream(f);
                        ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
                                byte[] buff = new byte[2048];
                        int read = fis.read(buff);
                        while (read != -1) {
                            bos.write(buff, 0, read);
                            read = fis.read(buff);
                        }
                        fis.close();
                        byte[] imgData = bos.toByteArray();
                        pm.setImage(ImageUtils.readImage(imgData));
                    }
                } catch (IOException e) {
                    // Ignore image and continue
                    logger.log(Level.WARNING,
                            "Unable to read payment mode image for " + pm.getId(),
                            e);
                }
                // Check for value images
                for (PaymentModeValue v : pm.getValues()) {
                    try {
                        String path = AppConfig.loadedInstance.getDataDir()
                                + "/" + PM_IMG_DIR + "/" + getPaymentModeValueId(pm, v);
                        File f = new File(path);
                        if (f.exists()) {
                            FileInputStream fis = new FileInputStream(f);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
                                    byte[] buff = new byte[2048];
                            int read = fis.read(buff);
                            while (read != -1) {
                                bos.write(buff, 0, read);
                                read = fis.read(buff);
                            }
                            fis.close();
                            byte[] imgData = bos.toByteArray();
                            v.setImage(ImageUtils.readImage(imgData));
                        }
                    } catch (IOException e) {
                        // Ignore image and continue
                        logger.log(Level.WARNING,
                                "Unable to read payment mode value image for "
                                + pm.getId() + "-" + v.getValue(), e);
                    }
                }
                pms.add(pm);
            }
            return pms;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }

    /** Clear and replace payment modes. */
    public static void refreshPaymentModes(List<PaymentMode> paymentModes)
        throws BasicException {
        try {
            LocalDB.execute("TRUNCATE TABLE paymentModes");
            PreparedStatement stmt = LocalDB.prepare("INSERT INTO paymentModes "
                    + "(id, dispOrder, data) VALUES (?, ?, ?)");
            for (PaymentMode pm : paymentModes) {
                stmt.setInt(1, pm.getId());
                stmt.setInt(2, pm.getDispOrder());
                ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(pm);
                stmt.setBytes(3, bos.toByteArray());
                os.close();
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    public static PaymentMode getPaymentMode(int pmId) throws BasicException {
        try {
            if (paymentMode == null) {
                init();
            }
            paymentMode.clearParameters();
            paymentMode.setInt(1, pmId);
            ResultSet rs = paymentMode.executeQuery();
            List<PaymentMode> pms = readPaymentModesResult(rs);
            if (pms.size() > 0) {
                return pms.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static List<PaymentMode> getPaymentModes() throws BasicException {
        try {
            if (paymentModes == null) {
                init();
            }
            ResultSet rs = paymentModes.executeQuery();
            List<PaymentMode> pms = readPaymentModesResult(rs);
            return pms;
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static void storePaymentModeImage(int pmId, byte[] image) throws BasicException {
        try {
            String path = AppConfig.loadedInstance.getDataDir()
                    + "/" + PM_IMG_DIR;
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File img = new File(dir, String.valueOf(pmId));
            if (img.exists()) {
                img.delete();
            }
            if (image == null) {
                return;
            }
            img.createNewFile();
            FileOutputStream fos = new FileOutputStream(img);
            fos.write(image);
            fos.close();
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    public static String getPaymentModeValueId(PaymentMode p, PaymentModeValue v) {
        String valId = String.valueOf(v.getValue());
        while (valId.endsWith("0")) {
            valId = valId.substring(0, valId.length() - 1);
        }
        if (valId.endsWith(".")) {
            valId = valId.substring(0, valId.length() - 1);
        }
        return String.valueOf(p.getId()) + "-" + valId;
    }

    public static void storePaymentModeValueImage(PaymentMode pm, PaymentModeValue v, byte[] image) throws BasicException {
        storePaymentModeValueImage(getPaymentModeValueId(pm, v), image);
    }

    public static void storePaymentModeValueImage(String pmvId, byte[] image) throws BasicException {
        try {
            String path = AppConfig.loadedInstance.getDataDir()
                    + "/" + PM_IMG_DIR;
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File img = new File(dir, pmvId);
            if (img.exists()) {
                img.delete();
            }
            if (image == null) {
                return;
            }
            img.createNewFile();
            FileOutputStream fos = new FileOutputStream(img);
            fos.write(image);
            fos.close();
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

}
