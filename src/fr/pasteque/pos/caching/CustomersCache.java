//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.customers.CustomerInfoExt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomersCache
{
    private static PreparedStatement customers;
    private static PreparedStatement customer;
    private static PreparedStatement customerCard;
    private static PreparedStatement ranking;
    private static PreparedStatement search;
    private static PreparedStatement update;
    private static PreparedStatement insert;

    private CustomersCache() {}

    private static void init() throws SQLException {
        customers = LocalDB.prepare("SELECT data FROM customers");
        customer = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE id = ?");
        customerCard = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE card = ?");
        ranking = LocalDB.prepare("SELECT data FROM customers, customerRanking "
                + "WHERE customers.id = customerRanking.id "
                + "ORDER BY rank ASC");
        search = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE LOWER(card) LIKE LOWER(?) "
                + "OR LOWER(name) LIKE LOWER(?)");
        update = LocalDB.prepare("UPDATE customers SET data = ? WHERE id = ?");
        insert = LocalDB.prepare("INSERT INTO customers "
                + "(id, number, key, name, card, data) VALUES "
                + "(?, ?, ?, ?, ?, ?)");
    }

    private static List<CustomerInfoExt> readCustomerResult(ResultSet rs)
        throws BasicException {
        try {
            List<CustomerInfoExt> custs = new ArrayList<CustomerInfoExt>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                CustomerInfoExt cust = (CustomerInfoExt) os.readObject();
                custs.add(cust);
            }
            return custs;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }

    public static void insertCustomer(CustomerInfoExt cust)
        throws BasicException {
        try {
            if (insert == null) {
                init();
            }
            // Local search fix: update all NULL values to ""
            // as the searcher doesn't allow to reach NULL values
            // (and we don't care about)
            String taxId = cust.getTaxid();
            String searchKey = cust.getSearchkey();
            String name = cust.getName();
            if (taxId == null) { taxId = "0"; }
            if (searchKey == null) { searchKey = ""; }
            if (name == null) { name = ""; }
            insert.clearParameters();
            insert.setString(1, cust.getId());
            insert.setString(2, taxId);
            insert.setString(3, searchKey);
            insert.setString(4, name);
            insert.setString(5, cust.getCard());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(cust);
            insert.setBytes(6, bos.toByteArray());
            os.close();
            insert.execute();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    /** Clear and replace customers. */
    public static void refreshCustomers(List<CustomerInfoExt> customers)
        throws BasicException {
        try {
            LocalDB.execute("TRUNCATE TABLE customers");
            for (CustomerInfoExt cust : customers) {
                insertCustomer(cust);
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
    /** Update a customer */
    public static void refreshCustomer(CustomerInfoExt cust)
        throws BasicException {
        try {
            if (update == null) {
                init();
            }
            update.clearParameters();
            update.setString(2, cust.getId());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(cust);
            update.setBytes(1, bos.toByteArray());
            os.close();
            update.execute();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }
    /** Clear and replace customer ranking. */
    public static void refreshRanking(List<String> ids)
        throws BasicException {
        try {
            LocalDB.execute("TRUNCATE TABLE customerRanking");
            PreparedStatement stmt = LocalDB.prepare("INSERT "
                    + "INTO customerRanking "
                    + "(id, rank) VALUES (?, ?)");
            for (int i = 0; i < ids.size(); i++) {
                stmt.setString(1, ids.get(i));
                stmt.setInt(2, i);
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    /** Get cached data if any, null otherwise */
    public static List<CustomerInfoExt> getCustomers() throws BasicException {
        try {
            if (customers == null) {
                init();
            }
            ResultSet rs = customers.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static CustomerInfoExt getCustomer(String id) throws BasicException {
        try {
            if (customer == null) {
                init();
            }
            customer.clearParameters();
            customer.setString(1, id);
            ResultSet rs = customer.executeQuery();
            List<CustomerInfoExt> custs = readCustomerResult(rs);
            if (custs.size() > 0) {
                return custs.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static CustomerInfoExt getCustomerByCard(String card)
        throws BasicException {
        try {
            if (customerCard == null) {
                init();
            }
            customerCard.clearParameters();
            customerCard.setString(1, card);
            ResultSet rs = customerCard.executeQuery();
            List<CustomerInfoExt> custs = readCustomerResult(rs);
            if (custs.size() > 0) {
                return custs.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    /** Get cached data if any, null otherwise */
    public static List<CustomerInfoExt> getTopCustomers()
        throws BasicException {
        try {
            if (ranking == null) {
                init();
            }
            ResultSet rs = ranking.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static List<CustomerInfoExt> searchCustomers(String searchString)
            throws BasicException {
        if (searchString == null) {
            searchString = "%%";
        } else {
            searchString = "%" + searchString + "%";
        }
        try {
            if (search == null) {
                init();
            }
            search.clearParameters();
            search.setString(1, searchString);
            search.setString(2, searchString);
            ResultSet rs = search.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
}
