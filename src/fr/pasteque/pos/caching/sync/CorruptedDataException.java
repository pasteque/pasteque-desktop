//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching.sync;

/** Exception raised when the API rejected a call
 * or when the content is not what is expected. */
public class CorruptedDataException extends java.lang.Exception
{
    private static final long serialVersionUID = 7172290814963576181L;

    private String info;
    private Exception cause;

    public CorruptedDataException(String info, Exception cause) {
        this.info = info;
        this.cause = cause;
    }

    public String getInfo() {
        return this.info;
    }

    public Exception getCause() {
        return this.cause;
    }
}
