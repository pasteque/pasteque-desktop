/*
 Pasteque Android client
 Copyright (C) Pasteque contributors, see the COPYRIGHT file

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.pasteque.pos.caching.sync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.caching.CallQueue;
import fr.pasteque.pos.caching.LocalDB;
import fr.pasteque.pos.caching.CashRegistersCache;
import fr.pasteque.pos.caching.CatalogCache;
import fr.pasteque.pos.caching.CustomersCache;
import fr.pasteque.pos.caching.CurrenciesCache;
import fr.pasteque.pos.caching.DiscountProfilesCache;
import fr.pasteque.pos.caching.FloorsCache;
import fr.pasteque.pos.caching.OptionsCache;
import fr.pasteque.pos.caching.PaymentModesCache;
import fr.pasteque.pos.caching.TariffAreasCache;
import fr.pasteque.pos.caching.TaxesCache;
import fr.pasteque.pos.caching.ResourcesCache;
import fr.pasteque.pos.caching.RolesCache;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.caching.UsersCache;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppUser;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.payment.PaymentMode;
import fr.pasteque.pos.payment.PaymentModeValue;
import fr.pasteque.pos.sales.restaurant.Floor;
import fr.pasteque.pos.sales.restaurant.Place;
import fr.pasteque.pos.ticket.CashRegisterInfo;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TariffAreaPrice;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.util.Base64Encoder;
import fr.pasteque.pos.util.ThumbNailBuilder;

public class SyncUpdate
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.SyncUpdate");
    private ExecutorService executor;

    public static final int STEP_UNKNOWN = 0;
    /** Get the API level and check it against the one from the client. */
    public static final int STEP_VERSION = 1;
    /** Request the sync data and parse the JSON data without reading them. */
    public static final int STEP_GET_SYNC = 2;
    /** Parse the cash register from the sync data. */
    public static final int STEP_CASH_REGISTER = 3;
    /** Parse the cash session from the sync data. */
    public static final int STEP_CASH_SESSION = 4;
    public static final int STEP_USERS = 5;
    public static final int STEP_ROLES = 6;
    public static final int STEP_CURRENCIES = 7;
    public static final int STEP_PAYMENT_MODES = 8;
    public static final int STEP_TARIFF_AREAS = 9;
    public static final int STEP_TAXES = 10;
    public static final int STEP_CATEGORIES = 11;
    public static final int STEP_PRODUCTS = 12;
    public static final int STEP_PLACES = 13;
    public static final int STEP_CUSTOMERS = 14;
    public static final int STEP_DISCOUNT_PROFILES = 15;
    public static final int STEP_RESOURCES = 16;
    public static final int STEP_IMAGE = 17;
    public static final int STEP_OPTIONS = 18;
    /** The total number of steps, excluding images */
    public static final int STEP_COUNT = 18;

    public static final int PHASE_DATA = 0;
    public static final int PHASE_IMAGES = 1;

    public static boolean isSyncDataPhase(int step) {
         return step >= STEP_GET_SYNC && step <= STEP_RESOURCES;
    }

    private Listener listener;
    private DataLogicSystem m_dlSystem;
    private String crName;
    /** The list of product ids that have an image. */
    private List<Integer> prdImgToLoad;
    /** The list of category ids that have an image. */
    private List<Integer> catImgToLoad;
    /** The list of payment mode ids that have an image. */
    private List<Integer> pmImgToLoad;
    /** The list of payment mode values that have an image. */
    private List<String> pmvImgToLoad;
    private List<Integer> userImgToLoad;

    public SyncUpdate(Listener listener, String cashRegisterName) {
        this.listener = listener;
        this.executor = Executors.newFixedThreadPool(12);
        m_dlSystem = new DataLogicSystem();
        this.crName = cashRegisterName;
        this.prdImgToLoad = new ArrayList<Integer>();
        this.catImgToLoad = new ArrayList<Integer>();
        this.pmImgToLoad = new ArrayList<Integer>();
        this.pmvImgToLoad = new ArrayList<String>();
        this.userImgToLoad = new ArrayList<Integer>();
    }

    private void notifyProgress(int step, Object data) {
        if (this.listener != null) {
            this.listener.onSyncProgress(step, data);
        }
    }

    private void notifyError(int step, Exception e) {
        if (this.listener != null) {
            this.listener.onSyncError(step, e);
        } else {
            Logger.getLogger(SyncUpdate.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void notifyFinished(int phase, int nextCount) {
        if (this.listener != null) {
            this.listener.onSyncFinished(phase, nextCount);
        }
    }

    /** Retreive data from server, excluding images. */
    public void synchronizeData() {
        // Check API level
        Future<Integer> apiLevelCall = executor.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return m_dlSystem.getAPILevel();
            }
        });
        Integer apiLevel = null;
        try {
            apiLevel = apiLevelCall.get();
        } catch (InterruptedException ex) {
            this.notifyError(SyncUpdate.STEP_VERSION, ex);
            return;
        } catch (ExecutionException ex) {
            this.notifyError(SyncUpdate.STEP_VERSION, new UnavailableAPIException(ex));
            return;
        }
        if (apiLevel == null) {
            this.notifyError(SyncUpdate.STEP_VERSION, new NotAnAPIException());
            return;
        } else if (AppLocal.API_LEVEL != apiLevel) {
            this.notifyError(SyncUpdate.STEP_VERSION, new IncompatibleVersionException(apiLevel));
            return;
        }
        this.notifyProgress(SyncUpdate.STEP_VERSION, apiLevel);

        // Reset image loading status
        this.prdImgToLoad.clear();
        this.catImgToLoad.clear();
        this.pmImgToLoad.clear();
        this.pmvImgToLoad.clear();
        this.userImgToLoad.clear();

        // Load data from server
        ServerLoader server = new ServerLoader();
        CashRegisterInfo cashRegister = null;
        try {
            ServerLoader.Response response = server.read("api/sync/"
                    + URLEncoder.encode(this.crName, "UTF-8"));
            if (!ServerLoader.Response.STATUS_OK.equals(response.getStatus())) {
                if (ServerLoader.Response.STATUS_REJECTED.equals(response.getStatus())) {
                    JSONObject r = response.getObjContent();
                    if ("RecordNotFound".equals(r.getString("error"))
                            && "Pasteque\\Server\\Model\\CashRegister".equals(r.getString("class"))) {
                        this.notifyError(SyncUpdate.STEP_CASH_REGISTER,
                                new InvalidCashRegisterException());
                        return;
                    }
                }
                this.notifyError(SyncUpdate.STEP_GET_SYNC, new UnexpectedResponseException(response));
                return;
            }
            JSONObject jsResp = response.getObjContent();
            if (jsResp == null) {
                this.notifyError(SyncUpdate.STEP_GET_SYNC, new UnexpectedResponseException(response));
                return;
            }
            this.notifyProgress(SyncUpdate.STEP_GET_SYNC, jsResp);

            // Cash register data
            try {
                cashRegister = new CashRegisterInfo(jsResp.getJSONObject("cashRegister"));
            } catch (JSONException jse) {
                this.notifyError(SyncUpdate.STEP_CASH_REGISTER,
                        new CorruptedDataException("Unable to decode cash register", jse));
                return;
            } catch (NullPointerException nulle) {
                this.notifyError(SyncUpdate.STEP_CASH_REGISTER,
                        new CorruptedDataException("NullPointerException while getting cash register", nulle));
                return;
            }
            // Check from CallQueue if nextTicketId should be incremented
            int pendingTicketCount = CallQueue.getPendingTicketCount();
            for (int i = 0; i < pendingTicketCount; i++) {
                cashRegister.incrementNextTicketId();
            }
            // Cache cashregister
            List<CashRegisterInfo> crs = new ArrayList<CashRegisterInfo>();
            crs.add(cashRegister);
            CashRegistersCache.refreshCashRegisters(crs);
            this.notifyProgress(SyncUpdate.STEP_CASH_REGISTER, cashRegister);

            // Cash session data
            CashSession cashSess = null;
            try {
                if (!jsResp.isNull("cash")) {
                    cashSess = new CashSession(jsResp.getJSONObject("cash"));
                }
            } catch (JSONException jse) {
                this.notifyError(SyncUpdate.STEP_CASH_SESSION,
                        new CorruptedDataException("Unable to decode cash session.", jse));
                return;
            }
            if (cashSess == null) {
                this.notifyError(SyncUpdate.STEP_CASH_SESSION,
                        new CorruptedDataException("Unable to get cash session. Cash session is null", new NullPointerException()));
                return;
            }
            this.notifyProgress(SyncUpdate.STEP_CASH_SESSION, cashSess);

            // Users
            List<AppUser> users = new LinkedList<AppUser>();
            try {
                JSONArray jsUsers = jsResp.getJSONArray("users");
                for (int i = 0; i < jsUsers.length(); i++) {
                    JSONObject jsU = jsUsers.getJSONObject(i);
                    String password = null;
                    String card = null;
                    if (!jsU.isNull("password")) {
                        password = jsU.getString("password");
                    }
                    if (!jsU.isNull("card")) {
                        card = jsU.getString("card");
                    }
                    final ThumbNailBuilder tnb = new ThumbNailBuilder(32, 32,
                            "default_user.png");
                    ImageIcon icon;
                    icon = new ImageIcon(tnb.getThumbNail(null));
                    AppUser u = new AppUser(String.valueOf(jsU.getInt("id")),
                            jsU.getString("name"), password, card,
                            String.valueOf(jsU.getInt("role")), icon,
                            jsU.getBoolean("active"));
                    users.add(u);
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_USERS,
                        new CorruptedDataException("Unable to parse users.", ee));
                return;
            }
            try {
                UsersCache.save(users);
            } catch (IOException ee) {
                logger.log(Level.SEVERE, "Error while caching customers " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_USERS, users);

            // Roles
            Map<String, String> roles = new HashMap<String, String>();
            try {
                JSONArray jsRoles = jsResp.getJSONArray("roles");
            for (int i = 0; i < jsRoles.length(); i++) {
                JSONObject o = jsRoles.getJSONObject(i);
                String permissions = "";
                JSONArray jsPerms = o.getJSONArray("permissions");
                for (int ri = 0; ri < jsPerms.length(); ri++) {
                    if (jsPerms.getString(ri).length() > 0) {
                        permissions += ";" + jsPerms.getString(ri);
                    }
                }
                if (permissions.length() > 0) {
                    permissions = permissions.substring(1);
                }
                roles.put(String.valueOf(o.getInt("id")), permissions);
            }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_ROLES,
                        new CorruptedDataException("Unable to parse roles.", ee));
                return;
            }
            try {
                RolesCache.save(roles);
            } catch (IOException ee) {
                logger.log(Level.SEVERE, "Error while caching roles " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_ROLES, roles);

            // Currencies
            List<CurrencyInfo> currencies = new ArrayList<CurrencyInfo>();
            try {
                JSONArray jsCurrencies = jsResp.getJSONArray("currencies");
                for (int i = 0; i < jsCurrencies.length(); i++) {
                    JSONObject o = jsCurrencies.getJSONObject(i);
                    CurrencyInfo currency = new CurrencyInfo(o);
                    currencies.add(currency);
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_CURRENCIES,
                        new CorruptedDataException("Unable to parse currencies.", ee));
                return;
            }
            try {
                CurrenciesCache.refreshCurrencies(currencies);
            } catch (BasicException ee) {
                logger.log(Level.SEVERE, "Error while caching currencies " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_CURRENCIES, currencies);

            // Payment modes
            List<PaymentMode> paymentModes = new ArrayList<PaymentMode>();
            try {
                JSONArray jsPaymentModes = jsResp.getJSONArray("paymentmodes");
                for (int i = 0; i < jsPaymentModes.length(); i++) {
                    JSONObject o = jsPaymentModes.getJSONObject(i);
                    PaymentMode pm = new PaymentMode(o);
                    paymentModes.add(pm);
                    if (o.getBoolean("hasImage")) {
                        this.pmImgToLoad.add(pm.getId());
                    } else {
                        PaymentModesCache.storePaymentModeImage(pm.getId(), null);
                    }
                    for (PaymentModeValue v : pm.getValues()) {
                        if (v.hasImage()) {
                            this.pmvImgToLoad.add(PaymentModesCache.getPaymentModeValueId(pm, v));
                        } else {
                            PaymentModesCache.storePaymentModeValueImage(pm, v, null);
                        }
                    }
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_PAYMENT_MODES,
                        new CorruptedDataException("Unable to parse payment modes.", ee));
                return;
            }
            try {
                PaymentModesCache.refreshPaymentModes(paymentModes);
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Error while caching payment modes " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_PAYMENT_MODES, paymentModes);

            // Tariff areas
            Map<Integer, List<TariffAreaPrice>> prices = new HashMap<Integer, List<TariffAreaPrice>>();
            List<TariffInfo> areas = new ArrayList<TariffInfo>();
            try {
                JSONArray a = jsResp.getJSONArray("tariffareas");
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    TariffInfo area = new TariffInfo(o);
                    areas.add(area);
                    List<TariffAreaPrice> areaPrices = new ArrayList<TariffAreaPrice>();
                    JSONArray aPrices = o.getJSONArray("prices");
                    for (int j = 0; j < aPrices.length(); j++) {
                        JSONObject oPrice = aPrices.getJSONObject(j);
                        TariffAreaPrice p = new TariffAreaPrice(oPrice);
                        areaPrices.add(p);
                    }
                    prices.put(area.getID(), areaPrices);
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_TARIFF_AREAS,
                        new CorruptedDataException("Unable to parse tariff areas.", ee));
                return;
            }
            try {
                TariffAreasCache.refreshTariffAreas(areas);
                TariffAreasCache.refreshPrices(prices);
            } catch (BasicException ee) {
                logger.log(Level.SEVERE, "Error while caching tariff areas " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_TARIFF_AREAS, areas);

            // Taxes
            List<TaxInfo> taxes = new ArrayList<TaxInfo>();
            try {
                JSONArray jsTaxes = jsResp.getJSONArray("taxes");
                for (int i = 0; i < jsTaxes.length(); i++) {
                    JSONObject o = jsTaxes.getJSONObject(i);
                    TaxInfo tax = new TaxInfo(o);
                    taxes.add(tax);
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_TAXES,
                        new CorruptedDataException("Unable to parse taxes.", ee));
                return;
            }
            try {
                TaxesCache.refreshTaxes(taxes);
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Error while caching taxes " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_TAXES, taxes);

            // Categories
            List<CategoryInfo> categories = new ArrayList<CategoryInfo>();
            try {
                JSONArray jsCategories = jsResp.getJSONArray("categories");
                for (int i = 0; i < jsCategories.length(); i++) {
                    JSONObject o = jsCategories.getJSONObject(i);
                    CategoryInfo cat = new CategoryInfo(o);
                    categories.add(cat);
                    if (o.getBoolean("hasImage")) {
                        this.catImgToLoad.add(o.getInt("id"));
                    } else {
                        CatalogCache.storeCategoryImage(cat.getID(), null);
                    }
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_CATEGORIES,
                        new CorruptedDataException("Unable to parse categories.", ee));
                return;
            }
            try {
                CatalogCache.refreshCategories(categories);
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Error while caching categories " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_CATEGORIES, categories);

            // Products and compositions
            List<ProductInfoExt> products = new ArrayList<ProductInfoExt>();
            Map<String, List<SubgroupInfo>> compos = new HashMap<String, List<SubgroupInfo>>();
            Map<Integer, List<String>> groups = new HashMap<Integer, List<String>>();
            try {
                JSONArray jsProducts = jsResp.getJSONArray("products");
                for (int i = 0; i < jsProducts.length(); i++) {
                    JSONObject o = jsProducts.getJSONObject(i);
                    if (!o.getBoolean("visible")) {
                        // Don't add products not sold
                        continue;
                    }
                    ProductInfoExt prd = new ProductInfoExt(o);
                    if (o.getBoolean("composition")) {
                        prd.setCompo(true);
                        compos.put(prd.getID(), new ArrayList<SubgroupInfo>());
                        JSONArray grps = o.getJSONArray("compositionGroups");
                        for (int j = 0; j < grps.length(); j++) {
                            JSONObject oGrp = grps.getJSONObject(j);
                            SubgroupInfo subgrp = new SubgroupInfo(oGrp);
                            compos.get(prd.getID()).add(subgrp);
                            groups.put(subgrp.getID(), new ArrayList<String>());
                            JSONArray choices = oGrp.getJSONArray("compositionProducts");
                            for (int k = 0; k < choices.length(); k++) {
                                JSONObject oPrd = choices.getJSONObject(k);
                                groups.get(subgrp.getID()).add(String.valueOf(oPrd.getInt("product")));
                            }
                        }
                    }
                    products.add(prd);
                    try {
                        if (o.getBoolean("hasImage")) {
                            this.prdImgToLoad.add(o.getInt("id"));
                        } else {
                            CatalogCache.storeProductImage(prd.getID(), null);
                        }
                    } catch (Exception eee) {
                        logger.log(Level.WARNING,
                                "Error on product image data "
                                + prd.getID(), eee.getMessage());
                    }
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_PRODUCTS,
                        new CorruptedDataException("Unable to parse products.", ee));
                return;
            }
            try {
                CatalogCache.refreshProducts(products);
                CatalogCache.refreshSubgroups(compos);
                CatalogCache.refreshSubgroupProds(groups);
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Error while caching products " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_PRODUCTS, products);

            // Places
            List<Floor> floors = new ArrayList<Floor>();
            List<Place> places = new ArrayList<Place>();
            try {
                JSONArray jsFloors = jsResp.getJSONArray("floors");
                for (int i = 0; i < jsFloors.length(); i++) {
                    JSONObject o = jsFloors.getJSONObject(i);
                    Floor f = new Floor(o);
                    floors.add(f);
                    JSONArray jsPlaces = o.getJSONArray("places");
                    for (int j = 0; j < jsPlaces.length(); j++) {
                        JSONObject oP = jsPlaces.getJSONObject(j);
                        Place p = new Place(oP);
                        places.add(p);
                    }
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_PLACES,
                        new CorruptedDataException("Unable to parse restaurant map.", ee));
                return;
            }
            try {
                FloorsCache.refreshFloors(floors, places);
            } catch (BasicException ee) {
                this.notifyError(SyncUpdate.STEP_PLACES,
                        new LocalException("Unable to cache restaurant map.", ee));
                return;
            }
            this.notifyProgress(SyncUpdate.STEP_PLACES, floors);

            // Customers
            List<CustomerInfoExt> customers = new ArrayList<CustomerInfoExt>();
            List<String> topCustomers = new ArrayList<String>();
            try {
                JSONArray jsCust = jsResp.getJSONArray("customers");
                for (int i = 0; i < jsCust.length(); i++) {
                    JSONObject o = jsCust.getJSONObject(i);
                    CustomerInfoExt customer = new CustomerInfoExt(o);
                    customers.add(customer);
                }
                JSONArray jsTopCust = jsResp.getJSONArray("topcustomers");
                for (int i = 0; i < jsTopCust.length(); i++) {
                    topCustomers.add(String.valueOf(jsTopCust.getInt(i)));
                    }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_CUSTOMERS,
                        new CorruptedDataException("Unable to parse customers.", ee));
                return;
            }
            try {
                CustomersCache.refreshCustomers(customers);
                CustomersCache.refreshRanking(topCustomers);
            } catch (BasicException ee) {
                logger.log(Level.SEVERE, "Error while caching customers " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_CUSTOMERS, customers);

            // DiscountProfiles
            List<DiscountProfile> discProfiles = new ArrayList<DiscountProfile>();
            try {
                JSONArray jsDiscProfiles = jsResp.getJSONArray("discountprofiles");
                for (int i = 0; i < jsDiscProfiles.length(); i++) {
                    JSONObject o = jsDiscProfiles.getJSONObject(i);
                    DiscountProfile profile = new DiscountProfile(o);
                    discProfiles.add(profile);
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_DISCOUNT_PROFILES,
                        new CorruptedDataException("Unable to parse discount profiles.", ee));
                return;
            }
            try {
                DiscountProfilesCache.save(discProfiles);
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Error while caching discount profiles " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_DISCOUNT_PROFILES, discProfiles);

            // Resources
            try {
                JSONArray jsResources = jsResp.getJSONArray("resources");
                ResourcesCache.clear();
                for (int i = 0; i < jsResources.length(); i++) {
                    byte[] resource;
                    JSONObject o = jsResources.getJSONObject(i);
                    String strRes = o.getString("content");
                    String name = o.getString("label");
                    if (o.getInt("type") == 0) {
                        resource = strRes.getBytes();
                    } else {
                        resource = Base64Encoder.decode(strRes);
                    }
                    try {
                        ResourcesCache.save(name, resource);
                    } catch (IOException eee) {
                        logger.log(Level.SEVERE, "Error while caching resource " + eee.getMessage());
                        eee.printStackTrace();
                    }
                }
            } catch (JSONException ee) {
                this.notifyError(SyncUpdate.STEP_RESOURCES,
                        new CorruptedDataException("Unable to parse resources.", ee));
                return;
            }
            this.notifyProgress(SyncUpdate.STEP_RESOURCES, null);

            // Options
            Properties options = new Properties();
            try {
                JSONArray jsOptions = jsResp.getJSONArray("options");
                for (int i = 0; i < jsOptions.length(); i++) {
                    JSONObject o = jsOptions.getJSONObject(i);
                    options.setProperty(o.getString("name"), o.getString("content"));
                }
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Unable to parse options " + ee.getMessage());
                ee.printStackTrace();
            }
            try {
                OptionsCache.save(options);
            } catch (Exception ee) {
                logger.log(Level.SEVERE, "Error while caching options " + ee.getMessage());
                ee.printStackTrace();
            }
            this.notifyProgress(SyncUpdate.STEP_OPTIONS, options);

        } catch (Exception e) {
            this.notifyError(SyncUpdate.STEP_UNKNOWN, new LocalException("Unexpected error", e));
            return;
        }
        LocalDB.setSyncDate(new Date());
        this.notifyFinished(SyncUpdate.PHASE_DATA, this.getImageCount());
        return;
    }

    /**
     * Get the number of images to load.
     * @return The number of images that will be loaded with synchronizeImages().
     * 0 when synchronizeData was not called previously.
     */
    public int getImageCount() {
        return this.prdImgToLoad.size() + this.catImgToLoad.size()
                + this.pmImgToLoad.size() + this.pmvImgToLoad.size()
                + this.userImgToLoad.size();
    }

    /** Read the image data. Images to load are set from syncronizeData. */
    public void synchronizeImages() {
        ServerLoader server = new ServerLoader();
        for (Integer catId : this.catImgToLoad) {
             try {
                byte[] img = null;
                try {
                    img = server.readImage("category", String.valueOf(catId));
                } catch (Exception e) {
                    logger.log(Level.WARNING,
                            "Error on category image data "
                            + catId, e);
                }
                CatalogCache.storeCategoryImage(String.valueOf(catId), img);
            } catch (BasicException e) {
                logger.log(Level.WARNING,
                        "Error on category image data "
                        + catId, e);
            } catch (IllegalArgumentException e) {
                logger.log(Level.WARNING,
                        "Error on category image data "
                        + catId, e);
            }
            this.notifyProgress(SyncUpdate.STEP_IMAGE, null);
        }
        for (Integer prdId : this.prdImgToLoad) {
            byte[] img = null;
            try {
                img = server.readImage("product", String.valueOf(prdId));
                CatalogCache.storeProductImage(String.valueOf(prdId), img);
            } catch (Exception e) {
                logger.log(Level.WARNING,
                        "Error on product image data "
                        + prdId, e.getMessage());
            }
            this.notifyProgress(SyncUpdate.STEP_IMAGE, null);
        }
        for (Integer pmId : this.pmImgToLoad) {
            byte[] img = null;
            try {
                img = server.readImage("paymentmode", String.valueOf(pmId));
                PaymentModesCache.storePaymentModeImage(pmId, img);
            } catch (Exception e) {
                logger.log(Level.WARNING,
                        "Error on payment mode image data "
                        + pmId, e);
            }
            this.notifyProgress(SyncUpdate.STEP_IMAGE, null);
        }
        for (String pmvId : this.pmvImgToLoad) {
            byte[] img = null;
            try{
                img = server.readImage("paymentmodevalue", pmvId);
                PaymentModesCache.storePaymentModeValueImage(pmvId, img);
            } catch (Exception e) {
                logger.log(Level.WARNING,
                        "Error on payment mode value image data "
                        + pmvId, e);
            }
            this.notifyProgress(SyncUpdate.STEP_IMAGE, null);
        }
        this.notifyFinished(SyncUpdate.PHASE_IMAGES, 0);
    }

    public interface Listener {
        /** Called when the sync progresses. */
        public void onSyncProgress(int step, Object data);
        /** Called when the sync finished because of an error. */
        public void onSyncError(int step, Exception e);
        /** Called when the sync finished without any issue. */
        public void onSyncFinished(int phase, int nextCount);
    }

}
