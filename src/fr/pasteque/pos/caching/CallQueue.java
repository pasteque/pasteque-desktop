//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.util.URLTextGetter.ServerException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketTimeoutException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Message queue for disconnected usage.
 * Instead of sending the operations directly to the server, calls are queued in
 * the local cache and a background thread is run to run them.
 * If the server is not available, it retries every once in a while.
 * When there are no call left, it stops and it is restarted once a new operation
 * is registered.
 * Read/write methods on the local queue are synchronized, and there can be
 * only one running timer at a time. */
public class CallQueue
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.CallQueue");

    private static final int START_DELAY_MS = 1000;
    private static final int RETRY_DELAY_MS = 10000;
    private static final int CONTINUE_DELAY_MS = 1000;
    private static final int TICKET_BATCH_SIZE = 10;

    private static PreparedStatement addTicket;
    private static PreparedStatement getTktOps;
    private static PreparedStatement getTktBatch;
    private static PreparedStatement delTktOp;
    private static PreparedStatement countTktOps;

    private static Timer recoverTimer;
    /** Mutex to be sure to run recovery only once. */
    private static Object startMutex = new Object();
    private static boolean recoveryRunning;

    private static void init() throws SQLException {
        addTicket = LocalDB.prepare("INSERT INTO ticketQueue (ticketId , data) "
                + "VALUES (?, ?)");
        getTktOps = LocalDB.prepare("SELECT * FROM ticketQueue");
        getTktBatch = LocalDB.prepare("SELECT * FROM ticketQueue ORDER BY ticketId ASC LIMIT " + TICKET_BATCH_SIZE);
        delTktOp = LocalDB.prepare("DELETE FROM ticketQueue "
                + "WHERE ticketId = ?");
        countTktOps = LocalDB.prepare("SELECT count(ticketId) as num "
                + "FROM ticketQueue");
    }

    // Public running methods

    /** Queue a call to save a ticket. Requests to unload the queue. */
    public static void queueTicketSave(TicketInfo ticket) {
        CallQueue.insertPendingTicket(ticket);
        unloadTheQueue(START_DELAY_MS);
    }

    /** Force start if required. It does nothing if it is already started. */
    public static void start() {
        if (!CallQueue.isEmpty()) {
            CallQueue.unloadTheQueue(START_DELAY_MS);
        }
    }

    // Public status methods

    public static boolean isEmpty() {
        return CallQueue.getOperationsCount() == 0;
    }

    public static int getPendingTicketCount() {
        return CallQueue.countTickets();
    }

    public static List<TicketInfo> getPendingTickets() {
        return CallQueue.selectTickets();
    }

    public static int getOperationsCount() {
        return CallQueue.countOperations();
    }

    // Private recovery methods

    /**
     * Try to send everything from the queue until it's gone.
     * @param delay Delay in ms before starting.
     */
    private static void unloadTheQueue(int delay) {
        // Start the timer to recover
        synchronized(startMutex) {
            if (CallQueue.recoverTimer == null) {
                recoverTimer = new Timer();
                TimerTask task = new TimerTask() {
                        public void run() {
                            checkRecovery();
                        }
                };
                recoverTimer.schedule(task, delay, RETRY_DELAY_MS);
            }
        }
    }

    /** Check if the server is available and save.
     * It is called from the recovery timer and does nothing if some requests
     * are already pending.
     * It clears the recovery timer once everything is done. */
    private static void checkRecovery() {
        // If it is already running, wait for the next timer tick.
        if (recoveryRunning) {
            return;
        }
        recoveryRunning = true;
        ServerLoader loader = new ServerLoader();
        ServerLoader.Response r;
        try {
            r = loader.read("api/version");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                // Server is there!
                logger.log(Level.INFO, "Recovery started");
                if (recover()) {
                    synchronized(startMutex) {
                        if (recoverTimer != null) {
                            recoverTimer.cancel();
                        }
                        recoverTimer = null;
                    }
                    if (!CallQueue.isEmpty()) {
                        // Continue with a short delay
                        unloadTheQueue(CONTINUE_DELAY_MS);
                    } // else nothing left, recovery is complete
                }
            }
        } catch (Exception e) {
            logger.log(Level.INFO, "Recovery failed, the server is not reachable.", e);
        }
        recoveryRunning = false;
    }

    /**
     * Try to send some pending operations.
     * It is called only from the recovery timer.
     * @return True when recovery progressed.
     */
    private static boolean recover() {
        ServerLoader loader = new ServerLoader();
        // Send ticket operations
        List<TicketInfo> tickets = CallQueue.getTicketBatch();
        try {
            if (tickets.size() > 0) {
                if (CallQueue.sendTickets(loader, tickets)) {
                    logger.log(Level.INFO, "Sent " + tickets.size() + " ticket(s)");
                    return true;
                }
            } else {
                logger.log(Level.INFO, "Tried to recover with an empty queue.");
                return true;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            logger.log(Level.INFO, "Server timed out.");
        } catch (ServerException e) {
            logger.log(Level.WARNING, "Server error while saving ticket", e);
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO error while saving ticket", e);
        }
        return false;
    }

    /**
     * Send a list of tickets.
     * Called from recover() from the recovery timer.
     * @return True when something was registered server-side.
     * The local queue was updated.
     */
    private static boolean sendTickets(ServerLoader loader,
            List<TicketInfo> tickets)
        throws SocketTimeoutException, ServerException, IOException {
        ServerLoader.Response r;
        JSONArray array = new JSONArray();
        for (TicketInfo t : tickets) {
            array.put(t.toJSON());
        }
        r = loader.write("api/ticket",
                "tickets", array.toString());
        if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
            logger.log(Level.WARNING, "Ticket recovery failed, "
                    + "server error: "
                    + r.getResponse().toString());
            return false;
        } else {
            JSONObject result = r.getObjContent();
            // Check which tickets were saved to remove them from the queue
            JSONArray jsSent = result.getJSONArray("successes");
            JSONArray jsFailures = result.getJSONArray("failures");
            for (int i = 0; i < jsFailures.length(); i++) {
                // Add failures to sent because they are registered
                // on the server anyway.
                jsSent.put(jsFailures.getJSONObject(i));
                // TODO: notify the user that there were failures to check
            }
            for (int i = 0; i < jsSent.length(); i++) {
                JSONObject jsTktInfo = jsSent.getJSONObject(i);
                int cashRegisterId = jsTktInfo.getInt("cashRegister");
                int sequence = jsTktInfo.getInt("sequence");
                int number = jsTktInfo.getInt("number");
                deletePendingTicket(ticketId(cashRegisterId, sequence, number));
            }
            return jsSent.length() > 0;
        }
    }

    /** Get the local database id for a ticket. */
    private static String ticketId(TicketInfo ticket) {
        return ticketId(ticket.getCashRegisterId(), ticket.getSequence(),
                ticket.getNumber());
    }
    /** Get the local database id for a ticket. */
    private static String ticketId(int cashRegisterId,
            int sequence, int number) {
        return cashRegisterId + "-" + sequence + "-" + number;
    }

    // Local database read/write operations
    // Every method must be synchronized

    private static synchronized int countTickets() {
            try {
                if (countTktOps == null) {
                    init();
                }
                int count = 0;
                ResultSet tktrs = countTktOps.executeQuery();
                if (tktrs.next()) {
                    count += tktrs.getInt("num");
                }
                tktrs.close();
                return count;
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
    }

    private static synchronized List<TicketInfo> selectTickets() {
        try {
            ResultSet tktrs = getTktOps.executeQuery();
            List<TicketInfo> tickets = readTicketResultSet(tktrs);
            try {
                tktrs.close();
            } catch (SQLException e2) {
                logger.log(Level.WARNING, "SQL error while closing result set.", e2);
                e2.printStackTrace();
            }
            return tickets;
        } catch (SQLException sqle) {
            logger.log(Level.SEVERE, "Unable to get ticket pending operations", sqle);
            return new ArrayList<TicketInfo>();
        }
    }

    private static synchronized List<TicketInfo> getTicketBatch() {
        try {
            ResultSet tktrs = getTktBatch.executeQuery();
            List<TicketInfo> tickets = readTicketResultSet(tktrs);
            try {
                tktrs.close();
            } catch (SQLException e2) {
                logger.log(Level.WARNING, "SQL error while closing result set.", e2);
                e2.printStackTrace();
            }
            return tickets;
        } catch (SQLException sqle) {
            logger.log(Level.SEVERE, "Unable to get ticket batch", sqle);
            return new ArrayList<TicketInfo>();
        }
    }

    private static List<TicketInfo> readTicketResultSet(ResultSet tktrs)
            throws SQLException {
        List<TicketInfo> tickets = new ArrayList<TicketInfo>();
        while (tktrs.next()) {
            try {
                byte[] data = tktrs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                TicketInfo tkt = (TicketInfo) os.readObject();
                tickets.add(tkt);
                os.close();
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Get ticket batch failed: "
                        + e.getMessage());
                e.printStackTrace();
            }
        }
        return tickets;
    }

    /** Get total count of pending operations */
    private static int countOperations() {
        return countTickets();
    }


    private static synchronized void insertPendingTicket(TicketInfo ticket) {
       try {
            if (addTicket == null) {
                init();
            }
            addTicket.clearParameters();
            addTicket.setString(1, ticketId(ticket));
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(ticket);
            byte[] data = bos.toByteArray();
            os.close();
            addTicket.setBytes(2, data);
            addTicket.execute();
            logger.log(Level.INFO, "Queued ticket " + ticketId(ticket));
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while queuing ticket!!!", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "IOError while queuing ticket!!!", e);
        }
    }

    /** Remove a ticket from the queue. */
    private static synchronized void deletePendingTicket(String ticketId) {
        try {
            if (getTktOps == null) {
                init();
            }
            delTktOp.clearParameters();
            delTktOp.setString(1, ticketId);
            delTktOp.execute();
            logger.log(Level.INFO, "Deleted local ticket " + ticketId + ".");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Unable to delete a ticket from CallQueue! We are doomed!!!", e);
        }
    }

}
