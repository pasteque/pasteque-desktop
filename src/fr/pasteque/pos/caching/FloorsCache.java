//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.sales.restaurant.Floor;
import fr.pasteque.pos.sales.restaurant.Place;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FloorsCache
{
    private static PreparedStatement floorsPS;
    private static PreparedStatement placesPS;

    private FloorsCache() {}

    private static void init() throws SQLException {
        floorsPS = LocalDB.prepare("SELECT data FROM floors");
        placesPS = LocalDB.prepare("SELECT data FROM places "
                + "WHERE floorId = ?");
    }

    private static List<Floor> readFloorsResult(ResultSet rs)
        throws BasicException {
        try {
            List<Floor> floors = new ArrayList<Floor>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                Floor f = (Floor) os.readObject();
                floors.add(f);
            }
            return floors;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }

    private static List<Place> readPlacesResult(ResultSet rs)
        throws BasicException {
        try {
            List<Place> places = new ArrayList<Place>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                Place p = (Place) os.readObject();
                places.add(p);
            }
            return places;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }

    public static void refreshFloors(List<Floor> floors, List<Place> places)
        throws BasicException {
        try {
            LocalDB.execute("TRUNCATE TABLE floors");
            LocalDB.execute("TRUNCATE TABLE places");
            PreparedStatement stmt = LocalDB.prepare("INSERT INTO floors "
                    + "(id, data) VALUES (?, ?)");
            for (Floor f : floors) {
                stmt.setString(1, f.getID());
                ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(f);
                stmt.setBytes(2, bos.toByteArray());
                os.close();
                stmt.execute();
            }
            stmt = LocalDB.prepare("INSERT INTO places "
                    + "(id, floorId, data) VALUES (?, ?, ?)");
            stmt.clearParameters();
            for (Place p : places) {
                stmt.setString(1, p.getId());
                stmt.setString(2, p.getFloor());
                ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(p);
                stmt.setBytes(3, bos.toByteArray());
                os.close();
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    public static List<Floor> getFloors() throws BasicException {
        try {
            if (floorsPS == null) {
                init();
            }
            ResultSet rs = floorsPS.executeQuery();
            List<Floor> floors = readFloorsResult(rs);
            return floors;
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static List<Place> getPlaces(String floorId)
        throws BasicException {
        try {
            if (placesPS == null) {
                init();
            }
            placesPS.setString(1, floorId);
            ResultSet rs = placesPS.executeQuery();
            List<Place> places = readPlacesResult(rs);
            return places;
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
}
