//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.ticket.CashMove;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CashMoveCache {

    private static PreparedStatement movements;
    private static PreparedStatement delete;
    private static PreparedStatement add;
    private static PreparedStatement clean;

    private CashMoveCache() {}

    private static void init() throws SQLException {
        movements = LocalDB.prepare("SELECT data FROM cashMovements "
                + "WHERE cashRegisterId = ? and sequence = ?");
        delete = LocalDB.prepare("DELETE FROM cashMovements "
                + "WHERE cashRegisterId = ? AND sequence = ?");
        add = LocalDB.prepare("INSERT INTO cashMovements "
                + "(cashRegisterId, sequence, date, data) "
                + "VALUES (?, ?, ?, ?)");
        clean = LocalDB.prepare("DELETE FROM cashMovements WHERE "
                + "cashRegisterId != ? AND sequence != ?");
    }

    private static List<CashMove> readMovementsResult(ResultSet rs)
        throws BasicException {
        try {
            List<CashMove> moves = new ArrayList<CashMove>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                CashMove move = (CashMove) os.readObject();
                moves.add(move);
            }
            return moves;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }

    public static void addCashMove(CashMove move)
        throws BasicException {
        try {
            if (add == null) {
                init();
            }
            // Clean movements from other cash registers that cannot be accessed anyway
            clean.setInt(1, move.getCashRegisterId());
            clean.setInt(2, move.getSequence());
            clean.execute();
            add.setInt(1, move.getCashRegisterId());
            add.setInt(2, move.getSequence());
            add.setLong(3, move.getDate().getTime());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(move);
            add.setBytes(4, bos.toByteArray());
            os.close();
            add.execute();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    public static List<CashMove> getCashMovements(int cashRegisterId, int sequence)
        throws BasicException {
        try {
            if (movements == null) {
                init();
            }
            movements.setInt(1, cashRegisterId);
            movements.setInt(2, sequence);
            ResultSet rs = movements.executeQuery();
            return readMovementsResult(rs);
        } catch (Exception e) {
            throw new BasicException(e);
        }
    }
    /** Clear and replace tariff areas. */
    public static void deleteMovements(int cashRegisterId, int sequence)
        throws BasicException {
        try {
            if (delete == null) {
                init();
            }
            delete.setInt(1, cashRegisterId);
            delete.setInt(2, sequence);
            delete.execute();
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

}
