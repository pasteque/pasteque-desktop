//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.panels;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.gui.TableRendererBasic;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.caching.CallQueue;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.BeanFactoryApp;
import fr.pasteque.pos.forms.BeanFactoryException;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.forms.JPanelView;
import fr.pasteque.pos.forms.JRootApp;
import fr.pasteque.pos.printer.TicketPrinterException;
import fr.pasteque.pos.printer.document.TicketParser;
import fr.pasteque.pos.printer.document.ZTicketPrintScript;
import fr.pasteque.pos.sales.DataLogicReceipts;
import fr.pasteque.pos.sales.SharedTicketInfo;
import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.CoinCountButton;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author adrianromero
 */
public class JPanelCloseMoney extends JPanel
implements JPanelView, BeanFactoryApp, CoinCountButton.Listener
{
    private static final long serialVersionUID = -3933324064799480633L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.panels.JPanelCloseMoney");
    private static final int PRINTER_COUNT = 3;

    private static Map<String, Integer> countSave = new HashMap<String, Integer>();

    private AppView m_App;
    private DataLogicSystem m_dlSystem;

    private PaymentsModel m_PaymentsToClose = null;

    private TicketParser m_TTP;

    private JPanel coinCountBtnsContainer;
    private List<CoinCountButton> coinButtons;
    private JEditorKeys keypad;
    private JLabel totalAmount;
    private double total;
    private JLabel expectedAmount;
    private JLabel moveAmount;
    private JLabel errorAmount;
    private boolean[] printAvailable;
    private boolean[] printOnClose;
    private int printCount;
    private JLabel jlblPrintCount;

    private static void clearCountSave() {
        countSave.clear();
    }

    private static void saveCount(double amount, int count) {
        countSave.put(Formats.CURRENCY.formatValue(amount), count);
    }

    public static void removeSavedCount(double amount) {
        countSave.remove(Formats.CURRENCY.formatValue(amount));
    }

    private static int getSavedCount(double amount) {
        Integer count = countSave.get(Formats.CURRENCY.formatValue(amount));
        return count == null ? 0 : count.intValue();
    }

    private static boolean isCountSaved(double amount) {
        return countSave.containsKey(Formats.CURRENCY.formatValue(amount));
    }

    private static boolean hasCountSaved() {
        return !countSave.isEmpty();
    }


    /** Creates new form JPanelCloseMoney */
    public JPanelCloseMoney() {
        this.printAvailable = new boolean[PRINTER_COUNT];
        this.printOnClose = new boolean[PRINTER_COUNT];
        AppConfig cfg = AppConfig.loadedInstance;
        if (!"Not defined".equals(cfg.getProperty("machine.printer"))
                && cfg.getBoolean("machine.printer.filter.z")) {
            this.printAvailable[0] = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.2"))
                && cfg.getBoolean("machine.printer.2.filter.z")) {
            this.printAvailable[1] = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.3"))
                && cfg.getBoolean("machine.printer.3.filter.z")) {
            this.printAvailable[2] = true;
        }
        initComponents();
    }

    public void init(AppView app) throws BeanFactoryException {

        m_App = app;
        m_dlSystem = new DataLogicSystem();
        // Init z ticket
        m_TTP = new TicketParser(m_App.getDeviceTicket(), m_dlSystem);

        m_jTicketTable.setDefaultRenderer(Object.class, new TableRendererBasic(
                new Formats[] {new FormatsPayment(), new FormatsPaymentValue()}));
        m_jTicketTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        m_jScrollTableTicket.getVerticalScrollBar().setPreferredSize(new Dimension(25,25));
        m_jTicketTable.getTableHeader().setReorderingAllowed(false);
        m_jTicketTable.setRowHeight(25);
        m_jTicketTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        m_jsalestable.setDefaultRenderer(Object.class, new TableRendererBasic(
                new Formats[] {Formats.STRING, Formats.CURRENCY, Formats.CURRENCY, Formats.CURRENCY}));
        m_jsalestable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        m_jScrollSales.getVerticalScrollBar().setPreferredSize(new Dimension(25,25));
        m_jsalestable.getTableHeader().setReorderingAllowed(false);
        m_jsalestable.setRowHeight(25);
        m_jsalestable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.categoriesTable.setDefaultRenderer(Object.class, new TableRendererBasic(
                new Formats[] {Formats.STRING, Formats.CURRENCY, Formats.CURRENCY, Formats.CURRENCY}));
        this.categoriesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.scrollTableCategories.getVerticalScrollBar().setPreferredSize(new Dimension(25,25));
        this.categoriesTable.getTableHeader().setReorderingAllowed(false);
        this.categoriesTable.setRowHeight(25);
        this.categoriesTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Init cash count
        this.resetPrintState();
        this.coinButtons = new ArrayList<CoinCountButton>();
        AppConfig cfg = AppConfig.loadedInstance;
        boolean showCount = cfg.getProperty("ui.countmoney").equals("1");
        if (showCount) {
            String code = this.m_dlSystem.getResourceAsXML("payment.cash");
            if (code != null) {
                try {
                    ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.BEANSHELL);
                    script.put("payment", new ScriptCash());
                    script.eval(code);
                } catch (ScriptException e) {
                    MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotexecute"), e);
                    msg.show(this);
                }
            }
        } else {
            clearCountSave();
        }
    }

    public Object getBean() {
        return this;
    }

    public JComponent getComponent() {
        return this;
    }

    public String getTitle() {
        return AppLocal.getIntString("Menu.CloseTPV");
    }

    public boolean requiresOpenedCash() {
        return true;
    }

    public void activate() throws BasicException {
        m_jCloseCash.setEnabled(false);
        m_jPrintCash.setEnabled(false);
        m_jPrintCash2.setEnabled(false);
        m_jPrintCash3.setEnabled(false);
        // Check there are no ticket in the local queue
        boolean canClose = CallQueue.isEmpty();
        if (!canClose) {
            MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE,
                    AppLocal.getIntString("message.pendingtickets"));
            msg.show(this);
            this.showZTicket();
            return;
        }
        // Check if there are pending orders
        boolean hasPendingOrders = false;
        DataLogicReceipts dlReceipts = new DataLogicReceipts();
        List<SharedTicketInfo> l = dlReceipts.getOrderList();
        if (l.size() > 0) {
            for (SharedTicketInfo order : l) {
                if (!order.isEmpty()) {
                    hasPendingOrders = true;
                    break;
                }
            }
        }
        String warning = (hasPendingOrders) ?
                AppLocal.tr("Label.CashSession.PendingOrderWarning") :
                "";
        jLblPendingOrderWarning.setText(warning);
        this.loadData();
        this.showZTicket();
        m_jCloseCash.setEnabled(canClose);
    }

    public boolean deactivate() {
        // Reset count
        this.resetCashCount();
        return true;
    }

    private void resetPrintState() {
        this.printOnClose[0] = true;
        this.printOnClose[1] = true;
        this.printOnClose[2] = true;
        try {
            this.printCount = Integer.parseInt(AppConfig.loadedInstance.getProperty("ui.printticketcount"));
        } catch (NumberFormatException e) {
            this.printCount = 1;
        }
    }

    private void resetCashCount() {
        this.total = 0.0;
        for (CoinCountButton btn : this.coinButtons) {
            btn.reset();
        }
    }

    private void showCashCount() {
        CardLayout cl = (CardLayout) this.getLayout();
        cl.show(this, "cashcount");
        if (hasCountSaved()) {
            for (CoinCountButton btn : this.coinButtons) {
                if (isCountSaved(btn.getValue())) {
                    btn.setCount(getSavedCount(btn.getValue()));
                }
            }
        } else {
            for (CoinCountButton btn : this.coinButtons) {
                btn.reset();
            }
        }
        this.updateExpectedAmount();
        this.updateAmount();
        this.refreshPrint();
        // Open drawer
        String code = this.m_dlSystem.getResourceAsXML("Printer.OpenDrawer");
        if (code != null) {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                this.m_TTP.printTicket(script.eval(code).toString());
            } catch (Exception e) {
                logger.log(Level.WARNING, "Could not open drawer", e);
                e.printStackTrace();
            }
        }
    }
    private void showZTicket() {
        CardLayout cl = (CardLayout) this.getLayout();
        cl.show(this, "zticket");
    }

    private void loadData() throws BasicException {

        // Reset
        m_jSequence.setText(null);
        m_jMinDate.setText(null);
        m_jMaxDate.setText(null);
        m_jCloseCash.setEnabled(false);
        m_jPrintCash.setEnabled(false);
        m_jPrintCash2.setEnabled(false);
        m_jPrintCash3.setEnabled(false);
        m_jCount.setText(null); // AppLocal.getIntString("label.noticketstoclose");
        m_jCash.setText(null);

        m_jSales.setText(null);
        m_jSalesSubtotal.setText(null);
        m_jSalesTaxes.setText(null);
        m_jSalesTotal.setText(null);

        m_jTicketTable.setModel(new DefaultTableModel());
        m_jsalestable.setModel(new DefaultTableModel());
        this.categoriesTable.setModel(new DefaultTableModel());

        // LoadData
        m_PaymentsToClose = PaymentsModel.loadInstance(m_App);

        // Populate Data
        m_jSequence.setText(m_PaymentsToClose.printSequence());
        m_jMinDate.setText(m_PaymentsToClose.printDateStart());
        m_jMaxDate.setText(m_PaymentsToClose.printDateEnd());

        if (m_PaymentsToClose.getPayments() != 0 || m_PaymentsToClose.getSales() != 0) {

            m_jPrintCash.setEnabled(true);
            m_jPrintCash2.setEnabled(true);
            m_jPrintCash3.setEnabled(true);

            m_jCount.setText(m_PaymentsToClose.printPayments());
            m_jCash.setText(m_PaymentsToClose.printPaymentsTotal());

            m_jSales.setText(m_PaymentsToClose.printSales());
            m_jSalesSubtotal.setText(m_PaymentsToClose.printSalesBase());
            m_jSalesTaxes.setText(m_PaymentsToClose.printSalesTaxes());
            m_jSalesTotal.setText(m_PaymentsToClose.printSalesTotal());
        }

        m_jTicketTable.setModel(m_PaymentsToClose.getPaymentsModel());

        TableColumnModel jColumns = m_jTicketTable.getColumnModel();
        jColumns.getColumn(0).setPreferredWidth(247);
        jColumns.getColumn(0).setResizable(false);
        jColumns.getColumn(1).setPreferredWidth(100);
        jColumns.getColumn(1).setResizable(false);

        m_jsalestable.setModel(m_PaymentsToClose.getSalesModel());

        jColumns = m_jsalestable.getColumnModel();
        jColumns.getColumn(0).setPreferredWidth(151);
        jColumns.getColumn(0).setResizable(false);
        jColumns.getColumn(1).setPreferredWidth(98);
        jColumns.getColumn(1).setResizable(false);
        jColumns.getColumn(2).setPreferredWidth(98);
        jColumns.getColumn(2).setResizable(false);

        this.categoriesTable.setModel(m_PaymentsToClose.getCategoriesModel());

        jColumns = this.categoriesTable.getColumnModel();
        jColumns.getColumn(0).setPreferredWidth(247);
        jColumns.getColumn(0).setResizable(false);
        jColumns.getColumn(1).setPreferredWidth(100);
        jColumns.getColumn(1).setResizable(false);
    }

    /** Show confirm popup to close cash and close cash if confirmed */
    private void confirmCloseCash() {
        JClosePeriodDialog periodPopup = new JClosePeriodDialog(this);
        periodPopup.open();
        Integer res = periodPopup.getChoice();
        if (res != null) {
            this.closeCash(res.intValue());
        }
    }

    private void closeCash(int closeType) {
        Date dNow = new Date();
        AppConfig cfg = AppConfig.loadedInstance;
        boolean showCount = cfg.getProperty("ui.countmoney").equals("1");
        double expectedWithoutMoves = this.m_PaymentsToClose.getExpectedCash();
        double expectedWithMoves = this.m_PaymentsToClose.getDrawerExpectedCash();
        double totalWithoutMoves = Price.add(this.total, -this.m_PaymentsToClose.getCashMoveAmount());
        double totalWithMoves = this.total;
        Map<Double, Integer> coinCount = null;
        if (showCount) {
            coinCount = new HashMap<Double, Integer>();
            for (CoinCountButton ccb : this.coinButtons) {
                if (ccb.getCount() > 0) {
                    coinCount.put(ccb.getValue(), ccb.getCount());
                }
            }
        }
        CashSession cashSess = m_App.getActiveCashSession();
        cashSess.close(dNow, closeType, this.m_PaymentsToClose, coinCount);
        // Should be the same reference as above but make sure it is updated
        this.m_PaymentsToClose.getCashSession().close(dNow,
                CashSession.CLOSE_SIMPLE, this.m_PaymentsToClose, coinCount);
        if (showCount) {
            // Remove cash movements from the total to send the right expectations to the API
            // Because it doesn't include count details and movements
            cashSess.setCloseCash(totalWithoutMoves);
            cashSess.setExpectedCash(expectedWithoutMoves);
        }
        try {
            // Close cash on server
            m_dlSystem.saveCashSession(cashSess, this.m_PaymentsToClose);
        } catch (BasicException e) {
            // Revert modifications
            cashSess.cancelClose();
            cashSess.setCloseCash(null);
            cashSess.setExpectedCash(null);
            this.m_PaymentsToClose.getCashSession().cancelClose();
            // Show error
            logger.log(Level.SEVERE, "Could not close cash", e);
            MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotclosecash"), e);
            msg.show(this);
            return;
        }
        cashSess.confirmClose();
        this.m_PaymentsToClose.getCashSession().confirmClose();
        try {
            // Set the next cash session locally and save it
            m_App.newActiveCash(cashSess);
        } catch (BasicException e) {
            logger.log(Level.SEVERE, "Unable to set new cash session", e);
            MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, "Unable to set new cash session. Please close and restart Pasteque", e);
            msg.show(this);
            return;
        }

        // Prepare report and print
        if (showCount) {
            // Restore the real count to print to be consistent with detail
            // And to avoid updating all the resources
            m_PaymentsToClose.getCashSession().setCloseCash(totalWithMoves);
            m_PaymentsToClose.getCashSession().setExpectedCash(expectedWithMoves);
            m_PaymentsToClose.setExpectedCash(expectedWithMoves);
            // Add count details
            for (CoinCountButton ccb : this.coinButtons) {
                if (ccb.getCount() > 0) {
                    m_PaymentsToClose.setCoinCount(ccb.getValue(),
                            ccb.getCount());
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            if (!this.isPrintSelected(i)) {
                continue;
            }
            printPayments("Printer.CloseCash", i);
        }

        // Show confirmation message
        JMessageDialog.showMessage(this,
                AppLocal.tr("message.title"),
                AppLocal.tr("message.closecashok"));
        // Log out
        if (m_App instanceof JRootApp) {
            ((JRootApp)m_App).closeAppView();
        }

    }

    /** Print cash summary */
    private void printPayments(String report, int printerNum) {
        String cplKey;
        if (printerNum == 0) {
            cplKey = "machine.printer.cpl";
        } else {
            cplKey = "machine.printer." + (printerNum + 1) + ".cpl";
        }
        Double cpl = AppConfig.loadedInstance.getNumber(cplKey);
        ZTicketPrintScript printScript = new ZTicketPrintScript(cpl.intValue());
        // Get the resource
        String sresource = null;
        if ("Printer.CloseCash".equals(report)) {
            sresource = printScript.getCloseXml(m_PaymentsToClose, printerNum + 1);
        } else if ("Printer.PartialCash".equals(report)) {
            sresource = printScript.getPartialXml(m_PaymentsToClose, printerNum + 1);
        }
        if (sresource == null) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"));
            msg.show(this);
        } else {
            // Put objects references and run resource script
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                script.put("payments", m_PaymentsToClose);
                if ("Printer.CloseCash".equals(report)) {
                    for (int count = 0; count < this.printCount; count++) {
                        m_TTP.printTicket(script.eval(sresource).toString());
                    }
                } else {
                    m_TTP.printTicket(script.eval(sresource).toString());
                }
            } catch (ScriptException e) {
                logger.log(Level.WARNING, String.format("Could not run close cash resource %s", report), e);
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                msg.show(this);
            } catch (TicketPrinterException e) {
                logger.log(Level.WARNING, String.format("Error while running close cash resource %s", report), e);
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                msg.show(this);
            }
        }
    }

    private class FormatsPayment extends Formats {
        protected String formatValueInt(Object value) {
            Object[] values = (Object[]) value;
            String paymentType = (String) values[0];
            String currencyName = (String) values[1];
            Boolean main = (Boolean) values[2];
            String ret = AppLocal.getIntString("transpayment." + (String) paymentType);
            if (main) {
                return ret;
            } else {
                return ret + " (" + currencyName + ")";
            }
        }
        protected Object parseValueInt(String value) throws ParseException {
            return value;
        }
        public int getAlignment() {
            return javax.swing.SwingConstants.LEFT;
        }
    }
    private class FormatsPaymentValue extends Formats {
        /** Format payment. Value is expected to be PaymentModel.PaymentLine */
        protected String formatValueInt(Object value) {
            PaymentsModel.PaymentsLine line = (PaymentsModel.PaymentsLine) value;
            return line.printValue();
        }
        protected Object parseValueInt(String value) throws ParseException {
            return value;
        }
        public int getAlignment() {
            return javax.swing.SwingConstants.RIGHT;
        }
    }

    public class ScriptCash {
        private int x, y;
        private int btnSpacing;
        private ThumbNailBuilder tnb;
        public ScriptCash() {
            AppConfig cfg = AppConfig.loadedInstance;
            this.btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
            this.tnb = new ThumbNailBuilder(64, 54, "cash.png");
        }
        public void addButton(String image, double amount) {
            ImageIcon icon = new ImageIcon(this.tnb.getThumbNailText(m_dlSystem.getResourceAsImage(image), Formats.CURRENCY.formatValue(amount)));
            JPanelCloseMoney parent = JPanelCloseMoney.this;
            CoinCountButton btn = new CoinCountButton(icon, amount,
                    parent.keypad, parent);
            parent.coinButtons.add(btn);
            GridBagConstraints cstr = new GridBagConstraints();
            cstr.gridx = this.x;
            cstr.gridy = this.y;
            cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing,
                    btnSpacing);
            parent.coinCountBtnsContainer.add(btn.getComponent(), cstr);
            if (this.x == 3) {
                this.x = 0;
                this.y++;
            } else {
                this.x++;
            }
        }
    }

    /** From CoinCount.Listener */
    public void coinAdded(double amount, int newCount) {
        this.total = Price.add(this.total, amount);
        this.totalAmount.setText(Formats.CURRENCY.formatValue(this.total));
        this.updateMatchingCount();
        saveCount(amount, newCount);
    }
    /** From CoinCount.Listener */
    public void countUpdated(double amount, Integer newCount) {
        if (newCount == null) {
            removeSavedCount(amount);
        } else {
            saveCount(amount, newCount);
        }
        this.updateAmount();
    }

    /** Reset the total amount from individual counters. */
    public void updateAmount() {
        if (this.totalAmount == null) {
            return;
        }
        this.total = 0.0;
        for (CoinCountButton btn : this.coinButtons) {
            this.total = Price.add(this.total, btn.getAmount());
        }
        this.totalAmount.setText(Formats.CURRENCY.formatValue(this.total));
        this.updateMatchingCount();
    }
    public void updateExpectedAmount() {
        this.expectedAmount.setText(this.m_PaymentsToClose.printDrawerExpectedCash());
        this.moveAmount.setText(this.m_PaymentsToClose.printMoveAmount());
    }
    /** Check if total and expected amount are equal
     * and update UI accordingly
     */
    public void updateMatchingCount() {
        double expectedCash = this.m_PaymentsToClose.getDrawerExpectedCash();
        double error = Price.add(this.total, -expectedCash);
        Font f = this.errorAmount.getFont();
        if (this.total != expectedCash) {
            this.totalAmount.setForeground(Color.RED);
            this.errorAmount.setForeground(Color.RED);
            if (error > 0.0) {
               this.errorAmount.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
               this.errorAmount.setText("+" + Formats.CURRENCY.formatValue(error));
            } else {
               this.errorAmount.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
               this.errorAmount.setText(Formats.CURRENCY.formatValue(error));
            }
        } else {
            this.totalAmount.setForeground(Color.BLACK);
            this.errorAmount.setForeground(Color.BLACK);
            this.errorAmount.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
            this.errorAmount.setText(AppLocal.tr("Label.CashSession.CloseCashNoError"));
        }
    }

    private boolean isPrintSelected(int printerNum) {
        return this.printAvailable[printerNum] && this.printOnClose[printerNum];
    }

    private void togglePrint(int printerNum) {
        this.printOnClose[printerNum] = !this.printOnClose[printerNum];
        this.refreshPrint();
    }

    private void refreshPrint() {
        this.jbtnPrintClose.setSelected(this.isPrintSelected(0));
        this.jbtnPrintClose2.setSelected(this.isPrintSelected(1));
        this.jbtnPrintClose3.setSelected(this.isPrintSelected(2));
        if (this.isPrintSelected(0) || this.isPrintSelected(1) || this.isPrintSelected(2)) {
            this.jlblPrintCount.setText(String.valueOf(this.printCount));
            this.jbtnPrintPlus.setEnabled(true);
            this.jbtnPrintMinus.setEnabled(this.printCount > 1);
        } else {
            this.jlblPrintCount.setText("0");
            this.jbtnPrintPlus.setEnabled(false);
            this.jbtnPrintMinus.setEnabled(false);
        }
    }


    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;

        this.setLayout(new CardLayout());
        JPanel zTicketContainer = new JPanel();
        zTicketContainer.setLayout(new GridBagLayout());
        JPanel cashCountContainer = new JPanel();
        cashCountContainer.setLayout(new GridBagLayout());
        this.add(zTicketContainer, "zticket");
        this.add(cashCountContainer, "cashcount");

        // z ticket
        m_jSequence = new javax.swing.JTextField();
        m_jMinDate = new javax.swing.JTextField();
        m_jMaxDate = new javax.swing.JTextField();
        m_jScrollTableTicket = new JScrollPane();
        m_jTicketTable = new javax.swing.JTable();
        m_jCount = new javax.swing.JTextField();
        m_jCash = new javax.swing.JTextField();
        m_jSalesTotal = new javax.swing.JTextField();
        m_jScrollSales = new JScrollPane();
        m_jsalestable = new javax.swing.JTable();
        this.scrollTableCategories = new JScrollPane();
        this.categoriesTable = new javax.swing.JTable();
        m_jSalesTaxes = new javax.swing.JTextField();
        m_jSalesSubtotal = new javax.swing.JTextField();
        m_jSales = new javax.swing.JTextField();
        jLblPendingOrderWarning = WidgetsBuilder.createLabel("");
        m_jCloseCash = WidgetsBuilder.createButton(AppLocal.getIntString("Button.CloseCash"));
        ImageIcon icon = ImageLoader.readImageIcon("tkt_print.png");
        m_jPrintCash = WidgetsBuilder.createButton(icon, "",
                WidgetsBuilder.SIZE_MEDIUM);
        m_jPrintCash2 = WidgetsBuilder.createButton(icon, "2",
                WidgetsBuilder.SIZE_MEDIUM);
        m_jPrintCash3 = WidgetsBuilder.createButton(icon, "3",
                WidgetsBuilder.SIZE_MEDIUM);
        GridBagConstraints cstr = null;

        JPanel mainContainer = new JPanel();
        mainContainer.setLayout(new GridBagLayout());
        JScrollPane scroll = new JScrollPane();
        scroll.getVerticalScrollBar().setPreferredSize(new Dimension(25,25));
        scroll.setViewportView(mainContainer);

        // Dates frame
        JPanel datesPanel = new JPanel();
        datesPanel.setLayout(new GridBagLayout());
        datesPanel.setBorder(BorderFactory.createTitledBorder(AppLocal.getIntString("label.datestitle")));

        JLabel sequenceLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("label.sequence"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.insets = new Insets(5, 5, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        datesPanel.add(sequenceLabel, cstr);
        m_jSequence.setEditable(false);
        m_jSequence.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.insets = new Insets(5, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        datesPanel.add(m_jSequence, cstr);

        JLabel startLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.StartDate"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.insets = new Insets(0, 5, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        datesPanel.add(startLabel, cstr);
        m_jMinDate.setEditable(false);
        m_jMinDate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        datesPanel.add(m_jMinDate, cstr);

        JLabel endLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.EndDate"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 2;
        cstr.insets = new Insets(0, 5, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        datesPanel.add(endLabel, cstr);
        m_jMaxDate.setEditable(false);
        m_jMaxDate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 2;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        datesPanel.add(m_jMaxDate, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 0;
        cstr.weightx = 1;
        datesPanel.add(new JPanel(), cstr);

        // Payments frame
        JPanel paymentsPanel = new JPanel();
        paymentsPanel.setLayout(new GridBagLayout());
        paymentsPanel.setBorder(BorderFactory.createTitledBorder(AppLocal.getIntString("label.paymentstitle")));

        m_jScrollTableTicket.setMinimumSize(new java.awt.Dimension(350, 140));
        m_jScrollTableTicket.setPreferredSize(new java.awt.Dimension(350, 140));
        m_jTicketTable.setFocusable(false);
        m_jTicketTable.setIntercellSpacing(new java.awt.Dimension(0, 1));
        m_jTicketTable.setRequestFocusEnabled(false);
        m_jTicketTable.setShowVerticalLines(false);
        m_jScrollTableTicket.setViewportView(m_jTicketTable);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.gridheight = 5;
        cstr.insets = new Insets(5, 5, 5, 5);
        paymentsPanel.add(m_jScrollTableTicket, cstr);

        JLabel paymentsLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Tickets"));
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr. gridy = 0;
        cstr.insets = new Insets(5, 0, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        paymentsPanel.add(paymentsLabel, cstr);

        m_jCount.setEditable(false);
        m_jCount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 0;
        cstr.insets = new Insets(5, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        paymentsPanel.add(m_jCount, cstr);

        JLabel cashLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("label.paymenttotal"));
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr. gridy = 1;
        cstr.insets = new Insets(0, 0, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        paymentsPanel.add(cashLabel, cstr);

        m_jCash.setEditable(false);
        m_jCash.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 1;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        paymentsPanel.add(m_jCash, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 3;
        cstr.gridy = 0;
        cstr.weightx = 1;
        paymentsPanel.add(new JPanel(), cstr);

        // Taxes and total frame
        JPanel salesPanel = new JPanel();
        salesPanel.setLayout(new GridBagLayout());
        salesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(AppLocal.getIntString("label.salestitle")));

        m_jsalestable.setFocusable(false);
        m_jsalestable.setIntercellSpacing(new java.awt.Dimension(0, 1));
        m_jsalestable.setRequestFocusEnabled(false);
        m_jsalestable.setShowVerticalLines(false);
        m_jScrollSales.setMinimumSize(new java.awt.Dimension(350, 140));
        m_jScrollSales.setPreferredSize(new java.awt.Dimension(350, 140));
        m_jScrollSales.setViewportView(m_jsalestable);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.gridheight = 5;
        cstr.insets = new Insets(5, 5, 5, 5);
        salesPanel.add(m_jScrollSales, cstr);

        this.categoriesTable.setFocusable(false);
        this.categoriesTable.setIntercellSpacing(new java.awt.Dimension(0, 1));
        this.categoriesTable.setRequestFocusEnabled(false);
        this.categoriesTable.setShowVerticalLines(false);
        this.scrollTableCategories.setMinimumSize(new java.awt.Dimension(350, 140));
        this.scrollTableCategories.setPreferredSize(new java.awt.Dimension(350, 140));
        this.scrollTableCategories.setViewportView(this.categoriesTable);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 6;
        cstr.gridheight = 5;
        cstr.insets = new Insets(0, 5, 5, 5);
        salesPanel.add(this.scrollTableCategories, cstr);

        JLabel salesLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("label.sales"));
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.insets = new Insets(0, 0, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        salesPanel.add(salesLabel, cstr);
        m_jSales.setEditable(false);
        m_jSales.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 0;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        salesPanel.add(m_jSales, cstr);

        JLabel subtotalLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("label.subtotalcash"));
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        cstr.insets = new Insets(0, 0, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        salesPanel.add(subtotalLabel, cstr);
        m_jSalesSubtotal.setEditable(false);
        m_jSalesSubtotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 1;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        salesPanel.add(m_jSalesSubtotal, cstr);

        JLabel taxesLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("label.taxcash"));
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 2;
        cstr.insets = new Insets(0, 0, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        salesPanel.add(taxesLabel, cstr);
        m_jSalesTaxes.setEditable(false);
        m_jSalesTaxes.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 2;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        salesPanel.add(m_jSalesTaxes, cstr);

        JLabel totalLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("label.totalcash"));
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 3;
        cstr.insets = new Insets(0, 0, 5, 5);
        cstr.anchor = GridBagConstraints.WEST;
        salesPanel.add(totalLabel, cstr);
        m_jSalesTotal.setEditable(false);
        m_jSalesTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 3;
        cstr.insets = new Insets(0, 0, 5, 0);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        salesPanel.add(m_jSalesTotal, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 3;
        cstr.gridy = 0;
        cstr.weightx = 1;
        salesPanel.add(new JPanel(), cstr);

        // Buttons
        m_jCloseCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jCloseCashActionPerformed(evt);
            }
        });
        m_jPrintCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPrintCashActionPerformed(evt, 0);
            }
        });
        m_jPrintCash2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPrintCashActionPerformed(evt, 1);
            }
        });
        m_jPrintCash3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPrintCashActionPerformed(evt, 2);
            }
        });

        // General layout
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.weightx = 1;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        mainContainer.add(datesPanel, cstr);
        mainContainer.add(paymentsPanel, cstr);
        mainContainer.add(salesPanel, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.gridwidth = 3;
        cstr.weightx = 1;
        cstr.weighty = 1;
        cstr.fill = GridBagConstraints.BOTH;
        zTicketContainer.add(scroll, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 3;
        cstr.weightx = 1;
        cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
        cstr.anchor = GridBagConstraints.EAST;
        zTicketContainer.add(jLblPendingOrderWarning, cstr);
        int x = 1;
        if (this.printAvailable[2]) {
            cstr = new GridBagConstraints();
            cstr.gridx = x++;
            cstr.gridy = 3;
            cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
            zTicketContainer.add(m_jPrintCash3, cstr);
        }
        if (this.printAvailable[1]) {
            cstr = new GridBagConstraints();
            cstr.gridx = x++;
            cstr.gridy = 3;
            cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
            zTicketContainer.add(m_jPrintCash2, cstr);
        }
        if (this.printAvailable[0]) {
            cstr = new GridBagConstraints();
            cstr.gridx = x++;
            cstr.gridy = 3;
            cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
            zTicketContainer.add(m_jPrintCash, cstr);
        }
        cstr = new GridBagConstraints();
        cstr.gridx = x;
        cstr.gridy = 3;
        cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
        zTicketContainer.add(m_jCloseCash, cstr);


        // Cash count
        this.coinCountBtnsContainer = new JPanel();
        this.coinCountBtnsContainer.setLayout(new GridBagLayout());
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.weighty = 0.5;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        cashCountContainer.add(this.coinCountBtnsContainer, cstr);
        // keypad and input
        int y = 0;
        JPanel inputContainer = new JPanel();
        inputContainer.setLayout(new GridBagLayout());
        this.keypad = new JEditorKeys();
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing,
                btnSpacing);
        inputContainer.add(this.keypad, cstr);
        JLabel amountLabel = WidgetsBuilder.createLabel(AppLocal.tr("Label.CashSession.CashAmount"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.anchor = GridBagConstraints.FIRST_LINE_START;
        inputContainer.add(amountLabel, cstr);
        this.totalAmount = WidgetsBuilder.createImportantLabel("");
        WidgetsBuilder.inputStyle(totalAmount);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.anchor = GridBagConstraints.CENTER;
        inputContainer.add(totalAmount, cstr);
        JLabel expectedAmountLabel = WidgetsBuilder.createLabel(AppLocal.tr("Label.CashSession.CloseCashExpectedAmount"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.anchor = GridBagConstraints.FIRST_LINE_START;
        inputContainer.add(expectedAmountLabel, cstr);
        this.expectedAmount = WidgetsBuilder.createImportantLabel("");
        WidgetsBuilder.inputStyle(expectedAmount);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.anchor = GridBagConstraints.CENTER;
        inputContainer.add(expectedAmount, cstr);
        JLabel moveAmountLabel = WidgetsBuilder.createLabel(AppLocal.tr("Label.CashSession.CloseCashMoveAmount"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.anchor = GridBagConstraints.FIRST_LINE_START;
        inputContainer.add(moveAmountLabel, cstr);
        this.moveAmount = WidgetsBuilder.createLabel("");
        moveAmount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.anchor = GridBagConstraints.CENTER;
        inputContainer.add(moveAmount, cstr);
        JLabel errorAmountLabel = WidgetsBuilder.createLabel(AppLocal.tr("Label.CashSession.CloseCashErrorAmount"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.anchor = GridBagConstraints.FIRST_LINE_START;
        inputContainer.add(errorAmountLabel, cstr);
        this.errorAmount = WidgetsBuilder.createLabel("");
        errorAmount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = y++;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.anchor = GridBagConstraints.CENTER;
        inputContainer.add(errorAmount, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.weightx = 0.5;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.anchor = GridBagConstraints.CENTER;
        cstr.insets = new Insets(0, btnSpacing, btnSpacing, btnSpacing);
        cashCountContainer.add(inputContainer, cstr);
        // buttons line
        JPanel buttonsContainer = new JPanel();
        buttonsContainer.setLayout(new FlowLayout(FlowLayout.TRAILING,
                marginInset, btnSpacing));
        jbtnPrintClose = new javax.swing.JToggleButton();
        jbtnPrintClose.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
        WidgetsBuilder.adaptSize(jbtnPrintClose, WidgetsBuilder.SIZE_MEDIUM);
        jbtnPrintClose.setSelected(true);
        jbtnPrintClose.setFocusPainted(false);
        jbtnPrintClose.setFocusable(false);
        jbtnPrintClose.setRequestFocusEnabled(false);
        jbtnPrintClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printActionPerformed(evt, 0);
            }
        });
        jbtnPrintClose2 = new javax.swing.JToggleButton();
        jbtnPrintClose2.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
        jbtnPrintClose2.setText("2");
        WidgetsBuilder.adaptSize(jbtnPrintClose2, WidgetsBuilder.SIZE_MEDIUM);
        jbtnPrintClose2.setSelected(true);
        jbtnPrintClose2.setFocusPainted(false);
        jbtnPrintClose2.setFocusable(false);
        jbtnPrintClose2.setRequestFocusEnabled(false);
        jbtnPrintClose2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printActionPerformed(evt, 1);
            }
        });
        jbtnPrintClose3 = new javax.swing.JToggleButton();
        jbtnPrintClose3.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
        jbtnPrintClose3.setText("3");
        WidgetsBuilder.adaptSize(jbtnPrintClose3, WidgetsBuilder.SIZE_MEDIUM);
        jbtnPrintClose3.setSelected(true);
        jbtnPrintClose3.setFocusPainted(false);
        jbtnPrintClose3.setFocusable(false);
        jbtnPrintClose3.setRequestFocusEnabled(false);
        jbtnPrintClose3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printActionPerformed(evt, 2);
            }
        });

        boolean hasPrinter = false;
        if (this.printAvailable[2]) {
            buttonsContainer.add(jbtnPrintClose3);
            hasPrinter = true;
        }
        if (this.printAvailable[1]) {
            buttonsContainer.add(jbtnPrintClose2);
            hasPrinter = true;
        }
        if (this.printAvailable[0]) {
            buttonsContainer.add(jbtnPrintClose);
            hasPrinter = true;
        }
        this.jlblPrintCount = WidgetsBuilder.createLabel("1");
        jbtnPrintPlus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_plus.png"));
        jbtnPrintMinus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_minus.png"));
        jbtnPrintPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printPlusActionPerformed(evt);
            }
        });
        jbtnPrintMinus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printMinusActionPerformed(evt);
            }
        });
        jbtnPrintPlus.setFocusable(false);
        jbtnPrintMinus.setFocusable(false);
        if (hasPrinter) {
            buttonsContainer.add(jbtnPrintMinus);
            buttonsContainer.add(jlblPrintCount);
            buttonsContainer.add(jbtnPrintPlus);
        }
        JPanel separator = new JPanel();
        separator.setMinimumSize(new java.awt.Dimension(btnSpacing * 2, 0));
        buttonsContainer.add(separator);
        JButton cancel = WidgetsBuilder.createButton(WidgetsBuilder.createIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_BIG);
        cancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    countCancelActionPerformed(evt);
                }
            });
        buttonsContainer.add(cancel);
        JButton closeCash = WidgetsBuilder.createButton(WidgetsBuilder.createIcon("open_cash.png"),
                AppLocal.getIntString("Button.CloseCash"),
                WidgetsBuilder.SIZE_BIG);
        closeCash.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    countCloseCashActionPerformed(evt);
                }
            });
        buttonsContainer.add(closeCash);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.gridwidth = 2;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cashCountContainer.add(buttonsContainer, cstr);
    }

    private void m_jCloseCashActionPerformed(java.awt.event.ActionEvent evt) {
        AppConfig cfg = AppConfig.loadedInstance;
        boolean showCount = cfg.getProperty("ui.countmoney").equals("1");
        if (showCount) {
            this.showCashCount();
        } else {
            this.confirmCloseCash();
        }
    }
    private void countCloseCashActionPerformed(ActionEvent evt) {
        this.confirmCloseCash();
    }
    private void countCancelActionPerformed(ActionEvent evt) {
        resetPrintState();
        clearCountSave();
        this.showZTicket();
    }

    private void m_jPrintCashActionPerformed(java.awt.event.ActionEvent evt, int printerNum) {
        // print report
        printPayments("Printer.PartialCash", printerNum);
    }

    private void printActionPerformed(java.awt.event.ActionEvent evt, int printerNum) {
        this.togglePrint(printerNum);
    }

    private void printPlusActionPerformed(java.awt.event.ActionEvent evt) {
        this.printCount++;
        this.refreshPrint();
    }

    private void printMinusActionPerformed(java.awt.event.ActionEvent evt) {
        this.printCount--;
        this.refreshPrint();
    }

    private javax.swing.JTextField m_jCash;
    private javax.swing.JLabel jLblPendingOrderWarning;
    private javax.swing.JButton m_jCloseCash;
    private javax.swing.JTextField m_jCount;
    private javax.swing.JTextField m_jMaxDate;
    private javax.swing.JTextField m_jMinDate;
    private javax.swing.JButton m_jPrintCash;
    private javax.swing.JButton m_jPrintCash2;
    private javax.swing.JButton m_jPrintCash3;
    private javax.swing.JToggleButton jbtnPrintClose;
    private javax.swing.JToggleButton jbtnPrintClose2;
    private javax.swing.JToggleButton jbtnPrintClose3;
    private javax.swing.JButton jbtnPrintPlus;
    private javax.swing.JButton jbtnPrintMinus;
    private javax.swing.JTextField m_jSales;
    private javax.swing.JTextField m_jSalesSubtotal;
    private javax.swing.JTextField m_jSalesTaxes;
    private javax.swing.JTextField m_jSalesTotal;
    private javax.swing.JScrollPane m_jScrollSales;
    private javax.swing.JScrollPane m_jScrollTableTicket;
    private javax.swing.JScrollPane scrollTableCategories;
    private javax.swing.JTextField m_jSequence;
    private javax.swing.JTable m_jTicketTable;
    private javax.swing.JTable m_jsalestable;
    private javax.swing.JTable categoriesTable;
}
