//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.panels;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.gui.JAbstractChoiceDialog;
import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.data.gui.StringItem;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.UIManager;

/**
 * Popup to select which period to close.
 */
public class JClosePeriodDialog extends JAbstractChoiceDialog<Integer>
implements ShortcutListener
{
    private static final long serialVersionUID = 3146117586509171010L;

    public JClosePeriodDialog(java.awt.Component parent) {
        super(parent, null);
    }

    protected void setShortcuts() {
        super.setShortcuts();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
    }

    protected void cleanShortcuts() {
        super.cleanShortcuts();
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
    }

    /**
     * Press button when pressing GENERAL_VALIDATE.
     */
    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                if (this.jcmdCancel.isFocusOwner()) {
                    this.jcmdCancel.doClick(Shortcuts.CLICK_TIME);
                } else if (this.jcmdOK.isFocusOwner() || this.field.isFocusOwner()) {
                    this.jcmdOK.doClick(Shortcuts.CLICK_TIME);
                } // else block further propagation
                return true;
            default: // Ignore
                return false;
        }
    }
    @Override // from JAbstractChoiceDialog
    protected void confirm() {
        this.setChoice(this.field.getSelectedValue());
        super.confirm();
    }

    @Override // from JAbstractDialog
    protected void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        this.field = WidgetsBuilder.createComboBoxVal();
        this.field.addItems(
                new StringItem<Integer>(
                        Integer.valueOf(CashSession.CLOSE_SIMPLE),
                        AppLocal.tr("close.simple")),
                new StringItem<Integer>(
                        Integer.valueOf(CashSession.CLOSE_PERIOD),
                        AppLocal.tr("close.period")),
                new StringItem<Integer>(
                        Integer.valueOf(CashSession.CLOSE_FYEAR),
                        AppLocal.tr("close.fyear")));
        JLabel icon = new JLabel();
        icon.setIcon(UIManager.getIcon("OptionPane.questionIcon"));
        setTitle(AppLocal.tr("message.title"));
        JLabel label = WidgetsBuilder.createLabel(AppLocal.tr("message.wannaclosecash"));
        jcmdOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"), WidgetsBuilder.SIZE_MEDIUM);
        jcmdCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        jcmdOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirm();
            }
        });
        jcmdCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close();
            }
        });

        getContentPane().setLayout(new GridBagLayout());
        // Icon
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridy = 0; cstr.gridx = 0;
        cstr.gridheight = 2;
        cstr.insets = new java.awt.Insets(marginInset, marginInset, marginInset, marginInset);
        getContentPane().add(icon, cstr);
        // Combo box
        cstr = new GridBagConstraints();
        cstr.gridx = 1; cstr.gridy = 0;
        cstr.anchor = GridBagConstraints.LINE_START;
        cstr.insets = new java.awt.Insets(marginInset, marginInset, 0, marginInset);
        getContentPane().add(label, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1; cstr.gridy = 1;
        cstr.gridwidth = 3;;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.insets = new java.awt.Insets(btnSpacing, marginInset, btnSpacing, marginInset);
        getContentPane().add(this.field, cstr);

        // Buttons
        javax.swing.JPanel btnsContainer = new javax.swing.JPanel();
        btnsContainer.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, btnSpacing, btnSpacing));
        btnsContainer.add(jcmdOK);
        btnsContainer.add(jcmdCancel);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = 2;
        cstr.gridwidth = 2;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        getContentPane().add(btnsContainer, cstr);
    }

    private JComboBoxVal<Integer> field;
    private javax.swing.JButton jcmdCancel;
    private javax.swing.JButton jcmdOK;
}
