//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.panels;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.BeanFactoryApp;
import fr.pasteque.pos.forms.BeanFactoryException;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.forms.JPanelView;
import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.ticket.CashMove;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.CoinCountButton;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author adrianromero
 */
public class JPanelPayments extends JPanel
implements JPanelView, BeanFactoryApp, CoinCountButton.Listener
{
    private static final long serialVersionUID = -2132348541499733417L;

    public static final String CASH_MOVE_OUT = "cashout";
    public static final String CASH_MOVE_IN = "cashin";

    private AppView app;
    private double amount;

    /** Creates a new instance of JPanelPayments */
    public JPanelPayments() {
        this.initComponents();

        this.reason.addItem(CASH_MOVE_IN,
                AppLocal.getIntString("transpayment.cashin"));
        this.reason.addItem(CASH_MOVE_OUT,
                AppLocal.getIntString("transpayment.cashout"));
        this.total.addEditorKeys(this.keypad);
        // Init coin buttons
        this.coinButtons = new ArrayList<>();
        DataLogicSystem dlSys = new DataLogicSystem();
        String code = dlSys.getResourceAsXML("payment.cash");
        if (code != null) {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.BEANSHELL);
                script.put("payment", new ScriptCash());
                script.eval(code);
            } catch (ScriptException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotexecute"), e);
                msg.show(this);
            }
        }
    }

    @Override
    public void init(AppView app) throws BeanFactoryException {
        this.app = app;
        this.updateMoveList();
    }

    private void updateMoveList() {
        DataLogicSales dlSales = new DataLogicSales();
        int cashRegisterId = this.app.getActiveCashSession().getCashRegisterId();
        int sequence = this.app.getActiveCashSequence();
        String moveListStr = "";
        if (this.app.getActiveCashSession().getOpenCash() != null) {
            moveListStr = String.format("<html>%s %s",
                AppLocal.tr("Label.CashSession.OpenCashAmount"),
                Formats.CURRENCY.formatValue(Double.valueOf(this.app.getActiveCashSession().getOpenCash())));
        }
        try {
            for (CashMove move : dlSales.getCashMoves(cashRegisterId, sequence)) {
                moveListStr += String.format("<br>%s %s",
                        Formats.TIMESTAMP.formatValue(move.getDate()),
                        Formats.CURRENCY.formatValue(Double.valueOf(move.getAmount())));
            }
            moveListStr += "</html>";
        } catch (BasicException e) {
            moveListStr = e.getMessage();
        }
        this.moveListLabel.setText(moveListStr);
    }

    @Override
    public String getTitle() {
        return AppLocal.getIntString("Menu.Payments");
    }

    @Override
    public Object getBean() {
        return this;
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public void activate() {
        this.reason.setSelectedValue(CASH_MOVE_IN);
        // Open drawer
        DataLogicSystem dlSys = new DataLogicSystem();
        String code = dlSys.getResourceAsXML("Printer.OpenDrawer");
        if (code != null) {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                script.eval(code);
            } catch (ScriptException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deactivate() {
        return true;
    }

    public boolean requiresOpenedCash() {
        return true;
    }

    private void reset() {
        this.reason.setSelectedValue(CASH_MOVE_IN);
        this.total.setDoubleValue(null);
        this.amount = 0.0;
        for (CoinCountButton btn : this.coinButtons) {
            btn.reset();
        }
    }

    public void coinAdded(double amount, int newCount) {
        this.amount = Price.add(this.amount, amount);
        this.total.setDoubleValue(this.amount);
    }
    public void countUpdated(double amount, Integer newCount) {
        this.updateAmount();
    }
    public void updateAmount() {
        this.amount = 0.0;
        for (CoinCountButton btn : this.coinButtons) {
            this.amount = Price.add(this.amount, btn.getAmount());
        }
        this.total.setDoubleValue(this.amount);
    }

    public class ScriptCash {
        private int x, y;
        private int btnSpacing;
        private ThumbNailBuilder tnb;
        public ScriptCash() {
            AppConfig cfg = AppConfig.loadedInstance;
            this.btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
            this.tnb = new ThumbNailBuilder(64, 54, "cash.png");
        }
        public void addButton(String image, double amount) {
            DataLogicSystem dlSys = new DataLogicSystem();
            ImageIcon icon = new ImageIcon(this.tnb.getThumbNailText(dlSys.getResourceAsImage(image), Formats.CURRENCY.formatValue(amount)));
            JPanelPayments parent = JPanelPayments.this;
            CoinCountButton btn = new CoinCountButton(icon, amount,
                    parent.keypad, parent);
            parent.coinButtons.add(btn);
            GridBagConstraints cstr = new GridBagConstraints();
            cstr.gridx = this.x;
            cstr.gridy = this.y;
            cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing,
                    btnSpacing);
            parent.coinCountBtnsContainer.add(btn.getComponent(), cstr);
            if (this.x == 3) {
                this.x = 0;
                this.y++;
            } else {
                this.x++;
            }
        }
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = null;
        this.moveListLabel = WidgetsBuilder.createLabel("");
        JLabel reasonLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.paymentreason"));
        this.reason = WidgetsBuilder.createComboBoxVal();
        JLabel totalLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.value"));
        this.total = new fr.pasteque.pos.widgets.JEditorCurrency();
        this.keypad = new fr.pasteque.pos.widgets.JEditorKeys();
        JButton ok = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });

        // movement list
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        this.add(this.moveListLabel, c);

        // coin buttons
        this.coinCountBtnsContainer = new JPanel();
        this.coinCountBtnsContainer.setLayout(new GridBagLayout());
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weighty = 0.5;
        c.weightx = 1.0;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.coinCountBtnsContainer, c);

        // Keypad
        c = new GridBagConstraints();
        c.gridx = 3;
        c.gridy = 0;
        c.gridwidth = 2;
        this.add(this.keypad, c);

        // Reason
        c = new GridBagConstraints();
        c.gridx = 3;
        c.gridy = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(btnSpacing, btnSpacing, 0, 0);
        this.add(reasonLbl, c);
        c = new GridBagConstraints();
        c.gridx = 4;
        c.gridy = 1;
        c.insets = new Insets(btnSpacing, btnSpacing, 0, btnSpacing);
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(this.reason, c);

        // Total
        c = new GridBagConstraints();
        c.gridx = 3;
        c.gridy = 2;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(btnSpacing, btnSpacing, 0, 0);
        this.add(totalLbl, c);
        c = new GridBagConstraints();
        c.gridx = 4;
        c.gridy = 2;
        c.insets = new Insets(btnSpacing, btnSpacing, 0, btnSpacing);
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(this.total, c);

        // Ok
        c = new GridBagConstraints();
        c.gridx = 3;
        c.gridy = 3;
        c.gridwidth = 2;
        c.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        this.add(ok, c);

    }

    private void okActionPerformed(java.awt.event.ActionEvent evt) {
        int cashRegisterId = this.app.getActiveCashSession().getCashRegisterId();
        int sequence = this.app.getActiveCashSequence();
        String reasonCode = this.reason.getSelectedValue();
        double amount = this.total.getDoubleValue();
        if (CASH_MOVE_OUT.equals(reasonCode)) {
            amount = -amount;
        }
        CashMove move = new CashMove(cashRegisterId, sequence, amount);
        DataLogicSales dlSales = new DataLogicSales();
        try {
            dlSales.saveMove(move);
            this.reset();
            this.updateMoveList();
            MessageInf msg = new MessageInf(MessageInf.SGN_SUCCESS,
                    AppLocal.getIntString("Message.CashMovementSaved"));
            msg.show(this);
        } catch (BasicException e) {
            e.printStackTrace();
            MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE,
                    AppLocal.getIntString("message.cannotexecute"), e);
            msg.show(this);
        }
    }


    private fr.pasteque.pos.widgets.JEditorCurrency total;
    private fr.pasteque.pos.widgets.JEditorKeys keypad;
    private JComboBoxVal<String> reason;
    private JPanel coinCountBtnsContainer;
    private JLabel moveListLabel;
    private List<CoinCountButton> coinButtons;

}
