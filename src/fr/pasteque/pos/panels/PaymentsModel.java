//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.panels;

import fr.pasteque.basic.BasicException;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.ticket.CashMove;
import fr.pasteque.pos.ticket.CashRegisterInfo;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.ZTicket;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author adrianromero
 */
public class PaymentsModel
{
    private CashSession cashSession;
    private CashRegisterInfo cashRegister;
    private Integer m_iPayments;
    private Double m_dPaymentsTotal;
    private java.util.List<PaymentsLine> m_lpayments;
    private List<CategoryLine> catSales;
    private Map<Double, Integer> coinCount;
    private List<ZTicket.CategoryTax> catTaxes;

    private final static String[] PAYMENTHEADERS = {"Label.Payment", "label.totalcash"};

    private Integer m_iSales;
    private Double m_dSalesBase;
    private Double m_dSalesTaxes;
    private java.util.List<SalesLine> m_lsales;
    private Integer custCount;
    /** Open cash + cash sales. It doesn't take movements in account.
     * This is the one sent to the API, because movements are local only.
     * TODO: shitty duplication with CashSession. */
    private Double expectedCash;
    /** Open cash + cash sales + movements.
     * TODO: shitty duplication with CashSession. */
    private Double drawerExpectedCash;
    private java.util.List<CashMove> cashMoves;
    private java.util.List<CustomerLine> custLines;

    private final static String[] SALEHEADERS = {"label.taxcash", "label.subtotalcash", "label.totalcash"};
    private final static String[] CATEGORYHEADERS = {"label.catname", "label.totalcash"};

    private PaymentsModel() {
        this.coinCount = new HashMap<Double, Integer>();
    }

    public static PaymentsModel loadOpenInstance(CashSession session) throws BasicException {
        DataLogicSales dlSales = new DataLogicSales();
        DataLogicSystem dlSys = new DataLogicSystem();
        PaymentsModel p = new PaymentsModel();
        p.cashSession = session;
        p.cashRegister = dlSys.getCashRegister(session.getCashRegisterId());
        p.m_lpayments = new ArrayList<PaymentsLine>();
        p.catSales = new ArrayList<CategoryLine>();
        p.m_lsales = new ArrayList<SalesLine>();
        p.custLines = new ArrayList<CustomerLine>();
        p.catTaxes = new ArrayList<ZTicket.CategoryTax>();
        p.cashMoves = dlSales.getCashMoves(session.getCashRegisterId(),
                session.getSequence());
        return p;
    }

    public static PaymentsModel loadInstance(CashSession session) throws BasicException {
        DataLogicSales dlSales = new DataLogicSales();
        DataLogicSystem dlSys = new DataLogicSystem();
        PaymentsModel p = new PaymentsModel();
        p.cashSession = session;
        p.cashRegister = dlSys.getCashRegister(session.getCashRegisterId());
        p.cashMoves = dlSales.getCashMoves(session.getCashRegisterId(),
                session.getSequence());
        ZTicket z = dlSales.getZTicket(session);
        // Get number of payments and total amount
        p.m_iPayments = z.getPaymentCount();
        p.m_dPaymentsTotal = 0.0;
        p.m_lpayments = new ArrayList<PaymentsLine>();
        for (ZTicket.Payment payment : z.getPayments()) {
            p.m_dPaymentsTotal += payment.getAmount();
            CurrencyInfo curr = dlSales.getCurrency(payment.getCurrencyId());
            PaymentsLine l = new PaymentsLine(payment.getType(),
                    curr, payment.getCurrencyAmount());
            p.m_lpayments.add(l);
        }
        // Sales
        p.m_iSales = z.getTicketCount();
        p.m_dSalesBase = z.getConsolidatedSales();
        p.custCount = z.getCustomersCount();
        // Sales by categories
        p.catSales = new ArrayList<CategoryLine>();
        for (ZTicket.Category cat : z.getCategories()) {
            String catId = cat.getId();
            CategoryInfo catInfo = dlSales.getCategory(catId);
            if (catInfo != null) {
                String name = catInfo.getName();
                String ref = catInfo.getReference();
                CategoryLine l = new CategoryLine(name, ref, cat.getAmount());
                p.catSales.add(l);
            }
        }
        // Taxes by category
        p.catTaxes = z.getCatTaxes();
        // Customer balances
        p.custLines = new ArrayList<CustomerLine>();
        for (ZTicket.CustomerBalance custBalance : z.getCustBalances()) {
            String custId = custBalance.getCustomerId();
            Double balance = custBalance.getBalance();
            CustomerLine l = new CustomerLine(custId, balance);
            p.custLines.add(l);
        }
        // Taxes amount
        p.m_lsales = new ArrayList<SalesLine>();
        p.m_dSalesTaxes = 0.0;
        List<TaxInfo> taxes = dlSales.getTaxList();
        for (ZTicket.Tax tax : z.getTaxes()) {
            p.m_dSalesTaxes += tax.getAmount();
            String taxId = tax.getId();
            String name = null;
            double rate = 0.0;
            for (TaxInfo t : taxes) {
                if (t.getId().equals(taxId)) {
                    name = t.getName();
                    rate = t.getRate();
                    break;
                }
            }
            SalesLine l = new SalesLine(name, rate, tax.getBase(),
                    tax.getAmount(), taxId);
            p.m_lsales.add(l);
        }
        // Count expected cash
        if (p.hasFunds()) {
            double expectedTotal = 0.0;
            // Get initial fund
            if (p.cashSession.getOpenCash() != null) {
                expectedTotal = p.cashSession.getOpenCash();
            }
            // Add cash payments
            for (PaymentsModel.PaymentsLine line : p.getPaymentLines()) {
                if (line.getType().equals("cash")
                 && line.getCurrency().isMain()) {
                    expectedTotal = Price.add(expectedTotal, line.getValue());
                } else if (line.getType().equals("cashrefund")
                        && line.getCurrency().isMain()) {
                    expectedTotal = Price.add(expectedTotal, line.getValue());
                }
            }
            p.expectedCash = expectedTotal;
            // Add movements to drawerExpectedCash
            for (CashMove move : p.cashMoves) {
                expectedTotal = Price.add(expectedTotal, move.getAmount());
            }
            p.drawerExpectedCash = expectedTotal;
        }
        return p;
    }

    public static PaymentsModel loadOpenInstance(AppView app) throws BasicException {
        CashSession cash = app.getActiveCashSession();
        return loadOpenInstance(cash);
    }
    public static PaymentsModel loadInstance(AppView app) throws BasicException {
        CashSession cash = app.getActiveCashSession();
        return loadInstance(cash);
    }

    public int getPayments() { return m_iPayments.intValue(); }
    public boolean hasCustomersCount() { return custCount != null; }
    public int getCustomersCount() { return custCount; }
    /** Get total CS. */
    public double getSalesBase() { return m_dSalesBase; }
    /** Get total with taxes. */
    public double getTotal() { return m_dPaymentsTotal.doubleValue(); }
    public CashSession getCashSession() { return this.cashSession; }
    public String getHost() { return this.cashRegister.getLabel(); }
    public int getSequence() { return this.cashSession.getSequence(); }
    public Date getDateStart() { return this.cashSession.getOpenDate(); }
    public Date getDateEnd() { return this.cashSession.getCloseDate(); }
    public Double getOpenCash() { return this.cashSession.getOpenCash(); }
    public Double getCloseCash() { return this.cashSession.getCloseCash(); }
    /** Check if cash was counted at open and/or close */
    public boolean hasFunds() {
        return this.cashSession.getOpenCash() != null
                || this.cashSession.getCloseCash() != null;
    }
    public void setCoinCount(Double amount, int count) {
        this.coinCount.put(amount, count);
    }
    public List<Double> getCountedCoins() {
        Set<Double> keys = this.coinCount.keySet();
        List<Double> coins = new ArrayList<Double>();
        coins.addAll(keys);
        Collections.sort(coins);
        return coins;
    }
    public Double getExpectedCash() { return this.expectedCash; }
    public void setExpectedCash(double cash) {
        // Workaround for local cash movements
        this.expectedCash = cash;
    }
    public Double getDrawerExpectedCash() { return this.drawerExpectedCash; }
    public List<CashMove> getCashMoves() { return this.cashMoves; }
    public double getCashMoveAmount() {
        double amount = 0.0;
        for (CashMove move : this.cashMoves) {
            amount = Price.add(amount, move.getAmount());
        }
        return amount;
    }

    public String printHost() {
        return StringUtils.encodeXML(this.cashRegister.getLabel());
    }
    public String printSequence() {
        return Formats.INT.formatValue(this.cashSession.getSequence());
    }
    public String printDateStart() {
        return Formats.TIMESTAMP.formatValue(this.cashSession.getOpenDate());
    }
    public String printDateEnd() {
        return Formats.TIMESTAMP.formatValue(this.cashSession.getCloseDate());
    }
    public String printOpenCash() {
        if (this.cashSession.getOpenCash() != null) {
            return Formats.CURRENCY.formatValue(this.cashSession.getOpenCash());
        } else {
            return "";
        }
    }
    public String printCloseCash() {
        if (this.cashSession.getCloseCash() != null) {
            return Formats.CURRENCY.formatValue(this.cashSession.getCloseCash());
        } else {
            return "";
        }
    }
    public String printExpectedCash() {
        if (this.expectedCash != null) {
            return Formats.CURRENCY.formatValue(this.expectedCash);
        } else {
            return "";
        }
    }
    public String printDrawerExpectedCash() {
        if (this.drawerExpectedCash != null) {
            return Formats.CURRENCY.formatValue(this.drawerExpectedCash);
        } else {
            return "";
        }
    }
    public String printMoveAmount() {
        if (this.drawerExpectedCash != null) {
            return Formats.CURRENCY.formatValue(this.getCashMoveAmount());
        } else {
            return "";
        }
    }
    public String printCashError() {
        if (this.drawerExpectedCash != null) {
            return Formats.CURRENCY.formatValue(Price.add(this.cashSession.getCloseCash(), -this.expectedCash));
        }
        return "";
    }
    public String printCoinValue(double val) {
        return Formats.CURRENCY.formatValue(val);
    }
    public String printCoinCount(double val) {
        return Formats.INT.formatValue(this.coinCount.get(val));
    }
    public String printCoinTotal(double coinVal) {
        return Formats.CURRENCY.formatValue(coinVal * this.coinCount.get(coinVal));
    }

    public String printPayments() {
        return Formats.INT.formatValue(m_iPayments);
    }

    public String printPaymentsTotal() {
        return Formats.CURRENCY.formatValue(m_dPaymentsTotal);
    }

    public List<PaymentsLine> getPaymentLines() {
        return m_lpayments;
    }
    /** Get ticket count. */
    public int getSales() {
        return m_iSales == null ? 0 : m_iSales.intValue();
    }
    /** Prints the number of tickets */
    public String printSales() {
        return Formats.INT.formatValue(m_iSales);
    }
    public String printCustomersCount() {
        return Formats.INT.formatValue(custCount);
    }
    /** Prints the subtotal */
    public String printSalesBase() {
        return Formats.CURRENCY.formatValue(m_dSalesBase);
    }
    /** Print taxes total */
    public String printSalesTaxes() {
        return Formats.CURRENCY.formatValue(m_dSalesTaxes);
    }
    /** Print total */
    public String printSalesTotal() {
        return Formats.CURRENCY.formatValue((m_dSalesBase == null || m_dSalesTaxes == null)
                ? null
                : m_dSalesBase + m_dSalesTaxes);
    }
    /** Get average sales per customer */
    public String printSalesPerCustomer() {
        if (custCount != 0) {
            return Formats.CURRENCY.formatValue((m_dSalesBase + m_dSalesTaxes) / custCount);
        } else {
            return "";
        }
    }
    public List<CustomerLine> getCustLines() {
        return this.custLines;
    }

    public List<SalesLine> getSaleLines() {
        return m_lsales;
    }
    public List<CategoryLine> getCategoryLines() {
        return this.catSales;
    }
    public List<ZTicket.CategoryTax> getCatTaxes() { return this.catTaxes; }

    public AbstractTableModel getPaymentsModel() {
        return new AbstractTableModel() {
            private static final long serialVersionUID = -4239152134097143269L;
            public String getColumnName(int column) {
                return AppLocal.getIntString(PAYMENTHEADERS[column]);
            }
            public int getRowCount() {
                return m_lpayments.size();
            }
            public int getColumnCount() {
                return PAYMENTHEADERS.length;
            }
            public Object getValueAt(int row, int column) {
                PaymentsLine l = m_lpayments.get(row);
                switch (column) {
                case 0: return new Object[] {l.getType(), l.getCurrency().getName(), l.getCurrency().isMain()};
                case 1: return l;
                default: return null;
                }
            }
        };
    }

    public static class SalesLine {

        private String taxId;
        private String m_SalesTaxName;
        private Double taxRate;
        private Double taxBase;
        private Double m_SalesTaxes;

        public SalesLine(String taxName, double rate, double base,
                double amount, String taxId) {
            this.m_SalesTaxName = taxName;
            this.taxRate = rate;
            this.taxBase = base;
            this.m_SalesTaxes = amount;
            this.taxId = taxId;
        }
        public String printTaxName() {
            return m_SalesTaxName;
        }
        public String printTaxRate() {
        	return Formats.PERCENT.formatValue(this.taxRate);
        }
        public String printTaxes() {
            return Formats.CURRENCY.formatValue(m_SalesTaxes);
        }
        public String printTaxBase() {
            return Formats.CURRENCY.formatValue(this.taxBase);
        }
        public String getTaxName() {
            return m_SalesTaxName;
        }
        public Double getTaxRate() {
            return this.taxRate;
        }
        public Double getTaxes() {
            return m_SalesTaxes;
        }
        public Double getTaxBase() {
            return this.taxBase;
        }
        public String getTaxId() {
            return this.taxId;
        }
    }

    public AbstractTableModel getSalesModel() {
        return new AbstractTableModel() {
            private static final long serialVersionUID = 7363987099264006915L;
            public String getColumnName(int column) {
                return AppLocal.getIntString(SALEHEADERS[column]);
            }
            public int getRowCount() {
                return m_lsales.size();
            }
            public int getColumnCount() {
                return SALEHEADERS.length;
            }
            public Object getValueAt(int row, int column) {
                SalesLine l = m_lsales.get(row);
                switch (column) {
                case 0: return l.getTaxName();
                case 1: return l.getTaxBase();
                case 2: return l.getTaxes();
                default: return null;
                }
            }
        };
    }

    public static class PaymentsLine {

        private String m_PaymentType;
        private CurrencyInfo currency;
        private Double m_PaymentValue;

        public PaymentsLine(String type, CurrencyInfo currency, double value) {
            this.m_PaymentType = type;
            this.currency = currency;
            this.m_PaymentValue = value;
        }

        public String printType() {
            return AppLocal.getIntString("transpayment." + m_PaymentType);
        }
        public String getType() {
            return m_PaymentType;
        }
        public String printValue() {
            Formats.setAltCurrency(this.currency);
            return Formats.CURRENCY.formatValue(m_PaymentValue);
        }
        public Double getValue() {
            return m_PaymentValue;
        }
        public CurrencyInfo getCurrency() {
            return this.currency;
        }
    }

    public AbstractTableModel getCategoriesModel() {
        return new AbstractTableModel() {
            private static final long serialVersionUID = 21434144561863896L;
            public String getColumnName(int column) {
                return AppLocal.getIntString(CATEGORYHEADERS[column]);
            }
            public int getRowCount() {
                return catSales.size();
            }
            public int getColumnCount() {
                return CATEGORYHEADERS.length;
            }
            public Object getValueAt(int row, int column) {
                CategoryLine l = catSales.get(row);
                switch (column) {
                case 0: return l.getCategory();
                case 1: return l.getValue();
                default: return null;
                }
            }
        };
    }

    public static class CategoryLine {
        private String category;
        private String reference;
        private Double amount;

        public CategoryLine(String category, String reference, double amount) {
            this.category = category;
            this.reference = reference;
            this.amount = amount;
        }
        public String getCategory() { return this.category; }
        public String getReference() { return this.reference; }
        public String printCategory() { return this.category; }
        public String printValue() {
            return Formats.CURRENCY.formatValue(this.amount);
        }
        public Double getValue() { return this.amount; }
    }

    public static class CustomerLine {
        private String customerId;
        private Double balance;

        public CustomerLine(String customerId, double balance) {
            this.customerId = customerId;
            this.balance = balance;
        }
        public String getCustomerId() {
            return this.customerId;
        }
        public Double getBalance() {
            return this.balance;
        }
    }
}
