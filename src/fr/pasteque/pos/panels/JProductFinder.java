//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.panels;

import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.ProductRenderer;
import javax.swing.*;
import java.awt.*;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JAbstractChoiceDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.WidgetsBuilder;

/**
 * Popup to search products and select one.
 * @author adrianromero
 */
public class JProductFinder extends JAbstractChoiceDialog<ProductInfoExt>
implements ShortcutListener
{
    private static final long serialVersionUID = 236478337572762103L;

    /** Creates new form JProductFinder */
    private JProductFinder(java.awt.Component parent,
            DataLogicSales dlSales) {
        super(parent, null);
        jListProducts.setCellRenderer(new ProductRenderer());
        getRootPane().setDefaultButton(okBtn);
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                if (this.refInput.isActive() || this.labelInput.isActive()
                        || this.searchBtn.hasFocus()) {
                    this.searchBtn.doClick(Shortcuts.CLICK_TIME);
                } else {
                    this.confirm();
                }
                return true;
            default: // Ignore
                return false;
        }
    }

    /**
     * Open the dialog and return the selected value.
     */
    public ProductInfoExt findProduct() {
        this.labelInput.activate();
        this.open();
        return this.getChoice();
    }

    /**
     * All-in-one function to create the dialog and return the selected value.
     */
    public static ProductInfoExt showMessage(Component parent, DataLogicSales dlSales) {
        JProductFinder finder = new JProductFinder(parent, dlSales);
        return finder.findProduct();
    }

    private static class MyListData<T> extends javax.swing.AbstractListModel<T>
    {
        private static final long serialVersionUID = 3590940592601895004L;
        private java.util.List<T> m_data;

        public MyListData(java.util.List<T> data) {
            m_data = data;
        }

        public T getElementAt(int index) {
            return m_data.get(index);
        }

        public int getSize() {
            return m_data.size();
        }
    }

    private void onSearch() {
        try {
            String ref = this.refInput.getText().trim();
            String label = this.labelInput.getText().trim();
            DataLogicSales dlSales = new DataLogicSales();
            java.util.List<ProductInfoExt> data = dlSales.searchProducts(label, ref);
            ((ProductRenderer)jListProducts.getCellRenderer()).clearCategoryCache();
            jListProducts.setModel(new MyListData<ProductInfoExt>(data));
            if (jListProducts.getModel().getSize() > 0) {
                jListProducts.setSelectedIndex(0);
                jListProducts.grabFocus();
            }
        } catch (BasicException e) {
            e.printStackTrace();
        }
    }

    /**
     * Pick the current selected product and close the dialog.
     */
    protected void confirm() {
        this.setChoice((ProductInfoExt) jListProducts.getSelectedValue());
        super.confirm();
    }

    protected void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnspacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;

        this.keyPad = new JEditorKeys();
        javax.swing.JLabel labelLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.prodname"));
        javax.swing.JLabel refLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.prodref"));
        this.labelInput = new JEditorString();
        this.labelInput.addEditorKeys(this.keyPad);
        this.refInput = new JEditorString();
        this.refInput.addEditorKeys(this.keyPad);
        searchBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("execute.png"),
                AppLocal.getIntString("button.executefilter"),
                WidgetsBuilder.SIZE_MEDIUM);
        searchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSearch();
            }
        });
        javax.swing.JScrollPane scrollContainer = new javax.swing.JScrollPane();
        jListProducts = new javax.swing.JList<ProductInfoExt>();
        jListProducts.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListProducts.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListProductsValueChanged(evt);
            }
        });
        jListProducts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListProductsMouseClicked(evt);
            }
        });

        okBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);
        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirm();
            }
        });
        cancelBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close();
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(AppLocal.getIntString("form.productslist"));


        scrollContainer.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollContainer.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));
        scrollContainer.setViewportView(jListProducts);

        this.setLayout(new GridBagLayout());

        GridBagConstraints cstr = null;
        // Left part: filter and list
        javax.swing.JPanel searchPanel = new javax.swing.JPanel();
        searchPanel.setLayout(new GridBagLayout());
        int y = 0;
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(btnspacing, marginInset, btnspacing, marginInset);
        searchPanel.add(labelLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y++;
        cstr.insets = new Insets(marginInset, 0, btnspacing, marginInset);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        searchPanel.add(labelInput, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        searchPanel.add(refLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y++;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        searchPanel.add(refInput, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y++;
        cstr.gridwidth = 2;
        cstr.anchor = GridBagConstraints.CENTER;
        cstr.insets = new Insets(btnspacing, btnspacing, btnspacing, btnspacing);
        searchPanel.add(searchBtn, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y++;
        cstr.gridwidth = 2;
        cstr.weightx = 1;
        cstr.weighty = 1;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.insets = new Insets(btnspacing, marginInset, btnspacing, marginInset);
        searchPanel.add(scrollContainer, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.weightx = 1;
        cstr.weighty = 1;
        cstr.fill = GridBagConstraints.BOTH;
        this.add(searchPanel, cstr);

        // Right part: keypad
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnspacing, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.NORTH;
        this.add(this.keyPad, cstr);


        // Bottom part: buttons
        javax.swing.JPanel buttonLine = new JPanel();
        buttonLine.setLayout(new FlowLayout(FlowLayout.RIGHT, marginInset, btnspacing));
        buttonLine.add(okBtn);
        buttonLine.add(cancelBtn);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.gridwidth = 2;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        this.add(buttonLine, cstr);

        this.setMinHeightRatio(0.8);
    }

    private void jListProductsMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            confirm();
        }
    }

    private void jListProductsValueChanged(javax.swing.event.ListSelectionEvent evt) {
        okBtn.setEnabled(jListProducts.getSelectedValue() != null);
    }

    private JEditorString labelInput;
    private JEditorString refInput;
    private javax.swing.JButton searchBtn;
    private javax.swing.JList<ProductInfoExt> jListProducts;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton okBtn;
    private JEditorKeys keyPad;
}
