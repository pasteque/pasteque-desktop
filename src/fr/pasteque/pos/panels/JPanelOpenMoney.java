//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.panels;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.caching.CashSessionsCache;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.forms.JPanelView;
import fr.pasteque.pos.forms.JPrincipalApp;
import fr.pasteque.pos.printer.TicketPrinterException;
import fr.pasteque.pos.printer.document.TicketParser;
import fr.pasteque.pos.printer.document.ZTicketPrintScript;
import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.CoinCountButton;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JPanelOpenMoney
extends JPanel
implements JPanelView, CoinCountButton.Listener
{
    private static final long serialVersionUID = 3282376629001471309L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.panels.JPanelOpenMoney");
    private static final int PRINTER_COUNT = 3;

    private AppView appView;
    private JPrincipalApp principalApp;
    private DataLogicSystem dlSystem;
    private String targetTask;
    private JPanel coinCountBtnsContainer;
    private List<CoinCountButton> coinButtons;
    private JEditorKeys keypad;
    private JLabel totalAmount;
    private double total;
    private PaymentsModel m_PaymentsToOpen;
    private TicketParser m_TTP;
    private boolean[] printAvailable;
    private boolean[] printOnOpen;
    private int printCount;
    private JLabel jlblPrintCount;

    public JPanelOpenMoney(AppView appView, JPrincipalApp principalApp,
            String targetTask) {
        this.appView = appView;
        this.principalApp = principalApp;
        this.targetTask = targetTask;
        this.dlSystem = new DataLogicSystem();
        this.coinButtons = new ArrayList<CoinCountButton>();
        this.printAvailable = new boolean[PRINTER_COUNT];
        this.printOnOpen = new boolean[PRINTER_COUNT];
        AppConfig cfg = AppConfig.loadedInstance;
        if (!"Not defined".equals(cfg.getProperty("machine.printer"))
                && cfg.getBoolean("machine.printer.filter.z")) {
            this.printAvailable[0] = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.2"))
                && cfg.getBoolean("machine.printer.2.filter.z")) {
            this.printAvailable[1] = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.3"))
                && cfg.getBoolean("machine.printer.3.filter.z")) {
            this.printAvailable[2] = true;
        }
        initComponents();
        boolean showCount = cfg.getProperty("ui.countmoney").equals("1");
        boolean canOpen = this.principalApp.getUser().hasPermission("button.openmoney");
        m_TTP = new TicketParser(this.appView.getDeviceTicket(), this.dlSystem);
        if (showCount && canOpen) {
            String code = this.dlSystem.getResourceAsXML("payment.cash");
            if (code != null) {
                try {
                    ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.BEANSHELL);
                    script.put("payment", new ScriptCash());
                    script.eval(code);
                } catch (ScriptException e) {
                    MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotexecute"), e);
                    msg.show(this);
                }
            }
        }
        this.resetPrintState();
    }

    public JComponent getComponent() {
        return this;
    }

    public String getTitle() {
        return AppLocal.getIntString("Menu.OpenTPV");
    }

    public boolean requiresOpenedCash() {
        return false;
    }

    public void activate() throws BasicException {
        this.m_PaymentsToOpen = PaymentsModel.loadOpenInstance(this.appView);
        this.updateAmount();
        // Open drawer if allowed to open and counting
        AppConfig cfg = AppConfig.loadedInstance;
        if (this.principalApp.getUser().hasPermission("button.openmoney")
                && cfg.getProperty("ui.countmoney").equals("1")) {
            String code = this.dlSystem.getResourceAsXML("Printer.OpenDrawer");
            if (code != null) {
                try {
                    ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                    this.m_TTP.printTicket(script.eval(code).toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // Set recall last coin count state
        CashSession cashSess = this.appView.getActiveCashSession();
        if (this.jbtnRecallLastCoinCount != null) {
            this.jbtnRecallLastCoinCount.setEnabled(cashSess.hasLastCloseCoinCount());
        }
    }

    public boolean deactivate() {
        this.resetCashCount();
        return true;
    }

    private void resetPrintState() {
        for (int i = 0; i < PRINTER_COUNT; i++) {
            this.printOnOpen[i] = true;
        }
        try {
            this.printCount = Integer.parseInt(AppConfig.loadedInstance.getProperty("ui.printticketcount"));
        } catch (NumberFormatException e) {
            this.printCount = 1;
        }
    }


    private void resetCashCount() {
        this.total = 0.0;
        for (CoinCountButton btn : this.coinButtons) {
            btn.reset();
        }
    }

    /**
     * Recall count from the last close count and update the buttons.
     */
    private void recallLastCoinCount() {
        CashSession cashSess = this.appView.getActiveCashSession();
        if (!cashSess.hasLastCloseCoinCount()) {
            return;
        }
        this.resetCashCount();
        for (Double coin : cashSess.getLastCloseCoinCount().keySet()) {
            for (CoinCountButton btn : this.coinButtons) {
                if (coin.equals(Double.valueOf(btn.getValue()))) {
                    btn.setCount(cashSess.getLastCloseCoinCount().get(coin));
                }
            }
        }
        this.updateAmount();
    }

    public class ScriptCash {
        private int x, y;
        private int btnSpacing;
        private ThumbNailBuilder tnb;
        public ScriptCash() {
            AppConfig cfg = AppConfig.loadedInstance;
            this.btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
            this.tnb = new ThumbNailBuilder(64, 54, "cash.png");
        }
        public void addButton(String image, double amount) {
            ImageIcon icon = new ImageIcon(this.tnb.getThumbNailText(dlSystem.getResourceAsImage(image), Formats.CURRENCY.formatValue(amount)));
            JPanelOpenMoney parent = JPanelOpenMoney.this;
            CoinCountButton btn = new CoinCountButton(icon, amount,
                    parent.keypad, parent);
            parent.coinButtons.add(btn);
            GridBagConstraints cstr = new GridBagConstraints();
            cstr.gridx = this.x;
            cstr.gridy = this.y;
            cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing,
                    btnSpacing);
            parent.coinCountBtnsContainer.add(btn.getComponent(), cstr);
            if (this.x == 3) {
                this.x = 0;
                this.y++;
            } else {
                this.x++;
            }
        }
    }

    /** From CoinCount.Listener */
    public void coinAdded(double amount, int newCount) {
        this.total += amount;
        this.totalAmount.setText(Formats.CURRENCY.formatValue(this.total));
    }
    /** From CoinCount.Listener */
    public void countUpdated(double amount, Integer newCount) {
        this.updateAmount();
    }

    public void updateAmount() {
        if (this.totalAmount == null) {
            return;
        }
        this.total = 0.0;
        for (CoinCountButton btn : this.coinButtons) {
            this.total += btn.getAmount();
        }
        this.totalAmount.setText(Formats.CURRENCY.formatValue(this.total));
    }

    private boolean isPrintSelected(int printerNum) {
        return this.printAvailable[printerNum] && this.printOnOpen[printerNum];
    }

    private void togglePrint(int printerNum) {
        this.printOnOpen[printerNum] = !this.printOnOpen[printerNum];
        this.refreshPrint();
    }

    private void refreshPrint() {
        this.jbtnPrintOpen.setSelected(this.isPrintSelected(0));
        this.jbtnPrintOpen2.setSelected(this.isPrintSelected(1));
        this.jbtnPrintOpen3.setSelected(this.isPrintSelected(2));
        if (this.isPrintSelected(0) || this.isPrintSelected(1) || this.isPrintSelected(2)) {
            this.jlblPrintCount.setText(String.valueOf(this.printCount));
            this.jbtnPrintPlus.setEnabled(true);
            this.jbtnPrintMinus.setEnabled(this.printCount > 1);
        } else {
            this.jlblPrintCount.setText("0");
            this.jbtnPrintPlus.setEnabled(false);
            this.jbtnPrintMinus.setEnabled(false);
        }
    }


    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        boolean showCount = cfg.getProperty("ui.countmoney").equals("1");
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints cstr = null;

        this.setLayout(gridbag);

        JLabel cashClosed = WidgetsBuilder.createLabel(AppLocal.getIntString("message.cashisclosed"));

        boolean canOpen = this.principalApp.getUser().hasPermission("button.openmoney");
        if (canOpen) {
            JButton openCash = WidgetsBuilder.createButton(WidgetsBuilder.createIcon("open_cash.png"), AppLocal.getIntString("label.opencash"), WidgetsBuilder.SIZE_BIG);
            openCash.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    openCash(evt);
                }
            });
            if (!showCount) {
                // Single button ui
                cstr = new GridBagConstraints();
                cstr.gridx = 0;
                cstr.gridy = 0;
                cstr.weighty = 0.5;
                this.add(cashClosed, cstr);
                cstr = new GridBagConstraints();
                cstr.gridx = 0;
                cstr.gridy = 1;
                cstr.weighty = 0.5;
                this.add(openCash, cstr);
            } else {
                this.jbtnRecallLastCoinCount = WidgetsBuilder.createButton(ImageLoader.readImageIcon("recall.png"), AppLocal.getIntString("Button.RecallLastCoinCount"), WidgetsBuilder.SIZE_MEDIUM);
                this.jbtnRecallLastCoinCount.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        recallLastCoinCount();
                    }
                });
                // Show count ui
                cstr = new GridBagConstraints();
                cstr.gridx = 0;
                cstr.gridy = 0;
                cstr.gridwidth = 2;
                cstr.weighty = 0.2;
                this.add(cashClosed, cstr);
                // coin buttons
                this.coinCountBtnsContainer = new JPanel();
                this.coinCountBtnsContainer.setLayout(new GridBagLayout());
                cstr = new GridBagConstraints();
                cstr.gridx = 0;
                cstr.gridy = 1;
                cstr.weighty = 0.5;
                cstr.weightx = 1.0;
                cstr.fill = GridBagConstraints.BOTH;
                this.add(this.coinCountBtnsContainer, cstr);
                // keypad and input
                int y = 0;
                JPanel inputContainer = new JPanel();
                inputContainer.setLayout(new GridBagLayout());
                this.keypad = new JEditorKeys();
                cstr = new GridBagConstraints();
                cstr.gridx = 0; cstr.gridy = y++;
                cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing,
                        btnSpacing);
                inputContainer.add(this.keypad, cstr);
                JLabel amountLabel = WidgetsBuilder.createLabel(AppLocal.tr("Label.CashSession.CashAmount"));
                cstr = new GridBagConstraints();
                cstr.gridx = 0; cstr.gridy = y++;
                cstr.anchor = GridBagConstraints.FIRST_LINE_START;
                inputContainer.add(amountLabel, cstr);
                this.totalAmount = WidgetsBuilder.createImportantLabel("");
                WidgetsBuilder.inputStyle(totalAmount);
                cstr = new GridBagConstraints();
                cstr.gridx = 0; cstr.gridy = y++;
                cstr.fill = GridBagConstraints.BOTH;
                cstr.anchor = GridBagConstraints.CENTER;
                inputContainer.add(totalAmount, cstr);
                cstr = new GridBagConstraints();
                cstr.gridx = 0; cstr.gridy = y++;
                cstr.fill = GridBagConstraints.BOTH;
                cstr.insets = new Insets(2 * btnSpacing, btnSpacing, btnSpacing,
                        btnSpacing);
                inputContainer.add(this.jbtnRecallLastCoinCount, cstr);
                cstr = new GridBagConstraints();
                cstr.gridx = 1;
                cstr.gridy = 1;
                cstr.weightx = 0.5;
                cstr.anchor = GridBagConstraints.CENTER;
                cstr.insets = new Insets(0, btnSpacing, btnSpacing, btnSpacing);
                this.add(inputContainer, cstr);
                // buttons line
                JPanel buttonsContainer = new JPanel();
                buttonsContainer.setLayout(new FlowLayout(FlowLayout.TRAILING,
                        marginInset, btnSpacing));
                jbtnPrintOpen = new javax.swing.JToggleButton();
                jbtnPrintOpen.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
                WidgetsBuilder.adaptSize(jbtnPrintOpen, WidgetsBuilder.SIZE_MEDIUM);
                jbtnPrintOpen.setSelected(true);
                jbtnPrintOpen.setFocusPainted(false);
                jbtnPrintOpen.setFocusable(false);
                jbtnPrintOpen.setRequestFocusEnabled(false);
                jbtnPrintOpen.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        printActionPerformed(evt, 0);
                    }
                });
                jbtnPrintOpen2 = new javax.swing.JToggleButton();
                jbtnPrintOpen2.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
                jbtnPrintOpen2.setText("2");
                WidgetsBuilder.adaptSize(jbtnPrintOpen2, WidgetsBuilder.SIZE_MEDIUM);
                jbtnPrintOpen2.setSelected(true);
                jbtnPrintOpen2.setFocusPainted(false);
                jbtnPrintOpen2.setFocusable(false);
                jbtnPrintOpen2.setRequestFocusEnabled(false);
                jbtnPrintOpen2.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        printActionPerformed(evt, 1);
                    }
                });
                jbtnPrintOpen3 = new javax.swing.JToggleButton();
                jbtnPrintOpen3.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
                jbtnPrintOpen3.setText("3");
                WidgetsBuilder.adaptSize(jbtnPrintOpen3, WidgetsBuilder.SIZE_MEDIUM);
                jbtnPrintOpen3.setSelected(true);
                jbtnPrintOpen3.setFocusPainted(false);
                jbtnPrintOpen3.setFocusable(false);
                jbtnPrintOpen3.setRequestFocusEnabled(false);
                jbtnPrintOpen3.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        printActionPerformed(evt, 2);
                    }
                });

                boolean hasPrinter = false;
                if (this.printAvailable[2]) {
                    buttonsContainer.add(jbtnPrintOpen3);
                    hasPrinter = true;
                }
                if (this.printAvailable[1]) {
                    buttonsContainer.add(jbtnPrintOpen2);
                    hasPrinter = true;
                }
                if (this.printAvailable[0]) {
                    buttonsContainer.add(jbtnPrintOpen);
                    hasPrinter = true;
                }

                this.jlblPrintCount = WidgetsBuilder.createLabel("1");
                jbtnPrintPlus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_plus.png"));
                jbtnPrintMinus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_minus.png"));
                jbtnPrintPlus.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        printPlusActionPerformed(evt);
                    }
                });
                jbtnPrintMinus.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        printMinusActionPerformed(evt);
                    }
                });
                jbtnPrintPlus.setFocusable(false);
                jbtnPrintMinus.setFocusable(false);
                if (hasPrinter) {
                    buttonsContainer.add(jbtnPrintMinus);
                    buttonsContainer.add(jlblPrintCount);
                    buttonsContainer.add(jbtnPrintPlus);
                }
                JPanel separator = new JPanel();
                separator.setMinimumSize(new java.awt.Dimension(btnSpacing * 2, 0));
                buttonsContainer.add(separator);
                buttonsContainer.add(openCash);
                cstr = new GridBagConstraints();
                cstr.gridx = 0;
                cstr.gridy = 2;
                cstr.gridwidth = 2;
                cstr.fill = GridBagConstraints.HORIZONTAL;
                this.add(buttonsContainer, cstr);
            }
        } else {
            cstr = new GridBagConstraints();
            cstr.gridx = 0;
            cstr.gridy = 0;
            this.add(cashClosed, cstr);
        }
    }

    private void openCash(java.awt.event.ActionEvent evt) {
        // Open cash
        Date now = new Date();
        CashSession cashSess = this.appView.getActiveCashSession();
        cashSess.open(now);
        if (AppConfig.loadedInstance.getProperty("ui.countmoney").equals("1")) {
            cashSess.setOpenCash(this.total);
        }
        // Save cash session on server
        try {
            this.dlSystem.saveCashSession(cashSess);
        } catch (BasicException e) {
                cashSess.cancelOpen();
                MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE,
                        AppLocal.getIntString("message.cannotopencash"), e);
                msg.show(this);
                return;
        }
        // Save cash session in cache
        try {
            CashSessionsCache.saveCashSession(cashSess);
        } catch (BasicException e) {
                logger.log(Level.SEVERE, "Unable to cache cash session.", e);
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                        "Unable to cache cash session", e));
        }

        // Prepere paymentsToOpen
        AppConfig cfg = AppConfig.loadedInstance;
        boolean showCount = cfg.getProperty("ui.countmoney").equals("1");
        if (showCount) {
            for (CoinCountButton ccb : this.coinButtons) {
                if (ccb.getCount() > 0) {
                    this.m_PaymentsToOpen.setCoinCount(ccb.getValue(),
                            ccb.getCount());
                }
            }
        }

        // Print ticket
        if (this.isPrintSelected(0) || this.isPrintSelected(1) || this.isPrintSelected(2)) {
            for (int i = 0; i < 3; i++) {
                if (!this.isPrintSelected(i)) {
                    continue;
                }
                String cplKey;
                if (i == 0) {
                    cplKey = "machine.printer.cpl";
                } else {
                    cplKey = "machine.printer." + (i + 1) + ".cpl";
                }
                Double cpl = AppConfig.loadedInstance.getNumber(cplKey);
                ZTicketPrintScript printScript = new ZTicketPrintScript(cpl.intValue());
                String sresource = printScript.getOpenXml(m_PaymentsToOpen, i + 1);
                if (sresource == null) {
                    MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"));
                    msg.show(this);
                } else {
                    // Put objects references and run resource script
                    try {
                        ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                        script.put("payments", m_PaymentsToOpen);
                        for (int count = 0; count < this.printCount; count++) {
                            m_TTP.printTicket(script.eval(sresource).toString());
                        }
                    } catch (ScriptException e) {
                        MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                        msg.show(this);
                    } catch (TicketPrinterException e) {
                        MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                        msg.show(this);
                    }
                }
            }
        }
        // Go to original task
        this.principalApp.showTask(this.targetTask);
    }

    private void printActionPerformed(java.awt.event.ActionEvent evt, int printerNum) {
        this.togglePrint(printerNum);
    }

    private void printPlusActionPerformed(java.awt.event.ActionEvent evt) {
        this.printCount++;
        this.refreshPrint();
    }

    private void printMinusActionPerformed(java.awt.event.ActionEvent evt) {
        this.printCount--;
        this.refreshPrint();
    }

    private javax.swing.JToggleButton jbtnPrintOpen;
    private javax.swing.JToggleButton jbtnPrintOpen2;
    private javax.swing.JToggleButton jbtnPrintOpen3;
    private javax.swing.JButton jbtnRecallLastCoinCount;
    private javax.swing.JButton jbtnPrintPlus;
    private javax.swing.JButton jbtnPrintMinus;
}
