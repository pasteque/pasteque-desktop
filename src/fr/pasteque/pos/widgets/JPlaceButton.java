//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.widgets;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.sales.SharedTicketInfo;
import fr.pasteque.pos.sales.restaurant.Place;
import fr.pasteque.pos.util.ThumbNailBuilder;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
 * Place selection button for restaurant maps.
 */
public class JPlaceButton extends JButton
{
    private static final long serialVersionUID = 1472007897262938255L;
    private static final Image ICO_OCU = ImageLoader.readImage("tkt_place_filled.png");
    private static final Image ICO_FRE = new BufferedImage(22, 22, BufferedImage.TYPE_INT_ARGB);

    /** Place name, non-nullable and use as default label. */
    private String placeLabel;
    /** Optional label to override the default place label. */
    private String customLabel;
    private boolean occupied;
    private boolean dirty;
    private int placeX;
    private int placeY;
    private ThumbNailBuilder tnb;

    public JPlaceButton(Place p, ThumbNailBuilder tnb) {
        super();
        this.placeLabel = p.getName();
        this.placeX = p.getX();
        this.placeY = p.getY();
        this.tnb = tnb;
        this.dirty = true;
        this.setFocusPainted(false);
        this.setFocusable(false);
        this.setRequestFocusEnabled(false);
        this.setSize(new Dimension(tnb.getWidth(), tnb.getHeight()));
    }

    /**
     * Set occupied state. It will change the icon on the button.
     * @param occupied The new value.
     * @return True when the state has changed, false otherwise.
     */
    public boolean setOccupied(boolean occupied) {
        if (this.occupied != occupied) {
            this.occupied = occupied;
            this.dirty = true;
            return true;
        }
        return false;
    }

    /**
     * Check the current state.
     * @return The visual state.
     */
    public boolean isOccupied() {
        return this.occupied;
    }

    /**
     * Set the label of the button.
     * @return True when the state has changed, false otherwise.
     */
    public boolean setCustomLabel(String label) {
        if (this.customLabel == null) {
            this.customLabel = label;
            return this.customLabel != null;
        }
        if (!this.customLabel.equals(label)) {
            this.customLabel = label;
            this.dirty = true;
            return true;
        }
        return false;
    }

    /**
     * Set the state of the button to reflect the given order.
     * @param order The order to read from.
     * @return True when the state has changed, false otherwise.
     */
    public boolean setState(SharedTicketInfo order) {
        boolean ret;
        if (order == null) {
            ret = this.setOccupied(false);
            ret = this.setCustomLabel(null) || ret;
            this.dirty = this.dirty || ret;
            return ret;
        }
        ret = this.setOccupied(!order.isEmpty());
        ret = this.setCustomLabel(order.getPlaceLabel(this.placeLabel)) || ret;
        this.dirty = this.dirty || ret;
        return ret;
    }

    /**
     * Update the state of the button according to its state.
     * Does nothing if its state hasn’t changed.
     */
    public void update() {
        if (!this.dirty) {
            return;
        }
        Image icon = this.occupied ? ICO_OCU : ICO_FRE;
        String lbl = this.customLabel;
        if (lbl == null || lbl.isEmpty()) {
            lbl = this.placeLabel;
        }
        this.setIcon(new ImageIcon(this.tnb.getThumbNailText(icon, lbl)));
        this.revalidate();
        this.dirty = false;
    }

    /**
     * Get the raw X coordinate in the floor.
     * @return The raw X coordinate.
     */
    public int getPlaceX() {
        return this.placeX;
    }

    /**
     * Get the raw Y coordinate in the floor.
     * @return The raw Y coordinate.
     */
    public int getPlaceY() {
        return this.placeY;
    }
}
