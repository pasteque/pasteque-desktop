//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.widgets;

import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.sales.OrderManager;
import fr.pasteque.pos.ticket.TicketInfo;

/**
 * Information textbar to show various informations on the sales screen.
 * It can have two texts to show from different sources.
 * The widget shows the order message, or the info message if no order message
 * is set.
 */
public class JSalesInfoBox extends javax.swing.JTextField
{
    private static final long serialVersionUID = -7770814669683085068L;

    /** Info displayed in the message box relative to the order. */
    private String orderMessage = "";
    /** Temporary info displayed in the message box. */
    private String infoMessage = "";

    /**
     *
     */
    public JSalesInfoBox() {
        super();
        this.setFont(WidgetsBuilder.getFont(WidgetsBuilder.SIZE_MEDIUM));
        this.setRequestFocusEnabled(false);
        this.setFocusable(false);
    }

    /**
     * Set the message to display in the info box when no other info
     * is shown.
     * @param message The message to display. Use an empty string to clear.
     */
    public void setOrderMessage(String message) {
        if (message == null) {
            message = "";
        }
        this.orderMessage = message;
        this.update();
    }

    /**
     * Set the order message from an order.
     * @param order The order to take the box state from.
     */
    public void updateOrderMessage(TicketInfo order) {
        if (order == null) {
            return;
        }
        if (order.getCustomer() != null) {
            // Show debt and prepaid amount
            CustomerInfoExt customer = order.getCustomer();
            this.setOrderMessage(AppLocal.tr("MsgBox.CustomerInfo",
                    customer.getName(), customer.printPrepaid(),
                    customer.printCurDebt()));
        } else {
            this.setOrderMessage("");
        }
    }

    /**
     * Update the order message from the current order.
     * @param manager The manager to get current order from.
     */
    public void updateOrderMessage(OrderManager manager) {
        this.updateOrderMessage(manager.getOrderContent());
    }

    /**
     * Set the message to display in the info box. It will be shown instead
     * of the order message until it is discarded.
     * @param message The message to display. Use an empty string to clear.
     */
    public void setInfoMessage(String message) {
        if (message == null) {
            message = "";
        }
        this.infoMessage = message;
        this.update();
    }

    /**
     * Show either the info message or order message if any of them.
     */
    private void update() {
        if (!"".equals(this.infoMessage)) {
            this.setText(this.infoMessage);
        } else if (!"".equals(this.orderMessage)) {
            this.setText(this.orderMessage);
        } else {
            this.setText("");
        }
    }
}
