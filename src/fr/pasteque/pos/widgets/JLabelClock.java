//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.widgets;

import fr.pasteque.pos.forms.AppLocal;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;

/**
 * A JLabel that updates itself with the current time.
 * The clock timer must be manually stared and stopped with activate()
 * and deactivate(). Not deactivating the clock will result in a runtime error.
 */
public class JLabelClock extends JLabel
{
    private static final long serialVersionUID = 6595095495883858602L;

    /** The auto-updater timer. */
    protected Timer clockTimer;

    /** Create a medium sized clock. */
    public JLabelClock() {
        super();
        WidgetsBuilder.setupLabel(this, WidgetsBuilder.SIZE_MEDIUM);
        this.setText("00:00");
    }

    /** Start the clock. */
    public void activate() {
        // Start clock
        this.updateClock();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 1);
        c.set(Calendar.SECOND, 0);
        this.clockTimer = new Timer();
        this.clockTimer.schedule(new TimerTask(){
                public void run() {
                    JLabelClock.this.updateClock();
                }
            }, c.getTime(), 60000);
    }

    /**
     * Stop the clock. Call this before removing the clock widget
     * from the GUI.
     */
    public void deactivate() {
        this.clockTimer.cancel();
        this.clockTimer = null;
    }

    /**
     * Update the clock to the current time.
     */
    protected void updateClock() {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minutes = c.get(Calendar.MINUTE);
        String strHour = String.valueOf(hour);
        if (strHour.length() == 1) {
            strHour = "0" + strHour;
        }
        String strMinute = String.valueOf(minutes);
        if (strMinute.length() == 1) {
            strMinute = "0" + strMinute;
        }
        this.setText(AppLocal.tr("Clock.Format", strHour, strMinute));
    }

    /** Get the currently displayed time. */
    public String getClock() {
        return this.getText();
    }
}
