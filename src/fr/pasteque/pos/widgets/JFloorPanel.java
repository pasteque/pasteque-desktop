//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.widgets;

import fr.pasteque.pos.util.FloorCoordStretcher;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.border.CompoundBorder;
import javax.swing.JPanel;

/**
 * Panel to draw a floor and its places. The coordinate of each place is
 * recomputed locally to fit the whole floor inside itself by stretching or
 * shrinking the map while respecting the aspect ratio.
 * The panel needs to know the size of the buttons to add margins but does not
 * update the actual size of the buttons.
 * @see fr.pasteque.pos.util.FloorCoordStretcher
 */
public class JFloorPanel extends JPanel
{
    private static final long serialVersionUID = 3649666776094581419L;
    private static java.awt.Image defaultImageCache = null;
    /**
     * Get the default image for a floor.
     * @return The default image or null if not found. It is cached once loaded.
     */
    public static java.awt.Image defaultImage() {
        if (defaultImageCache == null) {
            try {
                defaultImageCache = fr.pasteque.data.loader.ImageLoader.readImage("floor_map.png");
            } catch (Exception fnfe) {
            }
        }
        return defaultImageCache;
    }

    /** Whether the component should be updated or not. */
    private boolean dirty;

    private FloorCoordStretcher stretcher;
    private BufferedImage backgroundImage;
    private List<JPlaceButton> places;
    private String title;

    /**
     * Create a panel with explicitely defined button size.
     * @param buttonWidth The width in pixels of a place button.
     * @param buttonHeight The height in pixels of a place button.
     * @param margin The minimum margin in pixels around the border of the
     * container. When a title is set, it is added to the decorated border
     * like padding.
     * @param title The title displayed above the panel, with a border.
     * When null, no border is added.
     */
    public JFloorPanel(int buttonWidth, int buttonHeight, int margin, String title) {
        super();
        this.places = new ArrayList<JPlaceButton>();
        this.title = title;
        this.setLayout(null);
        this.addComponentListener(new ResizeListener());
        if (this.title != null) {
            CompoundBorder border = new CompoundBorder(
                    new javax.swing.border.EmptyBorder(new Insets(5, 5, 5, 5)),
                    new javax.swing.border.TitledBorder(this.title));
            this.setBorder(border);
            Insets out = border.getOutsideBorder().getBorderInsets(this);
            Insets in = border.getInsideBorder().getBorderInsets(this);
            int[] margins = {out.top + in.top + margin,
                    out.right + in.right + margin,
                    out.bottom + in.bottom + margin,
                    out.left + in.left + margin};
            this.stretcher = new FloorCoordStretcher(buttonWidth, buttonHeight,
                margins[0], margins[1], margins[2], margins[3]);
        } else {
            this.stretcher = new FloorCoordStretcher(buttonWidth, buttonHeight,
                margin);

        }
    }

    /**
     * Create a panel and read the button size from the configuration.
     * @param cfg The configuration to read sizes from.
     * @param title The title displayed above the panel, with a border.
     * When null, no border is added.
     */
    public JFloorPanel(fr.pasteque.pos.forms.AppConfig cfg, String title) {
        this(WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchhudgebtnminwidth"))),
                WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchhudgebtnminheight"))),
                WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing"))),
                title);
    }

    /**
     * Add a place button to the container. The layout will be recomputed on
     * next {@link #update()}.
     * @param place The button to add.
     */
    public void addPlace(JPlaceButton place) {
        if (!this.places.contains(place)) {
            this.places.add(place);
            this.stretcher.extendBoundsFor(place.getPlaceX(),
                    place.getPlaceY());
            this.dirty = true;
            this.add(place);
        }
    }

    @Override
    protected void paintComponent (java.awt.Graphics g) {
        super.paintComponent(g);
        if (this.backgroundImage != null) {
            java.awt.Dimension s = this.getSize();
            int imgWidth = this.backgroundImage.getWidth();
            int imgHeight = this.backgroundImage.getHeight();
            float ratio = Float.valueOf(Math.min(Float.valueOf(s.width) / imgWidth, Float.valueOf(s.height) / imgHeight));
            int scaledW = Double.valueOf(Math.floor(imgWidth * ratio)).intValue();
            int scaledH = Double.valueOf(Math.floor(imgHeight * ratio)).intValue();
            g.drawImage(this.backgroundImage,
                    (s.width - scaledW) / 2, (s.height - scaledH) / 2,
                    scaledW, scaledH, this);
        }
    }

    /**
     * Recompute the position of each place in the container according
     * to their position and the size of this container.
     * It is called automatically when the container is resized.
     */
    public void update() {
        if (!dirty) {
            return;
        }
        java.awt.Dimension s = this.getSize();
        this.stretcher.setContainerSize(s.width, s.height);
        for (JPlaceButton p : this.places) {
           FloorCoordStretcher.Point point = this.stretcher.stretchCoord(p.getPlaceX(), p.getPlaceY());
           p.setLocation(point.x, point.y);
        }
        this.invalidate();
    }

    private class ResizeListener extends java.awt.event.ComponentAdapter {
        @Override // from ComponentListener
        public void componentResized(java.awt.event.ComponentEvent e) {
            dirty = true;
            update();
        }
    }
}
