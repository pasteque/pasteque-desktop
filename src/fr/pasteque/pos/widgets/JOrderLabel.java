//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.widgets;

import fr.pasteque.pos.sales.SharedTicketInfo;
import java.awt.Color;
import javax.swing.JLabel;

/**
 * JLabel to present the name of an order by its name or its customer’s name.
 * Use {@link #update(order, orderExtra)} to update the widget on changes.
 */
public class JOrderLabel extends JLabel
{
    private static final long serialVersionUID = -2903360058395316030L;

    private static final Color ORDER_BGCOLOR = Color.WHITE;
    private static final Color ORDER_FGCOLOR = Color.DARK_GRAY;
    private static final Color CUSTOM_BGCOLOR = new Color(39, 183, 239);
    private static final Color CUSTOM_FGCOLOR = Color.BLACK;
    private static final Color CUSTOMER_BGCOLOR = new Color(33, 67, 92);
    private static final Color CUSTOMER_FGCOLOR = Color.WHITE;

    /** Create an empty label. */
    public JOrderLabel() {
        super();
        WidgetsBuilder.setupLabel(this, WidgetsBuilder.SIZE_MEDIUM);
        this.setBackground(java.awt.Color.white);
        this.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        this.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        this.setOpaque(true);
        this.setPreferredSize(new java.awt.Dimension(160, 25));
        this.setRequestFocusEnabled(false);
    }

    /**
     * Update the label to the given order
     * @param order The order to display.
     * @param orderExtra Extra information that are passed to
     * {@link fr.pasteque.pos.ticket.TicketInfo#getName(info)}.
     */
    public void update(SharedTicketInfo order, Object orderExtra) {
        if (order == null) {
            this.setText(null);
            return;
        }
        if (order.getCustomLabel() != null) {
            this.setBackground(CUSTOM_BGCOLOR);
            this.setForeground(CUSTOM_FGCOLOR);
        } else if (order.getTicket().getCustomer() != null) {
            this.setBackground(CUSTOMER_BGCOLOR);
            this.setForeground(CUSTOMER_FGCOLOR);
        } else {
            this.setBackground(ORDER_BGCOLOR);
            this.setForeground(ORDER_FGCOLOR);
        }
        this.setText(order.getShortLabel(orderExtra));
    }
}
