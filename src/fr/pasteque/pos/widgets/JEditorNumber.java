//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.widgets;

import java.awt.Toolkit;
import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.format.DoubleUtils;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.LimitChecker;

public abstract class JEditorNumber extends JEditorAbstract
{
    private static final long serialVersionUID = -8020553248009145452L;
    // Variable numerica
    private final static int NUMBER_ZERONULL = 0;
    private final static int NUMBER_INT = 1;
    private final static int NUMBER_DEC = 2;

    private int m_iNumberStatus;
    private String m_sNumber;
    private boolean m_bNegative;

    private Formats m_fmt;

    private LimitChecker limitChecker = new LimitChecker();

    /**
     * Creates a new instance of JEditorNumber and read min-max values
     * from ui.editornumber.maxvalue.
     */
    public JEditorNumber() {
        m_fmt = getFormat();
        reset();
    }

    protected abstract Formats getFormat();

    public void reset() {

        String sOldText = getText();

        m_sNumber = "";
        m_bNegative = false;
        m_iNumberStatus = NUMBER_ZERONULL;

        reprintText();

        firePropertyChange("Text", sOldText, getText());
    }

    public void setDoubleValue(Double dvalue) {

        String sOldText = getText();

        if (dvalue == null) {
            m_sNumber = "";
            m_bNegative = false;
            m_iNumberStatus = NUMBER_ZERONULL;
        } else if (dvalue >= 0.0) {
            m_sNumber = formatDouble(dvalue);
            m_bNegative = false;
            m_iNumberStatus = NUMBER_ZERONULL;
        } else {
            m_sNumber = formatDouble(-dvalue);
            m_bNegative = true;
            m_iNumberStatus = NUMBER_ZERONULL;
        }
        reprintText();

        firePropertyChange("Text", sOldText, getText());
    }

    public Double getDoubleValue() {

        String text = getText();
        if (text == null || text.equals("")) {
            return null;
        } else {
            try {
                return Double.parseDouble(text);
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    public void setValueInteger(int ivalue) {

        String sOldText = getText();

        if (ivalue >= 0) {
            m_sNumber = Integer.toString(ivalue);
            m_bNegative = false;
            m_iNumberStatus = NUMBER_ZERONULL;
        } else {
            m_sNumber = Integer.toString(-ivalue);
            m_bNegative = true;
            m_iNumberStatus = NUMBER_ZERONULL;
        }
        reprintText();

        firePropertyChange("Text", sOldText, getText());
    }

    public int getValueInteger() throws BasicException {
        try {
            return Integer.parseInt(getText());
        } catch (NumberFormatException e) {
            throw new BasicException(e);
        }
    }

    private String formatDouble(Double value) {
        String sNumber = Double.toString(DoubleUtils.fixDecimals(value));
        if (sNumber.endsWith(".0")) {
            sNumber = sNumber.substring(0,  sNumber.length() - 2);
        }
        return sNumber;
    }

    protected String getEditMode() {
        return "-1.23";
    }

    public String getText() {
        return (m_bNegative ? "-" : "") + m_sNumber;
    }

    protected int getAlignment() {
        return javax.swing.SwingConstants.RIGHT;
    }

    protected String getTextEdit() {
        return getText();
    }

    protected String getTextFormat() throws BasicException {
        return m_fmt.formatValue(getDoubleValue());
    }


    /**
     * Check if adding a new character will keep the value
     * within the limits.
     */
    protected boolean checkLimits(char c) {
        String text = getText();
        return checkLimits(text + c);
    }

    /**
     * Check the whole input and display a message when the input
     * is outside bounds. The message blocks execution until dismissed.
     * @param text The input to check.
     * @return True when the input is inside bounds and nothing happened,
     * false when outside bounds and a message was displayed.
     */
    protected boolean checkLimits(String text) {
        boolean result = this.limitChecker.checkLimits(text);

        if (!result) {
            String message;
            if (this.getMaxValue() != null && this.getMinValue() != null) {
                message = AppLocal.tr("Message.InvalidQuantityMinMax",
                    text, this.getMinValue(), this.getMaxValue());
            } else if (this.getMaxValue() != null) {
                message = AppLocal.tr("Message.InvalidQuantityMax",
                    text, this.getMaxValue());
            } else {
                message = AppLocal.tr("Message.InvalidQuantityMin",
                    text, this.getMinValue());
            }
            new MessageInf(MessageInf.SGN_NOTICE, message).show(this);
        }
        return result;
    }

    public Double getMaxValue() {
        return this.limitChecker.getMaxValue();
    }

    /** Set the maximum accepted value. Set to null to remove the limit. */
    public void setMaxValue(Double m_maxValue) {
        this.limitChecker.setMaxValue(m_maxValue);
    }

    public Double getMinValue() {
        return this.limitChecker.getMinValue();
    }

    /** Set the minimum accepted value. Set to null to remove the limit. */
    public void setMinValue(Double m_minValue) {
        this.limitChecker.setMinValue(m_minValue);
    }

    /** Set minimum and maximum accepted values. Set to null to remove the limit. */
    public void setBounds(Double minValue, Double maxValue) {
        this.limitChecker.setBounds(minValue, maxValue);
    }

    /**
     * Remove a character from the input.
     * @return True when the removal did something (the input was not empty).
     */
    private boolean processBackspace() {
        if (m_sNumber.length() > 0 || m_bNegative) {
            if (m_sNumber.length() == 0) {
                m_bNegative = false;
                return true;
            }
            char lastChar = m_sNumber.charAt(m_sNumber.length() - 1);
            m_sNumber = m_sNumber.substring(0, m_sNumber.length() - 1);
            if (lastChar == '.') {
                if ("0".equals(m_sNumber)) {
                    m_iNumberStatus = NUMBER_ZERONULL;
                } else {
                    m_iNumberStatus = NUMBER_INT;
                }
            } else {
                if ("0".equals(m_sNumber) || "".equals(m_sNumber)) {
                    m_iNumberStatus = NUMBER_ZERONULL;
                }
            }
            return true;
        }
        return false;
    }

    protected void typeCharInternal(char cTrans) {
        if (cTrans == '\u0008') { // backspace
            String oldText = getText();
            if (this.processBackspace()) {
                firePropertyChange("Text", oldText, getText());
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        } else {
            transChar(cTrans);
        }
    }

    protected void transCharInternal(char cTrans) {
        String sOldText = getText();
        boolean acceptInput = false;
        if (cTrans == '\u007f') {
            acceptInput = true;
            reset();
        } else if (cTrans == '\u0008') {
            acceptInput = this.processBackspace();
        } else if (cTrans == '-') {
            if (checkLimits('-' + m_sNumber)) {
                acceptInput = true;
                m_bNegative = !m_bNegative;
            }
        } else if ((cTrans == '0')
        && (m_iNumberStatus == NUMBER_ZERONULL)) {
            // m_iNumberStatus = NUMBER_ZERO;
            acceptInput = true;
            m_sNumber = "0";
        } else if ((cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9')
        && (m_iNumberStatus == NUMBER_ZERONULL)) {
            m_iNumberStatus = NUMBER_INT;
            if (checkLimits(cTrans)) {
                acceptInput = true;
                m_sNumber = Character.toString(cTrans);
            }
        } else if (cTrans == '.' &&  m_iNumberStatus == NUMBER_ZERONULL) {
            acceptInput = true;
            m_iNumberStatus = NUMBER_DEC;
            m_sNumber = "0.";
        } else if ((cTrans == '0' || cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9')
        && (m_iNumberStatus == NUMBER_INT)) {
            // m_iNumberStatus = NUMBER_INT;
            if (checkLimits(cTrans)) {
                acceptInput = true;
                m_sNumber += cTrans;
            }
        } else if (cTrans == '.' &&  m_iNumberStatus == NUMBER_INT) {
            acceptInput = true;
            m_iNumberStatus = NUMBER_DEC;
            m_sNumber += '.';
        } else if ((cTrans == '0' || cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9')
        && (m_iNumberStatus == NUMBER_DEC)) {
            if (checkLimits(cTrans)) {
                acceptInput = true;
                m_sNumber += cTrans;
            }
        }
        if (!acceptInput) {
            Toolkit.getDefaultToolkit().beep();
        } else {
            firePropertyChange("Text", sOldText, getText());
        }
    }
}
