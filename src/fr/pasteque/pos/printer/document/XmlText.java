//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.document;

/** Definition of a line of text, with some presentation options. */
/* package */ class XmlText
{
    /** Put the text to the left of it's bounding box. */
    public static final String ALIGN_LEFT = "left";
    /** Put the text to the right of it's bounding box. */
    public static final String ALIGN_RIGHT = "right";
    /** Center the text inside it's bounding box. */
    public static final String ALIGN_CENTER = "center";
    /** Bit mask to activate bold text. */
    public static final int BOLD = 1;
    /** Bit mask to activate italic text. */
    public static final int UNDERLINED = 2;

    private static String escXml(String s) {
        return s.replace("&", "&amp;").replace(">", "&gt;").replace("<", "&lt;").replace("'", "&apos;").replace("\"", "&quot;");
    }

    private String text;
    private int length;
    private String align;
    private int styling;

    /**
     * Create a formatted text with the given style
     * @param text The text to print. It will be escaped.
     * @param length The length in characters of the reserved space. If text
     * is smaller, a margin is left. If it is longer, it is truncated.
     * @param align Text alignment within the reserved space. See constants.
     * @param styling Style flags. See constants.
     */
    XmlText(String text, int length, String align, int styling) {
        this.text = text;
        this.length = length;
        this.align = align;
        this.styling = styling;
    }

    /**
     * Create a formatted text in the default style
     * @param text The text to print. It will be escaped.
     * @param length The length in characters of the reserved space. If text
     * is smaller, a margin is left. If it is longer, it is truncated.
     * @param align Text alignment within the reserved space. See constants.
     */
    XmlText(String text, int length, String align) {
        this.text = text;
        this.length = length;
        this.align = align;
    }

    /**
     * Create a formatted text in the default style aligned to the left.
     * @param text The text to print. It will be escaped.
     * @param length The length in characters of the reserved space. If text
     * is smaller, a margin is left. If it is longer, it is truncated.
     */
    XmlText(String text, int length) {
        this.text = text;
        this.length = length;
        this.align = ALIGN_LEFT;
    }

    /**
     * Create a inline text in the default style aligned to the left.
     * @param text The text to print. It will be escaped.
     */
    XmlText(String text) {
        this.text = text;
        this.length = text.length();
        this.align = ALIGN_LEFT;
    }

    /** Get the xml tag that represent this text. */
    String toXml() {
        String format = "<text align=\"%s\" length=\"%d\"";
        if ((this.styling & BOLD) > 0) {
            format += " bold=\"true\"";
        }
        if ((this.styling & UNDERLINED) > 0) {
            format += " underline=\"true\"";
        }
        format += ">%s</text>";
        return String.format(format,
                this.align, this.length, escXml(this.text));
    }
}
