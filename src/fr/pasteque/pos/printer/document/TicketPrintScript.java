//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.document;

import fr.pasteque.beans.LocaleResources;
import fr.pasteque.pos.payment.PaymentInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketTaxInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import java.util.ArrayList;
import java.util.List;

/** Helper class to generate the printing xml script. */
public class TicketPrintScript
{
    private static LocaleResources R;
    static {
        R = new LocaleResources();
        R.addBundleName("ticket_messages");
    }

    /** Max number of characters per line. */
    private int charPerLine;
    private XmlFormatterHelper xmlf;

    /**
     * Create a script generator for the given printer.
     * @param charPerLine Printing width in characters.
     */
    public TicketPrintScript(int charPerLine) {
        this.charPerLine = charPerLine;
        this.xmlf = new XmlFormatterHelper(this.charPerLine);
    }

    /** Get the space reserved for an article label. */
    private int getArticleLength() {
        int length = this.charPerLine - XmlFormatterHelper.QUANTITY_LENGTH - 2 * XmlFormatterHelper.PRICE_LENGTH;
        if (length < 0) {
            return 0;
        }
        return length;
    }

    /** Get the code for the header for the list of articles of the order or ticket. */
    private String getTicketContentHeader(boolean withVat) {
        int articleLength = this.getArticleLength();
        if (articleLength < R.getString("tkt_line_article").length()) {
            String xml = xmlf.getLineText(new XmlText(R.getString("tkt_line_article"), this.charPerLine, XmlText.ALIGN_LEFT));
            xml += xmlf.getLineText(
                    new XmlText("", articleLength),
                    new XmlText(R.getString("tkt_line_price"), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(R.getString("tkt_line_qty"), XmlFormatterHelper.QUANTITY_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(R.getString(withVat ? "tkt_line_total_vat" : "tkt_line_total"), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
            return xml;
        } else {
            return xmlf.getLineText(
                    new XmlText(R.getString("tkt_line_article"), articleLength, XmlText.ALIGN_LEFT),
                    new XmlText(R.getString("tkt_line_price"), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(R.getString("tkt_line_qty"), XmlFormatterHelper.QUANTITY_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(R.getString(withVat ? "tkt_line_total_vat" : "tkt_line_total"), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
        }
    }

    /**
     * Get the code for a line of the ticket or order.
     * @param line The article line.
     * @param withVat Wether to print the tax code or not.
     */
    private String getTicketLine(TicketInfo ticket, int lineNum, boolean withVat) {
        List<XmlText> texts = new ArrayList<XmlText>();
        TicketLineInfo line = ticket.getLine(lineNum);
        int articleLength = this.getArticleLength();
        String label;
        if (line.isProductCom()) {
            label = String.format("*%s", line.printName());
        } else {
            label = line.printName();
        }
        String xml = "";
        if (label.length() > articleLength) {
            xml = xmlf.getLineText(new XmlText(label, this.charPerLine));
            texts.add(new XmlText("", articleLength, XmlText.ALIGN_LEFT));
        } else {
            texts.add(new XmlText(label, articleLength, XmlText.ALIGN_LEFT));
        }
        texts.add(new XmlText(line.printFullPriceTax(), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT));
        texts.add(new XmlText("x" + line.printMultiply(), XmlFormatterHelper.QUANTITY_LENGTH, XmlText.ALIGN_RIGHT));
        if (withVat) {
            texts.add(new XmlText(line.printFullValue(), XmlFormatterHelper.PRICE_LENGTH - 1, XmlText.ALIGN_RIGHT));
            texts.add(new XmlText(ticket.printTaxCode(line), 1, XmlText.ALIGN_LEFT));
        } else {
            texts.add(new XmlText(line.printFullValue(), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT));
        }
        xml += xmlf.getLineText(texts);
        if (line.hasDiscount()) {
            String discount = String.format("%s %s",
                    R.getString("tkt_line_discount"), line.printDiscountRate());
            xml += xmlf.getLineText(
                    new XmlText(discount, this.charPerLine - XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_LEFT),
                    new XmlText(line.printDiscountAmount(), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
        }
        return xml;
    }

    /** Get the code for the list of articles of the order or ticket. */
    private String getTicketContent(TicketInfo ticket, boolean withVat) {
        String xml = "";
        for (int i = 0; i < ticket.getLinesCount(); i++) {
            xml += this.getTicketLine(ticket, i, withVat);
        }
        return xml;
    }

    /**
     * Get the code to print the total block.
     * @param ticket The order or ticket.
     * @param withVat When true, informations about VAT are included, otherwise
     * only the total amount and discounts are printed.
     */
    private String getTicketTotal(TicketInfo ticket, boolean withVat) {
        // Article count and discount
        String xml = xmlf.getLineText(
                new XmlText(R.getString("tkt_total_articles"), this.charPerLine - XmlFormatterHelper.PRICE_LENGTH - XmlFormatterHelper.QUANTITY_LENGTH, XmlText.ALIGN_LEFT),
                new XmlText(ticket.printArticlesCount(), XmlFormatterHelper.QUANTITY_LENGTH, XmlText.ALIGN_RIGHT)
        );
        if (ticket.hasDiscount()) {
            int discountLength = this.charPerLine - XmlFormatterHelper.PRICE_LENGTH;
            xml += xmlf.getLineText(
                    new XmlText(R.getString("tkt_total_prediscount"), discountLength, XmlText.ALIGN_LEFT),
                    new XmlText(ticket.printFullTotal(), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
            xml += xmlf.getLineText(
                    new XmlText(String.format("%s %s", R.getString("tkt_total_discount"), ticket.printDiscountRate()), discountLength, XmlText.ALIGN_LEFT),
                    new XmlText(ticket.printDiscountAmount(), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
        }
        // Tax details
        if (withVat) {
            // per tax
            xml += xmlf.getEmptyLine();
            xml += xmlf.getDoubleValueLine(R.getString("tkt_tax"), R.getString("tkt_tax_base"),
                    R.getString("tkt_tax_amount"));
            for (TicketTaxInfo tax : ticket.getTaxes()) {
                xml += xmlf.getDoubleValueLine(String.format("%s. %s", ticket.printTaxCode(tax), tax.getTax().getName()),
                        tax.printSubTotal(), tax.printTax());
            }
            // subtotal
            xml += xmlf.getEmptyLine();
            xml += xmlf.getValueLine(R.getString("tkt_total_subtotal"), ticket.printSubTotal(), false);
            xml += xmlf.getValueLine(R.getString("tkt_total_vat"), ticket.printTax(), false);
        }
        // Total
        xml += xmlf.getEmptyLine();
        xml += xmlf.getValueLine(R.getString("tkt_total"), ticket.printTotal(), true);
        return xml;
    }

    /**
     * Get the xml script to print an order.
     * @param order The order to print.
     * @param printerIndex Number of the printer ([1-3]).
     * @param placeName The name of the place when set (nullable).
     * @param withPrices Print prices or not. With prices is for customer,
     * without for the team to prepare the order.
     */
    public String getOrderXml(TicketInfo order, int printerIndex,
            String placeName, boolean withPrices) {
        String xml = String.format("<output><ticket printer=\"" + printerIndex + "\">");
        // Custom header
        if (withPrices) {
            xml += "<image>Printer.Ticket.Logo</image>";
            xml += xmlf.getEmptyLine();
        }
        String header = xmlf.getCustomResource("Printer.Order.Header");
        if (!"".equals(header)) {
            xml += header + xmlf.getEmptyLine();
        }
        // Ticket info
        String nameLbl = withPrices ? R.getString("tkt_bill") + " : " : R.getString("tkt_order") + " : ";
        String placeLbl = R.getString("tkt_place") + " : ";
        String dateLbl = R.getString("tkt_date") + " " ;
        String custLbl = (order.getCustomer() != null) ? R.getString("tkt_cust") + " " : "";
        String custCountLbl = (order.hasCustomersCount()) ? R.getString("tkt_custcount") + " " : "";
        int lblMax = Math.max(nameLbl.length(),
                Math.max(placeLbl.length(),
                Math.max(dateLbl.length(),
                Math.max(custCountLbl.length(), custLbl.length()))));
        xml += xmlf.getLabelText(nameLbl, order.printId(), lblMax);
        if (placeName != null) {
            xml += xmlf.getLabelText(placeLbl, placeName, lblMax);
        }
        xml += xmlf.getLabelText(dateLbl, order.printDate(), lblMax);
        if (order.getCustomer() != null) {
            xml += xmlf.getLabelText(custLbl, order.getCustomer().printName(), lblMax);
        }
        if (order.hasCustomersCount()) {
            xml += xmlf.getLabelText(custCountLbl, order.printCustomersCount(), lblMax);
        }
        // Ticket content
        xml += xmlf.getEmptyLine();
        xml += this.getTicketContentHeader(false);
        xml += xmlf.getSeparator();
        xml += this.getTicketContent(order, false);
        xml += xmlf.getEmptyLine();
        // Total
        if (withPrices) {
            xml += xmlf.getSeparator();
            xml += this.getTicketTotal(order, false);
            xml += xmlf.getEmptyLine();
        }
        if (withPrices) {
            String footer = "Note provisoire avant encaissement";
            if (footer.length() > this.charPerLine) {
                String[] words = footer.split(" ");
                // half should be sufficient
                int half = (int)(words.length / 2);
                String line1 = words[0];
                int i = 1;
                while (i < half) {
                    line1 += " " + words[i];
                    i++;
                }
                String line2 = words[i];
                i++;
                while (i < words.length) {
                    line2 += " " + words[i];
                    i++;
                }
                xml += xmlf.getLineText(new XmlText(line1, this.charPerLine, XmlText.ALIGN_CENTER));
                xml += xmlf.getLineText(new XmlText(line2, this.charPerLine, XmlText.ALIGN_CENTER));
            } else {
                xml += xmlf.getLineText(new XmlText(footer, this.charPerLine, XmlText.ALIGN_CENTER));
            }
            xml += xmlf.getEmptyLine();
        }
        // Custom footer
        String footer = xmlf.getCustomResource("Printer.Order.Footer");
        if (!"".equals(footer)) {
            xml += footer;
        }
        // Close
        xml += "</ticket></output>";
        return xml;
    }

    private String getTicketXml(TicketInfo ticket, int printerIndex, String placeName,
            boolean openDrawer, boolean isReprint) {
        String xml;
        if (!isReprint) {
            // Customer display
            xml = String.format("<output><display><line>"
                    + "<text align=\"left\" length=\"10\">%s</text>"
                    + "<text align=\"right\" length=\"10\">%s</text>"
                    + "</line>"
                    + "<line><text align=\"center\" length=\"20\">%s</text></line></display>",
                    R.getString("tkt_custdisp_total"),
                    ticket.printTotal(),
                    R.getString("tkt_custdisp_thanks"));
        } else {
            xml = "<output>";
        }
        // Ticket logo
        xml += "<ticket printer=\"" + printerIndex + "\"><image>Printer.Ticket.Logo</image>";
        xml += xmlf.getEmptyLine();
        // Custom header
        String header = xmlf.getCustomResource("Printer.Ticket.Header");
        if (!"".equals(header)) {
            xml += header + xmlf.getEmptyLine();
        }
        // Ticket info
        String nameLbl = R.getString("tkt_number") + " ";
        String placeLbl = R.getString("tkt_place") + " : ";
        String dateLbl = R.getString("tkt_date") + " ";
        String custLbl = (ticket.getCustomer() != null) ? R.getString("tkt_cust") + " " : "";
        String custCountLbl = (ticket.hasCustomersCount()) ? R.getString("tkt_custcount") + " " : "";
        String opLbl = R.getString("tkt_operator") + " ";
        int lblMax = Math.max(nameLbl.length(),
                Math.max(dateLbl.length(),
                Math.max(placeLbl.length(),
                Math.max(opLbl.length(),
                Math.max(custCountLbl.length(), custLbl.length())))));
        String tktId = String.format("%s - %s", ticket.printCashRegister(), ticket.printId());
        xml += xmlf.getLabelText(nameLbl, tktId, lblMax);
        if (isReprint) {
            xml += xmlf.getLabelText("", R.getString("tkt_reprint"), lblMax);
        }
        if (placeName != null) {
            xml += xmlf.getLabelText(placeLbl, placeName, lblMax);
        }
        xml += xmlf.getLabelText(dateLbl, ticket.printDate(), lblMax);
        if (ticket.getCustomer() != null) {
            xml += xmlf.getLabelText(custLbl, ticket.getCustomer().printName(), lblMax);
        }
        if (ticket.hasCustomersCount()) {
            xml += xmlf.getLabelText(custCountLbl, ticket.printCustomersCount(), lblMax);
        }
        xml += xmlf.getLabelText(opLbl, ticket.printUser(), lblMax);
        // Ticket content
        xml += xmlf.getEmptyLine();
        xml += getTicketContentHeader(true);
        xml += xmlf.getSeparator();
        xml += getTicketContent(ticket, true);
        // Total
        xml += xmlf.getSeparator();
        xml += getTicketTotal(ticket, true);
        xml += xmlf.getEmptyLine();
        // Payment details
        for (PaymentInfo pay : ticket.getPayments()) {
            String pmPrint = pay.printMode();
            if (!pay.getCurrency().isMain()) {
                pmPrint = String.format("%s (%s)", pmPrint, pay.getCurrency().getName());
            }
            xml += xmlf.getValueLine(pmPrint, pay.printPaid(), false);
            if (!pay.getCurrency().isMain()) {
                String mainAmount = String.format("(=%s)", pay.printPaidMain());
                xml += xmlf.getLineText(
                        new XmlText(mainAmount, this.charPerLine, XmlText.ALIGN_RIGHT)
                );
            }
            if (pay.hasBackPayment()) {
                PaymentInfo back = pay.getBackPayment();
                xml += xmlf.getValueLine(back.printMode(), back.printPaid(), false);
                if (!back.getCurrency().isMain()) {
                    String mainAmount = String.format("(=%s)", back.printPaidMain());
                    xml += xmlf.getLineText(
                            new XmlText(mainAmount, this.charPerLine, XmlText.ALIGN_RIGHT)
                    );
                }
            }
            xml += xmlf.getEmptyLine();
        }
        // Customer's balance
        if (ticket.getCustBalance() != 0.0 && ticket.getCustomer() != null && !isReprint) {
            xml += xmlf.getLineText(String.format(R.getString("tkt_custbalance"),
                    ticket.getCustomer().printBalance()));
            xml += xmlf.getEmptyLine();
        }
        // Custom footer
        String footer = xmlf.getCustomResource("Printer.Ticket.Footer");
        if (!"".equals(footer)) {
            xml += footer;
        }
        // Open cash drawer and close tags
        xml += "</ticket>";
        if (openDrawer) {
            xml += "<opendrawer/>";
        }
        xml += "</output>";
        return xml;
    }

    /**
     * Get the xml script to print a new ticket.
     * @param ticket The ticket to print.
     * @param printerIndex Number of the printer ([1-3]).
     * @param placeName The name of the place when set (nullable).
     * @param openDrawer Send a pulse to the printer to open the cash drawer.
     */
    public String getTicketXml(TicketInfo ticket, int printerIndex,
            String placeName, boolean openDrawer) {
        return this.getTicketXml(ticket, printerIndex, placeName, openDrawer, false);
    }

    /**
     * Get the xml script te reprint an older ticket.
     * @param ticket The ticket to reprint.
     * @param printerIndex Number of the printer ([1-3]).
     */
    public String getReprintTicketXml(TicketInfo ticket, int printerIndex) {
        return this.getTicketXml(ticket, printerIndex, null, false, true);
    }
}
