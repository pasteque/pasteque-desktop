//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.document;

import fr.pasteque.beans.LocaleResources;
import fr.pasteque.pos.panels.PaymentsModel;

/** Helper class to generate the printing xml script. */
public class ZTicketPrintScript
{
    private static LocaleResources R;
    static {
        R = new LocaleResources();
        R.addBundleName("ticket_messages");
    }
    /** Max number of characters per line. */
    private int charPerLine;
    private XmlFormatterHelper xmlf;

    /**
     * Create a script generator for the given printer.
     * @param charPerLine Printing width in characters.
     */
    public ZTicketPrintScript(int charPerLine) {
        this.charPerLine = charPerLine;
        this.xmlf = new XmlFormatterHelper(this.charPerLine);
    }

    /**
     * Get the xml script to print the opening cash session ticket.
     * @param payments The opening data.
     * @param printerIndex Number of the printer ([1-3]).
     */
    public String getOpenXml(PaymentsModel payments, int printerIndex) {
        String xml = String.format("<output><ticket printer=\"" + printerIndex + "\">");
        // Header
        xml += "<image>Printer.Ticket.Logo</image><line></line>";
        String header = xmlf.getCustomResource("Printer.Ticket.Header");
        if (!"".equals(header)) {
            xml += header + xmlf.getEmptyLine();
        }
        xml += xmlf.getLineText(1, new XmlText(R.getString("ztkt_open"),
                this.charPerLine, XmlText.ALIGN_CENTER, XmlText.BOLD));
        // Session info
        String crLbl = R.getString("ztkt_cashregister") + " ";
        String sequenceLbl = R.getString("ztkt_sequence") + " ";
        String dateLbl = R.getString("ztkt_opendate") + " ";
        String openCashLbl = R.getString("ztkt_cash") + " ";
        int lblMax = Math.max(crLbl.length(),
                Math.max(sequenceLbl.length(),
                dateLbl.length()));
        if (payments.hasFunds()) {
            lblMax = Math.max(lblMax, openCashLbl.length());
        }
        xml += xmlf.getLabelText(crLbl, payments.printHost(), lblMax);
        xml += xmlf.getLabelText(sequenceLbl, payments.printSequence(), lblMax);
        xml += xmlf.getLabelText(dateLbl, payments.printDateStart(), lblMax);
        // Funds
        if (payments.hasFunds()) {
            xml += xmlf.getEmptyLine();
            xml += xmlf.getLabelText(openCashLbl, payments.printOpenCash(), lblMax);
            xml += xmlf.getLineText(R.getString("ztkt_coindetails"));
            for (Double val : payments.getCountedCoins()) {
                xml += xmlf.getDoubleValueLine(payments.printCoinValue(val),
                        payments.printCoinCount(val), payments.printCoinTotal(val));
            }
        }
        // Footer
        String footer = xmlf.getCustomResource("Printer.Ticket.Footer");
        if (!"".equals(footer)) {
            xml += xmlf.getEmptyLine();
            xml += footer;
        }
        // Close
        xml += "</ticket></output>";
        return xml;
    }

    public String getPartialXml(PaymentsModel payments, int printerIndex) {
        return getCloseXml(payments, printerIndex, true);
    }

    public String getCloseXml(PaymentsModel payments, int printerIndex) {
        return getCloseXml(payments, printerIndex, false);
    }

    private String getCloseXml(PaymentsModel payments, int printerIndex, boolean partial) {
        String xml = String.format("<output><ticket printer=\"" + printerIndex + "\">");
        // Header
        xml += "<image>Printer.Ticket.Logo</image><line></line>";
        String header = xmlf.getCustomResource("Printer.Ticket.Header");
        if (!"".equals(header)) {
            xml += header + xmlf.getEmptyLine();
        }
        String title;
        if (partial) {
            title = R.getString("ztkt_partialclose");
        } else {
            title = R.getString("ztkt_close");
        }
        xml += xmlf.getLineText(1, new XmlText(title, this.charPerLine, XmlText.ALIGN_CENTER, XmlText.BOLD));
        // Session info
        String crLbl = R.getString("ztkt_cashregister") + " ";
        String sequenceLbl = R.getString("ztkt_sequence") + " ";
        String openDateLbl = R.getString("ztkt_opendate") + " ";
        String closeDateLbl = R.getString("ztkt_closedate") + " ";
        int lblMax = Math.max(crLbl.length(),
                Math.max(sequenceLbl.length(),
                Math.max(openDateLbl.length(),
                closeDateLbl.length())));
        xml += xmlf.getLabelText(crLbl, payments.printHost(), lblMax);
        xml += xmlf.getLabelText(sequenceLbl, payments.printSequence(), lblMax);
        xml += xmlf.getLabelText(openDateLbl, payments.printDateStart(), lblMax);
        if (!partial) {
            xml += xmlf.getLabelText(closeDateLbl, payments.printDateEnd(), lblMax);
        }
        xml += xmlf.getSeparator();
        // Sales
        xml += xmlf.getValueLine(R.getString("ztkt_tickets"), payments.printSales(), true);
        xml += xmlf.getEmptyLine();
        xml += xmlf.getValueLine(R.getString("ztkt_subtotal"), payments.printSalesBase(), true);
        xml += xmlf.getValueLine(R.getString("ztkt_taxtotal"), payments.printSalesTaxes(), true);
        xml += xmlf.getValueLine(R.getString("ztkt_total"), payments.printSalesTotal(), true);
        xml += xmlf.getSeparator();
        // Taxes
        xml += xmlf.getLineText(
                    new XmlText(R.getString("tkt_tax"), this.charPerLine - 2 * XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_LEFT),
                    new XmlText(R.getString("tkt_tax_base"), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(R.getString("tkt_tax_amount"), XmlFormatterHelper.PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
        for (PaymentsModel.SalesLine line : payments.getSaleLines()) {
            xml += xmlf.getDoubleValueLine(line.printTaxName(), line.printTaxBase(), line.printTaxes());
        }
        xml += xmlf.getSeparator();
        // Sales by category
        xml += xmlf.getLineText(R.getString("ztkt_categories"));
        for (PaymentsModel.CategoryLine line : payments.getCategoryLines()) {
            xml += xmlf.getValueLine(line.printCategory(), line.printValue(), false);
        }
        xml += xmlf.getSeparator();
        // Payments
        xml += xmlf.getValueLine(R.getString("ztkt_paymentcount"), payments.printPayments(), false);
        for (PaymentsModel.PaymentsLine line : payments.getPaymentLines()) {
            xml += xmlf.getValueLine(line.printType(), line.printValue(), false);
        }
        xml += xmlf.getEmptyLine();
        xml += xmlf.getValueLine(R.getString("ztkt_paymenttotal"), payments.printPaymentsTotal(), true);
        if (payments.hasCustomersCount()) {
            xml += xmlf.getSeparator();
            xml += xmlf.getValueLine(R.getString("ztkt_custcount"), payments.printCustomersCount(), false);
            xml += xmlf.getValueLine(R.getString("ztkt_custavg"), payments.printSalesPerCustomer(), false);
        }
        // Cash count
        if (!partial && payments.hasFunds()) {
            xml += xmlf.getSeparator();
            xml += xmlf.getValueLine(R.getString("ztkt_opencash"), payments.printOpenCash(), false);
            xml += xmlf.getValueLine(R.getString("ztkt_closecash"), payments.printCloseCash(), false);
            if (payments.getCashMoveAmount() != 0.0) {
                xml += xmlf.getValueLine(R.getString("ztkt_cashmove"), payments.printMoveAmount(), false);
            }
            xml += xmlf.getValueLine(R.getString("ztkt_expectedcash"), payments.printExpectedCash(), false);
            xml += xmlf.getValueLine(R.getString("ztkt_casherror"), payments.printCashError(), false);
            xml += xmlf.getLineText(R.getString("ztkt_coindetails"));
            for (Double val : payments.getCountedCoins()) {
                xml += xmlf.getDoubleValueLine(payments.printCoinValue(val),
                        payments.printCoinCount(val), payments.printCoinTotal(val));
            }
        }
        // Footer
        String footer = xmlf.getCustomResource("Printer.Ticket.Footer");
        if (!"".equals(footer)) {
            xml += xmlf.getEmptyLine();
            xml += footer;
        }
        // Close
        xml += "</ticket></output>";
        return xml;
    }
}
