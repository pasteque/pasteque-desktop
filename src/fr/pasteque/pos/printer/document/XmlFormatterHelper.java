//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.document;

import fr.pasteque.pos.forms.DataLogicSystem;
import java.util.List;

/** Utilities to transform text to xml. */
/* package */ class XmlFormatterHelper
{
    /** Number of characters reserved for pricing. */
    static final int PRICE_LENGTH = 10;
    /** Number of characters reserved for quantity. */
    static final int QUANTITY_LENGTH = 5;
    /** Max number of characters per line. */
    private int charPerLine;

    XmlFormatterHelper(int charPerLine) {
        this.charPerLine = charPerLine;
    }

    /** Get the code for a blank line. */
    String getEmptyLine() {
        return "<line></line>";
    }

    /** Get the code for a full width separator line. */
    String getSeparator() {
        StringBuffer buff = new StringBuffer(this.charPerLine);
        for (int i = 0; i < this.charPerLine; i++) {
            buff.append('-');
        }
        return this.getLineText(buff.toString());
    }

    /** Get the code for a text on a full line. */
    String getLineText(String text) {
        return this.getLineText(new XmlText(text, this.charPerLine));
    }

    /** Get the code for a line which contains multiple texts. */
    String getLineText(XmlText ... texts) {
        String line = "<line>";
        for (XmlText t : texts) {
            line += t.toXml();
        }
        line += "</line>";
        return line;
    }

    /** Get the code for a custom-sized line which contains multiple texts.
     * @param size Text size. 0 default, 1 large...
     * @param texts Elements of text.
     */
    String getLineText(int size, XmlText ... texts) {
        String line = String.format("<line size=\"%d\">", size);
        for (XmlText t : texts) {
            line += t.toXml();
        }
        line += "</line>";
        return line;
    }

    /** Get the code for a line which contains multiple texts. */
    String getLineText(List<XmlText> texts) {
        String line = "<line>";
        for (XmlText t : texts) {
            line += t.toXml();
        }
        line += "</line>";
        return line;
    }

    /**
     * Get some text formatted as "Label: value", trying to align values.
     * @param label The text label.
     * @param value The value for that label.
     * @param maxLabelWidth Try to align values with that left margin.
     * When the value is too long, it will tex more space until it reaches the
     * label. It is printed on an other line when no space is left.
     * @return The xml text for the single or multi lines.
     */
    String getLabelText(String label, String value, int maxLabelWidth) {
        if (value.length() + maxLabelWidth <= this.charPerLine) {
            return this.getLineText(
                    new XmlText(label, maxLabelWidth),
                    new XmlText(value, this.charPerLine - maxLabelWidth)
            );
        } else if (value.length() + label.length() < this.charPerLine) {
            return this.getLineText(
                    new XmlText(label, this.charPerLine - value.length()),
                    new XmlText(value, value.length())
            );
        } else {
            return this.getLineText(label) + this.getLineText(new XmlText(value, this.charPerLine, XmlText.ALIGN_RIGHT));
        }
    }

    /**
     * Get the code for a line which contains a text and it's price value.
     * @param label The label to print on the left.
     * @param value The value to print on the right.
     * @param strong If the line should be printed in bold and large-sized.
     */
    String getValueLine(String label, String value, boolean strong) {
        int size = strong ? 1 : 0;
        int style = strong ? XmlText.BOLD : 0;
        String xml;
        if (label.length() + PRICE_LENGTH >= this.charPerLine) {
            xml = getLineText(size, new XmlText(label));
            xml += getLineText(size,
                    new XmlText("", this.charPerLine - PRICE_LENGTH, XmlText.ALIGN_LEFT, style),
                    new XmlText(value, PRICE_LENGTH, XmlText.ALIGN_RIGHT, style)
            );
        } else {
            xml = getLineText(size,
                    new XmlText(label, this.charPerLine - PRICE_LENGTH, XmlText.ALIGN_LEFT, style),
                    new XmlText(value, PRICE_LENGTH, XmlText.ALIGN_RIGHT, style)
            );
        }
        return xml;
    }

    /**
     * Get the code for a line which contains a text and two price value.
     * @param label The label to print on the left.
     * @param value1 The value to print in the middle.
     * @param value2 The value to print on the right.
     */
    String getDoubleValueLine(String label, String value1, String value2) {
        String xml;
        if (label.length() + 2 * PRICE_LENGTH >= this.charPerLine) {
            xml = getLineText(new XmlText(label));
            xml += getLineText(
                    new XmlText("", this.charPerLine - 2 * PRICE_LENGTH),
                    new XmlText(value1, PRICE_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(value2, PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
        } else {
            xml = getLineText(
                    new XmlText(label, this.charPerLine - 2 * PRICE_LENGTH),
                    new XmlText(value1, PRICE_LENGTH, XmlText.ALIGN_RIGHT),
                    new XmlText(value2, PRICE_LENGTH, XmlText.ALIGN_RIGHT)
            );
        }
        return xml;
    }

    /**
     * Get the code for a multilined text from a resource. Each line is
     * printed in full length.
     * @param resource The resource name.
     */
    String getCustomResource(String resource) {
        DataLogicSystem dlSystem = new DataLogicSystem();
        String res = dlSystem.getResourceAsXML(resource);
        String xml = "";
        if (res != null) {
            String[] lines = res.split("\n");
            for (String line : lines) {
                xml += this.getLineText(line);
            }
        }
        return xml;
    }
}
