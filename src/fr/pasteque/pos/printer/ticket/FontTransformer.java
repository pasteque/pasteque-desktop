//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.ticket;

import fr.pasteque.pos.printer.DevicePrinter;
import java.awt.Font;
import java.awt.geom.AffineTransform;

/**
 * Manage font size and style from a base font.
 */
public abstract class FontTransformer
{
    protected int lineHeight;

    /**
     * Get the style int to apply to the base font.
     * @param style The font style,
     * see {@link fr.pasteque.pos.printer.DevicePrinter} STYLE_* constants.
     * @return A style int, combining Font style flag constants.
     */
    protected static int getStyleFlags(int style) {
        int fontStyle = Font.PLAIN;
        if ((style & DevicePrinter.STYLE_BOLD) > 0) {
            fontStyle = fontStyle | Font.BOLD;
        }
        return fontStyle;
    }

    /**
     * Build the font from the base font with variants.
     * @param size The font size,
     * see {@link fr.pasteque.pos.printer.DevicePrinter} SIZE_* constants.
     * @param style The font style,
     * see {@link fr.pasteque.pos.printer.DevicePrinter} STYLE_* constants.
     * @return The font to use.
     */
    public abstract Font getFont(int size, int style);
    /**
     * Get the height of a line.
     * @param size The font size,
     * see {@link fr.pasteque.pos.printer.DevicePrinter} SIZE_* constants.
     */
    public int getLineHeight(int size) {
        switch (size) {
            case DevicePrinter.SIZE_1:
            case DevicePrinter.SIZE_2:
                return 2 * this.lineHeight;
            default:
                return this.lineHeight;
        }
    }

    /**
     * Get the transformation required to adapt the base font to a given size.
     * @param size The font size,
     * see {@link fr.pasteque.pos.printer.DevicePrinter} SIZE_* constants.
     * @return An AffineTransform to apply to the base font, null when no
     * transformation is required.
     */
    protected AffineTransform getSizeTransform(int size) {
        AffineTransform a;
        float widthFactor = 1.0f;
        float heightFactor = 1.0f;
        switch (size) {
            case DevicePrinter.SIZE_1:
                heightFactor = 2.0f;
                break;
            case DevicePrinter.SIZE_2:
                widthFactor = 2.0f;
                break;
            case DevicePrinter.SIZE_3:
                widthFactor = 2.0f;
                heightFactor = 2.0f;
                break;
        }
        if (widthFactor != 1.0 || heightFactor != 1.0) {
                a = AffineTransform.getScaleInstance(widthFactor, heightFactor);
                return a;
        }
        return null;
    }
}
