//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.ticket;

import java.awt.Font;
import java.awt.geom.AffineTransform;

/**
 * Utility to print ticket elements that will be presented on screen.
 * It uses a more eye-candy font.
 */
public class BasicTicketForScreen extends BasicTicket
{
    private static final int LINE_HEIGHT = 20;
    private static final double IMAGE_SCALE = 0.6;

    private static final String FONT_REGULAR = "Hack-Regular.ttf";
    private static final String FONT_BOLD = "Hack-Bold.ttf";
    private static final String FONT_ITALIC = "Hack-Italic.ttf";
    private static final String FONT_BOLDITALIC = "Hack-BoldItalic.ttf";
    private static final double DOTS = 11.0;

    private FontTransformer fontTransformer;

    public BasicTicketForScreen(int cpl, int dpl) {
        super();
        try {
            String base = System.getProperty("dirname.path");
            if (base == null) {
                base = ".";
            }
            AffineTransform a = AffineTransform.getScaleInstance(DOTS*(dpl/(cpl*12.0)), DOTS*1.40);
            Font regular = Font.createFont(Font.TRUETYPE_FONT,
                new java.io.File(base + "/res/fonts/" + FONT_REGULAR)).deriveFont(a);
            Font bold = Font.createFont(Font.TRUETYPE_FONT,
                new java.io.File(base + "/res/fonts/" + FONT_BOLD)).deriveFont(a);
            Font italic = Font.createFont(Font.TRUETYPE_FONT,
                new java.io.File(base + "/res/fonts/" + FONT_ITALIC)).deriveFont(a);
            Font boldItalic = Font.createFont(Font.TRUETYPE_FONT,
                new java.io.File(base + "/res/fonts/" + FONT_BOLDITALIC)).deriveFont(a);
            this.fontTransformer = new MultiFontTransformer(regular,
                    bold, italic, boldItalic, LINE_HEIGHT);
        } catch (java.awt.FontFormatException e) {
            Font f = new Font("Monospaced", Font.PLAIN, 12).deriveFont(AffineTransform.getScaleInstance(1.0*(dpl/(cpl*12.0)), 1.40));
            this.fontTransformer = new SysFontTransformer(f, LINE_HEIGHT);
        } catch (java.io.IOException e) {
            Font f = new Font("Monospaced", Font.PLAIN, 12).deriveFont(AffineTransform.getScaleInstance(1.0*(dpl/(cpl*12.0)), 1.40));
            this.fontTransformer = new SysFontTransformer(f, LINE_HEIGHT);
        }
    }

    @Override
    protected FontTransformer getFontTransformer() {
        return this.fontTransformer;
    }

    @Override
    protected double getImageScale() {
        return IMAGE_SCALE;
    }
}
