//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.ticket;

import java.awt.Font;
import java.awt.geom.AffineTransform;

/**
 * Derive system fonts from a base font with flags.
 */
public class SysFontTransformer extends FontTransformer
{
    protected Font baseFont;

    public SysFontTransformer(Font baseFont, int lineHeight) {
        this.baseFont = baseFont;
        this.lineHeight = lineHeight;
    }

    public Font getFont(int size, int style) {
        AffineTransform trans = this.getSizeTransform(size);
        if (trans != null) {
            return this.baseFont.deriveFont(style, trans);
        } else {
            return this.baseFont.deriveFont(style);
        }
    }
}
