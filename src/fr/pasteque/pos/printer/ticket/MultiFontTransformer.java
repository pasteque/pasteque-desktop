//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.ticket;

import java.awt.Font;
import java.awt.geom.AffineTransform;

/**
 * Pick font from multiple ones according to the given style.
 */
public class MultiFontTransformer extends FontTransformer
{
    protected Font baseRegular;
    protected Font baseBold;
    protected Font baseItalic;
    protected Font baseBoldItalic;

    public MultiFontTransformer(Font regular, Font bold, Font italic,
            Font boldItalic, int lineHeight) {
        this.baseRegular = regular;
        this.baseBold = bold;
        this.baseItalic = italic;
        this.baseBoldItalic = boldItalic;
        this.lineHeight = lineHeight;
    }

    public Font getFont(int size, int style) {
        AffineTransform trans = this.getSizeTransform(size);
        int s = getStyleFlags(style);
        Font f;
        switch (s) {
            case Font.BOLD:
                f = this.baseBold;
                break;
            case Font.ITALIC:
                f = this.baseItalic;
                break;
            case (Font.BOLD | Font.ITALIC):
                f = this.baseBoldItalic;
                break;
            case Font.PLAIN:
            default:
                f = this.baseRegular;
                break;
        }
        if (trans != null) {
            trans.preConcatenate(f.getTransform());
            return f.deriveFont(style, trans);
        } else {
            return f;
        }
    }
}
