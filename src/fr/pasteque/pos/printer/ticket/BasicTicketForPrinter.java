//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.ticket;

import java.awt.Font;
import java.awt.geom.AffineTransform;

/**
 * Utility to print ticket elements that will be printed to
 * a system printer.
 * It uses a default monospaced font.
 * @author jaroslawwozniak
 * @author adrianromero
 */
public class BasicTicketForPrinter extends BasicTicket
{
//        private static BASEFONT = new Font("Monospaced", Font.PLAIN, 7).deriveFont(AffineTransform.getScaleInstance(1.0, 1.50));
//        private static int FONTHEIGHT = 14;
    private static Font BASEFONT = new Font("Monospaced", Font.PLAIN, 7).deriveFont(AffineTransform.getScaleInstance(1.0, 1.40));
    private static final int LINE_HEIGHT = 12;
    private static final double IMAGE_SCALE = 0.65;

    private FontTransformer fontTransformer;

    public BasicTicketForPrinter() {
        super();
        this.fontTransformer = new SysFontTransformer(BASEFONT, LINE_HEIGHT);
    }

    @Override
    protected FontTransformer getFontTransformer() {
        return this.fontTransformer;
    }

    @Override
    protected double getImageScale() {
        return IMAGE_SCALE;
    }
}
