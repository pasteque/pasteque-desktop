//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer;

/**
 * Interface to animate text on customer displays.
 * @author adrianromero
 */
public interface DisplayAnimator
{
    /**
     * Set the current animation time.
     * @param i The animation time. Animator should update the text to display
     * according to an evergrowing time at their given pace.
     */
    public void setTiming(int i);
    /** Get the first line of text according to the current time. */
    public String getLine1();
    /** Get the second line of text according to the current time. */
    public String getLine2();
}
