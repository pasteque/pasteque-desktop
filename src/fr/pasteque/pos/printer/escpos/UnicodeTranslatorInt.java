//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

public class UnicodeTranslatorInt extends UnicodeTranslator
{
    /** Creates a new instance of UnicodeTranslatorInt */
    public UnicodeTranslatorInt() {
    }

    public byte[] getCodeTable() {
        return ESCPOS.CODE_TABLE_13;
    }

    public byte[] transString(String sCad) {

        if (sCad == null)
            return null;

       	try {
            return sCad.getBytes("cp858");
       	} catch (java.io.UnsupportedEncodingException e) {
       		return null;
       	}
    }

    public byte transChar(char sChar) {
        return (byte) sChar;
    }
}
