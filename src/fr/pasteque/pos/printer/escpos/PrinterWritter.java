//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Printer and customer display access class that handles sending bytes
 * to the device.
 * The default implementation sends data in a separated thread.
 */
public abstract class PrinterWritter
{
    private boolean initialized = false;

    private ExecutorService exec;

    public PrinterWritter() {
        exec = Executors.newSingleThreadExecutor();
    }

    /** Send the byte array to the printer. */
    protected abstract void internalWrite(byte[] data);
    protected abstract void internalFlush();
    /** Flush and close the connection to the printer. */
    protected abstract void internalClose();

    /**
     * Initialize the connection to the printer and send
     * the initalization commands.
     */
    public void init(final byte[] data) {
        if (!initialized) {
            write(data);
            initialized = true;
        }
    }

    /**
     * Send a string to the printer.
     * @param sValue The string that will be converted to bytes.
     */
    public void write(String sValue) {
        write(sValue.getBytes());
    }

    /**
     * Send commands to the printer.
     * @param data The commands to send.
     */
    public void write(final byte[] data) {
        exec.execute(new Runnable() {
            public void run() {
                internalWrite(data);
            }
        });
    }

    /** Send the flush command to the printer. */
    public void flush() {
        exec.execute(new Runnable() {
            public void run() {
                internalFlush();
            }
        });
    }

    /**
     * Close the connection to the printer. A new connection must be
     * created to use the printer again.
     */
    public void close() {
        exec.execute(new Runnable() {
            public void run() {
                internalClose();
            }
        });
        exec.shutdown();
    }
}
