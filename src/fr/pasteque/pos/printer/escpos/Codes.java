//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

import fr.pasteque.pos.printer.DevicePrinter;
import fr.pasteque.pos.printer.DeviceTicket;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

/**
 * Helper and interface for command sets.
 */
public abstract class Codes
{
    /** Creates a new instance of Codes */
    public Codes() {
    }

    /** Get the byte sequence that initializes the device. */
    public abstract byte[] getInitSequence();

    /** Get the byte sequence to print in normal size. */
    public abstract byte[] getSize0();
    /** Get the byte sequence to print in tall size. */
    public abstract byte[] getSize1();
    /** Get the byte sequence to print in large size. */
    public abstract byte[] getSize2();
    /** Get the byte sequence to print in tall and large size. */
    public abstract byte[] getSize3();

    /** Get the byte sequence to start printing text in bold. */
    public abstract byte[] getBoldSet();
    /** Get the byte sequence to stop printing text in bold. */
    public abstract byte[] getBoldReset();
    /** Get the byte sequence to activate printing text underlined. */
    public abstract byte[] getUnderlineSet();
    /** Get the byte sequence to stop printing text underlined. */
    public abstract byte[] getUnderlineReset();

    /** Get the byte sequence to open the drawer. */
    public abstract byte[] getOpenDrawer();
    /** Get the byte sequence to cut paper. */
    public abstract byte[] getCutReceipt();
    /** Get the byte sequence to end line. */
    public abstract byte[] getNewLine();

    /**
     * Write bytes to the output stream. Similar to
     * ByteArrayOutputStream.writeBytes available from java 11.
     */
    protected void writeBytes(ByteArrayOutputStream out, byte[] b) {
        out.write(b, 0, b.length);
    }

    /**
     * Get the byte sequence that prints a barcode.
     * @param type See DevicePrinter constants
     * @param position See DevicePrinter constants
     * @param code The barcode encode and print.
     */
    public byte[] getBarcode(String type, String position, String code) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        if (DevicePrinter.BARCODE_EAN13.equals(type)) {
            writeBytes(out, getNewLine());
            writeBytes(out, ESCPOS.BAR_HEIGHT);
            if (DevicePrinter.POSITION_NONE.equals(position)) {
                writeBytes(out, ESCPOS.BAR_POSITIONNONE);
            } else {
                writeBytes(out, ESCPOS.BAR_POSITIONDOWN);
            }
            writeBytes(out, ESCPOS.BAR_HRIFONT1);
            writeBytes(out, ESCPOS.BAR_CODE02);
            writeBytes(out, DeviceTicket.transNumber(DeviceTicket.alignBarCode(code,13).substring(0,12)));
            writeBytes(out, new byte[] { 0x00 });
            writeBytes(out, getNewLine());
        }
        return out.toByteArray();
    }

    /** Convert a count of bits to the number of required bytes. */
    protected int getByteSize(int bits) {
        return (bits + 7) / 8; // same as +1 if (bits % 8 > 0)
    }

    /**
     * Get the byte sequence that prints a centered image.
     * @param image The image to print.
     * @param dotWidth The number of dots per line of the printer. The image
     * will be centered within these limits but the image is not resized.
     * If the image is smaller than dotWidth, it is centered, if it is larger
     * it is cropped and centered within this limit.
     */
    public byte[] getImage(BufferedImage image, int dotWidth) {
        int height = image.getHeight();
        int dotByteWidth = getByteSize(dotWidth);
        int dotByteSize = dotByteWidth * height;
        ByteArrayOutputStream out = new ByteArrayOutputStream(8 + dotByteSize + 8);
        /* Store the image in the buffer (GS v 0 of ESCPOS).
         * This function is marked as obsolete in Epson documentation but seems to
         * have better support and result than GS ( L.
         * The command is GS v 0 m xL xH yL yH data.
         * GS v 0 is the command name,
         * m=0 (print scale x1, maximum density)
         * xL xH is the number of bytes for a line, defined on 2 bytes,
         * yL yH is the height in dots of the image, defined on 2 bytes,
         * data is the image data, with bit 0 blank and bit 1 printed. */
        writeBytes(out, new byte[] {0x1D, 0x76, 0x30, 0x00}); // GS v 0 0
        out.write(dotByteWidth % 256); // xL
        out.write(dotByteWidth / 256); // xH
        out.write(height % 256); // yL
        out.write(height / 256); // yH
        this.appendImageData(out, image, dotWidth);
        return out.toByteArray();
    }

    /**
     * Append the byte content of the printed image to the stream. A line
     * is output as a sequence of bytes so the width is always a factor of 8.
     * @param out The stream to append the image data to.
     * @param image The image to print.
     * @param dotWidth The width of the frame to center the image into.
     */
    protected void appendImageData(ByteArrayOutputStream out,
            BufferedImage image, int dotWidth) {
        CenteredImage centeredImage = new CenteredImage(image, dotWidth);
        for (int y = 0; y < centeredImage.getHeight(); y++) {
            for (int x = 0; x < centeredImage.getFrameWidth(); x+=8) {
                int b = 0x00;
                for (int d = 0; d < 8; d++) {
                    b = b << 1;
                    if (centeredImage.isBlack(x + d, y)) {
                        b = b | 0x01;
                    }
                }
                out.write(b);
            }
        }
    }

    /** Utility class to access pixel data within a limited width frame. */
    protected class CenteredImage
    {
        private BufferedImage image;
        private int frameWidth;
        private int imageWidth;
        private int imageHeight;
        private int frameMarginLeft;
        private boolean hasAlpha;

        public CenteredImage(BufferedImage image, int width) {
            this.image = image;
            this.frameWidth = width;
            this.imageWidth = image.getWidth();
            this.imageHeight = image.getHeight();
            this.frameMarginLeft = (this.frameWidth - this.imageWidth) / 2;
            if (image.getType() == BufferedImage.TYPE_4BYTE_ABGR) {
                this.hasAlpha = true;
            }
        }

        /** Get image height. The height is not modified by centering. */
        public int getHeight() {
            return this.imageHeight;
        }

        /** Get the frame width limit. */
        public int getFrameWidth() {
            return this.frameWidth;
        }

        /**
         * Check if the pixel in the frame should be printed or not.
         * @param x The x coordinate of the pixel within the frame limit. This
         * matches the x coordinate of the outputed dot.
         * @param y The y coordinate of the pixel from the image.
         */
        public boolean isBlack(int x, int y) {
            int imageX = x - this.frameMarginLeft;
            if (imageX < 0 || imageX >= this.imageWidth
                    || y < 0 || y >= this.imageHeight) {
                return false;
            } else {
                int rgb = image.getRGB(imageX, y);
                int gray = (int)(0.30 * ((rgb >> 16) & 0xff) +
                                 0.59 * ((rgb >> 8) & 0xff) +
                                 0.11 * (rgb & 0xff));
                if (this.hasAlpha) {
                    float fAlpha = (int)(rgb >> 24 & 0xff) / 255.0f;
                    gray = gray + Math.round((255 - gray) * (1.0f - fAlpha));
                }
                return gray < 128;
            }
        }
    }
}
