//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Writter to send bytes over TCP/IP.
 */
public class PrinterWritterNetwork extends PrinterWritter
{
    public static final int DEFAULT_PORT = 9100;

    private InetAddress address;
    private int port;
    private OutputStream out;
    private Socket socket;

    /**
     * Create and open a connection to a printer on the network.
     * @param address The IP adress or hostname with port. Default port is 9100.
     * @throws NumberFormatException When the port is not a number.
     * @throws UnknownHostException When the IP address cannot be read.
     * @throws IOException When the connection cannot be established.
     */
    public PrinterWritterNetwork(String address) throws UnknownHostException, IOException, NumberFormatException {
        this.port = DEFAULT_PORT;
        if (address.startsWith("[")) {
            // IPv6 with port
            String[] parts = address.split("]");
            if (parts.length == 2) {
                this.port = Integer.parseInt(parts[1]);
                address = parts[0].substring(1);
            } else if (parts.length == 1) {
                address = parts[0].substring(1);
            } else {
                throw new UnknownHostException(address + ": IPv6 with port should be formatted as [<addr>]:<port>");
            }
        } else {
            String[] parts = address.split(":");
            if (parts.length == 2) {
                this.port = Integer.parseInt(parts[1]);
                address = parts[0];
            } // else assume IPv6 without port
        }
        this.address = InetAddress.getByName(address);
        // First connection try to check the address
        this.socket = new Socket(this.address, this.port);
        this.out = socket.getOutputStream();
    }

    private void connect() throws IOException {
        this.socket = new Socket(this.address, this.port);
        this.out = socket.getOutputStream();
    }

    protected void internalWrite(byte[] data) {
        try {
            if (this.out == null) {
                this.connect();
            }
            this.out.write(data);
        } catch (IOException e) {
            // Try to reconnect in case of broken pipe
            try {
                this.connect();
                this.out.write(data);
            } catch (IOException e2) {
                // Fail
                if (this.out != null) {
                    try {
                        this.out.close();
                    } catch (IOException closeE) {}
                    this.out = null;
                }
                System.err.println(e2);
            }
        }
    }

    protected void internalFlush() {
        // Flush and close connection to use the socket
        // as a single operation connection.
        this.internalClose();
    }

    protected void internalClose() {
        try {
            if (this.out != null) {
                this.out.flush();
                this.out.close();
                this.out = null;
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
