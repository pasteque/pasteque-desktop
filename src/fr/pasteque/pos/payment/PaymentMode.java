//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentMode implements Serializable
{
    private static final long serialVersionUID = -4160529106606464564L;

    /** Must be assigned to a customer */
    public static final int CUST_ASSIGNED = 1;
    /** Is debt (includes CUST_ASSIGNED) */
    public static final int CUST_DEBT = 2 + CUST_ASSIGNED;
    public static final int CUST_PREPAID = 4 + CUST_ASSIGNED;

    private int id;
    private String code;
    private String label;
    private String backLabel;
    private int flags;
    private boolean hasImage;
    private List<PaymentModeValue> values;
    private List<Return> rules;
    private boolean active;
    private int dispOrder;
    private transient BufferedImage image;

    public PaymentMode(int id, String code, String label, String backLabel,
            int flags, boolean hasImage, List<PaymentModeValue> values,
            List<Return> rules, boolean active, int dispOrder) {
        this.id = id;
        this.code = code;
        this.label = label;
        this.backLabel = backLabel;
        this.flags = flags;
        this.hasImage = hasImage;
        if (values == null) {
            this.values = new ArrayList<PaymentModeValue>();
        } else {
            this.values = values;
        }
        if (rules == null) {
            this.rules = new ArrayList<Return>();
        } else {
            this.rules = rules;
        }
        this.active = active;
        this.dispOrder = dispOrder;
    }

    public PaymentMode(JSONObject o) throws JSONException {
        this.id = o.getInt("id");
        this.code = o.getString("reference");
        this.label = o.getString("label");
        this.backLabel = o.getString("backLabel");
        this.flags = o.getInt("type");
        this.hasImage = o.getBoolean("hasImage");
        this.active = o.getBoolean("visible");
        this.dispOrder = o.getInt("dispOrder");
        List<PaymentModeValue> values = new ArrayList<PaymentModeValue>();
        JSONArray jsValues = o.getJSONArray("values");
        for (int i = 0; i < jsValues.length(); i++) {
            values.add(new PaymentModeValue(jsValues.getJSONObject(i)));
        }
        this.values = values;
        List<Return> rules = new ArrayList<Return>();
        JSONArray jsRules = o.getJSONArray("returns");
        for (int i = 0; i < jsRules.length(); i++) {
            rules.add(Return.fromJSON(jsRules.getJSONObject(i)));
        }
	this.rules = rules;
    }

    public int getId() {
        return this.id;
    }

    public String getLabel() {
        return this.label;
    }

    public String getBackLabel() {
        return this.backLabel;
    }

    public String getCode() {
        return this.code;
    }

    public boolean isActive() {
        return this.active;
    }

    public boolean hasImage() {
        return this.hasImage;
    }

    public boolean isDebt() {
        return (this.flags & CUST_DEBT) == CUST_DEBT;
    }
    public boolean isPrepaid() {
        return (this.flags & CUST_PREPAID) == CUST_PREPAID;
    }
    public boolean isCustAssigned() {
        return (this.flags & CUST_ASSIGNED) == CUST_ASSIGNED;
    }

    public int getDispOrder() {
        return this.dispOrder;
    }

    public List<PaymentModeValue> getValues() {
        return this.values;
    }

    public List<PaymentMode.Return> getRules() {
        return this.rules;
    }

    public Integer getReturnModeId(double exceedentAmount) {
        PaymentMode.Return ret = null;
        // Keep the latest mode that applies for the exceedent
        for (PaymentMode.Return r : this.rules) {
            if (r.appliesFor(exceedentAmount)) {
                ret = r;
            } else {
                break;
            }
        }
        // Return its return mode
        if (ret != null) {
            return ret.getReturnModeId();
        } else {
            return null;
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("code", this.code);
        return o;
    }

    public BufferedImage getImage() {
        return this.image;
    }

    public void setImage(BufferedImage img) {
        this.image = img;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof PaymentMode)
                && ((PaymentMode)o).code.equals(this.code);
    }
    @Override
    public int hashCode() {
        return this.code.hashCode();
    }

    @SuppressWarnings("serial")
    public static class Return implements Serializable
    {
        //TODO v9: add serialVersionUID not to mess with local caches
        //private static final long serialVersionUID = -6327375625208284252L;

        private double minVal;
        private Integer returnId;

        public Return(double minVal, Integer returnId) {
            this.minVal = minVal;
            this.returnId = returnId;
        }

        public static Return fromJSON(JSONObject o) throws JSONException {
            double minVal = o.getDouble("minAmount");
            Integer returnId = o.getInt("returnMode");
            return new Return(minVal, returnId);
        }

        public double getMinVal() {
            return this.minVal;
        }

        public boolean hasReturnMode() {
            return this.returnId != null;
        }

        public int getReturnModeId() {
            return this.returnId;
        }

        /** Check if the rule applies for a given exceedent */
        public boolean appliesFor(double exceedent) {
            return exceedent - this.minVal > -0.005;
        }
    }
}
