//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.forms.shortcuts.CustomShortcut;
import fr.pasteque.pos.forms.shortcuts.CustomShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.ComponentOrientation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author adrianromero
 */
public class JPaymentSelect extends javax.swing.JDialog
implements JPaymentNotifier, ActionListener, ShortcutListener, CustomShortcutListener
{
    private static final long serialVersionUID = -3141311557074549894L;
    public static final int PRINTER_COUNT = 3;

    private PaymentInfoList m_aPaymentInfo;
    private boolean[] printAvailable;
    private boolean[] printSelected;
    private int printCount;

    private boolean accepted;

    private AppView app;
    private ComponentOrientation orientation;
    private double m_dTotal;
    private int parts;
    private double nextPartAmount; // Rounded part amount to get exactly total
    private CustomerInfoExt customerext;
    private DataLogicSystem dlSystem;

    private Map<String, JPaymentInterface> payments = new HashMap<String, JPaymentInterface>();
    private String m_sTransactionID;

    private List<CurrencyInfo> currencies;
    private CurrencyInfo selectedCurrency;

    /** Creates new form JPaymentSelect */
    protected JPaymentSelect(java.awt.Frame parent, boolean modal, ComponentOrientation o) {
        super(parent, modal);
        this.orientation = o;
        this.printAvailable = new boolean[PRINTER_COUNT];
        this.printSelected = new boolean[PRINTER_COUNT];
    }
    /** Creates new form JPaymentSelect */
    protected JPaymentSelect(java.awt.Dialog parent, boolean modal, ComponentOrientation o) {
        super(parent, modal);
        this.orientation = o;
        this.printAvailable = new boolean[PRINTER_COUNT];
        this.printSelected = new boolean[PRINTER_COUNT];
    }

    public static JPaymentSelect getDialog(Component parent) {
        Window window = getWindow(parent);
        if (window instanceof Frame) {
            return new JPaymentSelect((Frame) window, true, parent.getComponentOrientation());
        } else {
            return new JPaymentSelect((Dialog) window, true, parent.getComponentOrientation());
        }
    }

    public void init(AppView app) {
        this.app = app;
        AppConfig cfg = AppConfig.loadedInstance;
        dlSystem = new DataLogicSystem();
        boolean autoPrint = cfg.getBoolean("ui.printticketbydefault");
        if (!"Not defined".equals(cfg.getProperty("machine.printer"))
                && cfg.getBoolean("machine.printer.filter.ticket")) {
            this.printSelected[0] = autoPrint;
            this.printAvailable[0] = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.2"))
                && cfg.getBoolean("machine.printer.2.filter.ticket")) {
            this.printSelected[1] = autoPrint;
            this.printAvailable[1] = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.3"))
                && cfg.getBoolean("machine.printer.3.filter.ticket")) {
            this.printSelected[2] = autoPrint;
            this.printAvailable[2] = true;
        }
        try {
            this.printCount = Integer.parseInt(cfg.getProperty("ui.printticketcount"));
        } catch (NumberFormatException e) {
            this.printCount = 1;
        }
        // Init currencies
        DataLogicSales dlSales = new DataLogicSales();
        try {
            this.currencies = dlSales.getCurrenciesList();
        } catch (BasicException e) {
            this.currencies = new ArrayList<CurrencyInfo>();
            e.printStackTrace();
        }
        // Init components
        initComponents();
        this.applyComponentOrientation(this.orientation);
        // Init currencies combo box
        this.currencyCbx.addItems(this.currencies);
        this.currencyCbx.setSelectedIndex(0);
        this.selectedCurrency = this.currencies.get(0);
        currencyCbx.addActionListener(this);
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts shortcut) {
        switch (shortcut) {
            case PAYMENT_UP:
                m_jTabPayment.setSelectedIndex(Math.max(0, m_jTabPayment.getSelectedIndex() - 1));
                return true;
            case PAYMENT_DOWN:
                m_jTabPayment.setSelectedIndex(Math.min(m_jTabPayment.getSelectedIndex() + 1, m_jTabPayment.getTabCount() - 1));
                return true;
            case PAYMENT_PRINT:
                this.togglePrintAll();
                return true;
            case PAYMENT_DIVPLUS:
                this.jbtnPartsPlus.doClick(Shortcuts.CLICK_TIME);
                return true;
            case PAYMENT_DIVMINUS:
                this.jbtnPartsMinus.doClick(Shortcuts.CLICK_TIME);
                return true;
            case GENERAL_VALIDATE:
                if (m_jButtonOK.isEnabled()) {
                    m_jButtonOK.doClick(Shortcuts.CLICK_TIME);
                } else if (m_jButtonAdd.isEnabled()) {
                    m_jButtonAdd.doClick(Shortcuts.CLICK_TIME);
                }
                return true;
            case GENERAL_CANCEL:
                if (m_jButtonRemove.isEnabled()) {
                    m_jButtonRemove.doClick(Shortcuts.CLICK_TIME);
                } else {
                    m_jButtonCancel.doClick(Shortcuts.CLICK_TIME);
                }
                return true;
            default: // Ignore
                return false;
        }
    }

    @Override
    public void customShortcutPressed(CustomShortcut cs) {
        // From CustomShortcutListener
        if (cs.getType() == CustomShortcut.TYPE_PAYMENTMODE) {
            // TODO: hardcoded payment mode code check
            Map<String, String> codeMap = new HashMap<String, String>();
            codeMap.put("cash", "payment.cash");
            codeMap.put("cheque", "payment.cheque");
            codeMap.put("paper", "payment.paper");
            codeMap.put("magcard", "payment.magcard");
            codeMap.put("free", "payment.free");
            codeMap.put("debt", "payment.debt");
            // Select the payment mode tab
            String tabKey = codeMap.get(cs.getShortcut());
            if (tabKey != null) {
                m_jTabPayment.setSelectedComponent(payments.get(tabKey).getComponent());
            }
        } else if (cs.getType() == CustomShortcut.TYPE_CURRENCY) {
            // TODO: select currency
        }
    }

    public void setPrintSelected(boolean value) {
        for (int i = 0; i < this.printSelected.length; i++) {
            if (this.printAvailable[i]) {
                this.printSelected[i] = value;
            }
        }
    }

    public boolean isPrintSelected() {
        for (int i = 0; i < this.printSelected.length; i++) {
            if (this.printAvailable[i] && this.printSelected[i]) {
                return true;
            }
        }
        return false;
    }
    public boolean isPrintSelected(int printerNum) {
        return this.printAvailable[printerNum] && this.printSelected[printerNum];
    }

    public int getPrintCount() {
        return this.printCount;
    }

    public List<PaymentInfo> getSelectedPayments() {
        return m_aPaymentInfo.getPayments();
    }

    public boolean showDialog(double total, CustomerInfoExt customerext) {
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.PAYMENTS, this);
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
        config.addCustomListener(CustomShortcut.TYPE_PAYMENTMODE,
                this.rootPane, this);

        m_aPaymentInfo = new PaymentInfoList();
        accepted = false;

        m_dTotal = total;
        this.parts = 1;

        this.customerext = customerext;

        this.refreshPrint();
        this.printTotal();

        addTabs();

        if (m_jTabPayment.getTabCount() == 0) {
            // No payment panels available
            addTabPayment(new JPaymentSelect.JPaymentEmptyCreator());
        } else {
            refreshParts();
            printState();
            setVisible(true);
        }

        // gets the print button state
        this.printSelected[0] = this.printAvailable[0] && m_jButtonPrint.isSelected();
        this.printSelected[1] = this.printAvailable[1] && m_jButtonPrint2.isSelected();
        this.printSelected[2] = this.printAvailable[2] && m_jButtonPrint3.isSelected();

        // remove all tabs
        m_jTabPayment.removeAll();

        return accepted;
    }

    @Override
    public void dispose() {
        // Remove shortcut listeners before destroying the components
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.removeListener(this.rootPane, ShortcutSections.PAYMENTS, this);
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
        config.removeCustomListener(CustomShortcut.TYPE_PAYMENTMODE,
                this.rootPane, this);
        super.dispose();
    }

    protected void addTabs() {
        AppConfig cfg = AppConfig.loadedInstance;

        // Add empty payment tab
        if (cfg != null
                && cfg.getProperty("ui.useemptypayment").equals("1")) {
            addTabPayment(new JPaymentSelect.JPaymentEmptyCreator());
        }
        // Add all other payment tabs
        DataLogicSales dlSales = new DataLogicSales();
        try {
            for (PaymentMode pm : dlSales.getPaymentModes()) {
                if (pm.isActive()) {
                    addTabPayment(new JPaymentSelect.JPaymentGenericCreator(pm));
                }
            }
        } catch (BasicException e) {

        }
        setHeaderVisible(true);
    }

    protected void setOKEnabled(boolean value) {
        m_jButtonOK.setEnabled(value);
    }

    protected void setAddEnabled(boolean value) {
        m_jButtonAdd.setEnabled(value);
    }

    protected void addTabPayment(JPaymentCreator jpay) {
        if (jpay.getKey().startsWith("#") | app.getAppUserView().getUser().hasPermission(jpay.getKey())) {

            JPaymentInterface jpayinterface = payments.get(jpay.getKey());
            if (jpayinterface == null) {
                jpayinterface = jpay.createJPayment();
                payments.put(jpay.getKey(), jpayinterface);
            }

            jpayinterface.getComponent().applyComponentOrientation(getComponentOrientation());
            ImageIcon icon = jpay.getIcon();
            m_jTabPayment.addTab(
                    AppLocal.getIntString(jpay.getLabelKey()),
                    icon,
                    jpayinterface.getComponent());
        }
    }


    public interface JPaymentCreator {
        public JPaymentInterface createJPayment();
        public String getKey();
        public String getLabelKey();
        public ImageIcon getIcon();
    }

    /** Special Payment creator for the empty tab. */
    public class JPaymentEmptyCreator implements JPaymentCreator {
        public JPaymentInterface createJPayment() {
            return new JPaymentEmpty(JPaymentSelect.this);
        }
        public String getKey() { return "#empty"; } // Start with '#', so won't check if enabled
        public String getLabelKey() { return "tab.empty"; }
        public ImageIcon getIcon() {
            return WidgetsBuilder.createIcon(ImageLoader.readImageIcon("button_generic.png"));
        }
    }

    public class JPaymentGenericCreator implements JPaymentCreator {
        private PaymentMode pm;
        public JPaymentGenericCreator(PaymentMode pm) {
            this.pm = pm;
        }
        public JPaymentInterface createJPayment() {
            return new JPaymentTab(this.pm, JPaymentSelect.this, JPaymentSelect.this.dlSystem);
        }
        public String getKey() { return "payment." + this.pm.getCode(); }
        public String getLabelKey() { return this.pm.getLabel(); }
        public ImageIcon getIcon() {
            if (this.pm.hasImage()) {
                AppConfig cfg = AppConfig.loadedInstance;
                int minWidth = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnminwidth")));
                int minHeight = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnminheight")));
                ThumbNailBuilder tnb = new ThumbNailBuilder(minWidth, minHeight, "button_generic.png");
                return new ImageIcon(tnb.getThumbNail(this.pm.getImage()));
            } else {
                return WidgetsBuilder.createIcon(ImageLoader.readImageIcon("button_generic.png"));
            }
        }
    }

    protected void setHeaderVisible(boolean value) {
        jPanel6.setVisible(value);
    }

    private void switchCurrency(CurrencyInfo currency) {
        this.selectedCurrency = currency;
        this.printTotal();
        this.printState();
    }

    private void printTotal() {
        double totalCurrency = this.selectedCurrency.convertFromMain(m_dTotal);
        Formats.setAltCurrency(this.selectedCurrency);
        m_jTotalEuros.setText(Formats.CURRENCY.formatValue(Double.valueOf(totalCurrency)));
    }

    private void printState() {
        double remainingMain = Price.add(m_dTotal, -m_aPaymentInfo.getTotal());
        double remainingCurrency = this.selectedCurrency.convertFromMain(remainingMain);
        Formats.setAltCurrency(this.selectedCurrency);
        m_jRemaininglEuros.setText(Formats.CURRENCY.formatValue(Double.valueOf(remainingCurrency)));
        m_jButtonRemove.setEnabled(!m_aPaymentInfo.isEmpty());
        // Refresh current payment panel
        double partCurrency = this.selectedCurrency.convertFromMain(this.nextPartAmount);
        ((JPaymentInterface) m_jTabPayment.getSelectedComponent()).activate(
                customerext, remainingCurrency, partCurrency,
                this.selectedCurrency, m_sTransactionID);
    }

    private void refreshParts() {
        double partAmount = this.m_dTotal / this.parts;
        int remainingParts = this.parts - this.m_aPaymentInfo.size();
        if (remainingParts <= 0) {
            // Not usefull isn't it ?
            this.nextPartAmount = Price.add(m_dTotal, -m_aPaymentInfo.getTotal());
        } else {
            double roundedPartAmount = (double)Math.round(partAmount * 100) / 100;
            if (remainingParts == 1) {
                // Last one is unlucky (or not)
                this.nextPartAmount = Price.add(m_dTotal, -m_aPaymentInfo.getTotal());
            } else {
                this.nextPartAmount = roundedPartAmount;
            }
        }
        // Refresh display
        this.jlblPartsNumber.setText(String.valueOf(this.parts));
        this.jlblPartAmount.setVisible(this.parts != 1);
        this.jlblPartAmountLabel.setVisible(this.parts != 1);
        this.jlblPartAmount.setText(Formats.CURRENCY.formatValue(Double.valueOf(partAmount)));
        // Reactivate current tab to refresh part
        double remainingCurrency = this.selectedCurrency.convertFromMain(
                Price.add(m_dTotal, -m_aPaymentInfo.getTotal()));
        double partCurrency = this.selectedCurrency.convertFromMain(this.nextPartAmount);
        ((JPaymentInterface) m_jTabPayment.getSelectedComponent()).activate(
                customerext, remainingCurrency, partCurrency,
                this.selectedCurrency, m_sTransactionID);
    }

    private void togglePrint(int printerNum) {
        if (this.printAvailable[printerNum]) {
            this.printSelected[printerNum] = !this.printSelected[printerNum];
            this.refreshPrint();
        }
    }
    private void togglePrintAll() {
        boolean allPrintEnabled = true;
        for (int i = 0; i < this.printSelected.length; i++) {
            if (this.printAvailable[i] && !this.printSelected[i]) {
                allPrintEnabled = false;
                break;
            }
        }
        for (int i = 0; i < this.printSelected.length; i++) {
            if (this.printAvailable[i]) {
                this.printSelected[i] = !allPrintEnabled;
            }
        }
        this.refreshPrint();
    }

    private void refreshPrint() {
        boolean hasPrint = false;
        for (int i = 0; i < this.printSelected.length; i++) {
            if (this.printAvailable[i] && this.printSelected[i]) {
                hasPrint = true;
                break;
            }
        }
        this.m_jButtonPrint.setSelected(this.isPrintSelected(0));
        this.m_jButtonPrint2.setSelected(this.isPrintSelected(1));
        this.m_jButtonPrint3.setSelected(this.isPrintSelected(2));
        if (hasPrint) {
            this.jlblPrintCount.setText(String.valueOf(this.printCount));
            this.jbtnPrintPlus.setEnabled(true);
            this.jbtnPrintMinus.setEnabled(this.printCount > 1);
        } else {
            this.jlblPrintCount.setText("0");
            this.jbtnPrintPlus.setEnabled(false);
            this.jbtnPrintMinus.setEnabled(false);
        }
    }

    protected static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window)parent;
        } else {
            return getWindow(parent.getParent());
        }
    }

    /**
     * Update the confirmation buttons.
     * @param acceptPayment When true, the current payment is acceptable.
     * @param isComplete When true, the current payment will fulfill the payment.
     */
    public void setStatus(boolean acceptPayment, boolean isComplete) {
        setAddEnabled(acceptPayment && !isComplete);
        setOKEnabled(isComplete);
    }

    public void setTransactionID(String tID){
        this.m_sTransactionID = tID;
    }

    public void actionPerformed(java.awt.event.ActionEvent evt) {
        CurrencyInfo c = this.currencies.get(currencyCbx.getSelectedIndex());
        this.switchCurrency(c);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        m_jButtonAdd = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_plus.png"));
        m_jButtonRemove = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_minus.png"));
        jPanel3 = new javax.swing.JPanel();
        m_jTabPayment = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        m_jButtonPrint = new javax.swing.JToggleButton();
        m_jButtonPrint2 = new javax.swing.JToggleButton();
        m_jButtonPrint3 = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        m_jButtonOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                                                  AppLocal.getIntString("Button.OK"),
                                                  WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                                                  AppLocal.getIntString("Button.Cancel"),
                                                  WidgetsBuilder.SIZE_MEDIUM);
        currencyCbx = WidgetsBuilder.createComboBoxVal();

        // Total/remaining/part amount init
        m_jLblTotalEuros1 = WidgetsBuilder.createImportantLabel(AppLocal.getIntString("label.totalcash"));
        m_jTotalEuros = WidgetsBuilder.createImportantLabel();
        m_jRemaininglEuros = WidgetsBuilder.createImportantLabel();
        m_jLblRemainingEuros = WidgetsBuilder.createImportantLabel(AppLocal.getIntString("label.remainingcash"));
        jlblPartAmountLabel = WidgetsBuilder.createImportantLabel(AppLocal.getIntString("label.partamount"));
        jlblPartAmount = WidgetsBuilder.createImportantLabel();
        WidgetsBuilder.inputStyle(jlblPartAmount);
        WidgetsBuilder.inputStyle(m_jTotalEuros);
        WidgetsBuilder.inputStyle(m_jRemaininglEuros);

        // Parts init
        JLabel jlblParts = WidgetsBuilder.createLabel(AppLocal.getIntString("label.parts"));
        jlblPartsNumber = WidgetsBuilder.createLabel(String.valueOf(parts));
        WidgetsBuilder.inputStyle(jlblPartsNumber);
        jbtnPartsPlus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_plus.png"));
        jbtnPartsMinus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_minus.png"));
        jbtnPartsPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                partsPlusActionPerformed(evt);
            }
        });
        jbtnPartsMinus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                partsMinusActionPerformed(evt);
            }
        });
        jbtnPartsPlus.setFocusable(false);
        jbtnPartsMinus.setFocusable(false);

        // Print count init
        jlblPrintCount = WidgetsBuilder.createLabel("1");
        jbtnPrintPlus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_plus.png"));
        jbtnPrintMinus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("payment_minus.png"));
        jbtnPrintPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printPlusActionPerformed(evt);
            }
        });
        jbtnPrintMinus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printMinusActionPerformed(evt);
            }
        });
        jbtnPrintPlus.setFocusable(false);
        jbtnPrintMinus.setFocusable(false);
        m_jButtonPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printActionPerformed(evt, 0);
            }
        });
        m_jButtonPrint2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printActionPerformed(evt, 1);
            }
        });
        m_jButtonPrint3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printActionPerformed(evt, 2);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(AppLocal.getIntString("payment.title")); // NOI18N
        setResizable(false);

        // Total/remaining/part amount line
        jPanel4.add(m_jLblTotalEuros1);
        jPanel4.add(m_jTotalEuros);

        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 0));
        jPanel4.add(m_jLblRemainingEuros);
        jPanel4.add(m_jRemaininglEuros);

        jPanel4.add(jlblPartAmountLabel);
        jPanel4.add(jlblPartAmount);

        // Currency select
        if (this.currencies.size() > 1) {
            jPanel4.add(currencyCbx);
        }

        // Add/remove payment
        m_jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonAddActionPerformed(evt);
            }
        });
        jPanel6.add(m_jButtonAdd);

        m_jButtonRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonRemoveActionPerformed(evt);
            }
        });
        jPanel6.add(m_jButtonRemove);

        jPanel4.add(jPanel6);

        getContentPane().add(jPanel4, java.awt.BorderLayout.NORTH);

        jPanel3.setLayout(new java.awt.BorderLayout());

        m_jTabPayment.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        m_jTabPayment.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        m_jTabPayment.setFocusable(false);
        m_jTabPayment.setRequestFocusEnabled(false);
        m_jTabPayment.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                m_jTabPaymentStateChanged(evt);
            }
        });
        jPanel3.add(m_jTabPayment, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel5.setLayout(new java.awt.BorderLayout());

        // Parts line
        JPanel partsContainer = new JPanel();
        partsContainer.add(jlblParts);
        partsContainer.add(jbtnPartsMinus);
        partsContainer.add(jlblPartsNumber);
        partsContainer.add(jbtnPartsPlus);

        jPanel5.add(partsContainer, java.awt.BorderLayout.LINE_START);

        // Print/Ok/Cancel line
        boolean hasPrinter = false;
        m_jButtonPrint.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
        WidgetsBuilder.adaptSize(m_jButtonPrint, WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonPrint.setSelected(true);
        m_jButtonPrint.setFocusPainted(false);
        m_jButtonPrint.setFocusable(false);
        m_jButtonPrint.setRequestFocusEnabled(false);
        m_jButtonPrint2.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
        m_jButtonPrint2.setText("2");
        WidgetsBuilder.adaptSize(m_jButtonPrint2, WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonPrint2.setSelected(true);
        m_jButtonPrint2.setFocusPainted(false);
        m_jButtonPrint2.setFocusable(false);
        m_jButtonPrint2.setRequestFocusEnabled(false);
        m_jButtonPrint3.setIcon(ImageLoader.readImageIcon("tkt_print.png"));
        m_jButtonPrint3.setText("3");
        WidgetsBuilder.adaptSize(m_jButtonPrint3, WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonPrint3.setSelected(true);
        m_jButtonPrint3.setFocusPainted(false);
        m_jButtonPrint3.setFocusable(false);
        m_jButtonPrint3.setRequestFocusEnabled(false);
        if (!"Not defined".equals(cfg.getProperty("machine.printer.3"))
                && cfg.getBoolean("machine.printer.3.filter.ticket")) {
            jPanel2.add(m_jButtonPrint3);
            hasPrinter = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.2"))
                && cfg.getBoolean("machine.printer.2.filter.ticket")) {
            jPanel2.add(m_jButtonPrint2);
            hasPrinter = true;
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer"))
                && cfg.getBoolean("machine.printer.filter.ticket")) {
            jPanel2.add(m_jButtonPrint);
            hasPrinter = true;
        }
        if (hasPrinter) {
            jPanel2.add(jbtnPrintMinus);
            jPanel2.add(jlblPrintCount);
            jPanel2.add(jbtnPrintPlus);
        }
        jPanel2.add(jPanel1);

        m_jButtonOK.setFocusPainted(false);
        m_jButtonOK.setFocusable(false);
        m_jButtonOK.setRequestFocusEnabled(false);
        m_jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonOKActionPerformed(evt);
            }
        });
        jPanel2.add(m_jButtonOK);

        m_jButtonCancel.setFocusPainted(false);
        m_jButtonCancel.setFocusable(false);
        m_jButtonCancel.setRequestFocusEnabled(false);
        m_jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonCancelActionPerformed(evt);
            }
        });
        jPanel2.add(m_jButtonCancel);

        jPanel5.add(jPanel2, java.awt.BorderLayout.LINE_END);

        getContentPane().add(jPanel5, java.awt.BorderLayout.SOUTH);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        // Icons are to big for a 72dpi density, they don't fit in a popup less than 640x480px
        int minWidth = 680;
        int minHeight = 480;
        int width = WidgetsBuilder.dipToPx(460);
        int height = WidgetsBuilder.dipToPx(350);
        width = Math.max(minWidth, width);
        height = Math.max(minHeight, height);
        // If popup is big enough, make it fullscreen
        if (width > 0.8 * screenSize.width || height > 0.8 * screenSize.height) {
            width = screenSize.width;
            height = screenSize.height;
            this.setUndecorated(true);
            setBounds((screenSize.width-width)/2, (screenSize.height-height)/2, width, height);
        } else {
            this.setMinimumSize(new java.awt.Dimension(width, height));
            this.pack();
            this.setLocationRelativeTo(getOwner());
        }
    }// </editor-fold>//GEN-END:initComponents

    private void m_jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jButtonRemoveActionPerformed

        m_aPaymentInfo.removeLast();
        this.refreshParts(); // Recalculate next part amount
        printState();

    }//GEN-LAST:event_m_jButtonRemoveActionPerformed

    private void m_jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jButtonAddActionPerformed
        JPaymentInterface tab = (JPaymentInterface)
                m_jTabPayment.getSelectedComponent();
        boolean valid = tab.validatePayment();
        if (!valid) {
            return;
        }
        PaymentInfo returnPayment = tab.executePayment();
        if (returnPayment != null) {
            m_aPaymentInfo.add(returnPayment);
            this.refreshParts(); // Recalculate next part amount
            printState();
        }

    }//GEN-LAST:event_m_jButtonAddActionPerformed

    private void m_jTabPaymentStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_m_jTabPaymentStateChanged

        if (m_jTabPayment.getSelectedComponent() != null) {
            double remainingCurrency = this.selectedCurrency.convertFromMain(
                    Price.add(m_dTotal, -m_aPaymentInfo.getTotal()));
            double partCurrency = this.selectedCurrency.convertFromMain(this.nextPartAmount);
            ((JPaymentInterface) m_jTabPayment.getSelectedComponent()).activate(
                    customerext, remainingCurrency, partCurrency,
                    this.selectedCurrency, m_sTransactionID);
        }

    }//GEN-LAST:event_m_jTabPaymentStateChanged

    private void m_jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jButtonOKActionPerformed
        JPaymentInterface tab = (JPaymentInterface)
                m_jTabPayment.getSelectedComponent();
        boolean valid = tab.validatePayment();
        if (!valid) {
            return;
        }
        PaymentInfo returnPayment = tab.executePayment();
        if (returnPayment != null) {
            m_aPaymentInfo.add(returnPayment);
            accepted = true;
            dispose();
        }

    }//GEN-LAST:event_m_jButtonOKActionPerformed

    private void m_jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jButtonCancelActionPerformed

        dispose();

    }//GEN-LAST:event_m_jButtonCancelActionPerformed

    private void partsPlusActionPerformed(java.awt.event.ActionEvent evt) {
        this.parts++;
        this.refreshParts();
    }

    private void partsMinusActionPerformed(java.awt.event.ActionEvent evt) {
        if (this.parts > 1) {
            this.parts--;
            this.refreshParts();
        }
    }

    private void printActionPerformed(java.awt.event.ActionEvent evt, int printerNum) {
        this.togglePrint(printerNum);
    }

    private void printPlusActionPerformed(java.awt.event.ActionEvent evt) {
        this.printCount++;
        this.refreshPrint();
    }

    private void printMinusActionPerformed(java.awt.event.ActionEvent evt) {
        this.printCount--;
        this.refreshPrint();
    }

    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JButton m_jButtonAdd;
    private javax.swing.JButton m_jButtonCancel;
    private javax.swing.JButton m_jButtonOK;
    private javax.swing.JToggleButton m_jButtonPrint;
    private javax.swing.JToggleButton m_jButtonPrint2;
    private javax.swing.JToggleButton m_jButtonPrint3;
    private javax.swing.JButton m_jButtonRemove;
    private javax.swing.JLabel m_jLblRemainingEuros;
    private javax.swing.JLabel m_jLblTotalEuros1;
    private javax.swing.JLabel m_jRemaininglEuros;
    private javax.swing.JTabbedPane m_jTabPayment;
    private javax.swing.JLabel m_jTotalEuros;
    private JComboBoxVal<CurrencyInfo> currencyCbx;

    private JLabel jlblPartsNumber;
    private JButton jbtnPartsPlus;
    private JButton jbtnPartsMinus;
    private JLabel jlblPartAmountLabel;
    private JLabel jlblPartAmount;

    private JLabel jlblPrintCount;
    private JButton jbtnPrintPlus;
    private JButton jbtnPrintMinus;
}
