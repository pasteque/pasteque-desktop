//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import org.json.JSONObject;
import java.awt.image.BufferedImage;
import java.io.Serializable;

public class PaymentModeValue implements Serializable
{
    private static final long serialVersionUID = 1391030252755774353L;

    private double value;
    private boolean hasImage;
    private transient BufferedImage image;

    public PaymentModeValue(double value, boolean hasImage) {
        this.value = value;
        this.hasImage = hasImage;
    }

    public PaymentModeValue(JSONObject o) {
        this.value = o.getDouble("value");
        this.hasImage = o.getBoolean("hasImage");
    }

    public double getValue() {
        return this.value;
    }

    public boolean hasImage() {
        return this.hasImage;
    }

    public void setImage(BufferedImage img) {
        this.image = img;
    }

    public BufferedImage getImage() {
        return this.image;
    }
}
