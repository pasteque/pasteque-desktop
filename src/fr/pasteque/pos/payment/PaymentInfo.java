//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import fr.pasteque.basic.BasicException;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.util.Price;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/** Generic PaymentInfo as stored in database. */
public class PaymentInfo implements Serializable
{
    private static final long serialVersionUID = 8590320506593649847L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.payment.PaymentInfo");

    private PaymentMode mode;
    protected CurrencyInfo currency;
    /** The target amount of the payment in main currency
     * (used for change). */
    private double targetAmount;
    /** The given amount of the payment in main currency. */
    private double amount;
    /** The given amount of the payment in currency. */
    private double currencyAmount;
    /** Used for equality */
    private int innerId;
    transient private PaymentInfo backPayment;

    /** @deprecated Old hardcoded payment mode. */
    public PaymentInfo(String code, CurrencyInfo currency, double currencyAmount) {
        // TODO: add given
        DataLogicSales dlSales = new DataLogicSales();
        try {
            for (PaymentMode pm : dlSales.getPaymentModes()) {
                if (pm.getCode().equals(code)) {
                    this.mode = pm;
                    break;
                }
            }
        } catch (BasicException e) {
            e.printStackTrace();
        }
        this.currency = currency;
        this.amount = currency.convertToMain(currencyAmount);
        this.currencyAmount = currencyAmount;
        this.innerId = (int) (Math.random() * Integer.MAX_VALUE);
    }

    public PaymentInfo(PaymentMode mode, CurrencyInfo currency,
            double targetAmount, double amount, double currencyAmount) {
        this.mode = mode;
        this.currency = currency;
        this.targetAmount = targetAmount;
        this.amount = amount;
        this.currencyAmount = currencyAmount;
        this.innerId = (int) (Math.random() * Integer.MAX_VALUE);
    }

    public PaymentInfo(JSONObject o) throws BasicException {
        DataLogicSales dlSales = new DataLogicSales();
        this.mode = dlSales.getPaymentMode(o.getInt("paymentMode"));
        this.amount = o.getDouble("amount");
        this.targetAmount = this.amount; // Split in multiple payments when read from server
        this.currencyAmount = o.getDouble("currencyAmount");
        this.currency = dlSales.getCurrency(o.getInt("currency"));
        // dispOrder is set within Receipt
        this.innerId = (int) (Math.random() * Integer.MAX_VALUE);
    }

    public PaymentInfo copyPayment() {
        PaymentInfo p = new PaymentInfo(this.mode, this.currency,
                this.targetAmount, this.amount, this.currencyAmount);
        return p;
    }

    public boolean hasBackPayment() {
        if (this.backPayment != null) {
            return true;
        } else {
            this.getBackPayment();
            return this.backPayment != null;
        }
    }

    /**
     * Get negative payment for overflow. May be null. It always has
     * amount = given ard are negative.
     */
    public PaymentInfo getBackPayment() {
        if (this.backPayment != null) {
            // Already computed (though not serialized)
            return this.backPayment;
        }
        // Try to generate it
        double mainOverflow = Price.add(this.amount, -this.targetAmount);
        if (mainOverflow < 0.005) {
            return null;
        }
        Integer backModeId = this.mode.getReturnModeId(mainOverflow);
        PaymentMode backMode = null;
        if (backModeId != null) {
            DataLogicSales dlSales = new DataLogicSales();
            try {
                backMode = dlSales.getPaymentMode(backModeId);
            } catch (BasicException e) {
                logger.log(Level.SEVERE, "Unable to load back payment mode " + backModeId, e);
            }
        }
        if (backMode == null) {
            return null;
        }
        double overflow = this.currency.convertFromMain(mainOverflow);
        this.backPayment = new PaymentInfo(backMode, this.currency,
                -mainOverflow, -mainOverflow, -overflow);
        return this.backPayment;
    }

    public PaymentMode getMode() {
        return this.mode;
    }

    /** @deprecated Use this.getMode().getCode() instead. */
    public String getName() {
        return this.getMode().getCode();
    }

    public CurrencyInfo getCurrency() {
        return this.currency;
    }

    public double getAmount() {
        return this.amount;
    }

    /** @deprecated Use this.getCurrencyAmount() instead. */
    public double getTotal() {
        return this.getCurrencyAmount();
    }
    public double getCurrencyAmount() {
        return this.currencyAmount;
    }

    public double getTargetAmount() {
        return this.targetAmount;
    }

    /** @deprecated Here for compatibility. */
    public String getTransactionID() {
        return "No ID";
    }

    public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        o.put("paymentMode", this.mode.getId());
        o.put("amount", this.amount);
        o.put("currencyAmount", this.currencyAmount);
        o.put("currency", this.currency.getID());
        // dispOrder is set with Receipt
        PaymentInfo backPmt = this.getBackPayment();
        if (backPmt != null) {
            // back is modified in Receipt
            JSONObject back = new JSONObject();
            back.put("paymentMode", backPmt.mode.getId());
            back.put("amount", backPmt.getAmount());
            back.put("currencyAmount", backPmt.getCurrencyAmount());
            back.put("currency", this.currency.getID());
            o.put("back", back);
        } else {
            o.put("back", JSONObject.NULL);
        }
        return o;
    }

    public String printTotal() {
        Formats.setAltCurrency(this.currency);
        return Formats.CURRENCY.formatValue(this.currencyAmount);
    }

    /** Return the name of the payment mode. Automatically pick backLabel
     * if available and the amount is negative. */
    public String printMode() {
        if (this.currencyAmount < 0 && this.mode.getBackLabel() != null
                && !"".equals(this.mode.getBackLabel())) {
            return this.mode.getBackLabel();
        } else {
            return this.mode.getLabel();
        }
    }

    /** Return the currency amount in the given currency format. */
    public String printPaid() {
        Formats.setAltCurrency(this.currency);
        return Formats.CURRENCY.formatValue(this.currencyAmount);
    }

    /** Return the main amount in the main currency format. */
    public String printPaidMain() {
        return Formats.CURRENCY.formatValue(this.amount);
    }

    /** @deprecated Use getBackPayment().printPaid() instead. */
    public String printChange() {
        PaymentInfo backPmt = this.getBackPayment();
        if (backPmt != null) {
            return backPmt.printPaid();
        } else {
            return "";
        }
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof PaymentInfo && ((PaymentInfo)o).innerId == this.innerId;
    }

    @Override
    public int hashCode() {
        return this.innerId;
    }
}
