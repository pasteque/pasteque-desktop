//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import javax.swing.JLabel;

public class JPaymentEmpty extends javax.swing.JPanel implements JPaymentInterface
{
    private static final long serialVersionUID = -2272136001670666350L;

    private JPaymentNotifier m_notifier;

    /** Creates new form JPaymentCash */
    public JPaymentEmpty(JPaymentNotifier notifier) {

        m_notifier = notifier;

        initComponents();
    }

    public void activate(CustomerInfoExt customerext, double dTotal,
            double partAmount, CurrencyInfo currency, String transID) {
        printState();
    }

    public boolean validatePayment() {
        return false;
    }

    public PaymentInfo executePayment() {
        return null;
    }

    public Component getComponent() {
        return this;
    }

    private void printState() {
        m_notifier.setStatus(false, false);
    }

    private void initComponents() {
        setLayout(new java.awt.BorderLayout());
        JLabel label = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.PaymentMode"));
        add(label, java.awt.BorderLayout.CENTER);
    }
}
