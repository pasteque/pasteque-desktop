//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JConfirmDialog;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.JEditorCurrency;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class JPaymentTab extends javax.swing.JPanel implements JPaymentInterface
{
    private static final long serialVersionUID = -1638155052799363087L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.JPaymentTab");

    /** The full remaining amount in the selected currency, to handle + or OK */
    private double totalAmount;
    /** The target amount in the selected currency (full ticket or division of it),
     * to handle giving excess. */
    private double requestedAmount;
    /** The given amount in the selected currency. */
    private double givenAmount;
    /** The maximum excess to allow before blocking the payment. */
    private double maxExcess;
    private PaymentMode paymentMode;
    private CurrencyInfo currency;
    private JPaymentNotifier notifier;
    private CustomerInfoExt customer;

    public JPaymentTab(PaymentMode pm, JPaymentNotifier notifier, DataLogicSystem dlSystem) {
        this.paymentMode = pm;
        this.notifier = notifier;
        this.initComponents();
        this.setValueButtons();
        this.keyPadDisplay.addPropertyChangeListener("Edition", new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                printState();
            }
        });
        this.keyPadDisplay.addEditorKeys(this.keyPad);
    }

    @Override // From JPaymentInterface
    public void activate(CustomerInfoExt customerext, double dTotal,
            double partAmount, CurrencyInfo currency, String transactionID) {
        this.totalAmount = dTotal;
        this.requestedAmount = partAmount;
        this.currency = currency;
        this.customer = customerext;

        this.keyPadDisplay.setAllowNegativeValues(dTotal < 0.0);
        this.keyPadDisplay.reset();
        this.keyPadDisplay.activate();

        // Set relative max value for payment
        AppConfig cfg = AppConfig.loadedInstance;
        this.maxExcess = cfg.getNumber("ticket.maxmanualpayment");

        List<PaymentMode.Return> rules = this.paymentMode.getRules();
        if (rules.size() == 0) {
            this.lblChange.setVisible(false);
            this.lblChangeAmount.setVisible(false);
        }

        printState();
    }

    private void printState() {
        Double givenValue = this.getGiven();
        this.givenAmount = givenValue;
        double exceedent = this.requestedAmount >= 0.0 ?
                Price.add(-this.requestedAmount, this.givenAmount):
                0.0; // deactivate give back for negative amounts
        boolean validAmount = true;
        this.lblMessage.setText(null);
        // Check if a customer is required and present
        if (this.paymentMode.isCustAssigned() && this.customer == null) {
            this.lblChange.setVisible(false);
            this.lblChangeAmount.setText("");
            this.lblMessage.setText(AppLocal.getIntString("Message.Payment.NoCustomerNoDebt"));
            this.notifier.setStatus(false, false);
            return;
        } else if (this.paymentMode.isPrepaid()) {
            String msg = AppLocal.getIntString("label.customerBalance", this.customer.printBalance());
            if (!this.customer.hasEnoughPrepaid(this.givenAmount)) {
                msg += "\n" + AppLocal.getIntString("Message.Payment.CustomerNotEnoughPrepaid");
                validAmount = false;
            }
            this.lblMessage.setText(msg);
        } else if (this.paymentMode.isDebt()) {
            String msg = AppLocal.getIntString("label.customerBalance", this.customer.printBalance());
            if (!this.customer.canContractDebt(this.givenAmount)) {
                msg += "\n" + AppLocal.getIntString("Message.Payment.CustomerDebtExceded", this.customer.printMaxDebt());
                validAmount = false;
            }
            this.lblMessage.setText(msg);
        }
        if (exceedent > 0.0) {
            for (PaymentMode.Return rule : this.paymentMode.getRules()) {
                if (exceedent >= rule.getMinVal() - 0.005) {
                    if (!rule.hasReturnMode()) {
                        this.lblChange.setVisible(false);
                        this.lblChangeAmount.setText("");
                        break;
                    }
                    DataLogicSales dlSales = new DataLogicSales();
                    int returnId = rule.getReturnModeId();
                    PaymentMode returnMode = null;
                    try {
                        returnMode = dlSales.getPaymentMode(returnId);
                    } catch (BasicException e) {
                        this.lblChange.setVisible(true);
                        this.lblChange.setText("ERROR");
                        this.lblChangeAmount.setText("(X _ X)");
                        logger.log(Level.WARNING, "Could not get return payment mode.", e);
                    }
                    if (returnMode == null) {
                        this.lblChange.setVisible(false);
                        this.lblChangeAmount.setText("");
                        break;
                    }
                    this.lblChange.setVisible(true);
                    this.lblChange.setText(returnMode.getBackLabel());
                    Formats.setAltCurrency(this.currency);
                    this.lblChangeAmount.setText(Formats.CURRENCY.formatValue(exceedent));
                }
            }
        } else {
            if (this.paymentMode.getRules().size() > 0) {
                this.lblChange.setVisible(true);
                PaymentMode returnMode = null;
                int returnId = this.paymentMode.getRules().get(0).getReturnModeId();
                try {
                    DataLogicSales dlSales = new DataLogicSales();
                    returnMode = dlSales.getPaymentMode(returnId);
                } catch (BasicException e) {
                    this.lblChange.setText("ERROR");
                    this.lblChangeAmount.setText("(X _ X)");
                    logger.log(Level.WARNING, "Could not get return payment mode.", e);
                }
                if (returnMode != null) {
                    this.lblChange.setText(returnMode.getBackLabel());
                    this.lblChangeAmount.setText("");
                }
            }
        }
        Formats.setAltCurrency(this.currency);
        this.lblGivenAmount.setText(Formats.CURRENCY.formatValue(Double.valueOf(this.givenAmount)));
        if (validAmount && this.maxExcess >= 0.0) {
            double excess = Math.abs(Price.add(this.givenAmount, -this.requestedAmount));
            if (Price.add(excess, -this.maxExcess) > -0.005) {
                validAmount = false;
                this.lblMessage.setText(AppLocal.tr("Message.Payment.BlockOverlyExcessiveAmount"));
            }
        }
        if (validAmount) {
            boolean complete = (this.totalAmount == this.requestedAmount)
                    && (this.totalAmount > 0.0 ?
                           Price.add(this.totalAmount, -this.givenAmount) <= 0.0 :
                           Price.add(this.totalAmount, -this.givenAmount) >= 0.0);
            this.notifier.setStatus(this.givenAmount != 0.0, complete);
        } else {
            this.notifier.setStatus(false, false);
        }
        // Keep the focus on the input in case a message catched it
        this.keyPadDisplay.activate();
    }

    public boolean validatePayment() {
        double given = this.getGiven();
        String config = AppConfig.loadedInstance.getProperty("ui.confirmexcessivepayment");
        if ("never".equals(config)) {
            return true;
        }
        double exceedent = Price.add(given, -this.requestedAmount);
        boolean hasExcess = this.requestedAmount >= 0.0 ?
                exceedent > 0.005 : exceedent < -0.005;
        boolean hasReturn = this.paymentMode.getReturnModeId(exceedent) != null;
        if (hasExcess && ("always".equals(config)
                || ("noreturn".equals(config) && !hasReturn))) {

            Formats.setAltCurrency(this.currency);
            String givenStr = Formats.CURRENCY.formatValue(given);
            Formats.setAltCurrency(this.currency);
            String requestedStr = Formats.CURRENCY.formatValue(this.requestedAmount);
            boolean confirm = JConfirmDialog.showConfirm(this,
                    AppLocal.tr("Message.ConfirmExcessivePaymentTitle"),
                    AppLocal.tr("Message.ConfirmExcessivePayment",
                            givenStr, requestedStr));
            this.keyPadDisplay.activate();
            return confirm;
        }
        return true;
    }

    @Override // From JPaymentInterface
    public PaymentInfo executePayment() {
        double given = this.getGiven();
        double mainGiven = this.currency.convertToMain(given);
        double mainRequested = this.currency.convertToMain(this.requestedAmount);
        boolean partial = this.requestedAmount >= 0.0 ?
                given < this.requestedAmount:
                given > this.requestedAmount;
        if (partial) {
            // Partial payment with no give back
            return new PaymentInfo(this.paymentMode, this.currency,
                    mainGiven, mainGiven, given);
        }
        // Full payment than can have change
        return new PaymentInfo(this.paymentMode, this.currency,
                mainRequested, mainGiven, given);
    }

    @Override // From JPaymentInterface
    public Component getComponent() {
        return this;
    }

    /** Get the input value, or requested amount if no custom amount is entered. */
    private double getGiven() {
        double given = this.requestedAmount;
        Double input = this.keyPadDisplay.getDoubleValue();
        if (input != null) {
            given = keyPadDisplay.getDoubleValue();
        }
        return given;
    }

    private void setValueButtons() {
        ThumbNailBuilder tnb = new ThumbNailBuilder(64, 54, "payment_cash.png");
        for (PaymentModeValue pmv : this.paymentMode.getValues()) {
            ImageIcon icon;
            if (pmv.hasImage()) {
                icon = new ImageIcon(tnb.getThumbNailText(pmv.getImage(),
                        Formats.CURRENCY.formatValue(pmv.getValue())));
            } else {
                icon = new ImageIcon(tnb.getThumbNailText(null,
                        Formats.CURRENCY.formatValue(pmv.getValue())));
            }
            javax.swing.JButton btn = WidgetsBuilder.createButton(icon);
            btn.setFocusPainted(false);
            btn.setFocusable(false);
            btn.setHorizontalTextPosition(SwingConstants.CENTER);
            btn.setVerticalTextPosition(SwingConstants.BOTTOM);
            btn.addActionListener(new ValueBtnListener(pmv));
            this.valueBtnContainer.add(btn);
        }
    }

    private class ValueBtnListener implements ActionListener {
        private PaymentModeValue value;
        public ValueBtnListener(PaymentModeValue pmv) {
            this.value = pmv;
        }
        public void actionPerformed(ActionEvent e) {
            Double given = keyPadDisplay.getDoubleValue();
            if (given == null) {
                given = this.value.getValue();
            } else {
                given = Price.add(given, this.value.getValue());
            }
            keyPadDisplay.setDoubleValue(given);
            printState();
        }
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        keyPad = new fr.pasteque.pos.widgets.JEditorKeys();
        keyPadDisplay = new JEditorCurrency();

        lblGiven = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.InputCash"));
        lblGiven.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblChange = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.ChangeCash"));
        lblChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblGivenAmount = WidgetsBuilder.createLabel();
        WidgetsBuilder.inputStyle(lblGivenAmount);
        lblGivenAmount.setBackground(new java.awt.Color(153, 153, 255));
        lblChangeAmount = WidgetsBuilder.createLabel();
        WidgetsBuilder.inputStyle(lblChangeAmount);

        this.setLayout(new java.awt.BorderLayout());

        // Right part (padContainer): Keyboard
        javax.swing.JPanel padContainer = new javax.swing.JPanel();
        padContainer.setLayout(new java.awt.BorderLayout());

        javax.swing.JPanel keyPadContainer = new javax.swing.JPanel();
        keyPadContainer.setLayout(new javax.swing.BoxLayout(keyPadContainer, javax.swing.BoxLayout.Y_AXIS));
        keyPadContainer.add(keyPad);

        javax.swing.JPanel keyPadDispContainer = new javax.swing.JPanel();
        keyPadDispContainer.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        keyPadDispContainer.setLayout(new java.awt.BorderLayout());
        keyPadDispContainer.add(keyPadDisplay, java.awt.BorderLayout.CENTER);

        keyPadContainer.add(keyPadDispContainer);
        padContainer.add(keyPadContainer, java.awt.BorderLayout.NORTH);

        // Left part (buttonContainer): amount and value buttons
        javax.swing.JPanel buttonContainer = new javax.swing.JPanel();
        buttonContainer.setLayout(new java.awt.BorderLayout());

        javax.swing.JPanel amountContainer = new javax.swing.JPanel();
        amountContainer.setLayout(new java.awt.GridLayout(2, 2, 8, 8));

        amountContainer.add(lblGiven);
        amountContainer.add(lblGivenAmount);
        amountContainer.add(lblChange);
        amountContainer.add(lblChangeAmount);

        buttonContainer.add(amountContainer, java.awt.BorderLayout.NORTH);

        this.valueBtnContainer = new javax.swing.JPanel();
        this.valueBtnContainer.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, btnSpacing, btnSpacing));
        buttonContainer.add(this.valueBtnContainer, java.awt.BorderLayout.CENTER);

        // Footer message
        this.lblMessage = new javax.swing.JTextArea();
        this.lblMessage.setFont(WidgetsBuilder.getFont(WidgetsBuilder.SIZE_MEDIUM));
        this.lblMessage.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        this.lblMessage.setEditable(false);
        this.lblMessage.setLineWrap(true);
        this.lblMessage.setWrapStyleWord(true);
        this.lblMessage.setFocusable(false);
        this.lblMessage.setRequestFocusEnabled(false);
        buttonContainer.add(this.lblMessage, java.awt.BorderLayout.SOUTH);

        // Add all to the main container
        this.add(buttonContainer, java.awt.BorderLayout.CENTER);
        this.add(padContainer, java.awt.BorderLayout.LINE_END);
    }

    private javax.swing.JLabel lblGiven;
    private javax.swing.JLabel lblGivenAmount;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblChangeAmount;
    private javax.swing.JTextArea lblMessage;
    private javax.swing.JPanel valueBtnContainer;
    private JEditorKeys keyPad;
    private JEditorCurrency keyPadDisplay;
}
