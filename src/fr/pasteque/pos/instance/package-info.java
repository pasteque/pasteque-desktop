/**
 * Manage a shared instance to recall when multiple applications are started.
 */
package fr.pasteque.pos.instance;
