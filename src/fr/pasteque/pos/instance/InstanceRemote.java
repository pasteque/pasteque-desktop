//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.instance;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The remote interface for instance which declares the methods
 * that can be called remotely. Meant only to be used locally to
 * check for an other running process in the JVM.
 */
public interface InstanceRemote extends Remote
{
    /**
     * Give the focus to the instance.
     * @throws RemoteException When a remote error occurs.
     */
    public void restoreInstance() throws RemoteException;
    /**
     * Get the id of the instance. Mostly for tests.
     * @return A unique instance id.
     * @throws RemoteException When a remote error occurs.
     */
    public String getId() throws RemoteException;
}
