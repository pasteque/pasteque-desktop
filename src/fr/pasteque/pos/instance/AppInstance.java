//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.instance;

import java.io.Serializable;
import java.util.UUID;
import javax.swing.JFrame;

/**
 * Instance of the application with a restorable frame.
 * This instance is managed by a {@link InstanceManager} to share it between
 * java processes and update its own state.
 */
public class AppInstance implements InstanceRemote, Serializable
{
    private static final long serialVersionUID = 264041050784279617L;

    private String id;
    private JFrame frame;

    /** Create a new and empty instance. */
    public AppInstance() {
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Mark the instance as ready with an existing frame.
     * @param frame The frame to restore when switching to this instance.
     */
    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Restore the frame of this instance. Does nothing if the frame
     * was not set by {@link setFrame(frame)}.
     */
    @Override // from InstanceRemote
    public void restoreInstance() {
        if (this.frame == null) {
            return;
        }
        final JFrame target = this.frame;
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                int extendedState = target.getExtendedState();
                if ((extendedState & JFrame.ICONIFIED) > 0) {
                    target.setExtendedState(extendedState - JFrame.ICONIFIED);
                }
                target.requestFocus();
            }
        });
    }

    /**
     * Get the automatically set id of this instance. Mostly for tests.
     * @return The automatically generated id.
     */
    @Override // from InstanceRemote
    public String getId() {
        return this.id;
    }

    /**
     * Instanced are equals when they share the same id.
     * @param o The object to check.
     * @return True when both are the same instance.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof AppInstance) {
            return this.id.equals(((AppInstance)o).id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}
