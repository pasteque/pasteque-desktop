//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.instance;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manage shared {@link AppInstance} through remote registries.
 * The manager acts as both rmi client and server.
 * Using {@link registerStartingApp()} will run the server.
 * Using {@link restoreInstance()} will act as a client.
 */
public class InstanceManager
{
    public static final String REGISTRY_KEY = "PastequeInstance";
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.instance.InstanceManager");

    private Registry registry;
    private AppInstance currentInstance;

    /** Create an empty instance manager. */
    public InstanceManager() {
    }

    /**
     * Register the starting app instance. It automatically creates a new
     * {@link AppInstance} and register it on the local registry. It can be
     * then recalled with {@link restoreInstance()} from an other process.
     * @return True when registration succeeds, false if an other instance
     * is already registered or when an error occurs.
     */
    public boolean registerStartingApp() {
        try {
            this.currentInstance = new AppInstance();
            try {
                this.registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
                // getRegistry doesn't fail on create but on call
                this.registry.bind(REGISTRY_KEY, this.currentInstance);
            } catch (RemoteException get) {
                this.registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
                this.registry.bind(REGISTRY_KEY, this.currentInstance);
            }
            return true;
        } catch (RemoteException | AlreadyBoundException e) {
            logger.log(Level.WARNING, "Cannot register instance", e);
            return false;
        }
    }

    /**
     * Mark the current instance as ready and pass the window to restore.
     * It does nothing if {@link registerStartingApp} wasn't called or failed.
     * @param frame The frame of the running instance to restore when requested.
     * @return True when the registration succeeded.
     */
    public boolean instanceReady(javax.swing.JFrame frame) {
        if (this.registry == null) {
            return false;
        }
        try {
            this.currentInstance.setFrame(frame);
            InstanceRemote stub = (InstanceRemote) UnicastRemoteObject.exportObject(this.currentInstance, 0);
            this.registry.rebind(REGISTRY_KEY, stub);
            return true;
        } catch (RemoteException e) {
            logger.log(Level.WARNING, "Cannot mark instance as ready", e);
            return false;
        }
    }

    /**
     * Request focus on the existing instance.
     * @return The restored instance id or null if no instance is running.
     * The instance will be focused if ready or do nothing when the existing
     * instance is starting.
     */
    public String restoreInstance() {
        if (this.registry != null) {
            // Restoring the current instance
            if (this.currentInstance != null) {
                this.currentInstance.restoreInstance();
                return this.currentInstance.getId();
            }
            // This could be an error, the registry is running without
            // registered instance
            return null;
        }
        try {
            // Get the local registry and request the running instance
            Registry localRegistry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
            Remote instance = localRegistry.lookup(InstanceManager.REGISTRY_KEY);
            if (instance != null && (instance instanceof InstanceRemote)) {
                ((InstanceRemote) instance).restoreInstance();
                return ((InstanceRemote) instance).getId();
            }
        } catch (RemoteException e) {
            logger.log(Level.WARNING, "Could not access to instance registry for restore.", e);
        } catch (NotBoundException e) {
            // Instance not found, nothing to do
        }
        return null;
    }

    /**
     * Get the current instance id. Mostly for tests.
     * @return The current instance id, null if not registered.
     */
    public String getInstanceId() {
        if (this.currentInstance == null) {
            return null;
        } else {
            return this.currentInstance.getId();
        }
    }

    /**
     * Check if an instance was registered from this manager.
     * Mostly for tests.
     * @return True when {@link registerStartingApp} was called successfully.
     */
    public boolean isRegistered() {
        return this.currentInstance != null;
    }

    /**
     * Remove the registered instance. Mostly for tests.
     */
    public void clear() {
        try {
            if (this.registry != null) {
                this.registry.unbind(REGISTRY_KEY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.registry = null;
        this.currentInstance = null;
    }
}
