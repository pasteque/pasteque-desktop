//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.forms.shortcuts;

/** Shortcut for variable user content, like product, payment mode and such. */
public class CustomShortcut
{
    // Constants for custom search shortcuts
    /** Type of shortcut that binds to a product. */
    public static final int TYPE_PRODUCT = 0;
    /** Type of shortcut that binds to a category. */
    public static final int TYPE_CATEGORY = 1;
    /** Type of shortcut that binds to a payment mode. */
    public static final int TYPE_PAYMENTMODE = 2;
    /** Type of shortcut that binds to a currency. */
    public static final int TYPE_CURRENCY = 3;

    public static CustomShortcut readCustomEntry(String code) {
        if (Shortcuts.isCustomShortcutCode(TYPE_PRODUCT, code)) {
            return new CustomShortcut(TYPE_PRODUCT, code);
        }
        if (Shortcuts.isCustomShortcutCode(TYPE_CATEGORY, code)) {
            return new CustomShortcut(TYPE_CATEGORY, code);
        }
        if (Shortcuts.isCustomShortcutCode(TYPE_PAYMENTMODE, code)) {
            return new CustomShortcut(TYPE_PAYMENTMODE, code);
        }
        return null;
    }

    /** Extract the targeted shortcut from the full code. */
    private static String extractShortcut(int type, String code) {
        switch (type) {
            case TYPE_PRODUCT:
                return code.substring(Shortcuts.PRODUCT_PREFIX.length());
            case TYPE_CATEGORY:
                return code.substring(Shortcuts.CATEGORY_PREFIX.length());
            case TYPE_PAYMENTMODE:
                return code.substring(Shortcuts.PAYMENT_PREFIX.length());
            default:
                return code;
        }
    }

    int type;
    String code;
    /** The referenced value (extracted from code). */
    String shortcut;

    public CustomShortcut(int type, String code) {
        this.type = type;
        this.code = code;
        this.shortcut = CustomShortcut.extractShortcut(type, code);
    }

    public int getType() {
        return this.type;
    }

    public String getShortcut() {
        return this.shortcut;
    }

    public String getCode() {
        return this.code;
    }
}
