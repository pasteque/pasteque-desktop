//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.forms.shortcuts;

import fr.pasteque.pos.forms.AppLocal;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 * Shortcut handler, save and load from files, manage listeners.
 * This is the main class for handling shortcuts.
 */
public final class ShortcutConfig
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.ShortcutConfig");

    private static final String DEFAULT_DIR = System.getProperty("user.home")
            + "/." + AppLocal.APP_ID;File shortcutFile;

    private static ShortcutConfig loadedInstance;
    /** Application shortcuts. */
    private Map<Shortcuts, KeyStroke> shortcuts;
    private Map<ShortcutSections, List<ShortcutListener>> listeners;
    /** Custom shortcuts to find user content (products, payments etc). */
    private Map<KeyStroke, Map<Integer, CustomShortcut>> customShortcuts;
    private Map<Integer, List<CustomShortcutListener>> customListeners;

    /**
     * Create a shortcut configuration with arguments from the command line.
     * @param args The command line arguments.
     * It uses the configuration file from the default configuration directory
     * or the explicitely given configuration file.
     */
    public ShortcutConfig(String[] args) {
        if (args.length < 2) {
            this.init(this.getDefaultShortcutFile());
        } else {
            this.init(new File(args[2]));
        }
    }

    public String getDataDir() {
        return DEFAULT_DIR;
    }

    /**
     * Create a shortcut configuration read from a given file.
     * @param shortcutFile The configuration file to read.
     */
    public ShortcutConfig(File shortcutFile) {
        this.init(shortcutFile);
    }

    private void init(File shortcutFile) {
        this.listeners = new HashMap<ShortcutSections, List<ShortcutListener>>();
        for (ShortcutSections s : ShortcutSections.values()) {
            this.listeners.put(s, new ArrayList<ShortcutListener>());
        }
        this.customListeners = new HashMap<Integer, List<CustomShortcutListener>>();
        this.customListeners.put(CustomShortcut.TYPE_PRODUCT, new ArrayList<CustomShortcutListener>());
        this.customListeners.put(CustomShortcut.TYPE_CATEGORY, new ArrayList<CustomShortcutListener>());
        this.customListeners.put(CustomShortcut.TYPE_PAYMENTMODE, new ArrayList<CustomShortcutListener>());
        this.shortcutFile = shortcutFile;
        logger.info("Reading shortcut file: " + shortcutFile.getAbsolutePath());
    }

    private File getDefaultShortcutFile() {
        return new File(DEFAULT_DIR + "/shortcuts.properties");
    }

    /**
     * Load and setup the shortcuts by reading the default shortcuts
     * and overriding those defined in the configuration file.
     */
    public void load() {
            this.shortcuts = DefaultShortcuts.getDefaultStandard();
            this.customShortcuts = new HashMap<KeyStroke, Map<Integer, CustomShortcut>>();
            if (!this.shortcutFile.exists()) {
                return;
            }
            try {
                Properties props = new Properties();
                props.load(new FileReader(shortcutFile));
                for (Map.Entry<Object, Object> e : props.entrySet()) {
                    // For parsing algorithm see here:
                    // https://docs.oracle.com/javase/7/docs/api/javax/swing/KeyStroke.html#getKeyStroke(java.lang.String)
                    // But, allow to use + instead of space, which would need to be escaped in
                    // properties files
                    String entry = e.getKey().toString().replaceAll("\\+(.)", " $1");
                    // Check if the entry is a valid one, alert if not.
                    Shortcuts shortcut = null;
                    for (Shortcuts s : Shortcuts.values()) {
                        if (s.getCode().equals(entry)) {
                            shortcut = s;
                            break;
                        }
                    }
                    CustomShortcut customShortcut = CustomShortcut.readCustomEntry(entry);
                    if (shortcut == null && customShortcut == null) {
                        logger.warning("Invalid shortcut entry " + entry + ".");
                        continue;
                    }
                    // Entry is valid, read the associated key stroke.
                    String key = e.getValue().toString();
                    KeyStroke stroke = KeyStroke.getKeyStroke(key);
                    if (stroke == null) {
                        logger.warning("The shortcut " + key + " for " + entry
                                + " could not be parsed.");
                        continue;
                    }
                    // Register the shortcut.
                    if (shortcut != null) {
                        shortcuts.put(shortcut, stroke);
                    } else {
                        Map<Integer, CustomShortcut> map = this.customShortcuts.get(stroke);
                        if (map == null) {
                            map = new HashMap<Integer, CustomShortcut>();
                            this.customShortcuts.put(stroke, map);
                        }
                        map.put(customShortcut.getType(), customShortcut);
                    }
                }
            } catch (IOException e) {
                logger.warning("Could not load shortcuts from "
                        + shortcutFile.getAbsolutePath() + " (" + e.getMessage() + ")");
            }
        }

    /**
     * Get the number of registered shortcuts. This doesn't include shortcuts
     * to custom elements.
     * @return The number of regular shortcuts.
     */
    public int getShortcutCount() {
        return this.shortcuts.size();
    }

    /**
     * Get the KeyStroke associated to a shortcut.
     * @param shortcut The Shortcut to read.
     * @return The associated KeyStroke.
     */
    public KeyStroke getKeyStroke(Shortcuts shortcut){
        return this.shortcuts.get(shortcut);
    }

    /**
     * Get the globally registered ShortcutConfig for the application.
     * @return A staticly registered instance set with {@link #setInstance(ShortcutConfig)}.
     */
    public static ShortcutConfig getInstance() {
        return ShortcutConfig.loadedInstance;
    }

    /**
     * Set the globally registered ShortcutConfig for the application.
     * @param s The ShortcutConfig to share through the application.
     */
    public static void setInstance(ShortcutConfig s) {
        ShortcutConfig.loadedInstance = s;
    }

    /**
     * Add a listener to receive shortcut events. The latest listener will
     * receive the events first.
     * @param comp The component that receives the keystrokes.
     * @param section The section of shortcuts to register.
     * @param listener The listener to register.
     */
    public void addListener(JComponent comp, ShortcutSections section,
            ShortcutListener listener) {
        InputMap input = comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        for (Shortcuts s : section.getShortcuts()) {
            KeyStroke stroke = this.getKeyStroke(s);
            if (stroke != null) {
                input.put(stroke, s);
                comp.getActionMap().put(s, new ActionPerformer(section, s));
            }
        }
        this.listeners.get(section).add(0, listener);
    }

    /**
     * Add a listener to receive custom shortcut events. The latest listener
     * will receive the events first.
     * @param type The type of custom shortcut to receive. See {@link CustomShortcut}.
     * @param comp The component that receives the keystrokes.
     * @param listener The listener to register.
     */
    public void addCustomListener(int type, JComponent comp, CustomShortcutListener listener) {
        InputMap input = comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        for (KeyStroke stroke : this.customShortcuts.keySet()) {
            Map<Integer, CustomShortcut> shortcuts = this.customShortcuts.get(stroke);
            if (shortcuts == null) {
                continue;
            }
            CustomShortcut shortcut = shortcuts.get(type);
            if (shortcut != null) {
                input.put(stroke, shortcut);
                comp.getActionMap().put(shortcut, new CustomActionPerformer(shortcut));
            }
        }
        this.customListeners.get(type).add(0, listener);
    }

    /**
     * Unregister a shortcut listener. Pass the same parameters as given
     * to {@link addListener}.
     * @param comp The component that receives the keystrokes.
     * @param section The section of shortcuts to unregister.
     * @param listener The listener to unregister.
     */
    public void removeListener(JComponent comp, ShortcutSections section,
            ShortcutListener listener) {
        InputMap input = comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        for (Shortcuts s : section.getShortcuts()) {
            KeyStroke stroke = this.getKeyStroke(s);
            if (stroke != null) {
                input.remove(stroke);
                comp.getActionMap().remove(stroke.hashCode());
            }
        }
        this.listeners.get(section).remove(listener);
    }

    /**
     * Unregister a custom shortcut listener. Pass the same arguments as given
     * to {@link addCustomListener}.
     * @param type The type of custom shortcut to receive. {@see CustomShortcut}.
     * @param comp The component that receives the keystrokes.
     * @param listener The listener to register.
     */
    public void removeCustomListener(int type, JComponent comp, CustomShortcutListener listener) {
        InputMap input = comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        for (KeyStroke stroke : this.customShortcuts.keySet()) {
            Map<Integer, CustomShortcut> shortcuts = this.customShortcuts.get(stroke);
            if (shortcuts == null) {
                continue;
            }
            input.remove(stroke);
            CustomShortcut shortcut = shortcuts.get(type);
            if (shortcut != null) {
                comp.getActionMap().remove(stroke.hashCode());
            }
        }
        this.customListeners.get(type).remove(listener);
    }

    /** Make a copy of this.listeners to be able to iterate over it
     * without fearing concurrential accesses. */
    private List<ShortcutListener> copyListeners(ShortcutSections s) {
        List<ShortcutListener> ret = new ArrayList<ShortcutListener>();
        ret.addAll(this.listeners.get(s));
        return ret;
    }

    /** Make a copy of this.customListeners.get(type) to be able to iterate over it
     * without fearing concurrential accesses. */
    private List<CustomShortcutListener> copyCustomListeners(int type) {
        List<CustomShortcutListener> ret = new ArrayList<CustomShortcutListener>();
        if (this.customListeners.get(type) != null) {
            ret.addAll(this.customListeners.get(type));
        }
        return ret;
    }

    private class ActionPerformer extends AbstractAction
    {
        private static final long serialVersionUID = -2070490164956688583L;

        private ShortcutSections section;
        private Shortcuts shortcut;
        public ActionPerformer(ShortcutSections section, Shortcuts s) {
            this.section = section;
            this.shortcut = s;
        }
        @Override
        public void actionPerformed(ActionEvent evt) {
            for (ShortcutListener listener : ShortcutConfig.this.copyListeners(this.section)) {
                if (listener.shortcutPressed(this.shortcut)) {
                    break;
                }
            }
        }
    }
    private class CustomActionPerformer extends AbstractAction
    {
        private static final long serialVersionUID = -2509145842980778296L;

        private CustomShortcut shortcut;
        public CustomActionPerformer(CustomShortcut shortcut) {
            this.shortcut = shortcut;
        }
        @Override
        public void actionPerformed(ActionEvent evt) {
            List<CustomShortcutListener> listenerList = ShortcutConfig.this.copyCustomListeners(this.shortcut.getType());
            for (CustomShortcutListener listener : listenerList) {
                listener.customShortcutPressed(this.shortcut);
            }
        }
    }
}
