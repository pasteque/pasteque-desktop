//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.forms.shortcuts;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.KeyStroke;

/** Hardcoding the default shortcut keys. */
public class DefaultShortcuts
{
    /** Get the standard (mostly French) shortcut map, not bound to any keyboard layout. */
    public static Map<Shortcuts, KeyStroke> getDefaultStandard() {
        Map<Shortcuts, KeyStroke> s = new HashMap<Shortcuts, KeyStroke>();
        s.put(Shortcuts.GENERAL_VALIDATE, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
        s.put(Shortcuts.GENERAL_CANCEL, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
        s.put(Shortcuts.MENU_SALES, KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        s.put(Shortcuts.MENU_TICKETS, KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
        s.put(Shortcuts.MENU_CASHMOVE, KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
        s.put(Shortcuts.MENU_CUSTOMERS, KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
        s.put(Shortcuts.MENU_CLOSE, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        s.put(Shortcuts.MENU_CONFIG, KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
        s.put(Shortcuts.MENU_TOGGLE, KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0));
        s.put(Shortcuts.SALES_CATUP, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0));
        s.put(Shortcuts.SALES_CATDOWN, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0));
        s.put(Shortcuts.SALES_RELOAD, KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.SALES_CUSTOMER, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK)); // C as in Client
        s.put(Shortcuts.SALES_DISCOUNT, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK)); // P as in Promo (R is for Refresh)
        s.put(Shortcuts.SALES_SPLIT, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK)); // S as in Split/Sciseaux
        s.put(Shortcuts.SALES_NEWORDER, KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK)); // T as in new Tab
        s.put(Shortcuts.SALES_ORDERS, KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.CTRL_DOWN_MASK)); // Like in Firefox
        s.put(Shortcuts.SALES_MAP, KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK)); // Like in Close tab
        s.put(Shortcuts.SALES_SEARCH, KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.SALES_BARCODE, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
        s.put(Shortcuts.SALES_PAY, KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0));
        s.put(Shortcuts.ORDER_INCLINE, KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.ORDER_DECLINE, KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.ORDER_DELLINE, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.ORDER_EDITLINE, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.ORDER_UP, KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
        s.put(Shortcuts.ORDER_DOWN, KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
        s.put(Shortcuts.PAYMENT_UP, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0));
        s.put(Shortcuts.PAYMENT_DOWN, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0));
        s.put(Shortcuts.PAYMENT_CURRENCY_UP, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.PAYMENT_CURRENCY_DOWN, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, InputEvent.CTRL_DOWN_MASK));
        s.put(Shortcuts.PAYMENT_PRINT, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
        // Shortcuts.PAYMENT_DIVPLUS not assigned
        // Shortcuts.PAYMENT_DIVMINUS not assigned
        return s;
    }
}
