//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.forms.shortcuts;

/**
 * Enumeration of all keyboard shortcut sections. A section defines a set
 * in which a key stroke could be assigned to only one shortcut.
 * But the same key stroke could be assigne to an other shortcut in an other
 * section.
 */
public enum ShortcutSections
{
    GENERAL (Shortcuts.GENERAL_VALIDATE, Shortcuts.GENERAL_CANCEL),
    MENU (Shortcuts.MENU_SALES, Shortcuts.MENU_TICKETS,
            Shortcuts.MENU_CASHMOVE, Shortcuts.MENU_CUSTOMERS,
            Shortcuts.MENU_CLOSE, Shortcuts.MENU_CONFIG,
            Shortcuts.MENU_EXIT, Shortcuts.MENU_TOGGLE),
    SALES (Shortcuts.SALES_CATUP, Shortcuts.SALES_CATDOWN,
            Shortcuts.SALES_RELOAD, Shortcuts.SALES_CUSTOMER,
            Shortcuts.SALES_DISCOUNT,
            Shortcuts.SALES_SPLIT, Shortcuts.SALES_NEWORDER,
            Shortcuts.SALES_ORDERS, Shortcuts.SALES_MAP,
            Shortcuts.SALES_SEARCH, Shortcuts.SALES_BARCODE,
            Shortcuts.SALES_PAY, Shortcuts.ORDER_INCLINE,
            Shortcuts.ORDER_DECLINE, Shortcuts.ORDER_DELLINE,
            Shortcuts.ORDER_EDITLINE, Shortcuts.ORDER_UP,
            Shortcuts.ORDER_DOWN),
    PAYMENTS (Shortcuts.PAYMENT_UP, Shortcuts.PAYMENT_DOWN,
            Shortcuts.PAYMENT_CURRENCY_UP, Shortcuts.PAYMENT_CURRENCY_DOWN,
            Shortcuts.PAYMENT_PRINT, Shortcuts.PAYMENT_DIVPLUS,
            Shortcuts.PAYMENT_DIVMINUS);

    private Shortcuts[] shortcuts;

    ShortcutSections(Shortcuts... shortcuts) {
        this.shortcuts = shortcuts;
    }

    public Shortcuts[] getShortcuts() {
        return this.shortcuts;
    }
}
