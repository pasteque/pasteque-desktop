//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.forms.shortcuts;

/** Enumeration of all keyboard shortcuts. */
public enum Shortcuts
{
    // General shortucts

    /** Confirm/OK. */
    GENERAL_VALIDATE ("general.validate"),
    /** Cancel an action. */
    GENERAL_CANCEL ("general.cancel"),
    /** Open the sales screen. */
    MENU_SALES ("menu.sales"),
    /** Open the previous ticket search screen. */
    MENU_TICKETS ("menu.tickets"),
    /** Open the cash movement screen. */
    MENU_CASHMOVE ("menu.cash_movements"),
    /** Open the customer screen. */
    MENU_CUSTOMERS ("menu.customers"),
    /** Open the close session screen. */
    MENU_CLOSE ("menu.close_cash"),
    /** Open the configuration screen. */
    MENU_CONFIG ("menu.configure"),
    /** Quit to main menu. */
    MENU_EXIT ("menu.exit"),
    /** Toggle the visibility of the menu. */
    MENU_TOGGLE ("menu.open_close"),

    // Sales shortcuts

    /** Select the previous category on the sales screen. */
    SALES_CATUP ("sales.category_move_up"),
    /** Select the next category on the sales screen. */
    SALES_CATDOWN ("sales.category_move_down"),
    /** Reload customers on the sales screen. */
    SALES_RELOAD ("sales.reload"),
    /** Open the customer selection popup. */
    SALES_CUSTOMER ("sales.select_customer"),
    /** Open the discount profile selection popup. */
    SALES_DISCOUNT ("sales.select_discount"),
    /** Open the split order popup. */
    SALES_SPLIT ("sales.split_order"),
    /** Create a new order. */
    SALES_NEWORDER ("sales.new_order"),
    /** Open the order selection popup.*/
    SALES_ORDERS ("sales.select_order"),
    /** Return to the restaurant map. */
    SALES_MAP ("sales.show_map"),
    /** Open the search product popup. */
    SALES_SEARCH ("sales.search_product"),
    /** Run a barcode search. */
    SALES_BARCODE ("sales.search_barcode"),
    /** Open the payment popup. */
    SALES_PAY ("sales.pay"),
    // Custom shortcut format
    // SALES_PRODUCTS ("sales.product.<barcode>"),
    // SALES_CATEGORIES ("sales.category.<reference>"),

    // Order shortcuts

    /** Increase the quantity of the selected order line. */
    ORDER_INCLINE ("order.increase_line_quantity"),
    /** Decrease the quantity of the selected order line. */
    ORDER_DECLINE ("order.decrease_line_quantity"),
    /** Remove the selected order line. */
    ORDER_DELLINE ("order.delete_line"),
    /** Edit the selected order line. */
    ORDER_EDITLINE ("order.edit_line"),
    /** Select the previous order line. */
    ORDER_UP ("order.move_up"),
    /** Select the next order line. */
    ORDER_DOWN ("order.move_down"),

    // Payment shortcuts

    /** Select the previous payment mode. */
    PAYMENT_UP ("payment.select_up"),
    /** Select the next payment mode. */
    PAYMENT_DOWN ("payment.select_down"),
    /** Select the previous currency. */
    PAYMENT_CURRENCY_UP ("payment.currency.select_up"),
    /** Select the next currency. */
    PAYMENT_CURRENCY_DOWN ("payment.currency.select_down"),
    /** Toggle printing the ticket after payment. */
    PAYMENT_PRINT ("payment.toggle_print"),
    /** Increase the division of the payment between multiple payments. */
    PAYMENT_DIVPLUS ("payment.divide_more"),
    /** Decrease the division of the payment between multiple payments. */
    PAYMENT_DIVMINUS ("payment.divide_less");
    // Custom shortcut format
    // PAYMENT_SELECT ("payment.select.<code>"),
    // PAYMENT_CURRENCY_SELECT ("payment.currency.select.<code>"),

    /** The time in milliseconds for which the buttons stay clicked
     * when activated from a keyboard shortcut. */
    public static final int CLICK_TIME = 250;

    public static final String PRODUCT_PREFIX = "sales.product.";
    public static final String CATEGORY_PREFIX = "sales.category.";
    public static final String PAYMENT_PREFIX = "payment.select.";
    public static final String CURRENCY_PREFIX = "payment.currency.select.";


    /** Code is the full name of the shortcut. */
    private String code;

    Shortcuts(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    /** Check if an entry is a custom shortcut entry for the given type. */
    public static boolean isCustomShortcutCode(int type, String key) {
        switch (type) {
            case CustomShortcut.TYPE_PRODUCT:
                return key.startsWith(Shortcuts.PRODUCT_PREFIX);
            case CustomShortcut.TYPE_CATEGORY:
                return key.startsWith(Shortcuts.CATEGORY_PREFIX);
            case CustomShortcut.TYPE_PAYMENTMODE:
                return key.startsWith(Shortcuts.PAYMENT_PREFIX);
            default:
                return false;
        }
    }
}
