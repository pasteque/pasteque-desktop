//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
//
//    cashin/cashout notes by Henri Azar

package fr.pasteque.pos.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.format.DateUtils;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.caching.CallQueue;
import fr.pasteque.pos.caching.CashMoveCache;
import fr.pasteque.pos.caching.CatalogCache;
import fr.pasteque.pos.caching.CurrenciesCache;
import fr.pasteque.pos.caching.PaymentModesCache;
import fr.pasteque.pos.caching.TariffAreasCache;
import fr.pasteque.pos.caching.TaxesCache;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.inventory.TaxCategoryInfo;
import fr.pasteque.pos.payment.PaymentMode;
import fr.pasteque.pos.sales.TaxesLogic;
import fr.pasteque.pos.ticket.CashMove;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TariffAreaPrice;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.ZTicket;

/**
 *
 * @author adrianromero
 */
public class DataLogicSales implements CatalogDataSource
{
    /** Creates a new instance of SentenceContainerGeneric */
    public DataLogicSales() {
    }

    /** Get a product by ID */
    public final ProductInfoExt getProductInfo(String id)
        throws BasicException {
        return CatalogCache.getProduct(id);
    }

    /** Get a product by code */
    public final ProductInfoExt getProductInfoByCode(String sCode) throws BasicException {
        return CatalogCache.getProductByCode(sCode);
    }

    public List<ProductInfoExt> searchProducts(String label, String reference)
        throws BasicException {
        return CatalogCache.searchProducts(label, reference);
    }

    /** Get root categories. Categories must be preloaded. */
    public final List<CategoryInfo> getRootCategories() throws BasicException {
        return CatalogCache.getRootCategories();
    }

    public final CategoryInfo getCategory(String id) throws BasicException {
        return CatalogCache.getCategory(id);
    }

    /** Get subcategories from parent ID. Categories must be preloaded. */
    public final List<CategoryInfo> getSubcategories(String category) throws BasicException  {
        return CatalogCache.getSubcategories(category);
    }

    public final List<CategoryInfo> getCategories() throws BasicException {
        return CatalogCache.getCategories();
    }

    public final List<SubgroupInfo> getSubgroups(String composition)
        throws BasicException  {
        return CatalogCache.getSubgroups(composition);
    }
    public final List<ProductInfoExt> getSubgroupCatalog(Integer subgroup)
        throws BasicException  {
        return CatalogCache.getSubgroupProducts(subgroup);
    }

    /** Get products from a category ID. Products must be preloaded. */
    public List<ProductInfoExt> getProductCatalog(String category) throws BasicException  {
        return CatalogCache.getProductsByCategory(category);
    }

    /** Get products associated to a first one by ID */
    public List<ProductInfoExt> getProductComments(String id) throws BasicException {
        return new ArrayList<ProductInfoExt>();
        // TODO: enable product comments
        /*return new PreparedSentence(s,
            "SELECT P.ID, P.REFERENCE, P.CODE, P.NAME, P.ISCOM, P.ISSCALE, "
            + "P.PRICEBUY, P.PRICESELL, P.TAXCAT, P.CATEGORY, "
            + "P.ATTRIBUTESET_ID, P.IMAGE, P.ATTRIBUTES, P.DISCOUNTENABLED, "
            + "P.DISCOUNTRATE "
            + "FROM PRODUCTS P, PRODUCTS_CAT O, PRODUCTS_COM M "
            + "WHERE P.ID = O.PRODUCT AND P.ID = M.PRODUCT2 "
            + "AND M.PRODUCT = ? AND P.ISCOM = " + s.DB.TRUE() + " "
            + "ORDER BY O.CATORDER, P.NAME",
            SerializerWriteString.INSTANCE,
            ProductInfoExt.getSerializerRead()).list(id);*/
    }

    private TicketInfo readTicket(JSONObject o) throws BasicException {
        TicketInfo tkt = new TicketInfo(o);
        TaxesLogic tl = new TaxesLogic(this.getTaxList());
        tl.calculateTaxes(tkt);
        return tkt;
    }

    /** Get the list of tickets from a cash session. */
    public List<TicketInfo> getSessionTickets(CashSession session) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("api/ticket/session/"
                    + session.getCashRegisterId() + "/"
                    + session.getSequence());
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<TicketInfo> list = new ArrayList<TicketInfo>(a.length());
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    list.add(this.readTicket(o));
                }
                // Add local tickets on top
                List<TicketInfo> tickets = CallQueue.getPendingTickets();
                List<TicketInfo> reverse = new ArrayList<TicketInfo>(tickets.size() + list.size());
                for (int i = tickets.size() - 1; i >= 0; i--) {
                    reverse.add(tickets.get(i));
                }
                reverse.addAll(list);
                return reverse;
            } else {
                throw new BasicException("Server Not available "
                        + r.getStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    //Tickets and Receipt list
    public List<TicketInfo> searchTickets(Integer ticketId, Integer ticketType,
            int cashId, Date start, Date stop, String customerId,
            String userId) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r;
            if (ticketId != null) {
                r = loader.read("api/ticket/" + cashId + "/" + ticketId);
                if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                    JSONObject o = r.getObjContent();
                    List<TicketInfo> list = new ArrayList<TicketInfo>();
                    list.add(this.readTicket(o));
                    return list;
                } else {
                    throw new BasicException("Unexpected server answer " + r.getResponse());
                }
            } else {
                 r = loader.read("api/ticket/search",
                         "cashRegister", String.valueOf(cashId),
                         "dateStart",
                         start != null ? String.valueOf(DateUtils.toSecTimestamp(start)) : null,
                         "dateStop",
                         stop != null ? String.valueOf(DateUtils.toSecTimestamp(stop)) : null,
                         "customer", customerId, "user", userId);
                if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                    JSONArray a = r.getArrayContent();
                    List<TicketInfo> list = new ArrayList<TicketInfo>();
                    for (int i = 0; i < a.length(); i++) {
                        JSONObject o = a.getJSONObject(i);
                        list.add(this.readTicket(o));
                    }
                    // Add local tickets on top
                    List<TicketInfo> tickets = CallQueue.getPendingTickets();
                    tickets.addAll(list);
                    return tickets;
                } else {
                    throw new BasicException("Unexpected server answer " + r.getResponse());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    public final List<TaxInfo> getTaxList() throws BasicException {
        return TaxesCache.getTaxes();
    }
    public TaxInfo getTax(String taxId) throws BasicException {
        return TaxesCache.getTax(taxId);
    }

    public final List<TaxCategoryInfo> getTaxCategoriesList()
        throws BasicException {
        return TaxesCache.getTaxCats();
    }

    public final List<CurrencyInfo> getCurrenciesList() throws BasicException {
        return CurrenciesCache.getCurrencies();
    }

    public CurrencyInfo getCurrency(int currencyId) throws BasicException {
        return CurrenciesCache.getCurrency(currencyId);
    }

    public CurrencyInfo getMainCurrency() throws BasicException {
        return CurrenciesCache.getMainCurrency();
    }

    public final List<PaymentMode> getPaymentModes() throws BasicException {
        return PaymentModesCache.getPaymentModes();
    }

    public PaymentMode getPaymentMode(int paymentModeId) throws BasicException {
        return PaymentModesCache.getPaymentMode(paymentModeId);
    }

    public ZTicket getZTicket(CashSession session) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("api/cashsession/summary/"
                    + session.getCashRegisterId() + "/"
                    + session.getSequence());
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONObject o = r.getObjContent();
                if (o == null) {
                    return null;
                } else {
                    return new ZTicket(o);
                }
            } else {
                throw new BasicException(r.getResponse().toString(4));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }


    /** Save or edit ticket */
    public final void saveTicket(final TicketInfo ticket) throws BasicException {
        if (!ticket.isValid()) {
            throw new BasicException("Invalid ticket");
        }
        // Couic
        CallQueue.queueTicketSave(ticket);
    }

    public void deleteTicket(TicketInfo ticket) throws BasicException {
        // TODO: remove this
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r;
            r = loader.write("TicketsAPI", "delete",
                    "id", ticket.getId());
            if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                throw new BasicException("Bad server response");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    public TariffAreaPrice getTariffAreaPrice(int tariffAreaId, String productId) throws BasicException {
        return TariffAreasCache.getPrice(tariffAreaId, productId);
    }

    public final List<TariffInfo> getTariffAreaList() throws BasicException {
        return TariffAreasCache.getAreas();
    }

    public TariffInfo getTariffArea(int id) throws BasicException {
        return TariffAreasCache.getArea(id);
    }

    public void saveMove(CashMove move) throws BasicException {
        try {
            CashMoveCache.addCashMove(move);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }
    public List<CashMove> getCashMoves(int cashRegisterId, int sequence) throws BasicException {
        try {
            return CashMoveCache.getCashMovements(cashRegisterId, sequence);
        } catch (Exception e) {
            throw new BasicException(e);
        }
    }
}
