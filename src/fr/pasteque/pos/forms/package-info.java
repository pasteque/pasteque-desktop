/**
 * Global application management. Contains the starting menu, access to
 * the local cache, configuration properties and a few other things.
 */
package fr.pasteque.pos.forms;
