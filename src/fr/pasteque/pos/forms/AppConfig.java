//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Logger;
import fr.pasteque.pos.util.URLTextGetter;

/**
 * Configuration reader and writer.
 * Instanciate a configuration then use {@link #load()} to initialize its values
 * and set the AppConfig.loadedInstance singleton that can be used through the
 * application.
 * @author adrianromero
 */
public class AppConfig implements AppProperties
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.AppConfig");

    private static final String DEFAULT_DIR = System.getProperty("user.home")
            + "/." + AppLocal.APP_ID;
    /**
     * A singleton shortcut for the last configuration that was loaded
     * in memory.
     */
    public static AppConfig loadedInstance;

    /** In memory configuration key-value pairs. */
    private Properties m_propsconfig;
    /** File to save/load configuration to/from. */
    private File configfile;

    /**
     * Create a configuration with file read from command line arguments,
     * or use the default configuration file.
     */
    public AppConfig(String[] args) {
        if (args.length == 0) {
            this.init(this.getDefaultConfig());
        } else {
            this.init(new File(args[0]));
        }
    }

    /**
     * Get the directory path for the local cache.
     * @return Path to the cache directory.
     */
    public String getDataDir() {
        return DEFAULT_DIR;
    }

    /**
     * Create an in-memory only configuration that cannot be saved.
     */
    public AppConfig() {
        m_propsconfig = new Properties();
    }

    /**
     * Create a configuration bound to a file.
     */
    public AppConfig(File configfile) {
        this.init(configfile);
    }

    /**
     * Load the configuration from a file.
     */
    private void init(File configfile) {
        this.configfile = configfile;
        m_propsconfig = new Properties();

        File dir = new File(this.getDataDir());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        logger.info("Reading configuration file: " + configfile.getAbsolutePath());
    }

    private File getDefaultConfig() {
        return new File(DEFAULT_DIR + "/config.properties");
    }
    private File getDefaultRestoreConfig() {
        return new File(DEFAULT_DIR + "/config.properties.restore");
    }

    /**
     * Get the in-memory value for the given property.
     * @param sKey The key to read.
     * @return The in-memory value for that key.
     */
    public String getProperty(String sKey) {
        String prop = m_propsconfig.getProperty(sKey);
        if (prop == null) {
            prop = DEFAULT_VALUES.get(sKey);
        }
        return prop;
    }

    /**
     * Get the in-memory value for the given property as a boolean.
     * @param bKey The key to read.
     * @return True when the in-memory value for that key is "1" or "true"
     * ignoring case.
     */
    public boolean getBoolean(String bKey) {
        String prop = getProperty(bKey);
        if (prop == null) {
            return false;
        }
        return "1".equals(prop) || "true".equals(prop.toLowerCase());
    }

    public Double getNumber(String bKey) {
        String prop = getProperty(bKey);
        if(prop == null || "".equals(prop)){
            return null;
        }else {
            try{
                return Double.parseDouble(prop);
            }catch(NumberFormatException e){
                return null;
            }
        }
    }

    /** Shortcut to getProperty("users.activated") with parsing.
     * @return The list of activated user ids or null if all.
     */
    public String[] getEnabledUsers() {
        String prop = getProperty("users.activated");
        if (prop.equals("all")) {
            return null;
        }
        String ids[] = prop.split(",");
        for (String id : ids) {
            id = id.trim();
        }
        return ids;
    }

    /**
     * Get the API url. It is stored in "machine.hostname".
     * @return The API url stored in "machine.hostname" property.
     */
    public String getHost() {
        return this.getProperty("machine.hostname");
    }

    /**
     * Get the underlying file.
     * @return The file bound to the configuration. Null for in-memory only
     * configuration.
     */
    public File getConfigFile() {
        return this.configfile;
    }

    /**
     * Set the in-memory value of a key.
     * @param sKey The key to update.
     * @param sValue The value to set.
     */
    public void setProperty(String sKey, String sValue) {
        if (sValue == null) {
            m_propsconfig.remove(sKey);
        } else {
            m_propsconfig.setProperty(sKey, sValue);
        }
    }

    private String getLocalHostName() {
        try {
            return java.net.InetAddress.getLocalHost().getHostName();
        } catch (java.net.UnknownHostException eUH) {
            return "localhost";
        }
    }

    public String getLocale() {
        String slang = this.getProperty("user.language");
        String scountry = this.getProperty("user.country");
        //String svariant = this.getProperty("user.variant"); Unused
        String locCode = slang;
        if (scountry != null) {
            locCode += "_" + scountry;
        }
        return locCode;
    }

    /**
     * Delete the underlying configuration file.
     */
    public boolean delete() {
        this.loadDefault();
        return this.configfile.delete();
    }
    public boolean canRestore() {
        File restore = new File(this.configfile.getAbsolutePath() + ".restore");
        File defaultRestore = this.getDefaultRestoreConfig();
        return restore.exists() || defaultRestore.exists();
    }
    public void restore() throws IOException {
        File restore = new File(this.configfile.getAbsolutePath() + ".restore");
        File currentConfig = this.configfile;
        if (restore.exists()) {
            this.configfile = restore;
            this.load();
            this.configfile = currentConfig; // keep same file for save
            logger.info("Restored configuration from: " + restore.getAbsolutePath());
        } else {
            // Try with default restore file
            File defaultConfig = this.getDefaultRestoreConfig();
            if (defaultConfig.exists()) {
                this.configfile = defaultConfig;
                this.load();
                this.configfile = currentConfig;
                logger.info("Restored configuration from: " + defaultConfig.getAbsolutePath());
            } else {
                // No custom restore settings, use default.
                this.loadDefault();
                logger.info("Restored default configuration");
            }
        }
        this.save();
    }

    /**
     * Setup the default values and load the configuration from the underlying
     * file if any.
     * Also set AppConfig.loadedInstance to this instance.
     */
    public void load() {
        loadDefault();
        if (this.configfile != null) {
            try {
                InputStream in = new FileInputStream(configfile);
                if (in != null) {
                    m_propsconfig.load(in);
                    in.close();
                }
            } catch (IOException e){
                this.loadDefault();
            }
        }
        AppConfig.loadedInstance = this;
    }

    /**
     * Persist the current configuration to the underlying file.
     * Does nothing with in-memory only configuration.
     */
    public void save() throws IOException {
        if (this.configfile != null) {
            OutputStream out = new FileOutputStream(configfile);
            if (out != null) {
                m_propsconfig.store(out, AppLocal.APP_NAME
                        + ". Configuration file.");
                out.close();
            }
        }
    }

    /** The defaults values. These values are taken when the property is not
     * found in file. These values are not saved in properties files if they
     * are not set.
     */
    private static HashMap<String, String> DEFAULT_VALUES;
    static {
        DEFAULT_VALUES = new HashMap<String, String>();
        DEFAULT_VALUES.put("ui.touchbtnminwidth", "0.4"); // in inches
        DEFAULT_VALUES.put("ui.touchbtnminheight", "0.4"); // in inches
        DEFAULT_VALUES.put("ui.touchbigbtnminwidth", "0.5"); // in inches
        DEFAULT_VALUES.put("ui.touchbigbtnminheight", "0.5"); // in inches
        DEFAULT_VALUES.put("ui.touchhudgebtnminwidth", "0.8");  // in inches
        DEFAULT_VALUES.put("ui.touchhudgebtnminheight", "0.6"); // in inches
        DEFAULT_VALUES.put("ui.touchsmallbtnminwidth", "0.3"); // in inches
        DEFAULT_VALUES.put("ui.touchsmallbtnminheight", "0.3"); // in inches
        DEFAULT_VALUES.put("ui.touchbtnspacing", "0.08"); // in inches
        DEFAULT_VALUES.put("ui.fontsize", "12");
        DEFAULT_VALUES.put("ui.fontsizebig", "14");
        DEFAULT_VALUES.put("ui.fontsizesmall", "10");
        DEFAULT_VALUES.put("ui.showupdownbuttons", "0");
        DEFAULT_VALUES.put("ui.showbarcode", "1");
        DEFAULT_VALUES.put("ui.beepline", "0");
        DEFAULT_VALUES.put("prices.setmode", "taxed");
        DEFAULT_VALUES.put("prices.roundto", "0");
        DEFAULT_VALUES.put("server.backoffice", "");
        DEFAULT_VALUES.put("db.user", "");
        DEFAULT_VALUES.put("db.password", "");
        DEFAULT_VALUES.put("ui.printticketbydefault", "1");
        DEFAULT_VALUES.put("ui.printticketcount", "1");
        DEFAULT_VALUES.put("ui.buttons.catbyref", "0");
        DEFAULT_VALUES.put("ui.buttons.prodbyref", "0");
        DEFAULT_VALUES.put("ui.buttons.bold", "0");
        DEFAULT_VALUES.put("ui.buttons.fontface", "arial");
        DEFAULT_VALUES.put("ui.buttons.fontsize", "12px");
        DEFAULT_VALUES.put("ui.buttons.prdwidth", "128");
        DEFAULT_VALUES.put("ui.buttons.prdheight", "64");
        DEFAULT_VALUES.put("ui.buttons.catheight", "300");
        DEFAULT_VALUES.put("ui.catalog.catlistwidth", "275");

        DEFAULT_VALUES.put("ui.editornumber.maxvalue", "1000000");
        DEFAULT_VALUES.put("ticket.maxmanualpayment", "500");

        DEFAULT_VALUES.put("server.timeout", String.valueOf(URLTextGetter.DEFAULT_TIMEOUT));
    }

    /** Load "default file", which values are expanded or overriden by the
     * actually property file.
     * The properties that then saved along other in the properties file.
     */
    private void loadDefault() {
        m_propsconfig = new Properties();
        String dirname = System.getProperty("dirname.path");
        dirname = dirname == null ? "./" : dirname;

        m_propsconfig.setProperty("machine.hostname", "Caisse");

        Locale l = Locale.getDefault();
        m_propsconfig.setProperty("user.language", l.getLanguage());
        m_propsconfig.setProperty("user.country", l.getCountry());
        m_propsconfig.setProperty("user.variant", l.getVariant());

        m_propsconfig.setProperty("users.activated", "all");

        m_propsconfig.setProperty("swing.defaultlaf", System.getProperty("swing.defaultlaf", "javax.swing.plaf.metal.MetalLookAndFeel"));

        m_propsconfig.setProperty("machine.printer", "printer:(Default),receipt");
        m_propsconfig.setProperty("machine.printer.cpl", "32");			// 58mm, 32cpl, 12ppc
        m_propsconfig.setProperty("machine.printer.dpl", "384");
        m_propsconfig.setProperty("machine.printer.filter.order", "1");
        m_propsconfig.setProperty("machine.printer.filter.ticket", "1");
        m_propsconfig.setProperty("machine.printer.filter.z", "1");
        m_propsconfig.setProperty("machine.printer.2", "Not defined");
        m_propsconfig.setProperty("machine.printer.2.cpl", "48");		// 60mm, 48cpl, 12ppc
        m_propsconfig.setProperty("machine.printer.2.dpl", "576");
        m_propsconfig.setProperty("machine.printer.2.filter.order", "1");
        m_propsconfig.setProperty("machine.printer.2.filter.ticket", "0");
        m_propsconfig.setProperty("machine.printer.2.filter.z", "0");
        m_propsconfig.setProperty("machine.printer.3", "Not defined");
        m_propsconfig.setProperty("machine.printer.3.cpl", "42");		// 58mm, 42cpl, 9ppc
        m_propsconfig.setProperty("machine.printer.3.dpl", "384");
        m_propsconfig.setProperty("machine.printer.3.filter.order", "1");
        m_propsconfig.setProperty("machine.printer.3.filter.ticket", "0");
        m_propsconfig.setProperty("machine.printer.3.filter.z", "0");
        m_propsconfig.setProperty("machine.display", "Not defined");
        m_propsconfig.setProperty("machine.scale.1", "Not defined");
        m_propsconfig.setProperty("machine.scale.2", "Not defined");
        m_propsconfig.setProperty("machine.scale.3", "Not defined");
        m_propsconfig.setProperty("machine.screenmode", "fullscreenwindow"); // fullscreen, window, fullscreenwindow
        m_propsconfig.setProperty("machine.ticketsbag", "standard");

        // Receipt printer paper set to 72mmx200mm
        m_propsconfig.setProperty("paper.receipt.x", "10");
        m_propsconfig.setProperty("paper.receipt.y", "10");
        m_propsconfig.setProperty("paper.receipt.width", "190");
        m_propsconfig.setProperty("paper.receipt.height", "546");
        m_propsconfig.setProperty("paper.receipt.mediasizename", "A4");

        // Normal printer paper for A4
        m_propsconfig.setProperty("paper.standard.x", "72");
        m_propsconfig.setProperty("paper.standard.y", "72");
        m_propsconfig.setProperty("paper.standard.width", "451");
        m_propsconfig.setProperty("paper.standard.height", "698");
        m_propsconfig.setProperty("paper.standard.mediasizename", "A4");

        m_propsconfig.setProperty("machine.uniqueinstance", "1");

        // UI stuff
        m_propsconfig.setProperty("machine.screendensity", "100"); // Roughly in pixel per inch
        m_propsconfig.setProperty("ui.autohidemenu", "1");
        m_propsconfig.setProperty("ui.showkeypad", "1");
        m_propsconfig.setProperty("ui.beepline", "0");
        m_propsconfig.setProperty("ui.countmoney", "1");
        m_propsconfig.setProperty("ui.pricevisible", "none");
        m_propsconfig.setProperty("ui.useemptypayment", "0");
        m_propsconfig.setProperty("ui.confirmexcessivepayment", "noreturn");
        m_propsconfig.setProperty("ui.syncmode", "full");
        m_propsconfig.setProperty("ui.enablediscounts", "1");
        m_propsconfig.setProperty("ui.orderautoswitch", "new");
        m_propsconfig.setProperty("ui.autodisplaycustcount", "1");
    }
}
