//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import fr.pasteque.pos.caching.OptionsCache;
import java.util.HashMap;
import java.util.Properties;
import java.io.IOException;

/**
 * Map of general options retreived from the API.
 */
public class AppOptions
{
    /** Option name for the preferred backoffice url. */
    public static final String OPT_BACKOFFICE_URL = "backoffice.url";
    /** Option name for the contact field name replacements. */
    public static final String OPT_CUSTOMER_FIELDS = "customer.customFields";

    public static AppOptions loadedInstance;

    private Properties options;

    public AppOptions() {
        this.options = new Properties();
        loadedInstance = this;
    }

    /** Load the options from the option cache and set loadedInstance. */
    public void load() {
         try {
             this.options = OptionsCache.load();
         } catch (IOException e) {
             this.options = new Properties();
         }
         loadedInstance = this;
    }

    /** Load the options from memory and set loadedInstance. */
    public void load(Properties options) {
        this.options = options;
        loadedInstance = this;
    }

    /**
     * Get the value of an option. If it is not set, it's local default value
     * is returned.
     */
    public String getOption(String name) {
        String val = this.options.getProperty(name);
        if (val == null) {
            val = DEFAULT_VALUES.get(name);
        }
        return val;
    }

    /**
     * The default option values. These values are taken when the option
     * is not set, but are not stored.
     */
    private static HashMap<String, String> DEFAULT_VALUES;
    static {
        DEFAULT_VALUES = new HashMap<String, String>();
    }
}
