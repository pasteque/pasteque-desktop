//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.instance.InstanceManager;
import fr.pasteque.pos.util.URLBinGetter;
import fr.pasteque.pos.util.URLTextGetter;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceSkin;

/**
 *
 * @author adrianromero
 */
public class StartPOS
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.StartPOS");

    public static InstanceManager instanceManager;

    /** Creates a new instance of StartPOS */
    private StartPOS() {
    }

    /**
     * Mark this current instance as ready.
     * @param frame The frame to restore when requesting the current instance.
     */
    public static void instanceReady(javax.swing.JFrame frame) {
        if (instanceManager != null) {
            instanceManager.instanceReady(frame);
        }
    }

    public static void main (final String args[]) {
        // Load config
        final AppConfig config = new AppConfig(args);
        config.load();
        try {
            URLTextGetter.setTimeout(Integer.parseInt(config.getProperty("server.timeout")));
            URLBinGetter.setTimeout(Integer.parseInt(config.getProperty("server.timeout")));
        } catch (NumberFormatException e) {
            logger.warning("Invalid value for server.timeout " + config.getProperty("server.timeout"));
        }
        // Check for running instances
        instanceManager = new InstanceManager();
        if (config.getBoolean("machine.uniqueinstance")) {
            if (instanceManager.restoreInstance() != null) {
                System.exit(1);
            }
        }
        instanceManager.registerStartingApp();
        // Load shortcuts.
        ShortcutConfig shortcuts = new ShortcutConfig(args);
        shortcuts.load();
        ShortcutConfig.setInstance(shortcuts);
        // set Locale.
        String slang = config.getProperty("user.language");
        String scountry = config.getProperty("user.country");
        String svariant = config.getProperty("user.variant");
        if (slang != null && !slang.equals("") && scountry != null
                && svariant != null) {
            Locale.setDefault(new Locale(slang, scountry, svariant));
        }
        String locale = config.getLocale();
        // Set logo according to language
        ImageIcon splashImage = ImageLoader.readImageIcon("logo.png", locale);
        // Show splash
        final JFrame splash = new JFrame();
        splash.setUndecorated(true);
        splash.setResizable(false);
        splash.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        splash.setPreferredSize(new java.awt.Dimension(220, 220));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int splashWidth = 220;
        int splashHeight = 220;
        int splashX = (screenSize.width - splashWidth) / 2;
        int splashY = (screenSize.height - splashHeight) / 2;
        splash.setBounds(splashX, splashY, splashWidth, splashHeight);
        JLabel splashLabel = new JLabel(splashImage);
        splash.add(splashLabel, BorderLayout.CENTER);
        splash.setTitle(AppLocal.APP_NAME + " - " + AppLocal.APP_VERSION);
        splash.setLocationRelativeTo(null);
        splash.pack();
        splash.setVisible(true);
        splash.setIconImage(ImageLoader.readImage("favicon.png"));

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                // Set the format patterns
                Formats.setIntegerPattern(config.getProperty("format.integer"));
                Formats.setDoublePattern(config.getProperty("format.double"));
                Formats.setCurrencyPattern(config.getProperty("format.currency"));
                Formats.setPercentPattern(config.getProperty("format.percent"));
                Formats.setDatePattern(config.getProperty("format.date"));
                Formats.setTimePattern(config.getProperty("format.time"));
                Formats.setDateTimePattern(config.getProperty("format.datetime"));
                // Set the look and feel.
                try {
                    Object laf = Class.forName(config.getProperty("swing.defaultlaf")).getDeclaredConstructor().newInstance();
                    if (laf instanceof LookAndFeel){
                        UIManager.setLookAndFeel((LookAndFeel) laf);
                    } else if (laf instanceof SubstanceSkin) {
                        SubstanceLookAndFeel.setSkin((SubstanceSkin) laf);
                    }
                } catch (Exception e) {
                    logger.log(Level.WARNING, "Cannot set look and feel", e);
                }

                String screenmode = config.getProperty("machine.screenmode");
                if ("fullscreen".equals(screenmode)) {
                    JRootKiosk rootkiosk = new JRootKiosk();
                    rootkiosk.initFrame(config);
                } else {
                    JRootFrame rootframe = new JRootFrame();
                    rootframe.initFrame(config);

                    if ("fullscreenwindow".equals(screenmode)) {
                         rootframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    }
                }
                splash.dispose();
            }
        });
    }
}
