//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.*;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.caching.CashRegistersCache;
import fr.pasteque.pos.caching.ResourcesCache;
import fr.pasteque.pos.caching.RolesCache;
import fr.pasteque.pos.caching.UsersCache;
import fr.pasteque.pos.panels.PaymentsModel;
import fr.pasteque.pos.ticket.CashRegisterInfo;
import fr.pasteque.pos.ticket.CashSession;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class DataLogicSystem
{
    /** Creates a new instance of DataLogicSystem */
    public DataLogicSystem() {
    }

    public final Integer getAPILevel() throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("api/version");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONObject v = r.getObjContent();
                return v.getInt("level");
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    /** Get all users */
    public final List<AppUser> listPeople() throws BasicException {
        List<AppUser> data = null;
        try {
            data = UsersCache.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
    /** Get visible users */
    public final List<AppUser> listPeopleVisible() throws BasicException {
        List<AppUser> allUsers = this.listPeople();
        List<AppUser> visUsers = new LinkedList<AppUser>();
        for (AppUser user : allUsers) {
            if (user.isVisible()) {
                visUsers.add(user);
            }
        }
        return visUsers;
    }
    public final AppUser getPeople(String id) throws BasicException {
        List<AppUser> allUsers = this.listPeople();
        for (AppUser user : allUsers) {
            if (user.getId() != null && user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }
    /** Get user by card. Return null if nothing is found. */
    public final AppUser findPeopleByCard(String card) throws BasicException {
        List<AppUser> allUsers = this.listPeople();
        for (AppUser user : allUsers) {
            if (user.getCard() == null || "".equals(user.getCard())) {
                continue;
            }
            if (user.getCard().equals(card)) {
                return user;
            }
        }
        return null;
    }

    public final String findRolePermissions(String sRole)
        throws BasicException {
        Map<String, String> data = null;
        try {
            data = RolesCache.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (data != null) {
            return data.get(sRole);
        } else {
            return null;
        }
    }

    public final boolean changePassword(String userId, String oldPwd,
            String newPwd) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.write("api/user/" + userId + "/password",
                    "oldPassword", oldPwd, "newPassword", newPwd);
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                boolean ok = r.getResponse().getBoolean("content");
                return ok;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private byte[] getResource(String name) {
        byte[] data = null;
        try {
            data = ResourcesCache.load(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public final String getResourceAsText(String sName) {
        return Formats.BYTEA.formatValue(getResource(sName));
    }

    public final String getResourceAsXML(String sName) {
        return Formats.BYTEA.formatValue(getResource(sName));
    }

    public final BufferedImage getResourceAsImage(String sName) {
        try {
            byte[] img = getResource(sName); // , ".png"
            return img == null ? null : ImageIO.read(new ByteArrayInputStream(img));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public final CashRegisterInfo getCashRegister(int id) throws BasicException {
        return CashRegistersCache.getCashRegister(id);
    }
    public final CashRegisterInfo getCashRegister(String host)
        throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("api/cashregister/getByName/" + host);
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONObject o = r.getObjContent();
                if (o == null) {
                    return null;
                }
                return new CashRegisterInfo(o);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    /** Send a cash session to the server. */
    public final void saveCashSession(CashSession cashSess)
        throws BasicException {
        this.saveCashSession(cashSess, null);
    }
    /** Send a closed cash session to the server. */
    public final void saveCashSession(CashSession cashSess, PaymentsModel z)
        throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.write("api/cash",
                    "session", cashSess.toJSON(z).toString());
            if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                throw new BasicException("Server not available " + r.getResponse());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }
}
