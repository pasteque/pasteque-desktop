//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import fr.pasteque.basic.BasicException;
import fr.pasteque.beans.JFlowPanel;
import fr.pasteque.beans.JPasswordDialog;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.caching.CallQueue;
import fr.pasteque.pos.caching.CashRegistersCache;
import fr.pasteque.pos.caching.CashSessionsCache;
import fr.pasteque.pos.caching.LocalDB;
import fr.pasteque.pos.caching.sync.CorruptedDataException;
import fr.pasteque.pos.caching.sync.IncompatibleVersionException;
import fr.pasteque.pos.caching.sync.InvalidCashRegisterException;
import fr.pasteque.pos.caching.sync.SyncUpdate;
import fr.pasteque.pos.caching.sync.UnexpectedResponseException;
import fr.pasteque.pos.printer.DeviceTicket;
import fr.pasteque.pos.printer.TicketPrinterException;
import fr.pasteque.pos.printer.document.TicketParser;
import fr.pasteque.pos.ticket.CashRegisterInfo;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.util.AltEncrypter;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 *
 * @author adrianromero
 */
public class JRootApp extends JPanel implements AppView, SyncUpdate.Listener
{
    private static final long serialVersionUID = -5180139028899211039L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.JRootApp");
    private ExecutorService executor;

    private AppProperties m_props;
    private DataLogicSystem m_dlSystem;
    protected Timer updateLocalInfoTimer;

    private CashSession activeCashSession;
    private CashRegisterInfo cashRegister;
    private SyncUpdate updater;
    private boolean syncImages;

    private StringBuffer inputtext;

    private DeviceTicket m_TP;
    private TicketParser m_TTP;

    private Map<String, BeanFactory> m_aBeanFactories;

    private JPrincipalApp m_principalapp = null;

    private static HashMap<String, String> m_oldclasses; // This is for backwards compatibility purposes

    static {
        initOldClasses();
    }

    /** Creates new form JRootApp */
    public JRootApp() {
        this.executor = Executors.newFixedThreadPool(12);
        m_aBeanFactories = new HashMap<String, BeanFactory>();
        initComponents ();
        jScrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));
    }

    public boolean initApp(AppProperties props) {

        ArrayList<Future<?>> futures = new ArrayList<>();

        m_props = props;
        this.updater = new SyncUpdate(this, m_props.getHost());

        // support for different component orientation languages.
        applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));

        m_dlSystem = new DataLogicSystem();

        // Initialize printer...s
        m_TP = new DeviceTicket(this, m_props);

        // Initialization
        m_TTP = new TicketParser(getDeviceTicket(), m_dlSystem);
        printerStart();

        updateDataWoImages.setVisible("advanced".equals(m_props.getProperty("ui.syncmode")));

        // Init local cache
        futures.add(executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    AppOptions options = new AppOptions();
                    options.load();
                    LocalDB.init();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }));
        for (Future<?> fut : futures) {
            try {
                fut.get();
            } catch (InterruptedException ex) {
                Logger.getLogger(JRootApp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(JRootApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Check data from cache and load it if available
        boolean hasCache = this.setupFromCache();
        if (!hasCache) {
            this.showInit();
            return true;
        }

        // Start
        if ("startup".equals(m_props.getProperty("ui.syncmode"))) {
            syncUpdate();
        } else {
            this.showLogin();
        }
        return true;
    }

    /** Load and set cash register and session from cache.
     * @return True when loaded and set, false otherwise (i.e. no cache). When
     * false, the data are left untouched.
     */
    private boolean setupFromCache() {
        if ("".equals(m_props.getProperty("db.user"))
                || "".equals(m_props.getProperty("server.backoffice"))) {
            LocalDB.setSyncDate(null);
            return false;
        }
        if (LocalDB.getSyncDate() == null) {
            return false;
        }
        try {
            CashSession currentSession = CashSessionsCache.getCurrentCashSession();
            if (currentSession == null) {
                return false;
            } else {
                int crId = currentSession.getCashRegisterId();
                CashRegisterInfo cr = CashRegistersCache.getCashRegister(crId);
                if (cr == null) {
                    return false;
                }
                // Everything is OK, setup
                this.cashRegister = cr;
                this.activeCashSession = currentSession;
                // Initialize currency format
                try {
                    final DataLogicSales dlSales = new DataLogicSales();
                    fr.pasteque.format.Formats.setDefaultCurrency(dlSales.getMainCurrency());
                } catch (BasicException e) {
                    e.printStackTrace();
                }
                // Send pending tickets operations if there are any
                CallQueue.start();
                return true;
            }
        } catch (BasicException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void syncUpdate() {
        this.showLoading();
        this.syncProgress.setValue(0);
        this.syncProgress.setMaximum(SyncUpdate.STEP_COUNT);
        this.syncTitle.setText(AppLocal.getIntString("Label.ReloadingData"));
        new Thread() {
            public void run() {
                updater.synchronizeData();
            }
        }.start();
        //this.updater.synchronizeData();
    }

    @Override // From SyncUpdate.Listener
    public void onSyncProgress(int step, Object data) {
        this.syncProgress.setValue(this.syncProgress.getValue() + 1);
        if (step == SyncUpdate.STEP_CASH_SESSION) {
            try {
                setActiveCash((CashSession) data);
            } catch (BasicException ee) {
                JMessageDialog.showMessage(JRootApp.this,
                        new MessageInf(MessageInf.SGN_DANGER,
                                "Unable to set cash session.", ee));
                logger.log(Level.SEVERE, "Unable to set cash session. " + ee.getMessage());
            }
        } else if (step == SyncUpdate.STEP_CURRENCIES) {
            // Initialize currency format
            try {
                final DataLogicSales dlSales = new DataLogicSales();
                fr.pasteque.format.Formats.setDefaultCurrency(dlSales.getMainCurrency());
            } catch (BasicException e) {
                e.printStackTrace();
            }
        } else if (step == SyncUpdate.STEP_OPTIONS) {
            AppOptions.loadedInstance.load((Properties) data);
        }
    }

    @Override // From SyncUpdate.Listener
    public void onSyncFinished(int phase, int nextCount) {
        if (syncImages && phase == SyncUpdate.PHASE_DATA) {
            this.syncTitle.setText(AppLocal.getIntString("Label.ReloadImages"));
            this.syncProgress.setValue(0);
            this.syncProgress.setMaximum(nextCount);
            this.updater.synchronizeImages();
            return;
        }
        boolean hasCache = this.setupFromCache();
        if (!hasCache) {
            showInit();
        } else {
            updateLocalInfo();
            showLogin();
        }
    }

    @Override // From SyncUpdate.Listener
    public void onSyncError(int step, Exception e) {
        if (step == SyncUpdate.STEP_VERSION) {
            if (e instanceof IncompatibleVersionException) {
                int apiLevel = ((IncompatibleVersionException) e).getAPILevel();
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                        AppLocal.getIntString("message.incompatibleversion"),
                        AppLocal.getIntString("message.incompatibleversiondetails",
                        apiLevel, AppLocal.API_LEVEL)));
            } else {
                // Generic error
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_NOTICE,
                        AppLocal.getIntString("message.invalidserver",
                        AppConfig.loadedInstance.getProperty("server.backoffice")),
                        AppLocal.getIntString("message.invalidserverdetails")));
                Logger.getLogger(JRootApp.class.getName()).log(Level.SEVERE, null, e);
            }
        } else if (SyncUpdate.isSyncDataPhase(step)) {
            if (e instanceof UnexpectedResponseException) {
                UnexpectedResponseException ure = (UnexpectedResponseException) e;
                ServerLoader.Response response = ure.getResponse();
                if (ServerLoader.Response.STATUS_REJECTED.equals(response.getStatus())) {
                    JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                            "Unable to connect to server",
                            response.getResponse().toString()));
                    logger.log(Level.SEVERE, "Server rejected sync call "
                            + response.getResponse().toString());
                } else if (ServerLoader.Response.STATUS_ERROR.equals(response.getStatus())) {
                    JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                            "Server error",
                            response.getResponse().toString()));
                    logger.log(Level.SEVERE, "Server error on sync "
                            + response.getResponse().toString());
                } else {
                    // content is not parseable
                    JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                            "Unable to parse data",
                            response.getResponse().toString()));
                    logger.log(Level.SEVERE, "Unable to parse result "
                            + response.getResponse().toString());
                }
            } else if (e instanceof CorruptedDataException) {
                CorruptedDataException cde = (CorruptedDataException) e;
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                        "Corrupted data.", cde.getCause().getMessage()));
                logger.log(Level.SEVERE, "Unable to decode data.", e);
            } else if (e instanceof InvalidCashRegisterException) {
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                        AppLocal.getIntString("Message.UnknownCash")));
                logger.log(Level.WARNING, "Unknown cash register " +  m_props.getHost());
            } else {
                logger.log(Level.SEVERE, "Server unavailable.", e);
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                        "Unable to load data from server", e));
            }
        } else {
            // Unexpected error
            logger.log(Level.SEVERE, "Unknown error.", e);
            JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                    "Unknown error", e));
        }
        boolean hasCache = (LocalDB.getSyncDate() != null);
        if (!hasCache) {
            showInit();
        } else {
            updateLocalInfo();
            showLogin();
        }
    }

    public void tryToClose() {

        if (closeAppView()) {

            // success. continue with the shut down

            // apago el visor
            m_TP.getDeviceDisplay().clearVisor();

            // Download Root form
            SwingUtilities.getWindowAncestor(this).dispose();
        }
    }

    // Interfaz de aplicacion
    public DeviceTicket getDeviceTicket(){
        return m_TP;
    }

    public CashRegisterInfo getCashRegister() {
        return this.cashRegister;
    }
    /** @deprecated */
    public String getInventoryLocation() {
        return "0";
    }
    public CashSession getActiveCashSession() {
        return this.activeCashSession;
    }
    public String getActiveCashIndex() {
        return this.activeCashSession.getId();
    }
    public int getActiveCashSequence() {
        return this.activeCashSession.getSequence();
    }
    public Date getActiveCashDateStart() {
        return this.activeCashSession.getOpenDate();
    }

    public boolean isCashOpened() {
        return this.activeCashSession.isOpened();
    }

    public void setActiveCash(CashSession cashSess) throws BasicException {
        if (this.activeCashSession != null) {
            // Cache the current one for the continuous flag.
            CashSessionsCache.saveCashSession(this.activeCashSession);
        }
        cashSess.updateFromPrevious();
        this.activeCashSession = cashSess;
        CashSessionsCache.saveCashSession(this.activeCashSession);
        logger.log(Level.INFO, "Set new cash session "
                + cashSess.getCashRegisterId() + "-"
                + cashSess.getSequence() + " continuous: "
                + cashSess.isContinuous());

    }

    public void newActiveCash(CashSession cashSess) throws BasicException {
        // Cache the current one for the continuous flag.
        CashSessionsCache.saveCashSession(cashSess);
        // Set the next one and cache it.
        CashSession next = cashSess.next();
        next.updateFromPrevious(); // will be true
        this.activeCashSession = next;
        CashSessionsCache.saveCashSession(this.activeCashSession);
    }

    public AppProperties getProperties() {
        return m_props;
    }

    public Object getBean(String beanfactory) throws BeanFactoryException {

        // For backwards compatibility
        beanfactory = mapNewClass(beanfactory);


        BeanFactory bf = m_aBeanFactories.get(beanfactory);
        if (bf == null) {

            // testing sripts
            if (beanfactory.startsWith("/")) {
                bf = new BeanFactoryScript(beanfactory);
            } else {
                // Class BeanFactory
                try {
                    Class<?> bfclass = Class.forName(beanfactory);

                    if (BeanFactory.class.isAssignableFrom(bfclass)) {
                        bf = (BeanFactory) bfclass.getDeclaredConstructor().newInstance();
                    } else {
                        // the old construction for beans...
                        Constructor<?> constMyView = bfclass.getConstructor(new Class[] {AppView.class});
                        Object bean = constMyView.newInstance(new Object[] {this});

                        bf = new BeanFactoryObj(bean);
                    }

                } catch (Exception e) {
                    // ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException
                    throw new BeanFactoryException(e);
                }
            }

            // cache the factory
            m_aBeanFactories.put(beanfactory, bf);

            // Initialize if it is a BeanFactoryApp
            if (bf instanceof BeanFactoryApp) {
                ((BeanFactoryApp) bf).init(this);
            }
        }
        return bf.getBean();
    }

    private static String mapNewClass(String classname) {
        String newclass = m_oldclasses.get(classname);
        return newclass == null
                ? classname
                : newclass;
    }

    private static void initOldClasses() {
        m_oldclasses = new HashMap<String, String>();

        // update bean names from 2.00 to 2.20
        m_oldclasses.put("fr.pasteque.pos.reports.JReportCustomers", "/com/openbravo/reports/customers.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportCustomersB", "/com/openbravo/reports/customersb.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportClosedPos", "/com/openbravo/reports/closedpos.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportClosedProducts", "/com/openbravo/reports/closedproducts.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JChartSales", "/com/openbravo/reports/chartsales.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportInventory", "/com/openbravo/reports/inventory.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportInventory2", "/com/openbravo/reports/inventoryb.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportInventoryBroken", "/com/openbravo/reports/inventorybroken.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportInventoryDiff", "/com/openbravo/reports/inventorydiff.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportPeople", "/com/openbravo/reports/people.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportTaxes", "/com/openbravo/reports/taxes.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportUserSales", "/com/openbravo/reports/usersales.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportProducts", "/com/openbravo/reports/products.bs");
        m_oldclasses.put("fr.pasteque.pos.reports.JReportCatalog", "/com/openbravo/reports/productscatalog.bs");

        // update bean names from 2.10 to 2.20
        m_oldclasses.put("fr.pasteque.pos.panels.JPanelTax", "com.openbravo.pos.inventory.TaxPanel");

    }

    public void waitCursorBegin() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    public void waitCursorEnd(){
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    public AppUserView getAppUserView() {
        return m_principalapp;
    }


    private void printerStart() {

        String sresource = m_dlSystem.getResourceAsXML("Printer.Start");
        if (sresource == null) {
            m_TP.getDeviceDisplay().writeVisor(AppLocal.APP_NAME, AppLocal.APP_VERSION);
        } else {
            try {
                m_TTP.printTicket(sresource);
            } catch (TicketPrinterException eTP) {
                m_TP.getDeviceDisplay().writeVisor(AppLocal.APP_NAME, AppLocal.APP_VERSION);
            }
        }
    }

    private void updatePastequeCredentials() {
        String user = m_props.getProperty("db.user");
        String password = m_props.getProperty("db.password");
        if (password != null && password != null && password.startsWith("crypt:")) {
            AltEncrypter cypher = new AltEncrypter("cypherkey" + user);
            password = cypher.decrypt(password.substring(6));
        }
        ptHost.setText(m_props.getProperty("server.backoffice"));
        ptUser.setText(user);
        ptPassword.setText(password);
        ptCashRegisterName.setText(m_props.getProperty("machine.hostname"));
    }

    private void listPeople() {

        try {

            jScrollPane1.getViewport().setView(null);

            JFlowPanel jPeople = new JFlowPanel();
            jPeople.applyComponentOrientation(getComponentOrientation());

            java.util.List<AppUser> people = m_dlSystem.listPeopleVisible();
            String[] enabledUsers = m_props.getEnabledUsers();

            for (int i = 0; i < people.size(); i++) {
                AppUser user = people.get(i);
                // Check if user is not disabled on this machine
                if (enabledUsers != null) {
                    boolean enabled = false;
                    for (String id : enabledUsers) {
                        if (id.equals(user.getId())) {
                            enabled = true;
                            break;
                        }
                    }
                    if (!enabled) {
                        continue;
                    }
                }

                JButton btn = new JButton(new AppUserAction(user));
                btn.applyComponentOrientation(getComponentOrientation());
                btn.setFocusPainted(false);
                btn.setFocusable(false);
                btn.setRequestFocusEnabled(false);
                btn.setHorizontalAlignment(SwingConstants.LEADING);
                btn.setMaximumSize(new Dimension(150, 50));
                btn.setPreferredSize(new Dimension(150, 50));
                btn.setMinimumSize(new Dimension(150, 50));

                jPeople.add(btn);
            }
            jScrollPane1.getViewport().setView(jPeople);

        } catch (BasicException ee) {
            ee.printStackTrace();
        }
    }
    // Action performed when clicking on a people button
    private class AppUserAction extends AbstractAction
    {
        private static final long serialVersionUID = 5242320876936837177L;

        private AppUser m_actionuser;

        public AppUserAction(AppUser user) {
            m_actionuser = user;
            putValue(Action.SMALL_ICON, m_actionuser.getIcon());
            putValue(Action.NAME, m_actionuser.getName());
        }

        public void actionPerformed(ActionEvent evt) {
            // Try auto-logging if user has no password set
            if (m_actionuser.authenticate()) {
                // It works!
                openAppView(m_actionuser);
            } else {
                // Show password input
                String sPassword = JPasswordDialog.showEditPassword(JRootApp.this,
                        AppLocal.getIntString("Label.Password"),
                        m_actionuser.getName(),
                        m_actionuser.getIcon());
                if (sPassword != null) {
                    if (m_actionuser.authenticate(sPassword)) {
                        // Password is valid, enter app
                        openAppView(m_actionuser);
                    } else {
                        // Wrong password, show message
                        MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.BadPassword"));
                        msg.show(JRootApp.this);
                    }
                }
            }
        }
    }

    private void showView(String view) {
        CardLayout cl = (CardLayout)(m_jPanelContainer.getLayout());
        cl.show(m_jPanelContainer, view);
    }

    /** Enter app, show main screen with selected user. */
    private void openAppView(AppUser user) {
        // Make sure app is not already opened before continuing
        if (closeAppView()) {

            m_principalapp = new JPrincipalApp(this, user);

            // The user status notificator
            jPanel3.add(m_principalapp.getNotificator());
            jPanel3.revalidate();

            // The main panel
            m_jPanelContainer.add(m_principalapp, "_" + m_principalapp.getUser().getId());
            showView("_" + m_principalapp.getUser().getId());

            m_principalapp.activate();
        }
    }

    /** Return to login screen.
     * @return True if not opened or successfuly closed, false if close failed.
     */
    public boolean closeAppView() {

        if (m_principalapp == null) {
            return true;
        } else if (!m_principalapp.deactivate()) {
            m_TP.getDeviceDisplay().clearVisor();
            return false;
        } else {
            m_TP.getDeviceDisplay().clearVisor();
            // the status label
            jPanel3.remove(m_principalapp.getNotificator());
            jPanel3.revalidate();
            jPanel3.repaint();

            // remove the card
            m_jPanelContainer.remove(m_principalapp);
            m_principalapp = null;

            showLogin();

            return true;
        }
    }

    private void showInit() {
        updatePastequeCredentials();
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weighty = 3;
        m_jPanelInit.add(postechLogo, c);
        showView("init");
    }

    private void showLoading() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weighty = 3;
        m_jPanelLoading.add(postechLogo, c);
        showView("loading");
    }

    private void showLogin() {

        // Show Login
        listPeople();
        updateLocalInfo();
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weighty = 3;
        m_jPanelLogin.add(postechLogo, c);
        showView("login");

        // show welcome message
        printerStart();

        // keyboard listener activation
        inputtext = new StringBuffer();
        m_txtKeys.setText(null);
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                m_txtKeys.requestFocus();
            }
        });
    }

    private void processKey(char c) {

        if (c == '\n') {

            AppUser user = null;
            try {
                user = m_dlSystem.findPeopleByCard(inputtext.toString());
            } catch (BasicException e) {
                e.printStackTrace();
            }

            if (user == null)  {
                // user not found
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.nocard"));
                msg.show(this);
            } else {
                openAppView(user);
            }

            inputtext = new StringBuffer();
        } else {
            inputtext.append(c);
        }
    }

    private void updateLocalInfo() {
        Date syncDate = LocalDB.getSyncDate();
        boolean hasPendingData = !CallQueue.isEmpty();
        DateFormat dfDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
        DateFormat dfTime = DateFormat.getTimeInstance(DateFormat.SHORT);
        String info = "<html><div style=\"text-align:center\">";
        info += AppLocal.getIntString("Label.WelcomeInfo",
                m_props.getProperty("db.user"), m_props.getProperty("server.backoffice"));
        info += "<br>";
        info += AppLocal.getIntString("Label.WelcomeInfoLocalData",
                dfDate.format(syncDate), dfTime.format(syncDate));
        if (hasPendingData) {
            int pendingCount = CallQueue.getPendingTicketCount();
            info += "<br>" + AppLocal.getIntString("Label.WelcomeInfoPendingData", pendingCount);
        }
        info += "</div>";
        localInfo.setText(info);
        updateData.setEnabled(!hasPendingData);
        if (hasPendingData && this.updateLocalInfoTimer == null) {
            this.updateLocalInfoTimer = new Timer();
            this.updateLocalInfoTimer.schedule(new TimerTask(){
                public void run() {
                    updateLocalInfoTimer.cancel();
                    updateLocalInfoTimer = null;
                    updateLocalInfo();
                }
            }, 1000);
        }
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnspacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        m_jPanelContainer = new javax.swing.JPanel();
        m_jPanelLogin = new javax.swing.JPanel();
        m_jPanelLoading = new javax.swing.JPanel();
        syncTitle = WidgetsBuilder.createLabel();
        syncProgress = new javax.swing.JProgressBar();
        postechLogo = new javax.swing.JLabel();
        m_jLogonName = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        m_jClose = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        m_txtKeys = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        localInfo = WidgetsBuilder.createLabel();
        updateData = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("reload.png"),
                AppLocal.getIntString("Button.ReloadData"),
                WidgetsBuilder.SIZE_MEDIUM);
        updateData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateDataActionPerformed(evt);
            }
        });
        updateDataWoImages = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("reload.png"),
                AppLocal.getIntString("Button.ReloadDataWithoutImages"),
                WidgetsBuilder.SIZE_MEDIUM);
        updateDataWoImages.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateDataWoImagesActionPerformed(evt);
            }
        });

        setPreferredSize(new java.awt.Dimension(1024, 768));
        setLayout(new java.awt.BorderLayout());

        m_jPanelContainer.setLayout(new java.awt.CardLayout());
        int gridy = 0;
        GridBagConstraints c = null;

        // Shared logo
        postechLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        postechLogo.setIcon(ImageLoader.readImageIcon("logo.png",
                cfg.getLocale()));
        postechLogo.setAlignmentX(0.5F);
        postechLogo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        postechLogo.setMaximumSize(new java.awt.Dimension(800, 1024));
        postechLogo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        // Init (account login) content
        m_jPanelInit = new javax.swing.JPanel();
        m_jPanelInit.setLayout(new GridBagLayout());
        javax.swing.JLabel ptHostLbl = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.APIHost"));
        ptHost = WidgetsBuilder.createTextField();
        javax.swing.JLabel ptUserLbl = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.APILogin"));
        ptUser = WidgetsBuilder.createTextField();
        javax.swing.JLabel ptPasswordLbl = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.APIPassword"));
        ptPassword = WidgetsBuilder.createPasswordField();
        javax.swing.JLabel ptCashRegisterNameLbl = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.CashRegisterName"));
        ptCashRegisterName = WidgetsBuilder.createTextField();

        gridy = 1;
        javax.swing.JPanel initContainer = new javax.swing.JPanel();
        initContainer.setBorder(BorderFactory.createTitledBorder(AppLocal.tr("Label.Config.API")));
        initContainer.setLayout(new GridBagLayout());
        initContainer.setMinimumSize(new java.awt.Dimension(510, 118));
        // Credential inputs
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 0; c.weightx = 0.25; c.anchor = GridBagConstraints.LINE_START;
        initContainer.add(ptHostLbl, c);
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 0; c.weightx = 0.75; c.fill = GridBagConstraints.HORIZONTAL;
        initContainer.add(ptHost, c);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 1; c.weightx = 0.25; c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(btnspacing, 0, 0, 0);
        initContainer.add(ptUserLbl, c);
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 1; c.weightx = 0.75; c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(btnspacing, 0, 0, 0);
        initContainer.add(ptUser, c);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 2; c.weightx = 0.25; c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(btnspacing, 0, 0, 0);
        initContainer.add(ptPasswordLbl, c);
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 2; c.weightx = 0.75; c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(btnspacing, 0, 0, 0);
        initContainer.add(ptPassword, c);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 3; c.weightx = 0.25; c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(btnspacing, 0, 0, 0);
        initContainer.add(ptCashRegisterNameLbl, c);
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 3; c.weightx = 0.75; c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(btnspacing, 0, 0, 0);
        initContainer.add(ptCashRegisterName, c);
        // Button line
        javax.swing.JPanel initButtons = new javax.swing.JPanel();
        java.awt.FlowLayout flow = new java.awt.FlowLayout();
        flow.setVgap(btnspacing); flow.setHgap(btnspacing);
        javax.swing.JButton connect = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"), AppLocal.getIntString("Button.Connect"), WidgetsBuilder.SIZE_MEDIUM);
        initButtons.add(connect);
        connect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectActionPerformed(evt);
            }
        });
        javax.swing.JButton exit = WidgetsBuilder.createButton(ImageLoader.readImageIcon("exit.png"), AppLocal.getIntString("Button.Close"), WidgetsBuilder.SIZE_MEDIUM);
        exit.setText(AppLocal.getIntString("Button.Close")); // NOI18N
        initButtons.add(exit);
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jCloseActionPerformed(evt);
            }
        });
        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 4; c.anchor = GridBagConstraints.LINE_END;
        initContainer.add(initButtons, c);
        c = new GridBagConstraints();
        initContainer.setPreferredSize(new java.awt.Dimension(625, (int) initContainer.getPreferredSize().getHeight()));
        c.gridx = 0;
        c.gridy = gridy++;
        c.weighty = 3;
        m_jPanelInit.add(initContainer, c);

        m_jPanelContainer.add(m_jPanelInit, "init");

        // Loading content
        m_jPanelLoading.setLayout(new GridBagLayout());

        javax.swing.JPanel loadingContainer = new javax.swing.JPanel();
        loadingContainer.setLayout(new GridBagLayout());
        loadingContainer.setMinimumSize(new java.awt.Dimension(510, 118));
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 0; c.weightx = 1; c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.BOTH;
        loadingContainer.add(syncTitle, c);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 1; c.weightx = 1; c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.BOTH;
        syncProgress.setMinimumSize(new java.awt.Dimension(400, 30));
        loadingContainer.add(syncProgress, c);
        loadingContainer.setPreferredSize(new java.awt.Dimension(625, (int) loadingContainer.getPreferredSize().getHeight() + 30));
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.weighty = 3;
        c.anchor = GridBagConstraints.NORTH;
        m_jPanelLoading.add(loadingContainer, c);

        m_jPanelContainer.add(m_jPanelLoading, "loading");


        // Main login content
        m_jPanelLogin.setLayout(new GridBagLayout());

        gridy = 1;
        // Sync info
        javax.swing.JPanel syncInfoPanel = new javax.swing.JPanel();
        syncInfoPanel.setLayout(new BoxLayout(syncInfoPanel, BoxLayout.PAGE_AXIS));
        localInfo.setAlignmentX(Component.CENTER_ALIGNMENT);
        updateData.setAlignmentX(Component.CENTER_ALIGNMENT);
        updateDataWoImages.setAlignmentX(Component.CENTER_ALIGNMENT);
        syncInfoPanel.add(localInfo, c);
        syncInfoPanel.add(javax.swing.Box.createRigidArea(new Dimension(0, 5)));
        syncInfoPanel.add(updateData, c);
        syncInfoPanel.add(javax.swing.Box.createRigidArea(new Dimension(0, 5)));
        syncInfoPanel.add(updateDataWoImages);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = gridy++;
        c.weighty = 1;
        m_jPanelLogin.add(syncInfoPanel, c);

        // Login frame
        m_jLogonName.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        m_jLogonName.setLayout(new java.awt.BorderLayout());
        // Scroll
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setPreferredSize(new java.awt.Dimension(510, 118));
        jScrollPane1.setMinimumSize(new java.awt.Dimension(510, 118));
        m_jLogonName.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 5, 0, 5));
        jPanel2.setLayout(new java.awt.BorderLayout());
        // Close button
        m_jClose.setIcon(ImageLoader.readImageIcon("exit.png"));
        m_jClose.setText(AppLocal.getIntString("Button.Close")); // NOI18N
        m_jClose.setFocusPainted(false);
        m_jClose.setFocusable(false);
        m_jClose.setPreferredSize(new java.awt.Dimension(115, 35));
        m_jClose.setRequestFocusEnabled(false);
        m_jClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jCloseActionPerformed(evt);
            }
        });
        jPanel2.add(m_jClose, java.awt.BorderLayout.NORTH);
        // Login by scan
        jPanel1.setLayout(null);
        m_txtKeys.setPreferredSize(new java.awt.Dimension(0, 0));
        m_txtKeys.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                m_txtKeysKeyTyped(evt);
            }
        });
        jPanel1.add(m_txtKeys);
        m_txtKeys.setBounds(0, 0, 0, 0);
        jPanel2.add(jPanel1, java.awt.BorderLayout.CENTER);
        m_jLogonName.add(jPanel2, java.awt.BorderLayout.LINE_END);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = gridy++;
        c.weighty = 1;
        c.anchor = GridBagConstraints.SOUTH;
        m_jPanelLogin.add(m_jLogonName, c);
        // About
        JButton about = WidgetsBuilder.createButton(ImageLoader.readImageIcon("about.png"), WidgetsBuilder.SIZE_SMALL);
        about.setBorder(BorderFactory.createEmptyBorder());
        about.setContentAreaFilled(false);
        about.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutActionPerformed(evt);
            }
        });
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = gridy++;
        c.weighty = 0.1;
        c.weightx = 1;
        int versionInset = WidgetsBuilder.dipToPx(5);
        c.insets = new Insets(0, 0, 0, versionInset);
        c.anchor = GridBagConstraints.LINE_END;
        m_jPanelLogin.add(about, c);
        m_jPanelContainer.add(m_jPanelLogin, "login");
        add(m_jPanelContainer, java.awt.BorderLayout.CENTER);
    }

    private void updateDataActionPerformed(java.awt.event.ActionEvent evt) {
        syncImages = true;
        syncUpdate();
    }

    private void updateDataWoImagesActionPerformed(java.awt.event.ActionEvent evt) {
        syncImages = false;
        syncUpdate();
    }

    private void m_jCloseActionPerformed(java.awt.event.ActionEvent evt) {
        tryToClose();
    }

    private void connectActionPerformed(java.awt.event.ActionEvent evt) {
        AppConfig cfg = (AppConfig) m_props;
        cfg.setProperty("server.backoffice", ptHost.getText());
        cfg.setProperty("db.user", ptUser.getText());
        AltEncrypter cypher = new AltEncrypter("cypherkey" + ptUser.getText());
        cfg.setProperty("db.password", "crypt:" + cypher.encrypt(new String(ptPassword.getPassword())));
        cfg.setProperty("machine.hostname", ptCashRegisterName.getText());
        try {
            cfg.save();
        } catch (IOException e) {
            JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotsaveconfig"), e));
            logger.log(Level.SEVERE, "Unable to save configuration file", e);
            return;
        }
        this.updater = new SyncUpdate(this, m_props.getHost());
        syncImages = true;
        syncUpdate();
    }

    private void aboutActionPerformed(java.awt.event.ActionEvent evt) {
        final JFrame about = new JFrame();
        about.setLayout(new GridBagLayout());
        about.setIconImage(ImageLoader.readImage("favicon.png"));
        GridBagConstraints c = null;

        ImageIcon icon = ImageLoader.readImageIcon("favicon.png");
        JLabel iconLabel = new JLabel(icon);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.3;
        c.weighty = 0.3;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 5, 5);
        about.add(iconLabel, c);

        String version = String.format(AppLocal.getIntString("Label.Version"),
                AppLocal.getIntString("Version.Code"), AppLocal.APP_VERSION);
        JLabel aboutLabel = WidgetsBuilder.createSmallLabel(version);
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.7;
        c.weighty = 0.3;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 5, 5);
        about.add(aboutLabel, c);

        String licence = "<html>" + AppLocal.getIntString("Licence") + "</html>";
        JLabel licenceLabel = WidgetsBuilder.createSmallLabel(licence);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0.7;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 5, 5);
        about.add(licenceLabel, c);
        licenceLabel.setPreferredSize(new java.awt.Dimension(
                WidgetsBuilder.pixelSize(4.0f),
                WidgetsBuilder.pixelSize(1.0f)));

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));
        JButton site = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_web.png"),
                AppLocal.getIntString("Button.AboutSite"),
                WidgetsBuilder.SIZE_MEDIUM);
        site.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().browse(new java.net.URI(AppLocal.getIntString("Button.AboutSiteURL")));
                    } catch (Exception e) {
                        /* TODO: error handling */
                    }
                } else {
                    /* TODO: error handling */
                }
            }
        });
        buttonsPanel.add(site);
        JButton close = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                about.dispose();
            }
        });
        buttonsPanel.add(close);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 2;
        c.weightx = 1;
        c.anchor = GridBagConstraints.LINE_END;
        about.add(buttonsPanel, c);

        about.setTitle(AppLocal.APP_NAME + " - " + AppLocal.APP_VERSION);
        about.pack();
        about.setLocationRelativeTo(null);
        about.setVisible(true);
    }

    private void m_txtKeysKeyTyped(java.awt.event.KeyEvent evt) {
        m_txtKeys.setText("0");
        processKey(evt.getKeyChar());
    }

    private javax.swing.JLabel postechLogo;
    private javax.swing.JLabel localInfo;
    private javax.swing.JButton updateData;
    private javax.swing.JButton updateDataWoImages;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton m_jClose;
    private javax.swing.JPanel m_jLogonName;
    private javax.swing.JPanel m_jPanelContainer;
    private javax.swing.JPanel m_jPanelLogin;
    private javax.swing.JPanel m_jPanelLoading;
    private javax.swing.JTextField m_txtKeys;
    private javax.swing.JLabel syncTitle;
    private javax.swing.JProgressBar syncProgress;
    private javax.swing.JPanel m_jPanelInit;
    private javax.swing.JTextField ptHost;
    private javax.swing.JTextField ptUser;
    private javax.swing.JPasswordField ptPassword;
    private javax.swing.JTextField ptCashRegisterName;
}
