//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.Icon;

/**
 * List item renderer for CustomerInfoExt.
 */
public class CustomerRenderer extends DefaultListCellRenderer
{
    private static final long serialVersionUID = -4564276910703452175L;

    private Icon icocustomer;

    /** Creates a new instance of ProductRenderer */
    public CustomerRenderer() {

        icocustomer = ImageLoader.readImageIcon("customer_default.png");
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, null, index, isSelected, cellHasFocus);
        setText(value.toString());
        if (value instanceof CustomerInfoExt) {
            CustomerInfoExt c = (CustomerInfoExt) value;
            if (c.isExpired()) {
                setText(AppLocal.tr("Label.Customer.NameExpired",
                        value.toString()));
            }
        }
        setIcon(icocustomer);
        WidgetsBuilder.adaptSize(this, WidgetsBuilder.SIZE_MEDIUM);
        return this;
    }
}
