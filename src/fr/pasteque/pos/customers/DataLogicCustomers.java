//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.caching.CustomersCache;
import fr.pasteque.pos.caching.DiscountProfilesCache;
import fr.pasteque.pos.datasource.CustomerDataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class DataLogicCustomers implements CustomerDataSource
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.customers.DataLogicCustomers");

    /** Load customers list from server */
    private List<CustomerInfoExt> loadCustomers() throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("api/customer/getAll");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                List<CustomerInfoExt> data = new ArrayList<CustomerInfoExt>();
                JSONArray a = r.getArrayContent();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    CustomerInfoExt customer = new CustomerInfoExt(o);
                    data.add(customer);
                }
                return data;
            }
        } catch (Exception e) {
            throw new BasicException(e);
        }
        return null;
    }
    private List<String> loadTopCustomers() throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("api/customer/getTop");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                List<String> data = new ArrayList<String>();
                JSONArray a = r.getArrayContent();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    data.add(String.valueOf(o.getInt("id")));
                }
                return data;
            }
        } catch (Exception e) {
            throw new BasicException(e);
        }
        return null;
    }
    /** Preload and update cache if possible. Return true if succes. False
     * otherwise and cache is not modified.
     */
    public boolean preloadCustomers() throws BasicException {
        logger.log(Level.INFO, "Preloading customers");
        List<CustomerInfoExt> data = this.loadCustomers();
        List<String> topIds = this.loadTopCustomers();
        if (data == null) {
            return false;
        }
        CustomersCache.refreshCustomers(data);
        CustomersCache.refreshRanking(topIds);
        return true;
    }

    /** Get all customers */
    public List<CustomerInfoExt> getCustomerList() throws BasicException {
        return CustomersCache.getCustomers();
    }

    /** Get customer from local cache */
    public CustomerInfoExt getCustomer(String id) throws BasicException {
        return CustomersCache.getCustomer(id);
    }
    /** Load a customer from server, update it in cache and return it.
     * This is an asynchronous call.
     */
    public void updateCustomer(final String id,
            final CustomerListener callback) {
        Thread t = new Thread() {
                public void run() {
                    try {
                        logger.log(Level.INFO, "Refreshing customer");
                        ServerLoader loader = new ServerLoader();
                        ServerLoader.Response r = loader.read("CustomersAPI",
                                "get", "id", id);
                        if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                            JSONObject o = r.getObjContent();
                            CustomerInfoExt customer = new CustomerInfoExt(o);
                            CustomersCache.refreshCustomer(customer);
                            if (callback != null) {
                                callback.customerLoaded(customer);
                            }
                        } else {
                            logger.log(Level.WARNING,
                                    "Unable to load customer: "
                                    + r.getStatus());
                        }
                    } catch (Exception e) {
                        logger.log(Level.WARNING,
                                "Unable to update customer", e);
                        if (callback != null) {
                            callback.customerLoaded(null);
                        }
                    }
                }
            };
        t.start();
    }

    public CustomerInfoExt getCustomerByCard(String card)
        throws BasicException {
        return CustomersCache.getCustomerByCard(card);
    }

    /** Search customers, use null as argument to disable filter */
    public List<CustomerInfoExt> searchCustomers(String search)
            throws BasicException {
        return CustomersCache.searchCustomers(search);
    }

    /** Gets the TOP 10 customer's list by number of tickets
     * with their id
     */
    public List<CustomerInfoExt> getTop10CustomerList() throws BasicException {
        return CustomersCache.getTopCustomers();
    }

    /** Send customer data to the server.
     * @param customer The customer to save. If it's id is set, it's an update.
     * @return The customer id (new id for a new customer). */
    public CustomerInfoExt saveCustomer(final CustomerInfoExt customer)
            throws BasicException {
        ServerLoader loader = new ServerLoader();
        ServerLoader.Response r;
        try {
            r = loader.request("POST", "api/customer", customer.toJSON());
        } catch (java.net.SocketTimeoutException e) {
            throw new BasicException(e);
        } catch (fr.pasteque.pos.util.URLTextGetter.ServerException e) {
            throw new BasicException(e);
        } catch (java.io.IOException e) {
            throw new BasicException(e);
        }
        CustomerInfoExt customerRet = null;
        if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
            JSONObject o = r.getObjContent();
            if (o == null) {
                logger.log(Level.WARNING, "Unable to save customer: invalid response");
                return null;
            }
            customerRet = new CustomerInfoExt(o);
            return customerRet;
        } else {
            logger.log(Level.WARNING,
                    "Unable to load customer: " + r.getStatus());
            return null;
        }
    }

    public List<DiscountProfile> getDiscountProfiles() throws BasicException {
        try {
            return DiscountProfilesCache.load();
        } catch (IOException e) {
            throw new BasicException(e.getMessage());
        }
    }

    public DiscountProfile getDiscountProfile(int id) throws BasicException {
        try {
            List<DiscountProfile> profiles = DiscountProfilesCache.load();
            if (profiles == null) { return null; }
            for (DiscountProfile p : profiles) {
                if (id == p.getId()) {
                    return p;
                }
            }
            return null;
        } catch (IOException e) {
            throw new BasicException(e.getMessage());
        }
    }

    public interface CustomerListener {
        /** Callback for asynchronous customer refresh */
        public void customerLoaded(CustomerInfoExt customer);
    }
}
