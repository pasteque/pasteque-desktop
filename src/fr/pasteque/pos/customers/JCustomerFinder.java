//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author  adrianromero
 */
public class JCustomerFinder extends javax.swing.JDialog
implements ShortcutListener
{
    private static final long serialVersionUID = 1255684034719724441L;

    private CustomerInfoExt selectedCustomer;
    /** The customer that was previously assigned to keep it on cancel. */
    private CustomerInfoExt originalCustomer;
    private DataLogicCustomers dlc;

    /** Creates new form JCustomerFinder */
    private JCustomerFinder(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
    }

    /** Creates new form JCustomerFinder */
    private JCustomerFinder(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
    }

    /**
     * Show the popup and return the selected customer.
     * @return The selected customer on OK, the passed customer on cancel.
     */
    public static CustomerInfoExt show(Component parent,
            DataLogicCustomers dlCustomers, CustomerInfoExt customer) {
        Window window = getWindow(parent);
        JCustomerFinder myMsg;
        if (window instanceof Frame) {
            myMsg = new JCustomerFinder((Frame) window, true);
        } else {
            myMsg = new JCustomerFinder((Dialog) window, true);
        }
        return myMsg.init(dlCustomers, customer);
    }

    /**
     * Show the popup and return the selected customer, without preselection.
     * @return The selected customer on OK, null on cancel.
     */
    public static CustomerInfoExt show(Component parent,
            DataLogicCustomers dlCustomers) {
        return show(parent, dlCustomers, null);
    }

    private CustomerInfoExt init(DataLogicCustomers dlCustomers, CustomerInfoExt customer) {
        this.dlc = dlCustomers;
        this.originalCustomer = customer;
        initComponents();

        jScrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));

        m_jtxtSearch.addEditorKeys(m_jKeys);

        jListCustomers.setCellRenderer(new CustomerRenderer());
        // Set automatic first search
        if (customer == null || customer.getName() == null
                || customer.getName().equals("")) {
            // Default filter: show top 10
            m_jtxtSearch.reset();
            automaticTop10ClientSearch();
        } else {
            // Filter by selected customer
            m_jtxtSearch.setText(customer.getName());
            executeSearch();
        }
        m_jtxtSearch.activate();
        // Set shortcuts
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
        //show();
        setVisible(true);
        // Post edit
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
        return this.selectedCustomer;
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                if (m_jtxtSearch.isActive() || this.searchBtn.hasFocus()) {
                    this.searchBtn.doClick(Shortcuts.CLICK_TIME);
                } else if (this.top10Btn.hasFocus()) {
                    this.top10Btn.doClick(Shortcuts.CLICK_TIME);
                } else {
                    this.onOk();
                }
                return true;
            case GENERAL_CANCEL:
                this.onCancel();
                return true;
            default: // Ignore
                return false;
        }
    }

    private void cleanSearch() {
        jListCustomers.setModel(new MyListData<CustomerInfoExt>(new ArrayList<CustomerInfoExt>()));
    }

    public void executeSearch() {
        // Read field values
        String search = this.m_jtxtSearch.getText();
        try {
            List<CustomerInfoExt> results = this.dlc.searchCustomers(search);
            jListCustomers.setModel(new MyListData<CustomerInfoExt>(results));
            if (jListCustomers.getModel().getSize() > 0) {
                jListCustomers.setSelectedIndex(0);
                jListCustomers.grabFocus();
            }
        } catch (BasicException e) {
            e.printStackTrace();
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("Label.LoadError"), e);
            msg.show(this);

        }
    }

    /** Automatic filtering of customers when choosing the form
     * to choose a client appears. Displays the top10
     * of the customer's list by number of tickets with their id
     */
    public void automaticTop10ClientSearch(){
        try {
            jListCustomers.setModel(new MyListData<CustomerInfoExt>(this.dlc.getTop10CustomerList()));
        } catch (BasicException e) {
            e.printStackTrace();
        }
        if (jListCustomers.getModel().getSize() > 0) {
            jListCustomers.setSelectedIndex(0);
            jListCustomers.grabFocus();
        }
    }

    private static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window) parent;
        } else {
            return getWindow(parent.getParent());
        }
    }

    private static class MyListData<T> extends javax.swing.AbstractListModel<T>
    {
        private static final long serialVersionUID = 2290182794229424994L;

        private java.util.List<T> m_data;

        public MyListData(java.util.List<T> data) {
            m_data = data;
        }

        public T getElementAt(int index) {
            return m_data.get(index);
        }

        public int getSize() {
            return m_data.size();
        }
    }

    public void onOk() {
        this.selectedCustomer = (CustomerInfoExt) jListCustomers.getSelectedValue();
        dispose();
    }

    public void onUnassign() {
        this.selectedCustomer = null;
        dispose();
    }

    public void onCancel() {
        this.selectedCustomer = this.originalCustomer;
        dispose();
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;
        m_jKeys = new JEditorKeys();
        JLabel searchLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Customer.Search.Prompt"));
        m_jtxtSearch = new JEditorString();

        JButton clearBtn = WidgetsBuilder.createButton(null,
                                                       AppLocal.getIntString("button.clean"),
                                                       WidgetsBuilder.SIZE_MEDIUM);
        this.searchBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("execute.png"),
                AppLocal.getIntString("button.executefilter"),
                WidgetsBuilder.SIZE_MEDIUM);
        this.top10Btn = WidgetsBuilder.createButton(null,
                AppLocal.getIntString("Button.Top10Customers"),
                WidgetsBuilder.SIZE_MEDIUM);

        jScrollPane1 = new javax.swing.JScrollPane();
        jListCustomers = new javax.swing.JList<CustomerInfoExt>();
        jcmdOK = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);
        jcmdCancel = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        unassignBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_generic.png"),
                AppLocal.getIntString("Button.Unassign"),
                WidgetsBuilder.SIZE_MEDIUM);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(AppLocal.getIntString("form.customertitle")); // NOI18N

        this.setLayout(new GridBagLayout());

        GridBagConstraints c;

        javax.swing.JPanel leftContainer = new javax.swing.JPanel();
        leftContainer.setLayout(new GridBagLayout());
        // Search form inputs
        javax.swing.JPanel searchContainer = new javax.swing.JPanel();
        searchContainer.setLayout(new GridBagLayout());

        int y = 0;
        int x = 0;
        c = new GridBagConstraints();
        c.gridx = x++ % 2; c.gridy = y;
        c.insets = new java.awt.Insets(btnSpacing, btnSpacing, 0, btnSpacing);
        searchContainer.add(searchLbl, c);
        c = new GridBagConstraints();
        c.gridx = x++ % 2; c.gridy = y++; c.weightx = 1;
        c.insets = new java.awt.Insets(btnSpacing, 0, 0, 0);
        c.fill = GridBagConstraints.HORIZONTAL;
        searchContainer.add(m_jtxtSearch, c);

        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 0; c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        leftContainer.add(searchContainer, c);

        // Search buttons
        y = 1;

        searchBtn.setFocusPainted(false);
        searchBtn.setFocusable(false);
        searchBtn.setRequestFocusEnabled(false);
        searchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBtnActionPerformed(evt);
            }
        });
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = y++;
        c.insets = new java.awt.Insets(btnSpacing, btnSpacing,
                btnSpacing, btnSpacing);
        leftContainer.add(searchBtn, c);

        JPanel searchButtonsContainer = new JPanel();

        clearBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBtnActionPerformed(evt);
            }
        });
        searchButtonsContainer.add(clearBtn);

        top10Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                automaticTop10ClientSearch();
            }
        });
        searchButtonsContainer.add(top10Btn);

        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = y++;
        c.insets = new java.awt.Insets(btnSpacing, btnSpacing,
                btnSpacing, btnSpacing);
        leftContainer.add(searchButtonsContainer, c);

        // Keypad input
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = y++;
        c.insets = new java.awt.Insets(btnSpacing, btnSpacing,
                btnSpacing, btnSpacing);
        leftContainer.add(m_jKeys, c);

        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 0; c.weightx = 1;
        c.anchor = GridBagConstraints.NORTH;
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(leftContainer, c);

        // Customer list
        jListCustomers.setFocusable(false);
        jListCustomers.setRequestFocusEnabled(false);
        jListCustomers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListCustomersMouseClicked(evt);
            }
        });
        jListCustomers.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListCustomersValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListCustomers);

        c = new GridBagConstraints();
        c.gridx = 1; c.gridy = 0;
        c.weightx = 3; c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new java.awt.Insets(btnSpacing, btnSpacing,
                btnSpacing, btnSpacing);
        this.add(jScrollPane1, c);

        // Ok/cancel buttons
        JPanel buttonsContainer = new JPanel();
        buttonsContainer.setLayout(new FlowLayout(FlowLayout.CENTER,
                marginInset, btnSpacing));

        jcmdOK.setEnabled(false);
        jcmdOK.setFocusPainted(false);
        jcmdOK.setFocusable(false);
        jcmdOK.setRequestFocusEnabled(false);
        jcmdOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onOk();
            }
        });
        buttonsContainer.add(jcmdOK);

        this.unassignBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onUnassign();
            }
        });
        if (this.originalCustomer != null) {
            buttonsContainer.add(this.unassignBtn);
        }

        jcmdCancel.setFocusPainted(false);
        jcmdCancel.setFocusable(false);
        jcmdCancel.setRequestFocusEnabled(false);
        jcmdCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCancel();
            }
        });
        buttonsContainer.add(jcmdCancel);

        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = 1; c.gridwidth = 2;
        c.anchor = GridBagConstraints.EAST;
        this.add(buttonsContainer, c);

        // Popup size
        java.awt.Window owner = this.getOwner();
        java.awt.Rectangle ownerSize = owner.getBounds();
        this.setMinimumSize(new java.awt.Dimension(
                WidgetsBuilder.pixelSize(7.0f),
                Math.min(ownerSize.height - btnSpacing * 10, WidgetsBuilder.pixelSize(5.0f))));
        this.pack();
        this.setLocationRelativeTo(getOwner());
    }

    private void searchBtnActionPerformed(java.awt.event.ActionEvent evt) {
        executeSearch();
    }

    private void jListCustomersValueChanged(javax.swing.event.ListSelectionEvent evt) {

        jcmdOK.setEnabled(jListCustomers.getSelectedValue() != null);

    }

    private void jListCustomersMouseClicked(java.awt.event.MouseEvent evt) {

        if (evt.getClickCount() == 2) {
            selectedCustomer = (CustomerInfoExt) jListCustomers.getSelectedValue();
            dispose();
        }

    }

    private void clearBtnActionPerformed(java.awt.event.ActionEvent evt) {
        m_jtxtSearch.reset();
        m_jtxtSearch.activate();
        cleanSearch();
    }

    private javax.swing.JList<CustomerInfoExt> jListCustomers;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jcmdCancel;
    private javax.swing.JButton jcmdOK;
    private javax.swing.JButton unassignBtn;
    private javax.swing.JButton searchBtn;
    private javax.swing.JButton top10Btn;
    private JEditorKeys m_jKeys;
    private JEditorString m_jtxtSearch;
}
