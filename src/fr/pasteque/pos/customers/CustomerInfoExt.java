//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import fr.pasteque.format.DateUtils;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.RoundUtils;
import java.util.Date;
import org.json.JSONObject;

/**
 * Full customer account.
 * @author adrianromero
 */
public class CustomerInfoExt extends CustomerInfo
{
    private static final long serialVersionUID = 8994206194953433731L;

    protected String taxcustomerid;
    protected Integer discountProfileId;
    protected Integer tariffAreaId;
    protected String notes;
    protected boolean visible;
    protected String card;
    protected Double maxdebt;
    protected double balance;
    protected String firstname;
    protected String lastname;
    protected String email;
    protected String phone;
    protected String phone2;
    protected String fax;
    protected String address;
    protected String address2;
    protected String postal;
    protected String city;
    protected String region;
    protected String country;
    protected Date expireDate;
    
    /** Creates a new instance of UserInfoBasic */
    public CustomerInfoExt(String id) {
        super(id);
    } 

    public CustomerInfoExt(JSONObject o) {
        super(null);
        this.id = String.valueOf(o.getInt("id"));
        this.taxid = this.id;
        this.name = o.getString("dispName");
        this.searchkey = this.id + "-" + this.name;
        if (!o.isNull("tax")) {
            this.taxcustomerid = String.valueOf(o.getInt("tax"));
        }
        if (!o.isNull("discountProfile")) {
            this.discountProfileId = o.getInt("discountProfile");
        }
        if (!o.isNull("tariffArea")) {
            this.tariffAreaId = o.getInt("tariffArea");
        }
        if (!o.isNull("note")) {
            this.notes = o.getString("note");
        }
        this.visible = o.getBoolean("visible");
        if (!o.isNull("card")) {
            this.card = o.getString("card");
        }
        if (!o.isNull("maxDebt")) {
            this.maxdebt = o.getDouble("maxDebt");
        }
        if (!o.isNull("balance")) {
            this.balance = o.getDouble("balance");
        }
        if (!o.isNull("firstName")) {
            this.firstname = o.getString("firstName");
        }
        if (!o.isNull("lastName")) {
            this.lastname = o.getString("lastName");
        }
        if (!o.isNull("email")) {
            this.email = o.getString("email");
        }
        if (!o.isNull("phone1")) {
            this.phone = o.getString("phone1");
        }
        if (!o.isNull("phone2")) {
            this.phone2 = o.getString("phone2");
        }
        if (!o.isNull("fax")) {
            this.fax = o.getString("fax");
        }
        if (!o.isNull("addr1")) {
            this.address = o.getString("addr1");
        }
        if (!o.isNull("addr2")) {
            this.address2 = o.getString("addr2");
        }
        if (!o.isNull("zipCode")) {
            this.postal = o.getString("zipCode");
        }
        if (!o.isNull("city")) {
            this.city = o.getString("city");
        }
        if (!o.isNull("region")) {
            this.region = o.getString("region");
        }
        if (!o.isNull("country")) {
            this.country = o.getString("country");
        }
        this.expireDate = DateUtils.readDate(o, "expireDate");
    }

    private void putString(JSONObject o, String key, String value) {
        if (value != null) {
            o.put(key, value);
        } else {
            o.put(key, "");
        }
    }

    public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        if (this.id != null) {
            o.put("id", Integer.parseInt(this.id));
        }
        this.putString(o, "dispName", this.name);
        if (this.taxcustomerid != null) {
            o.put("tax", Integer.parseInt(this.taxcustomerid));
        } else {
            o.put("tax", JSONObject.NULL);
        }
        if (this.discountProfileId != null) {
            o.put("discountProfile", this.discountProfileId);
        } else {
            o.put("discountProfile", JSONObject.NULL);
        }
        if (this.tariffAreaId != null) {
            o.put("tariffArea", this.tariffAreaId);
        } else {
            o.put("tariffArea", JSONObject.NULL);
        }
        this.putString(o, "note", this.notes);
        this.putString(o, "card", this.card);
        o.put("maxDebt", this.maxdebt);
        o.put("balance", this.balance);
        this.putString(o, "firstName", this.firstname);
        this.putString(o, "lastName", this.lastname);
        this.putString(o, "email", this.email);
        this.putString(o, "phone1", this.phone);
        this.putString(o, "phone2", this.phone2);
        this.putString(o, "fax", this.fax);
        this.putString(o, "addr1", this.address);
        this.putString(o, "addr2", this.address2);
        this.putString(o, "zipCode", this.postal);
        this.putString(o, "city", this.city);
        this.putString(o, "region", this.region);
        this.putString(o, "country", this.country);
        if (this.expireDate != null) {
            o.put("expireDate", DateUtils.toSecTimestamp(this.expireDate));
        } else {
            o.put("expireDate", JSONObject.NULL);
        }
        return o;
    }

    /** @deprecated */
    public String getTaxCustCategoryID() {
        return taxcustomerid;
    }
    public String getTaxId() {
        return taxcustomerid;
    }
    
    public void setTaxCustomerID(String taxcustomerid) {
        this.taxcustomerid = taxcustomerid;
    }

    public Integer getDiscountProfileId() {
        return this.discountProfileId;
    }
    public void setDiscountProfileId(Integer id) {
        this.discountProfileId = id;
    }

    public Integer getTariffAreaId() {
        return this.tariffAreaId;
    }
    public void setTariffAreaId(Integer id) {
        this.tariffAreaId = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public Double getMaxdebt() {
        return maxdebt;
    }
    
    public String printMaxDebt() {       
        return Formats.CURRENCY.formatValue(RoundUtils.getValue(getMaxdebt()));
    }
    
    public void setMaxdebt(Double maxdebt) {
        this.maxdebt = maxdebt;
    }

    /** @deprecated */
    public Date getCurdate() {
        return null;
    }
    /** @deprecated */
    public void setCurdate(Date curdate) { }
    /**
     * @deprecated
     * Get Current debt amount.
     * @return Positive number, when balance is negative, 0.0 otherwise.
     * @see #getBalance()
     */
    public Double getCurdebt() {
        if (this.balance < 0.0) {
            return -1 * this.balance;
        } else {
            return null;
        }
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public String printCurDebt() {
        return Formats.CURRENCY.formatValue(RoundUtils.getValue(getCurdebt()));
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public void setCurdebt(Double curdebt) {
        this.balance = -curdebt;
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public void updateCurDebt(Double amount, Date d) {
        this.updateBalance(-1 * amount);
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public double getPrepaid() {
        if (this.balance > 0.0) {
            return this.balance;
        } else {
            return 0.0;
        }
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public void setPrepaid(double prepaid) {
        this.balance = prepaid;
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public void updatePrepaid(double amount) {
        this.updateBalance(amount);
    }
    /**
     * @deprecated
     * Debt and prepaid were merged into balance.
     */
    public String printPrepaid() {
        return Formats.CURRENCY.formatValue(RoundUtils.getValue(getPrepaid()));
    }

    public double getBalance() {
        return this.balance;
    }

    /**
     * Check if the customer's balance is positive enough
     * to pay the requested amount.
     * @param amount The amount to pay.
     * @return True when the customer's balance is higher than the amount.
     * False otherwise.
     */
    public boolean hasEnoughPrepaid(double amount) {
        return Price.add(this.balance, -amount) > -0.005;
    }

    /**
     * Check if the customer's balance can be reduced by the given amount
     * without exceeding their maximum allowed debt.
     * @param amount The amount of debt to contract.
     * @return True when the customer's balance will not go below their maximum
     * debt allowed. False otherwise.
     */
    public boolean canContractDebt(double amount) {
        if (this.maxdebt == null) {
            return false;
        }
        double newBalance = Price.add(this.balance, -amount);
        return Price.add(newBalance, this.maxdebt.doubleValue()) > -0.005;
    }

    /**
     * Update the balance. Use positive amount to fill the account
     * and negative value to use it.
     */
    public void updateBalance(double amount) {
        this.balance = Price.add(this.balance, amount);
    }

    public String printBalance() {
        return Formats.CURRENCY.formatValue(RoundUtils.getValue(getBalance()));
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getExpireDate() {
        return this.expireDate;
    }

    public void setExpireDate(Date date) {
        this.expireDate = date;
    }

    /**
     * Check if this customer account can expire and has expired now.
     * Shortcut for isExpired(new Date()).
     * @see #isExpired(Date)
     */
    public boolean isExpired() {
        return this.isExpired(new Date());
    }

    /**
     * Check if this customer account can expire and has expired at
     * a given date.
     * @param d The date to compare.
     * @return True if expireDate is set and is before d. False otherwise.
     */
    public boolean isExpired(Date d) {
        return (this.expireDate != null && d != null
                && this.expireDate.before(d));
    }
}
