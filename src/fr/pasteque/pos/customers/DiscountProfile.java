//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import java.io.Serializable;
import org.json.JSONObject;

public class DiscountProfile  implements Serializable
{
    static final long serialVersionUID = 4719301298475208037L;

    private Integer id;
    private String name;
    private double rate;

    /**
     * Create profile to use only as a discount rate holder.
     */
    public DiscountProfile(double rate) {
        this.rate = rate;
    }

    /**
     * Create a profile localy, use {@link #DiscountProfile(JSONObject)} for regular use.
     */
    public DiscountProfile(int id, double rate) {
        this.id = id;
        this.rate = rate;
    }

    public DiscountProfile(JSONObject o) {
        if (!o.isNull("id")) {
            this.id = o.getInt("id");
        }
        this.name = o.getString("label");
        this.rate = o.getDouble("rate");
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getRate() {
        return this.rate;
    }
}
