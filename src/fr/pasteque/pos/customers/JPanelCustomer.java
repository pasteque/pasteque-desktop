//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import fr.pasteque.basic.BasicException;
import fr.pasteque.beans.JCalendarDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.gui.JConfirmDialog;
import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.user.DirtyManager;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.caching.CustomersCache;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppOptions;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.BeanFactoryApp;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.JPanelView;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import org.json.JSONException;
import org.json.JSONObject;

/** Form to edit customer's info or register a new one. */
public class JPanelCustomer extends JPanel implements JPanelView, BeanFactoryApp
{
    private static final long serialVersionUID = 8790019621724689820L;

    /** String code for first name field label */
    public static final String LBL_FIRSTNAME = "Label.Customer.FirstName";
    /** String code for last name field label */
    public static final String LBL_LASTNAME = "Label.Customer.LastName";
    /** String code for email field label */
    public static final String LBL_EMAIL = "Label.Customer.Email";
    /** String code for phone field label */
    public static final String LBL_PHONE = "Label.Customer.Phone";
    /** String code for phone2 field label */
    public static final String LBL_PHONE2 = "Label.Customer.Phone2";
    /** String code for fax field label */
    public static final String LBL_FAX = "Label.Customer.Fax";
    /** String code for address field label */
    public static final String LBL_ADDR = "Label.Customer.Addr";
    /** String code for address2 field label */
    public static final String LBL_ADDR2 = "Label.Customer.Addr2";
    /** String code for zipcode field label */
    public static final String LBL_ZIPCODE = "Label.Customer.ZipCode";
    /** String code for city field label */
    public static final String LBL_CITY = "Label.Customer.City";
    /** String code for region field label */
    public static final String LBL_REGION = "Label.Customer.Region";
    /** String code for country field label */
    public static final String LBL_COUNTRY = "Label.Customer.Country";

    private static Logger logger = Logger.getLogger("fr.pasteque.pos.customers.JPanelCustomer");

    protected DirtyManager dirty;
    protected int y;
    protected int btnSpacing;
    protected JPanel currentContainer;
    /**
     * The map holding the custom names for contact informations
     * to add flexibility in it until arbitrary fields can be added.
     */
    protected Map<String, String> fieldsReplacements;

    private DataLogicCustomers dlCust;
    private DataLogicSales dlSales;

    /** The customer being edited. Null if new. */
    protected CustomerInfoExt currentCustomer;

    public JPanelCustomer() {
        this.dlCust = new DataLogicCustomers();
        this.dlSales = new DataLogicSales();
        this.dirty = new DirtyManager();
        this.setLayout(new GridBagLayout());
        this.fieldsReplacements = new HashMap<String, String>();
        try {
            String opt = AppOptions.loadedInstance.getOption(AppOptions.OPT_CUSTOMER_FIELDS);
            if (opt != null) {
                this.buildFieldMapFromOption(opt);
            }
        } catch (JSONException e) {
            logger.log(Level.WARNING, String.format("Unable to read option %s",
                    AppOptions.OPT_CUSTOMER_FIELDS), e);
        }
        this.initComponents();
    }

    protected void buildFieldMapFromOption(String optionContent)
    throws JSONException {
        JSONObject o = new JSONObject(optionContent);
        for (String origin : JSONObject.getNames(o)) {
            this.fieldsReplacements.put(origin, o.getString(origin));
        }
    }

    @Override // From BeanFactoryApp
    public void init(AppView app) {
        this.switchToCustomer(null);
    }

    @Override // From BeanFactoryApp/BeanFactory
    public Object getBean() {
        return this;
    }

    @Override // From JPanelView
    public JComponent getComponent() {
        return this;
    }

    @Override // From JPanelView
    public String getTitle() {
        return AppLocal.getIntString("Menu.Customers");
    }

    @Override // From JPanelView
    public boolean requiresOpenedCash() {
        return false;
    }

    @Override // From JPanelView
    public void activate() throws BasicException {
        this.cboDiscountProfile.removeAllItems();
        this.cboDiscountProfile.addItem("", AppLocal.tr("Label.Customer.DiscountProfile.None"));
        try {
            for (DiscountProfile dp : this.dlCust.getDiscountProfiles()) {
                this.cboDiscountProfile.addItem(String.valueOf(dp.getId()), dp.getName());
            }
        } catch (BasicException e) {
            e.printStackTrace();
            logger.log(Level.WARNING, "Could not load discount profiles", e);
        }
        this.cboTariffArea.removeAllItems();
        this.cboTariffArea.addItem("", AppLocal.tr("Label.Customer.TariffArea.None"));
        try {
            for (TariffInfo ta : this.dlSales.getTariffAreaList()) {
                this.cboTariffArea.addItem(String.valueOf(ta.getID()), ta.getName());
            }
        } catch (BasicException e) {
            e.printStackTrace();
            logger.log(Level.WARNING, "Could not load tariff areas", e);
        }
        this.cboTax.removeAllItems();
        this.cboTax.addItem("", AppLocal.tr("Label.Customer.Tax.Unchanged"));
        try {
            for (TaxInfo tax : this.dlSales.getTaxList()) {
                this.cboTax.addItem(tax.getId(), tax.getName());
            }
        } catch (BasicException e) {
            e.printStackTrace();
            logger.log(Level.WARNING, "Could not load taxes", e);
        }
        this.dirty.reset();
    }

    /**
     * Ask to save or reset the form and save or reset.
     * @param resetFields Wether the fields should be reset or not
     * when resetting.
     * @return Boolean.TRUE when saved, Boolean.FALSE when discarded,
     * null when canceled.
     */
    private Boolean showDirtySaveConfirm(boolean resetFields) {
        Boolean save = JConfirmDialog.showAcceptAction(this,
                AppLocal.tr("title.editor"),
                AppLocal.tr("message.wannasave"));
        if (Boolean.TRUE.equals(save)) {
            if (this.save()) {
                return Boolean.TRUE;
            } else {
                // Don't quit if an error occurs
                return null;
            }
        } else if (save == null) {
            return null;
        } else {
            // Discard changes before quitting
            if (resetFields) {
                this.resetFields();
            }
            return Boolean.FALSE;
        }
    }

    @Override // From JPanelView
    public boolean deactivate() {
        if (this.dirty.isDirty()) {
            return (this.showDirtySaveConfirm(true) != null);
        }
        return true;
    }

    /** Reset the values of the fields to the one before edition
     * or empty for a new customer. */
    protected void resetFields() {
        if (this.currentCustomer == null) {
            this.titleCustomer.setText(AppLocal.getIntString("Button.btnCustomerNew.toolTip"));
            this.txtDispName.setText("");
            this.txtCard.setText("");
            this.txtExpire.setText("");
            this.txtNotes.setText("");
            this.txtPrepayAmount.setText("");
            this.txtMaxDebt.setText("0");
            this.cboDiscountProfile.setSelectedValue("");
            this.cboTariffArea.setSelectedValue("");
            this.cboTax.setSelectedValue("");
            this.txtFirstName.setText("");
            this.txtLastName.setText("");
            this.txtEmail.setText("");
            this.txtPhone.setText("");
            this.txtPhone2.setText("");
            this.txtFax.setText("");
            this.txtAddr.setText("");
            this.txtAddr2.setText("");
            this.txtZipCode.setText("");
            this.txtCity.setText("");
            this.txtRegion.setText("");
            this.txtCountry.setText("");
        } else {
            this.titleCustomer.setText(this.currentCustomer.getName());
            this.txtDispName.setText(this.currentCustomer.getName());
            this.txtCard.setText(this.currentCustomer.getCard());
            if (this.currentCustomer.getExpireDate() != null) {
                this.txtExpire.setText(Formats.TIMESTAMP.formatValue(this.currentCustomer.getExpireDate()));
            } else {
                this.txtExpire.setText("");
            }
            this.txtNotes.setText(this.currentCustomer.getNotes());
            this.txtPrepayAmount.setText(String.valueOf(this.currentCustomer.getBalance()));
            this.txtMaxDebt.setText(String.valueOf(this.currentCustomer.getMaxdebt()));
            Integer dpId = this.currentCustomer.getDiscountProfileId();
            if (dpId == null) {
                this.cboDiscountProfile.setSelectedValue("");
            } else {
                this.cboDiscountProfile.setSelectedValue(String.valueOf(dpId));
            }
            Integer taId = this.currentCustomer.getTariffAreaId();
            if (taId == null) {
                this.cboTariffArea.setSelectedValue("");
            } else {
                this.cboTariffArea.setSelectedValue(String.valueOf(taId));
            }
            String taxId = this.currentCustomer.getTaxId();
            if (taxId == null) {
                this.cboTax.setSelectedValue("");
            } else {
                this.cboTax.setSelectedValue(taxId);
            }
            this.txtFirstName.setText(this.currentCustomer.getFirstname());
            this.txtLastName.setText(this.currentCustomer.getLastname());
            this.txtEmail.setText(this.currentCustomer.getEmail());
            this.txtPhone.setText(this.currentCustomer.getPhone());
            this.txtPhone2.setText(this.currentCustomer.getPhone2());
            this.txtFax.setText(this.currentCustomer.getFax());
            this.txtAddr.setText(this.currentCustomer.getAddress());
            this.txtAddr2.setText(this.currentCustomer.getAddress2());
            this.txtZipCode.setText(this.currentCustomer.getPostal());
            this.txtCity.setText(this.currentCustomer.getCity());
            this.txtRegion.setText(this.currentCustomer.getRegion());
            this.txtCountry.setText(this.currentCustomer.getCountry());
        }
        this.dirty.reset();
    }

    /** Reset the form to an other or new customer.
     * @param cust The selected customer, null for a new one. */
    protected void switchToCustomer(CustomerInfoExt cust) {
        this.currentCustomer = cust;
        this.resetFields();
    }

    protected void openCustomerSelect() {
        CustomerInfoExt customer = JCustomerFinder.show(this.getComponent(),
                this.dlCust);
        if (customer == null) {
            // Canceled
            return;
        }
        if (this.dirty.isDirty()) {
            // Ask for confirmation before switching
            Boolean save = this.showDirtySaveConfirm(false);
            if (save == null) {
                return; // cancel
            }
        }
        this.switchToCustomer(customer);
    }

    protected void newCustomer() {
        if (this.dirty.isDirty()) {
            // Ask for confirmation before switching
            Boolean save = this.showDirtySaveConfirm(false);
            if (save == null) {
                return; // cancel
            }
        }
        this.switchToCustomer(null);
    }

    protected boolean save() {
        CustomerInfoExt cust;
        if (this.currentCustomer == null) {
            cust = new CustomerInfoExt((String) null);
        } else {
            cust = new CustomerInfoExt(this.currentCustomer.getId());
        }
        // Read form values
        cust.setName(this.txtDispName.getText());
        cust.setCard(this.txtCard.getText());
        try {
            Date expire = (Date) Formats.TIMESTAMP.parseValue(this.txtExpire.getText());
            cust.setExpireDate(expire);
        } catch (BasicException e) {
            JMessageDialog.showMessage(this,
                    new MessageInf(MessageInf.SGN_DANGER,
                            AppLocal.getIntString("Message.InvalidDate", AppLocal.getIntString("label.cardexpdate"))));
            return false;
        }
        cust.setNotes(this.txtNotes.getText());
        cust.setMaxdebt(Double.parseDouble(this.txtMaxDebt.getText()));
        String dp = this.cboDiscountProfile.getSelectedValue();
        if ("".equals(dp)) {
            cust.setDiscountProfileId(null);
        } else {
            cust.setDiscountProfileId(Integer.valueOf(dp));
        }
        String ta = this.cboTariffArea.getSelectedValue();
        if ("".equals(ta)) {
            cust.setTariffAreaId(null);
        } else {
            cust.setTariffAreaId(Integer.valueOf(ta));
        }
        String tax = this.cboTax.getSelectedValue();
        if ("".equals(tax)) {
            cust.setTaxCustomerID(null);
        } else {
            cust.setTaxCustomerID(tax);
        }
        cust.setFirstname(this.txtFirstName.getText());
        cust.setLastname(this.txtLastName.getText());
        cust.setEmail(this.txtEmail.getText());
        cust.setPhone(this.txtPhone.getText());
        cust.setPhone2(this.txtPhone2.getText());
        cust.setFax(this.txtFax.getText());
        cust.setAddress(this.txtAddr.getText());
        cust.setAddress2(this.txtAddr2.getText());
        cust.setPostal(this.txtZipCode.getText());
        cust.setCity(this.txtCity.getText());
        cust.setRegion(this.txtRegion.getText());
        cust.setCountry(this.txtCountry.getText());
        // Save
        try {
            CustomerInfoExt newCust = this.dlCust.saveCustomer(cust);
            /* Keep the local balance when editing
             * because it shouldn't be modified that way and will be updated
             * when reloading data as usual. */
            if (this.currentCustomer != null) {
                newCust.updateBalance(this.currentCustomer.getBalance() - newCust.getBalance());
            }
            // Select the newly created customer (with id) and refresh
            if (newCust == null) {
                JMessageDialog.showMessage(this,
                    new MessageInf(MessageInf.SGN_DANGER,
                            "Unable to save customer", "Please check the form for invalid/missing values"));
                logger.log(Level.WARNING, "Unable to save customer: invalid response");
                return false;
            } else {
                // Update in local cache
                try {
                    if (this.currentCustomer == null) {
                        CustomersCache.insertCustomer(newCust);
                    } else {
                        CustomersCache.refreshCustomer(newCust);
                    }
                } catch (BasicException e) {
                    JMessageDialog.showMessage(this,
                        new MessageInf(MessageInf.SGN_DANGER,
                                "Unable to cache this customer. Please restart Pasteque to take your modifications in account.", e));
                    logger.log(Level.SEVERE, "Unable to save customer locally.", e);
                }
                this.switchToCustomer(newCust);
                JMessageDialog.showMessage(this,
                        new MessageInf(MessageInf.SGN_SUCCESS,
                                AppLocal.tr("Message.CustomerSaved")));
                return true;
            }
        } catch (BasicException e) {
            JMessageDialog.showMessage(this,
                    new MessageInf(MessageInf.SGN_DANGER,
                            "Unable to save customer",
                            e.getMessage()));
                logger.log(Level.WARNING, "Unable to save customer", e);
            return false;
        }
    }

    /**
     * Get the actual contact field name. Either from custom replacement
     * or the localized default name. It is called from the add widget
     * methods.
     * @param fieldCode The i18n entry.
     * @return The entry in custom field or the localized default string.
     */
    protected String getFieldName(String fieldCode) {
        String customName = this.fieldsReplacements.get(fieldCode);
        if (customName == null || customName.isEmpty()) {
            return AppLocal.tr(fieldCode);
        } else {
            return customName;
        }
    }

    /**
     * Create a container and select it for other widgets methods.
     * @param titleCode The container title, shown in the border.
     * @return The container. It is also set in this.currentContainer
     * for the other widgets methods.
     */
    protected JPanel addOptionsContainer(String titleCode) {
        JPanel optContainer = new JPanel();
        optContainer.setBorder(javax.swing.BorderFactory.createTitledBorder(AppLocal.getIntString(titleCode)));
        optContainer.setLayout(new GridBagLayout());
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 1;
        this.container.add(optContainer, cstr);
        this.currentContainer = optContainer;
        this.y = 0;
        return optContainer;
    }

    /**
     * Create a text field with a label and add it into the current container.
     * @param labelCode The text code for the label of the field.
     * @return The created text field which was inserted into the current
     * container.
     */
    protected javax.swing.JTextField addTextField(String labelCode) {
        javax.swing.JTextField field = WidgetsBuilder.createTextField();
        field.getDocument().addDocumentListener(this.dirty);
        JLabel label = WidgetsBuilder.createLabel(this.getFieldName(labelCode));
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridy = this.y;
        cstr.gridx = 0;
        cstr.weightx = 0.25;
        cstr.anchor = GridBagConstraints.LINE_START;
        this.cstrInsets(cstr);
        this.currentContainer.add(label, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = this.y;
        cstr.gridx = 1;
        cstr.gridwidth = 2;
        cstr.weightx = 0.75;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        this.cstrInsets(cstr);
        this.currentContainer.add(field, cstr);
        this.y++;
        return field;
    }

    /**
     * Create a date field with a label and add it into the current container.
     * @param labelCode The text code for the label of the field.
     * @return The created date field which was inserted into the current
     * container.
     */
    protected javax.swing.JTextField addDateField(String labelCode) {
        final javax.swing.JTextField field = WidgetsBuilder.createTextField();
        field.getDocument().addDocumentListener(this.dirty);
        JLabel label = WidgetsBuilder.createLabel(this.getFieldName(labelCode));
        JButton btnDatePick = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("calendar.png"));
        btnDatePick.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Date date;
                try {
                    date = (Date) Formats.TIMESTAMP.parseValue(field.getText());
                } catch (BasicException e) {
                    date = null;
                }
                date = JCalendarDialog.showCalendarTimeHours(JPanelCustomer.this, date);
                if (date != null) {
                    field.setText(Formats.TIMESTAMP.formatValue(date));
                }
            }
        });
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridy = this.y;
        cstr.gridx = 0;
        cstr.weightx = 0.25;
        cstr.anchor = GridBagConstraints.LINE_START;
        this.cstrInsets(cstr);
        this.currentContainer.add(label, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = this.y;
        cstr.gridx = 1;
        cstr.weightx = 0.75;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        this.cstrInsets(cstr);
        this.currentContainer.add(field, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = this.y;
        cstr.gridx = 2;
        this.cstrInsets(cstr);
        this.currentContainer.add(btnDatePick, cstr);
        this.y++;
        return field;
    }

    /**
     * Create a combo box field with a label and add it into the current container.
     * @param labelCode The text code for the label of the field.
     * @return The created text field which was inserted into the current
     * container.
     */
    protected JComboBoxVal<String> addComboBox(String labelCode) {
        JComboBoxVal<String> field = WidgetsBuilder.createComboBoxVal();
        field.setEditable(false);
        field.addActionListener(this.dirty);
        JLabel label = WidgetsBuilder.createLabel(this.getFieldName(labelCode));
        if (this.currentContainer != null) {
            GridBagConstraints cstr = new GridBagConstraints();
            cstr.gridy = this.y;
            cstr.gridx = 0;
            cstr.weightx = 0.25;
            cstr.anchor = GridBagConstraints.LINE_START;
            this.cstrInsets(cstr);
            this.currentContainer.add(label, cstr);
            cstr = new GridBagConstraints();
            cstr.gridy = this.y;
            cstr.gridx = 1;
            cstr.gridwidth = 2;
            cstr.weightx = 0.75;
            this.cstrInsets(cstr);
            cstr.fill = GridBagConstraints.HORIZONTAL;
            this.currentContainer.add(field, cstr);
            this.y++;
        }
        return field;
    }

    /** Set insets to a GridBagConstraints according to its gridx and gridy. */
    protected void cstrInsets(GridBagConstraints cstr) {
        int top = (cstr.gridy != 0) ? btnSpacing : 0;
        int left = (cstr.gridx == 2) ? btnSpacing : 0;
        cstr.insets = new Insets(top, left, 0, 0);
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        this.btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        this.setLayout(new GridBagLayout());

        // Top bar buttons: title, new, search
        this.titleCustomer = WidgetsBuilder.createImportantLabel("Cookies are important");
        this.titleCustomer.setBackground(java.awt.Color.white);
        this.titleCustomer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        this.titleCustomer.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        this.titleCustomer.setOpaque(true);
        this.titleCustomer.setPreferredSize(new java.awt.Dimension(320, 25));
        this.btnNew = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_new.png"),
                AppLocal.getIntString("Button.btnCustomerNew.toolTip"),
                WidgetsBuilder.SIZE_MEDIUM);
        this.btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        this.btnSearch = WidgetsBuilder.createButton(ImageLoader.readImageIcon("search.png"),
                AppLocal.getIntString("Button.btnCustomer.toolTip"),
                WidgetsBuilder.SIZE_MEDIUM);
        this.btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        JPanel topBar = new JPanel();
        topBar.setLayout(new GridBagLayout());
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        topBar.add(this.titleCustomer);

        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        topBar.add(this.btnSearch, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
        topBar.add(this.btnNew, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.anchor = GridBagConstraints.LINE_START;
        this.add(topBar, cstr);

        // Center: form
        this.container = new javax.swing.JPanel();
        this.container.setLayout(new GridBagLayout());
        this.addOptionsContainer("Label.Customer.Group.Display");
        this.txtDispName = this.addTextField("Label.Customer.DispName");
        this.txtCard = this.addTextField("Label.Customer.Card");
        // image field not available
        // visible field not available
        this.txtNotes = this.addTextField("Label.Customer.Notes");
        this.txtExpire = this.addDateField("Label.Customer.ExpireDate");

        // Customer's balance
        this.addOptionsContainer("Label.Customer.Group.Prepay");
        this.txtPrepayAmount = this.addTextField("Label.Customer.PrepayAmount");
        this.txtPrepayAmount.setEditable(false);
        this.txtMaxDebt = this.addTextField("Label.Customer.MaxDebt");

        // Pricing
        this.addOptionsContainer("Label.Customer.Group.Pricing");
        this.cboDiscountProfile = this.addComboBox("Label.Customer.DiscountProfile");
        this.cboTariffArea = this.addComboBox("Label.Customer.TariffArea");
        this.cboTax = this.addComboBox("Label.Customer.Tax");

        // Contact info
        this.addOptionsContainer("Label.Customer.Group.Contact");
        this.txtFirstName = this.addTextField(LBL_FIRSTNAME);
        this.txtLastName = this.addTextField(LBL_LASTNAME);
        this.txtEmail = this.addTextField(LBL_EMAIL);
        this.txtPhone = this.addTextField(LBL_PHONE);
        this.txtPhone2 = this.addTextField(LBL_PHONE2);
        this.txtFax = this.addTextField(LBL_FAX);
        this.txtAddr = this.addTextField(LBL_ADDR);
        this.txtAddr2 = this.addTextField(LBL_ADDR2);
        this.txtZipCode = this.addTextField(LBL_ZIPCODE);
        this.txtCity = this.addTextField(LBL_CITY);
        this.txtRegion = this.addTextField(LBL_REGION);
        this.txtCountry = this.addTextField(LBL_COUNTRY);

        javax.swing.JScrollPane scroll = new javax.swing.JScrollPane();
        scroll.setViewportView(this.container);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.weightx = 1;
        cstr.weighty = 1;
        this.add(scroll, cstr);

        // Bottom bar: save/cancel
        this.btnSave = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.Save"),
                WidgetsBuilder.SIZE_MEDIUM);
        this.btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        this.btnCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        this.btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        JPanel bottomBar = new JPanel();
        bottomBar.setLayout(new GridBagLayout());
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
        bottomBar.add(this.btnSave, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnSpacing, 0, btnSpacing, btnSpacing);
        bottomBar.add(this.btnCancel, cstr);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 2;
        cstr.anchor = GridBagConstraints.LINE_END;
        this.add(bottomBar, cstr);
    }

    private void btnNewActionPerformed(ActionEvent evt) {
        this.newCustomer();
    }

    private void btnSearchActionPerformed(ActionEvent evt) {
        this.openCustomerSelect();
    }

    private void btnSaveActionPerformed(ActionEvent evt) {
        this.save();
    }

    private void btnCancelActionPerformed(ActionEvent evt) {
        this.resetFields();
    }

    private javax.swing.JPanel container;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel titleCustomer;
    private javax.swing.JTextField txtCard;
    private javax.swing.JTextField txtDispName;
    private javax.swing.JTextField txtExpire;
    private javax.swing.JTextField txtAddr;
    private javax.swing.JTextField txtAddr2;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtCountry;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFax;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtPrepayAmount;
    private javax.swing.JTextField txtMaxDebt;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPhone2;
    private javax.swing.JTextField txtRegion;
    private javax.swing.JTextField txtZipCode;
    private javax.swing.JTextField txtNotes;
    private JComboBoxVal<String> cboDiscountProfile;
    private JComboBoxVal<String> cboTariffArea;
    private JComboBoxVal<String> cboTax;
}
