//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.datasource;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DiscountProfile;
import java.util.List;

/**
 * Interface to get product and price related informations.
 */
public interface CustomerDataSource
{
    public CustomerInfoExt getCustomer(String id) throws BasicException;
    public CustomerInfoExt getCustomerByCard(String card) throws BasicException;
    public List<CustomerInfoExt> getTop10CustomerList() throws BasicException;
    /**
     * @param search Search in customer's card and display name.
     * @return The list of customers matching the criterias.
     */
    public List<CustomerInfoExt> searchCustomers(String search) throws BasicException;
    public DiscountProfile getDiscountProfile(int id) throws BasicException;
    public List<DiscountProfile> getDiscountProfiles() throws BasicException;
}
