/**
 * Interfaces for classes that can load and bind records from their id.
 * Mostly used for separating real use from testing classes that load
 * predefined data sets.
 */
package fr.pasteque.pos.datasource;
