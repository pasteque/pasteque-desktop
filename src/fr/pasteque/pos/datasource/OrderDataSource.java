//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.datasource;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.sales.SharedTicketInfo;
import java.util.List;

/**
 * Interface for getting and saving orders.
 */
public interface OrderDataSource
{
    /** Load an order from the source. */
    public SharedTicketInfo getOrder(String id) throws BasicException;
    /** Get the list of available orders. */
    public List<SharedTicketInfo> getOrderList() throws BasicException;
    /**
     * Create or update a shared ticket in local cache. Id must always be set.
     * For list mode it is generated with UUID.randomUUID().toString().
     * In restaurant mode it is the place id.
     */
    public void saveOrder(SharedTicketInfo order) throws BasicException;
    /**
     * Delete an order from the source. It can be used either because the order
     * is canceled, or it was transformed to a ticket.
     */
    public void deleteOrder(final String id) throws BasicException;
}
