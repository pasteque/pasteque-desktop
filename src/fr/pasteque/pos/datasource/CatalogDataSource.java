//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.datasource;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.inventory.TaxCategoryInfo;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TariffAreaPrice;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import java.util.List;

/**
 * Interface to get product and price related informations.
 */
public interface CatalogDataSource
{
    public ProductInfoExt getProductInfo(String id) throws BasicException;
    public ProductInfoExt getProductInfoByCode(String sCode) throws BasicException;
    public List<ProductInfoExt> searchProducts(String label, String reference) throws BasicException;
    public List<CategoryInfo> getRootCategories() throws BasicException;
    public CategoryInfo getCategory(String id) throws BasicException;
    public List<CategoryInfo> getSubcategories(String category) throws BasicException;
    public List<CategoryInfo> getCategories() throws BasicException;
    public List<SubgroupInfo> getSubgroups(String composition) throws BasicException;
    public List<ProductInfoExt> getSubgroupCatalog(Integer subgroup) throws BasicException;
    public List<ProductInfoExt> getProductCatalog(String category) throws BasicException;
    public List<ProductInfoExt> getProductComments(String id) throws BasicException;
    public List<TaxInfo> getTaxList() throws BasicException;
    public TaxInfo getTax(String taxId) throws BasicException;
    public List<TaxCategoryInfo> getTaxCategoriesList() throws BasicException;
    public List<CurrencyInfo> getCurrenciesList() throws BasicException;
    public CurrencyInfo getCurrency(int currencyId) throws BasicException;
    public CurrencyInfo getMainCurrency() throws BasicException;

    /**
     * Get a price for a product in a given tariff area.
     * @param tariffAreaId Id of the area to check.
     * @param productId Id of the product to look the price in the area.
     * @return A TariffAreaPrice or null if none is set.
     */
    public TariffAreaPrice getTariffAreaPrice(int tariffAreaId, String productId) throws BasicException;
    /**
     * Get all tarif areas managed by the source.
     * @return The list of managed areas.
     */
    public List<TariffInfo> getTariffAreaList() throws BasicException;
    /**
     * Get a tariff area from its id.
     * @param id Id of the area to get.
     * @return A TariffInfo or null if not found.
     */
    public TariffInfo getTariffArea(int id) throws BasicException;
}
