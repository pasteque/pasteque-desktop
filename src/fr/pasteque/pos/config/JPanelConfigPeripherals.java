//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.config;

import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.ReportUtils;
import fr.pasteque.pos.util.StringParser;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** Display, printer and scale options. */
public class JPanelConfigPeripherals extends PanelConfig
{
    private static final long serialVersionUID = -5393725462733557377L;

    private ParametersConfig printer1printerparams;
    private ParametersConfig printer2printerparams;
    private ParametersConfig printer3printerparams;

    /** Add printer options to a printer selection combo box. */
    private void addPrinterComboItems(JComboBoxVal<String> cbo) {
        cbo.addItem("screen",
                AppLocal.tr("Label.Config.Printer.Screen"));
        cbo.addItem("printer",
                AppLocal.tr("Label.Config.Printer.Printer"));
        cbo.addItem("citizen",
                AppLocal.tr("Label.Config.Printer.CitizenESCPOS"));
        cbo.addItem("epson",
                AppLocal.tr("Label.Config.Printer.EpsonESCPOS"));
        cbo.addItem("tmu220",
                AppLocal.tr("Label.Config.Printer.EpsonTMU220ESCPOS"));
        cbo.addItem("star",
                AppLocal.tr("Label.Config.Printer.StarESCPOS"));
        cbo.addItem("ithaca",
                AppLocal.tr("Label.Config.Printer.IthacaESCPOS"));
        cbo.addItem("surepos",
                AppLocal.tr("Label.Config.Printer.SurePOSESCPOS"));
        cbo.addItem("plain", "plain");
        cbo.addItem("javapos", "javapos");
        cbo.addItem("Not defined",
                AppLocal.tr("Label.Config.Printer.NotDefined"));
    }

    /** Add printer connection type to a combo box. */
    private void addPrinterConnComboItems(JComboBoxVal<String> cbo) {
        cbo.addItem("serial",
                AppLocal.tr("Label.Config.PrinterConn.Serial"));
        cbo.addItem("file",
                AppLocal.tr("Label.Config.PrinterConn.File"));
        cbo.addItem("network",
                AppLocal.tr("Label.Config.PrinterConn.Network"));
    }

    private void addPrinterPortComboItems(javax.swing.JComboBox<String> cbo) {
        cbo.addItem("COM1");
        cbo.addItem("COM2");
        cbo.addItem("COM3");
        cbo.addItem("COM4");
        cbo.addItem("LPT1");
        cbo.addItem("/dev/ttyS0");
        cbo.addItem("/dev/ttyS1");
        cbo.addItem("/dev/ttyS2");
        cbo.addItem("/dev/ttyS3");
        cbo.addItem("/dev/usb/lp0");
        cbo.addItem("/dev/usb/lp1");
        cbo.addItem("/dev/usb/lp2");
        cbo.addItem("/dev/usb/lp3");
    }

    /** Creates new form JPanelConfigPeripherals */
    public JPanelConfigPeripherals() {
        initComponents();

        String[] printernames = ReportUtils.getPrintNames();

        jcboMachineDisplay.addActionListener(dirty);
        jcboConnDisplay.addActionListener(dirty);
        jcboSerialDisplay.addActionListener(dirty);
        m_jtxtJPOSName.getDocument().addDocumentListener(dirty);

        jcboMachinePrinter.addActionListener(dirty);
        jcboConnPrinter.addActionListener(dirty);
        jcboSerialPrinter.addActionListener(dirty);
        m_jtxtJPOSPrinter.getDocument().addDocumentListener(dirty);
        m_jtxtJPOSDrawer.getDocument().addDocumentListener(dirty);

        printer1printerparams = new ParametersPrinter(printernames);
        printer1printerparams.addDirtyManager(dirty);
        m_jPrinterParams1.add(printer1printerparams.getComponent(), "printer");
        jtxtPrinterCharsPerLine.getDocument().addDocumentListener(dirty);
        jtxtPrinterDotsPerLine.getDocument().addDocumentListener(dirty);

        jcboMachinePrinter2.addActionListener(dirty);
        jcboConnPrinter2.addActionListener(dirty);
        jcboSerialPrinter2.addActionListener(dirty);
        m_jtxtJPOSPrinter2.getDocument().addDocumentListener(dirty);
        m_jtxtJPOSDrawer2.getDocument().addDocumentListener(dirty);

        printer2printerparams = new ParametersPrinter(printernames);
        printer2printerparams.addDirtyManager(dirty);
        m_jPrinterParams2.add(printer2printerparams.getComponent(), "printer");
        jtxtPrinter2CharsPerLine.getDocument().addDocumentListener(dirty);
        jtxtPrinter2DotsPerLine.getDocument().addDocumentListener(dirty);

        jcboMachinePrinter3.addActionListener(dirty);
        jcboConnPrinter3.addActionListener(dirty);
        jcboSerialPrinter3.addActionListener(dirty);
        m_jtxtJPOSPrinter3.getDocument().addDocumentListener(dirty);
        m_jtxtJPOSDrawer3.getDocument().addDocumentListener(dirty);

        printer3printerparams = new ParametersPrinter(printernames);
        printer3printerparams.addDirtyManager(dirty);
        m_jPrinterParams3.add(printer3printerparams.getComponent(), "printer");
        jtxtPrinter3CharsPerLine.getDocument().addDocumentListener(dirty);
        jtxtPrinter3DotsPerLine.getDocument().addDocumentListener(dirty);

        // Printer 1
        this.addPrinterComboItems(jcboMachinePrinter);
        this.addPrinterConnComboItems(jcboConnPrinter);
        this.addPrinterPortComboItems(jcboSerialPrinter);

        // Printer 2
        this.addPrinterComboItems(jcboMachinePrinter2);
        this.addPrinterConnComboItems(jcboConnPrinter2);
        this.addPrinterPortComboItems(jcboSerialPrinter2);

        // Printer 3
        this.addPrinterComboItems(jcboMachinePrinter3);
        this.addPrinterConnComboItems(jcboConnPrinter3);
        this.addPrinterPortComboItems(jcboSerialPrinter3);

        // Display
        jcboMachineDisplay.addItem("screen",
                AppLocal.tr("Label.Config.Display.Screen"));
        jcboMachineDisplay.addItem("window",
                AppLocal.tr("Label.Config.Display.Window"));
        jcboMachineDisplay.addItem("javapos", "JavaPOS");
        jcboMachineDisplay.addItem("epson", "Epson");
        jcboMachineDisplay.addItem("ld200", "LD200");
        jcboMachineDisplay.addItem("surepos", "SurePOS");
        jcboMachineDisplay.addItem("glancetron", "Glancetron");
        jcboMachineDisplay.addItem("Not defined",
                AppLocal.tr("Label.Config.Display.NotDefined"));

        jcboConnDisplay.addItem("serial",
                AppLocal.tr("Label.Config.DisplayConn.Serial"));
        jcboConnDisplay.addItem("file",
                AppLocal.tr("Label.Config.DisplayConn.File"));

        jcboSerialDisplay.addItem("COM1");
        jcboSerialDisplay.addItem("COM2");
        jcboSerialDisplay.addItem("COM3");
        jcboSerialDisplay.addItem("COM4");
        jcboSerialDisplay.addItem("LPT1");
        jcboSerialDisplay.addItem("/dev/ttyS0");
        jcboSerialDisplay.addItem("/dev/ttyS1");
        jcboSerialDisplay.addItem("/dev/ttyS2");
        jcboSerialDisplay.addItem("/dev/ttyS3");
    }

    /**
     * Load a printer configuration and update the GUI to the new values.
     * @param config The configuration to load.
     * @param printerNum Printer number, from 1 to 3.
     */
    private void loadPrinterConfig(AppConfig config, int printerNum) {
        String property = null;
        JComboBoxVal<String> printerCbo = null;
        JComboBoxVal<String> printerConnCbo = null;
        javax.swing.JComboBox<String> printerSerialCbo = null;
        javax.swing.JTextField charsPerLine = null;
        javax.swing.JTextField dotsPerLine = null;
        javax.swing.JTextField javaposPrinter = null;
        javax.swing.JTextField javaposDrawer = null;
        ParametersConfig systemPrinterParams = null;
        javax.swing.JCheckBox canPrintOrder = null;
        javax.swing.JCheckBox canPrintTicket = null;
        javax.swing.JCheckBox canPrintZ = null;
        switch (printerNum) {
            case 1:
                property = "machine.printer";
                printerCbo = jcboMachinePrinter;
                printerConnCbo = jcboConnPrinter;
                printerSerialCbo = jcboSerialPrinter;
                charsPerLine = jtxtPrinterCharsPerLine;
                dotsPerLine = jtxtPrinterDotsPerLine;
                javaposPrinter = m_jtxtJPOSPrinter;
                javaposDrawer = m_jtxtJPOSDrawer;
                systemPrinterParams = printer1printerparams;
                canPrintOrder = m_jPrinterFilterOrder1;
                canPrintTicket = m_jPrinterFilterTicket1;
                canPrintZ = m_jPrinterFilterZ1;
                break;
            case 2:
                property = "machine.printer.2";
                printerCbo = jcboMachinePrinter2;
                printerConnCbo = jcboConnPrinter2;
                printerSerialCbo = jcboSerialPrinter2;
                charsPerLine = jtxtPrinter2CharsPerLine;
                dotsPerLine = jtxtPrinter2DotsPerLine;
                javaposPrinter = m_jtxtJPOSPrinter2;
                javaposDrawer = m_jtxtJPOSDrawer2;
                systemPrinterParams = printer2printerparams;
                canPrintOrder = m_jPrinterFilterOrder2;
                canPrintTicket = m_jPrinterFilterTicket2;
                canPrintZ = m_jPrinterFilterZ2;
                break;
            case 3:
                property = "machine.printer.3";
                printerCbo = jcboMachinePrinter3;
                printerConnCbo = jcboConnPrinter3;
                printerSerialCbo = jcboSerialPrinter3;
                charsPerLine = jtxtPrinter3CharsPerLine;
                dotsPerLine = jtxtPrinter3DotsPerLine;
                javaposPrinter = m_jtxtJPOSPrinter3;
                javaposDrawer = m_jtxtJPOSDrawer3;
                systemPrinterParams = printer3printerparams;
                canPrintOrder = m_jPrinterFilterOrder3;
                canPrintTicket = m_jPrinterFilterTicket3;
                canPrintZ = m_jPrinterFilterZ3;
                break;
        }
        StringParser p = new StringParser(config.getProperty(property));
        String sparam = unifySerialInterface(p.nextToken(':'));
        if ("serial".equals(sparam) || "file".equals(sparam)) {
            printerCbo.setSelectedValue("epson");
            printerConnCbo.setSelectedValue(sparam);
            printerSerialCbo.setSelectedItem(p.nextToken(','));
        } else if ("javapos".equals(sparam)) {
            printerCbo.setSelectedValue(sparam);
            javaposPrinter.setText(p.nextToken(','));
            javaposDrawer.setText(p.nextToken(','));
        } else if ("printer".equals(sparam)) {
            printerCbo.setSelectedValue(sparam);
            systemPrinterParams.setParameters(p);
        } else {
            printerCbo.setSelectedValue(sparam);
            printerConnCbo.setSelectedValue(unifySerialInterface(p.nextToken(',')));
            printerSerialCbo.setSelectedItem(p.nextToken(','));
        }
        charsPerLine.setText(config.getProperty(property + ".cpl"));
        dotsPerLine.setText(config.getProperty(property + ".dpl"));
        canPrintOrder.setSelected(!config.getProperty(property + ".filter.order").equals("0"));
        canPrintTicket.setSelected(!config.getProperty(property + ".filter.ticket").equals("0"));
        canPrintZ.setSelected(!config.getProperty(property + ".filter.z").equals("0"));
    }

    /** Load all properties from the configuration and update the GUI. */
    public void loadProperties(AppConfig config) {
        this.loadPrinterConfig(config, 1);
        this.loadPrinterConfig(config, 2);
        this.loadPrinterConfig(config, 3);

        StringParser p = new StringParser(config.getProperty("machine.display"));
        String sparam = unifySerialInterface(p.nextToken(':'));
        if ("serial".equals(sparam) || "file".equals(sparam)) {
            jcboMachineDisplay.setSelectedValue("epson");
            jcboConnDisplay.setSelectedValue(sparam);
            jcboSerialDisplay.setSelectedItem(p.nextToken(','));
        } else if ("javapos".equals(sparam)) {
            jcboMachineDisplay.setSelectedValue(sparam);
            m_jtxtJPOSName.setText(p.nextToken(','));
        } else {
            jcboMachineDisplay.setSelectedValue(sparam);
            jcboConnDisplay.setSelectedValue(unifySerialInterface(p.nextToken(',')));
            jcboSerialDisplay.setSelectedItem(p.nextToken(','));
        }

        dirty.setDirty(false);
    }

    /**
     * Save a printer configuration.
     * @param config The configuration to save to.
     * @param printerNum Printer number, from 1 to 3.
     */
    private void savePrinterConfig(AppConfig config, int printerNum) {
        String property = null;
        JComboBoxVal<String> printerCbo = null;
        JComboBoxVal<String> printerConnCbo = null;
        javax.swing.JComboBox<String> printerSerialCbo = null;
        javax.swing.JTextField charsPerLine = null;
        javax.swing.JTextField dotsPerLine = null;
        javax.swing.JTextField javaposPrinter = null;
        javax.swing.JTextField javaposDrawer = null;
        ParametersConfig systemPrinterParams = null;
        String canPrintOrder = "1";
        String canPrintTicket = "1";
        String canPrintZ = "1";
        switch (printerNum) {
            case 1:
                property = "machine.printer";
                printerCbo = jcboMachinePrinter;
                printerConnCbo = jcboConnPrinter;
                printerSerialCbo = jcboSerialPrinter;
                charsPerLine = jtxtPrinterCharsPerLine;
                dotsPerLine = jtxtPrinterDotsPerLine;
                javaposPrinter = m_jtxtJPOSPrinter;
                javaposDrawer = m_jtxtJPOSDrawer;
                systemPrinterParams = printer1printerparams;
                canPrintOrder = checkboxValue(m_jPrinterFilterOrder1);
                canPrintTicket = checkboxValue(m_jPrinterFilterTicket1);
                canPrintZ = checkboxValue(m_jPrinterFilterZ1);
                break;
            case 2:
                property = "machine.printer.2";
                printerCbo = jcboMachinePrinter2;
                printerConnCbo = jcboConnPrinter2;
                printerSerialCbo = jcboSerialPrinter2;
                charsPerLine = jtxtPrinter2CharsPerLine;
                dotsPerLine = jtxtPrinter2DotsPerLine;
                javaposPrinter = m_jtxtJPOSPrinter2;
                javaposDrawer = m_jtxtJPOSDrawer2;
                systemPrinterParams = printer2printerparams;
                canPrintOrder = checkboxValue(m_jPrinterFilterOrder2);
                canPrintTicket = checkboxValue(m_jPrinterFilterTicket2);
                canPrintZ = checkboxValue(m_jPrinterFilterZ2);
                break;
            case 3:
                property = "machine.printer.3";
                printerCbo = jcboMachinePrinter3;
                printerConnCbo = jcboConnPrinter3;
                printerSerialCbo = jcboSerialPrinter3;
                charsPerLine = jtxtPrinter3CharsPerLine;
                dotsPerLine = jtxtPrinter3DotsPerLine;
                javaposPrinter = m_jtxtJPOSPrinter3;
                javaposDrawer = m_jtxtJPOSDrawer3;
                systemPrinterParams = printer3printerparams;
                canPrintOrder = checkboxValue(m_jPrinterFilterOrder3);
                canPrintTicket = checkboxValue(m_jPrinterFilterTicket3);
                canPrintZ = checkboxValue(m_jPrinterFilterZ3);
                break;
        }
        String printer = printerCbo.getSelectedValue();
        if ("citizen".equals(printer)
                || "epson".equals(printer)
                || "tmu220".equals(printer)
                || "star".equals(printer)
                || "ithaca".equals(printer)
                || "surepos".equals(printer) ) {
            config.setProperty(property, printer + ":"
                    + printerConnCbo.getSelectedValue() + ","
                    + comboValue(printerSerialCbo.getSelectedItem()));
        } else if ("javapos".equals(printer)) {
            config.setProperty(property, printer + ":"
                    + javaposPrinter.getText() + ","
                    + javaposDrawer.getText());
        } else if ("printer".equals(printer)) {
            config.setProperty(property, printer + ":"
                    + systemPrinterParams.getParameters());
        } else {
            config.setProperty(property, printer);
        }
        config.setProperty(property + ".cpl", charsPerLine.getText());
        config.setProperty(property + ".dpl", dotsPerLine.getText());
        config.setProperty(property + ".filter.order", canPrintOrder);
        config.setProperty(property + ".filter.ticket", canPrintTicket);
        config.setProperty(property + ".filter.z", canPrintZ);
    }

    /** Save all properties to the configuration. */
    public void saveProperties(AppConfig config) {
        this.savePrinterConfig(config, 1);
        this.savePrinterConfig(config, 2);
        this.savePrinterConfig(config, 3);

        String sMachineDisplay = jcboMachineDisplay.getSelectedValue();
        if (   "epson".equals(sMachineDisplay) || "ld200".equals(sMachineDisplay)
            || "surepos".equals(sMachineDisplay)
            || "glancetron".equals(sMachineDisplay)) {
            config.setProperty("machine.display", sMachineDisplay + ":" + jcboConnDisplay.getSelectedValue() + "," + comboValue(jcboSerialDisplay.getSelectedItem()));
        } else if ("javapos".equals(sMachineDisplay)) {
            config.setProperty("machine.display", sMachineDisplay + ":" + m_jtxtJPOSName.getText());
        } else {
            config.setProperty("machine.display", sMachineDisplay);
        }

        dirty.setDirty(false);
    }

    private String unifySerialInterface(String sparam) {
        // for backward compatibility
        return ("rxtx".equals(sparam))
                ? "serial"
                : sparam;
    }

    private String comboValue(Object value) {
        return value == null ? "" : value.toString();
    }

    private void addPrinterFilter(JPanel container,
            javax.swing.JCheckBox checkBoxOrder,
            javax.swing.JCheckBox checkBoxTicket,
            javax.swing.JCheckBox checkBoxZ) {
        JLabel filterLblOrder = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterFilterOrder"));
        JLabel filterLblTicket = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterFilterTicket"));
        JLabel filterLblZ = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterFilterZ"));
        JPanel jPanelFilter = new JPanel();
        jPanelFilter.setLayout(new GridBagLayout());
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = 0;
        jPanelFilter.add(checkBoxOrder, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1; cstr.gridy = 0; cstr.anchor = GridBagConstraints.WEST;
        jPanelFilter.add(filterLblOrder, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = 1;
        jPanelFilter.add(checkBoxTicket, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1; cstr.gridy = 1; cstr.anchor = GridBagConstraints.WEST;
        jPanelFilter.add(filterLblTicket, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0; cstr.gridy = 2;
        jPanelFilter.add(checkBoxZ, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1; cstr.gridy = 2; cstr.anchor = GridBagConstraints.WEST;
        jPanelFilter.add(filterLblZ, cstr);
        container.add(jPanelFilter);
    }

    private void initComponents() {
        // Printer 1
        this.addOptionsContainer("Label.Config.Printer1");
        this.jcboMachinePrinter = this.addComboBoxValParam("Label.Config.Driver");
        this.jtxtPrinterCharsPerLine = this.addTextParam("Label.Config.PrinterCpl");
        this.jtxtPrinterDotsPerLine = this.addTextParam("Label.Config.PrinterDpl");
        this.m_jPrinterParams1 = this.addSubparamZone();
        // Printer 1 filters
        m_jPrinterFilterOrder1 = WidgetsBuilder.createCheckBox();
        m_jPrinterFilterTicket1 = WidgetsBuilder.createCheckBox();
        m_jPrinterFilterZ1 = WidgetsBuilder.createCheckBox();
        JPanel printerFilterContainer = this.addSubparamZone();
        this.addPrinterFilter(printerFilterContainer,
                m_jPrinterFilterOrder1,
                m_jPrinterFilterTicket1,
                m_jPrinterFilterZ1);

        // Printer 2
        this.addOptionsContainer("Label.Config.Printer2");
        this.jcboMachinePrinter2 = this.addComboBoxValParam("Label.Config.Driver");
        this.jtxtPrinter2CharsPerLine = this.addTextParam("Label.Config.PrinterCpl");
        this.jtxtPrinter2DotsPerLine = this.addTextParam("Label.Config.PrinterDpl");
        this.m_jPrinterParams2 = this.addSubparamZone();
        // Printer 2 filters
        m_jPrinterFilterOrder2 = WidgetsBuilder.createCheckBox();
        m_jPrinterFilterTicket2 = WidgetsBuilder.createCheckBox();
        m_jPrinterFilterZ2 = WidgetsBuilder.createCheckBox();
        printerFilterContainer = this.addSubparamZone();
        this.addPrinterFilter(printerFilterContainer,
                m_jPrinterFilterOrder2,
                m_jPrinterFilterTicket2,
                m_jPrinterFilterZ2);

        // Printer 3
        this.addOptionsContainer("Label.Config.Printer3");
        this.jcboMachinePrinter3 = this.addComboBoxValParam("Label.Config.Driver");
        this.jtxtPrinter3CharsPerLine = this.addTextParam("Label.Config.PrinterCpl");
        this.jtxtPrinter3DotsPerLine = this.addTextParam("Label.Config.PrinterDpl");
        this.m_jPrinterParams3 = this.addSubparamZone();
        // Printer 3 filters
        m_jPrinterFilterOrder3 = WidgetsBuilder.createCheckBox();
        m_jPrinterFilterTicket3 = WidgetsBuilder.createCheckBox();
        m_jPrinterFilterZ3 = WidgetsBuilder.createCheckBox();
        printerFilterContainer = this.addSubparamZone();
        this.addPrinterFilter(printerFilterContainer,
                m_jPrinterFilterOrder3,
                m_jPrinterFilterTicket3,
                m_jPrinterFilterZ3);

        // Customer display
        this.addOptionsContainer("Label.Config.Display");
        this.jcboMachineDisplay = this.addComboBoxValParam("Label.Config.Driver");
        this.m_jDisplayParams = this.addSubparamZone();

        GridBagConstraints cstr;

        // Customer display subparams
        jcboMachineDisplay.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcboMachineDisplayActionPerformed(evt);
            }
        });
        // Empty
        jPanel2 = new JPanel();
        m_jDisplayParams.add(jPanel2, "empty");
        // comm (serial)
        jPanel1 = new JPanel();
        jPanel1.setLayout(new GridBagLayout());
        JLabel jlblConnDisplay = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.DisplayConn"));
        jcboConnDisplay = new JComboBoxVal<String>();
        JLabel jlblDisplayPort = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.DisplayPort"));
        jcboSerialDisplay = new javax.swing.JComboBox<String>();
        jcboSerialDisplay.setEditable(true);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel1.add(jlblConnDisplay, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel1.add(jcboConnDisplay, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel1.add(jlblDisplayPort, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel1.add(jcboSerialDisplay, cstr);
        m_jDisplayParams.add(jPanel1, "comm");
        // javapos
        jPanel3 = new JPanel();
        jPanel3.setLayout(new GridBagLayout());
        JLabel javaposNameLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Name"));
        m_jtxtJPOSName = new javax.swing.JTextField();
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel3.add(javaposNameLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel3.add(m_jtxtJPOSName, cstr);
        m_jDisplayParams.add(jPanel3, "javapos");

        // Printer1 subparams
        jcboMachinePrinter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcboMachinePrinterActionPerformed(evt);
            }
        });
        // empty
        jPanel5 = new JPanel();
        m_jPrinterParams1.add(jPanel5, "empty");
        // comm (serial)
        jPanel6 = new JPanel();
        jPanel6.setLayout(new GridBagLayout());
        jlblConnPrinter = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterConn"));
        jcboConnPrinter = new JComboBoxVal<String>();
        jlblPrinterPort = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterPort"));
        jcboSerialPrinter = new javax.swing.JComboBox<String>();
        jcboSerialPrinter.setEditable(true);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel6.add(jlblConnPrinter, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel6.add(jcboConnPrinter, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel6.add(jlblPrinterPort, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel6.add(jcboSerialPrinter, cstr);
        m_jPrinterParams1.add(jPanel6, "comm");
        // javapos
        jPanel4 = new JPanel();
        jPanel4.setLayout(new GridBagLayout());
        JLabel printer1javaposNameLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.javapos.printer"));
        m_jtxtJPOSPrinter = new javax.swing.JTextField();
        JLabel printer1javaposDrawerLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.javapos.drawer"));
        m_jtxtJPOSDrawer = new javax.swing.JTextField();
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel4.add(printer1javaposNameLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel4.add(m_jtxtJPOSPrinter, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel4.add(printer1javaposDrawerLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel4.add(m_jtxtJPOSDrawer, cstr);
        m_jPrinterParams1.add(jPanel4, "javapos");

        // Printer2 subparams
        jcboMachinePrinter2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcboMachinePrinter2ActionPerformed(evt);
            }
        });
        // empty
        jPanel7 = new JPanel();
        m_jPrinterParams2.add(jPanel7, "empty");
        // comm (serial)
        jPanel8 = new JPanel();
        jPanel8.setLayout(new GridBagLayout());
        jlblConnPrinter2 = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterConn"));
        jcboConnPrinter2 = new JComboBoxVal<String>();
        jlblPrinterPort2 = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterPort"));
        jcboSerialPrinter2 = new javax.swing.JComboBox<String>();
        jcboSerialPrinter2.setEditable(true);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel8.add(jlblConnPrinter2, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel8.add(jcboConnPrinter2, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel8.add(jlblPrinterPort2, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel8.add(jcboSerialPrinter2, cstr);
        m_jPrinterParams2.add(jPanel8, "comm");
        // javapos
        jPanel11 = new JPanel();
        jPanel11.setLayout(new GridBagLayout());
        JLabel printer2javaposNameLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.javapos.printer"));
        m_jtxtJPOSPrinter2 = new javax.swing.JTextField();
        JLabel printer2javaposDrawerLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.javapos.drawer"));
        m_jtxtJPOSDrawer2 = new javax.swing.JTextField();
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel11.add(printer2javaposNameLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel11.add(m_jtxtJPOSPrinter2, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel11.add(printer2javaposDrawerLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel11.add(m_jtxtJPOSDrawer2, cstr);
        m_jPrinterParams2.add(jPanel11, "javapos");

        // Printer3 subparams
        jcboMachinePrinter3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcboMachinePrinter3ActionPerformed(evt);
            }
        });
        // empty
        jPanel9 = new JPanel();
        m_jPrinterParams3.add(jPanel9, "empty");
        // comm (serial)
        jPanel10 = new JPanel();
        jPanel10.setLayout(new GridBagLayout());
        jlblConnPrinter3 = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterConn"));
        jcboConnPrinter3 = new JComboBoxVal<String>();
        jlblPrinterPort3 = WidgetsBuilder.createLabel(AppLocal.tr("Label.Config.PrinterPort"));
        jcboSerialPrinter3 = new JComboBox<String>();
        jcboSerialPrinter3.setEditable(true);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel10.add(jlblConnPrinter3, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel10.add(jcboConnPrinter3, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel10.add(jlblPrinterPort3, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel10.add(jcboSerialPrinter3, cstr);
        m_jPrinterParams3.add(jPanel10, "comm");
        // javapos
        jPanel12 = new JPanel();
        jPanel12.setLayout(new GridBagLayout());
        JLabel printer3javaposNameLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.javapos.printer"));
        m_jtxtJPOSPrinter3 = new javax.swing.JTextField();
        JLabel printer3javaposDrawerLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.javapos.drawer"));
        m_jtxtJPOSDrawer3 = new javax.swing.JTextField();
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        jPanel12.add(printer3javaposNameLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        jPanel12.add(m_jtxtJPOSPrinter3, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        jPanel12.add(printer3javaposDrawerLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        jPanel12.add(m_jtxtJPOSDrawer3, cstr);
        m_jPrinterParams3.add(jPanel12, "javapos");
    }

    private void printerChanged(int printerNum, String val) {
        javax.swing.JPanel printerParamsPanel = null;
        switch (printerNum) {
            case 1:
                printerParamsPanel = m_jPrinterParams1;
                break;
            case 2:
                printerParamsPanel = m_jPrinterParams2;
                break;
            case 3:
                printerParamsPanel = m_jPrinterParams3;
                break;
        }
        CardLayout cl = (CardLayout) (printerParamsPanel.getLayout());
        if ("citizen".equals(val) || "epson".equals(val)
                || "tmu220".equals(val) || "star".equals(val)
                || "ithaca".equals(val) || "surepos".equals(val) ) {
            cl.show(printerParamsPanel, "comm");
        } else if ("javapos".equals(val)) {
            cl.show(printerParamsPanel, "javapos");
        } else if ("printer".equals(val)) {
            cl.show(printerParamsPanel, "printer");
        } else {
            cl.show(printerParamsPanel, "empty");
        }
    }

    private void jcboMachinePrinter3ActionPerformed(java.awt.event.ActionEvent evt) {
        this.printerChanged(3, jcboMachinePrinter3.getSelectedValue());
    }

    private void jcboMachinePrinter2ActionPerformed(java.awt.event.ActionEvent evt) {
        this.printerChanged(2, jcboMachinePrinter2.getSelectedValue());
    }

    private void jcboMachineDisplayActionPerformed(java.awt.event.ActionEvent evt) {
        CardLayout cl = (CardLayout) (m_jDisplayParams.getLayout());
        String val = jcboMachineDisplay.getSelectedValue();
        if ("epson".equals(val) || "ld200".equals(val) || "surepos".equals(val)
                || "glancetron".equals(val)) {
            cl.show(m_jDisplayParams, "comm");
        } else if ("javapos".equals(jcboMachineDisplay.getSelectedItem())) {
            cl.show(m_jDisplayParams, "javapos");
        } else {
            cl.show(m_jDisplayParams, "empty");
        }
    }

    private void jcboMachinePrinterActionPerformed(java.awt.event.ActionEvent evt) {
        this.printerChanged(1, jcboMachinePrinter.getSelectedValue());
    }

    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private JComboBoxVal<String> jcboConnDisplay;
    private JComboBoxVal<String> jcboConnPrinter;
    private JComboBoxVal<String> jcboConnPrinter2;
    private JComboBoxVal<String> jcboConnPrinter3;
    private JComboBoxVal<String> jcboMachineDisplay;
    private JComboBoxVal<String> jcboMachinePrinter;
    private JComboBoxVal<String> jcboMachinePrinter2;
    private JComboBoxVal<String> jcboMachinePrinter3;
    private javax.swing.JComboBox<String> jcboSerialDisplay;
    private javax.swing.JComboBox<String> jcboSerialPrinter;
    private javax.swing.JComboBox<String> jcboSerialPrinter2;
    private javax.swing.JComboBox<String> jcboSerialPrinter3;
    private javax.swing.JLabel jlblConnPrinter;
    private javax.swing.JLabel jlblConnPrinter2;
    private javax.swing.JLabel jlblConnPrinter3;
    private javax.swing.JLabel jlblPrinterPort;
    private javax.swing.JLabel jlblPrinterPort2;
    private javax.swing.JLabel jlblPrinterPort3;
    private javax.swing.JPanel m_jDisplayParams;
    private javax.swing.JPanel m_jPrinterParams1;
    private javax.swing.JPanel m_jPrinterParams2;
    private javax.swing.JPanel m_jPrinterParams3;
    private javax.swing.JCheckBox m_jPrinterFilterOrder1;
    private javax.swing.JCheckBox m_jPrinterFilterOrder2;
    private javax.swing.JCheckBox m_jPrinterFilterOrder3;
    private javax.swing.JCheckBox m_jPrinterFilterTicket1;
    private javax.swing.JCheckBox m_jPrinterFilterTicket2;
    private javax.swing.JCheckBox m_jPrinterFilterTicket3;
    private javax.swing.JCheckBox m_jPrinterFilterZ1;
    private javax.swing.JCheckBox m_jPrinterFilterZ2;
    private javax.swing.JCheckBox m_jPrinterFilterZ3;
    private javax.swing.JTextField jtxtPrinterCharsPerLine;
    private javax.swing.JTextField jtxtPrinterDotsPerLine;
    private javax.swing.JTextField jtxtPrinter2CharsPerLine;
    private javax.swing.JTextField jtxtPrinter2DotsPerLine;
    private javax.swing.JTextField jtxtPrinter3CharsPerLine;
    private javax.swing.JTextField jtxtPrinter3DotsPerLine;
    private javax.swing.JTextField m_jtxtJPOSDrawer;
    private javax.swing.JTextField m_jtxtJPOSDrawer2;
    private javax.swing.JTextField m_jtxtJPOSDrawer3;
    private javax.swing.JTextField m_jtxtJPOSName;
    private javax.swing.JTextField m_jtxtJPOSPrinter;
    private javax.swing.JTextField m_jtxtJPOSPrinter2;
    private javax.swing.JTextField m_jtxtJPOSPrinter3;
}
