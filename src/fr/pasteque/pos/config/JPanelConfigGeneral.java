//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.config;

import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import java.awt.Component;
import java.util.Map;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceSkin;
import org.jvnet.substance.skin.SkinInfo;

/** Misc. options */
public class JPanelConfigGeneral extends PanelConfig
{
    private static final long serialVersionUID = 7104695218810652423L;

    /** Creates new form JPanelConfigGeneral */
    public JPanelConfigGeneral() {

        initComponents();

        jcboLAF.addActionListener(dirty);
        jtxtScreenDensity.getDocument().addDocumentListener(dirty);
        jcboMachineScreenmode.addActionListener(dirty);
        jcboTicketsBag.addActionListener(dirty);

        jcbAutoHideMenu.addActionListener(dirty);
        jcbCollapseKeypad.addActionListener(dirty);
        jcbBeepLine.addActionListener(dirty);
        jcbUseEmptyPayment.addActionListener(dirty);
        jcboConfirmExcessivePayment.addActionListener(dirty);
        jcboPriceVisible.addActionListener(dirty);
        jcboOrderAutoSwitch.addActionListener(dirty);
        jcbAskCustCount.addActionListener(dirty);

        jcbAutoPrint.addActionListener(dirty);

//        // Openbravo Skin
//        jcboLAF.addItem(new UIManager.LookAndFeelInfo("Openbravo", "fr.pasteque.pos.skin.OpenbravoLookAndFeel"));

        // Installed skins
        LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
        for (int i = 0; i < lafs.length; i++) {
            jcboLAF.addItem(lafs[i].getClassName(), lafs[i].getName());
        }

        // Substance skins
        // new SubstanceLookAndFeel(); // TODO: Remove in Substance 5.0. Workaround for Substance 4.3 to initialize static variables
        Map<String, SkinInfo> skins = SubstanceLookAndFeel.getAllSkins();
        for (SkinInfo skin : skins.values()) {
            jcboLAF.addItem(skin.getDisplayName(), skin.getClassName());
        }

        jcboLAF.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeLAF();
            }
        });

        jcboMachineScreenmode.addItem("window",
                AppLocal.tr("Label.Config.Screen.Window"));
        jcboMachineScreenmode.addItem("fullscreenwindow",
                AppLocal.tr("Label.Config.Screen.FullscreenWindow"));
        jcboMachineScreenmode.addItem("fullscreen",
                AppLocal.tr("Label.Config.Screen.Fullscreen"));

        // Excessive payment confirm values
        jcboConfirmExcessivePayment.addItem("never",
                AppLocal.tr("Label.Config.ConfirmExcessivePayment.Never"));
        jcboConfirmExcessivePayment.addItem("noreturn",
                AppLocal.tr("Label.Config.ConfirmExcessivePayment.NoReturn"));
        jcboConfirmExcessivePayment.addItem("always",
                AppLocal.tr("Label.Config.ConfirmExcessivePayment.Always"));

        // Display price values
        jcboPriceVisible.addItem("none",
                AppLocal.tr("Label.Config.PriceVisible.None"));
        jcboPriceVisible.addItem("untaxed",
                AppLocal.tr("Label.Config.PriceVisible.Untaxed"));
        jcboPriceVisible.addItem("taxed",
                AppLocal.tr("Label.Config.PriceVisible.Taxed"));

        // Order autoswitch
        jcboOrderAutoSwitch.addItem("new",
                AppLocal.tr("Label.Config.OrderAutoSwitch.New"));
        jcboOrderAutoSwitch.addItem("recall",
                AppLocal.tr("Label.Config.OrderAutoSwitch.Recall"));

        // Ticket mode
        jcboTicketsBag.addItem("standard",
                AppLocal.tr("Label.Config.TicketsBag.Standard"));
        jcboTicketsBag.addItem("restaurant",
                AppLocal.tr("Label.Config.TicketsBag.Restaurant"));

        // Sync type
        jcboSyncMode.addItem("full",
                AppLocal.tr("Label.Config.SyncMode.Full"));
        jcboSyncMode.addItem("advanced",
                AppLocal.tr("Label.Config.SyncMode.Advanced"));
        jcboSyncMode.addItem("startup",
                AppLocal.tr("Label.Config.SyncMode.StartUp"));
    }

    public Component getConfigComponent() {
        return this;
    }

    public void loadProperties(AppConfig config) {
        String lafclass = config.getProperty("swing.defaultlaf");
        jcboLAF.setSelectedItem(null);
        for (int i = 0; i < jcboLAF.getItemCount(); i++) {
            String lafClassName = jcboLAF.getItemAt(i).getItem();
            if (lafClassName.equals(lafclass)) {
                jcboLAF.setSelectedIndex(i);
                break;
            }
        }
        // jcboLAF.setSelectedItem(new LookAndFeelInfo());

        jcboMachineScreenmode.setSelectedValue(config.getProperty("machine.screenmode"));
        jtxtScreenDensity.setText(config.getProperty("machine.screendensity"));
        jcboTicketsBag.setSelectedValue(config.getProperty("machine.ticketsbag"));

        jcbAutoHideMenu.setSelected(!config.getProperty("ui.autohidemenu").equals("0"));
        jcbCollapseKeypad.setSelected(!config.getProperty("ui.showkeypad").equals("0"));

        jcbBeepLine.setSelected(!config.getProperty("ui.beepline").equals("0"));

        jcboPriceVisible.setSelectedValue(config.getProperty("ui.pricevisible"));
        jcbUseEmptyPayment.setSelected(config.getProperty("ui.useemptypayment").equals("1"));
        jcboConfirmExcessivePayment.setSelectedValue(config.getProperty("ui.confirmexcessivepayment"));
        jcboOrderAutoSwitch.setSelectedValue(config.getProperty("ui.orderautoswitch"));
        jcbAskCustCount.setSelected(config.getProperty("ui.autodisplaycustcount").equals("1"));

        jcbAutoPrint.setSelected(!config.getProperty("ui.printticketbydefault").equals("0"));
        jtxtPrintCount.setText(config.getProperty("ui.printticketcount"));

        jcboSyncMode.setSelectedValue(config.getProperty("ui.syncmode"));

        dirty.setDirty(false);
    }

    public void saveProperties(AppConfig config) {
        String lafClassName = jcboLAF.getSelectedValue();
        config.setProperty("swing.defaultlaf", lafClassName == null
                ? System.getProperty("swing.defaultlaf", "javax.swing.plaf.metal.MetalLookAndFeel")
                : lafClassName);

        config.setProperty("machine.screenmode",
                jcboMachineScreenmode.getSelectedValue());
        config.setProperty("machine.screendensity", jtxtScreenDensity.getText());
        config.setProperty("machine.ticketsbag",
                jcboTicketsBag.getSelectedValue());

        config.setProperty("ui.autohidemenu", checkboxValue(jcbAutoHideMenu));
        config.setProperty("ui.showkeypad", checkboxValue(jcbCollapseKeypad));
        config.setProperty("ui.beepline", checkboxValue(jcbBeepLine));
        config.setProperty("ui.pricevisible",
                jcboPriceVisible.getSelectedValue());
        config.setProperty("ui.useemptypayment", checkboxValue(jcbUseEmptyPayment));
        config.setProperty("ui.confirmexcessivepayment",
                jcboConfirmExcessivePayment.getSelectedValue());
        config.setProperty("ui.orderautoswitch",
                jcboOrderAutoSwitch.getSelectedValue());
        config.setProperty("ui.autodisplaycustcount", checkboxValue(jcbAskCustCount));

        config.setProperty("ui.printticketbydefault", checkboxValue(jcbAutoPrint));
        try {
            int printCount = Integer.parseInt(jtxtPrintCount.getText().trim());
            if (printCount > 0) {
                config.setProperty("ui.printticketcount", String.valueOf(printCount));
            } else {
                config.setProperty("ui.printticketcount", "1");
            }
        } catch (NumberFormatException e) {
            config.setProperty("ui.printticketcount", "1");
        }
        config.setProperty("ui.syncmode", jcboSyncMode.getSelectedValue());

        dirty.setDirty(false);
    }

    private void changeLAF() {

        final String lafClassName = jcboLAF.getSelectedValue();
        if (lafClassName != null && !lafClassName.equals(UIManager.getLookAndFeel().getClass().getName())) {
            // The selected look and feel is different from the current look and feel.
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    try {
                        Object laf = Class.forName(lafClassName).getDeclaredConstructor().newInstance();

                        if (laf instanceof LookAndFeel) {
                            UIManager.setLookAndFeel((LookAndFeel) laf);
                        } else if (laf instanceof SubstanceSkin) {
                            SubstanceLookAndFeel.setSkin((SubstanceSkin) laf);
                        }

                        SwingUtilities.updateComponentTreeUI(JPanelConfigGeneral.this.getTopLevelAncestor());
                    } catch (Exception e) {
                    }
                }
            });
        }
    }

    private void initComponents() {
        this.addOptionsContainer("Label.Config.UI");
        this.jcboLAF = this.addComboBoxValParam("Label.Config.LookNFeel");
        this.jcboMachineScreenmode = this.addComboBoxValParam("Label.Config.Screen");
        this.jtxtScreenDensity = this.addTextParam("Label.Config.ScreenDensity");
        this.jcbAutoHideMenu = this.addCheckBoxParam("Label.Config.AutoHideMenu");
        this.jcbCollapseKeypad = this.addCheckBoxParam("Label.Config.ShowKeypad");
        this.jcbBeepLine = this.addCheckBoxParam("Label.Config.BeepLine");
        this.jcbUseEmptyPayment = this.addCheckBoxParam("Label.Config.UseEmptyPayment");
        this.jcboConfirmExcessivePayment = this.addComboBoxValParam("Label.Config.ConfirmExcessivePayment");
        this.jcboPriceVisible = this.addComboBoxValParam("Label.Config.PriceVisible");
        this.jcboTicketsBag = this.addComboBoxValParam("Label.Config.TicketsBag");
        this.jcboTicketsBag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onTicketsBagChanged(evt);
            }
        });
        this.jcboOrderAutoSwitch = this.addComboBoxValParam("Label.Config.OrderAutoSwitch");
        this.jlblOrderAutoSwitch = this.getLastInputLabel();
        this.jcbAskCustCount = this.addCheckBoxParam("Label.Config.AskCustCount");
        this.jlblAskCustCount = this.getLastInputLabel();
        this.jcbAutoPrint = this.addCheckBoxParam("Label.Config.PrintDefault");
        this.jtxtPrintCount = this.addTextParam("Label.Config.PrintTicketCount");
        this.jcboSyncMode = this.addComboBoxValParam("Label.Config.SyncMode");
    }

    private void onTicketsBagChanged(java.awt.event.ActionEvent evt) {
        boolean stdMode = "standard".equals(jcboTicketsBag.getSelectedValue());
        jcboOrderAutoSwitch.setVisible(stdMode);
        jlblOrderAutoSwitch.setVisible(stdMode);
        jcbAskCustCount.setVisible(!stdMode);
        jlblAskCustCount.setVisible(!stdMode);
    }

    private JComboBoxVal<String> jcboPriceVisible;
    private JComboBoxVal<String> jcboLAF;
    private JComboBoxVal<String> jcboMachineScreenmode;
    private JComboBoxVal<String> jcboTicketsBag;
    private javax.swing.JTextField jtxtScreenDensity;
    private javax.swing.JCheckBox jcbAutoHideMenu;
    private javax.swing.JCheckBox jcbCollapseKeypad;
    private javax.swing.JCheckBox jcbBeepLine;
    private javax.swing.JCheckBox jcbUseEmptyPayment;
    private JComboBoxVal<String> jcboConfirmExcessivePayment;
    private javax.swing.JCheckBox jcbAutoPrint;
    private javax.swing.JTextField jtxtPrintCount;
    private JComboBoxVal<String> jcboSyncMode;
    private javax.swing.JLabel jlblOrderAutoSwitch;
    private javax.swing.JCheckBox jcbAskCustCount;
    private javax.swing.JLabel jlblAskCustCount;
    private JComboBoxVal<String> jcboOrderAutoSwitch;
}
