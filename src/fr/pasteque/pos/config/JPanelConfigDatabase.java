//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.config;

import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.util.AltEncrypter;

/** Credentials section for configuration panel (former local database configuration) */
public class JPanelConfigDatabase extends PanelConfig
{
    private static final long serialVersionUID = 7688331081404626942L;

    /** Creates new form JPanelConfigDatabase */
    public JPanelConfigDatabase() {
        super();
        initComponents();
    }

    public void loadProperties(AppConfig config) {
        jtxtServerURL.setText(config.getProperty("server.backoffice"));

        String sDBUser = config.getProperty("db.user");
        String sDBPassword = config.getProperty("db.password");
        if (sDBUser != null && sDBPassword != null && sDBPassword.startsWith("crypt:")) {
            // La clave esta encriptada.
            AltEncrypter cypher = new AltEncrypter("cypherkey" + sDBUser);
            sDBPassword = cypher.decrypt(sDBPassword.substring(6));
        }
        jtxtDbUser.setText(sDBUser);
        jtxtDbUser.getDocument().addDocumentListener(dirty);
        jtxtDbPassword.setText(sDBPassword);
        jtxtDbPassword.getDocument().addDocumentListener(dirty);

        jtxtMachineHostname.setText(config.getProperty("machine.hostname"));
        jtxtMachineHostname.getDocument().addDocumentListener(dirty);

        dirty.setDirty(false);
    }

    public void saveProperties(AppConfig config) {
        config.setProperty("server.backoffice", jtxtServerURL.getText());
        config.setProperty("db.user", jtxtDbUser.getText());
        AltEncrypter cypher = new AltEncrypter("cypherkey" + jtxtDbUser.getText());
        config.setProperty("db.password", "crypt:" + cypher.encrypt(new String(jtxtDbPassword.getPassword())));
        config.setProperty("machine.hostname", jtxtMachineHostname.getText());
        dirty.setDirty(false);
    }

    private void initComponents() {
        this.addOptionsContainer("Label.Config.API");
        this.jtxtServerURL = this.addTextParam("Label.Config.APIHost");
        this.jtxtDbUser = this.addTextParam("Label.Config.APILogin");
        this.jtxtDbPassword = this.addPasswordParam("Label.Config.APIPassword");
        this.jtxtMachineHostname = this.addTextParam("Label.Config.CashRegisterName");
    }

    private javax.swing.JTextField jtxtServerURL;
    private javax.swing.JPasswordField jtxtDbPassword;
    private javax.swing.JTextField jtxtDbUser;
    private javax.swing.JTextField jtxtMachineHostname;
}
