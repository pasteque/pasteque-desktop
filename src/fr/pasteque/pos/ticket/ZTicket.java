//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This is the session summary retreived form server. Used only locally.
 * For the actual ZTicket that is printed and built from this class,
 * see fr.pasteque.pos.panels.PaymentsModel.
 */
public class ZTicket
{
    private int cashRegisterId;
    private int sequence;
    private int ticketCount;
    private Integer custCount;
    private int paymentCount;
    private Double cs;
    private List<ZTicket.Payment> payments;
    private List<ZTicket.CustomerBalance> custBalances;
    private List<ZTicket.Tax> taxes;
    private List<ZTicket.Category> catSales;
    private List<ZTicket.CategoryTax> catTaxes;

    public ZTicket(CashSession session) {
        this.cashRegisterId = session.getCashRegisterId();
        this.sequence = session.getSequence();
        this.payments = new ArrayList<ZTicket.Payment>();
        this.custBalances = new ArrayList<ZTicket.CustomerBalance>();
        this.taxes = new ArrayList<ZTicket.Tax>();
        this.catSales = new ArrayList<ZTicket.Category>();
        this.catTaxes = new ArrayList<ZTicket.CategoryTax>();
    }

    public ZTicket(JSONObject o) {
        this.cashRegisterId = o.getInt("cashRegister");
        this.sequence = o.getInt("sequence");
        this.ticketCount = o.getInt("ticketCount");
        if (!o.isNull("custCount")) {
            this.custCount = o.getInt("custCount");
        }
        this.cs = o.getDouble("cs");
        this.paymentCount = o.getInt("paymentCount");
        this.payments = new ArrayList<ZTicket.Payment>();
        this.custBalances = new ArrayList<ZTicket.CustomerBalance>();
        this.taxes = new ArrayList<ZTicket.Tax>();
        this.catSales = new ArrayList<ZTicket.Category>();
        this.catTaxes = new ArrayList<ZTicket.CategoryTax>();
        JSONArray aPayments = o.getJSONArray("payments");
        for (int i = 0; i < aPayments.length(); i++) {
            JSONObject oPayment = aPayments.getJSONObject(i);
            ZTicket.Payment p = new ZTicket.Payment(oPayment.getString("type"),
                    oPayment.getDouble("amount"), oPayment.getInt("currency"),
                    oPayment.getDouble("currencyAmount"));
            this.payments.add(p);
        }
        JSONArray aCustBalances = o.getJSONArray("custBalances");
        for (int i = 0; i < aCustBalances.length(); i++) {
            JSONObject oCustBalance = aCustBalances.getJSONObject(i);
            ZTicket.CustomerBalance b = new ZTicket.CustomerBalance(String.valueOf(oCustBalance.getInt("customer")),
                    oCustBalance.getDouble("balance"));
            this.custBalances.add(b);
        }
        JSONArray aTaxes = o.getJSONArray("taxes");
        for (int i = 0; i < aTaxes.length(); i++) {
            JSONObject oTax = aTaxes.getJSONObject(i);
            ZTicket.Tax t = new ZTicket.Tax(String.valueOf(oTax.getInt("tax")),
                    oTax.getDouble("base"), oTax.getDouble("amount"));
            this.taxes.add(t);
        }
        JSONArray aCat = o.getJSONArray("catSales");
        for (int i = 0; i < aCat.length(); i++) {
            JSONObject oCat = aCat.getJSONObject(i);
            ZTicket.Category c = new ZTicket.Category(String.valueOf(oCat.getInt("category")),
                    oCat.getDouble("amount"));
            this.catSales.add(c);
        }
        JSONArray aCatTax = o.getJSONArray("catTaxes");
        for (int i = 0; i < aCatTax.length(); i++) {
            JSONObject oCatTax = aCatTax.getJSONObject(i);
            ZTicket.CategoryTax ct = new ZTicket.CategoryTax(oCatTax.getString("reference"),
                    oCatTax.getString("label"), oCatTax.getInt("tax"),
                    oCatTax.getDouble("base"), oCatTax.getDouble("amount"));
            this.catTaxes.add(ct);
        }
    }

    public int getCashRegisterId() { return this.cashRegisterId; }
    public int getSequence() { return this.sequence; }

    public Double getConsolidatedSales() { return this.cs; }
    public Integer getCustomersCount() { return this.custCount; }
    public int getTicketCount() { return this.ticketCount; }
    public int getPaymentCount() { return this.paymentCount; }
    public List<ZTicket.Payment> getPayments() { return this.payments; }
    public List<ZTicket.CustomerBalance> getCustBalances() {
        return this.custBalances;
    }
    public List<ZTicket.Tax> getTaxes() { return this.taxes; }
    public List<ZTicket.Category> getCategories() { return this.catSales; }
    public List<ZTicket.CategoryTax> getCatTaxes() { return this.catTaxes; }

    public class Payment {
        private String type;
        private double amount;
        private int currencyId;
        private double currencyAmount;

        public Payment(String type, double amount, int currencyId,
                double currencyAmount) {
            this.type = type;
            this.amount = amount;
            this.currencyId = currencyId;
            this.currencyAmount = currencyAmount;
        }
        public String getType() { return this.type; }
        public double getAmount() { return this.amount; }
        public int getCurrencyId() { return this.currencyId; }
        public double getCurrencyAmount() { return this.currencyAmount; }
    }

    public class CustomerBalance {
        private String customerId;
        private double balance;

        public CustomerBalance(String id, double balance) {
            this.customerId = id;
            this.balance = balance;
        }
        public String getCustomerId() { return this.customerId; }
        public double getBalance() { return this.balance; }
    }

    public class Tax {
        private String taxId;
        private double base;
        private double amount;

        public Tax(String id, double base, double amount) {
            this.taxId = id;
            this.base = base;
            this.amount = amount;
        }
        public String getId() { return this.taxId; }
        public double getBase() { return this.base; }
        public double getAmount() { return this.amount; }
    }

    public class Category {
        private String catId;
        private double amount;

        public Category(String id, double amount) {
            this.catId = id;
            this.amount = amount;
        }
        public String getId() { return this.catId; }
        public double getAmount() { return this.amount; }
    }

    public class CategoryTax {
        private String catReference;
        private String catLabel;
        private int taxId;
        private double base;
        private double amount;
        public CategoryTax(String catReference, String catLabel, int taxId,
                double base, double amount) {
            this.catReference = catReference;
            this.catLabel = catLabel;
            this.taxId = taxId;
            this.base = base;
            this.amount = amount;
        }
        public String getCatReference() { return this.catReference; }
        public String getCatLabel() { return this.catLabel; }
        public int getTaxId() { return this.taxId; }
        public double getBase() { return this.base; }
        public double getAmount() { return this.amount; }
    }
}
