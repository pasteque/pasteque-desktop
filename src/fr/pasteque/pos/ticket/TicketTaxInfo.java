//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import fr.pasteque.basic.BasicException;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.util.Price;
import java.io.Serializable;
import org.json.JSONObject;

@SuppressWarnings("serial")
public class TicketTaxInfo implements Serializable
{
    //TODO v9: add serialVersionUID not to mess with local caches
    //private static final long serialVersionUID = 5739915106563018654L;

    private TaxInfo tax;
    /** Subtotal holds either untaxed or taxed values.
     * Once finalize is called, it is set to its final value.
     * This is pretty ugly, but I'm lazy. */
    private double subtotal;
    private double taxtotal;

    /** Creates a new instance of TicketTaxInfo */
    public TicketTaxInfo(TaxInfo tax) {
        this.tax = tax;
        subtotal = 0.0;
        taxtotal = 0.0;
    }

    public TicketTaxInfo(JSONObject o) {
        DataLogicSales dlSales = new DataLogicSales();
        try {
            this.tax = dlSales.getTax(String.valueOf(o.getInt("tax")));
        } catch (BasicException e) {
            // The given Id is not present anymore in cache
            String name = Formats.PERCENT.formatValue(o.getDouble("rate") * 100);
            this.tax = new TaxInfo("", name, "", null, "", "",
                    o.getDouble("rate"), false, 9999);
            e.printStackTrace(); // TODO: ugly exception handling
        }
        this.subtotal = o.getDouble("base");
        this.taxtotal = o.getDouble("amount");
    }

    public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        o.put("base", this.getSubTotal());
        o.put("amount", this.getTaxAmount());
        o.put("tax", Integer.parseInt(this.tax.getId()));
        return o;
    }

    public TaxInfo getTaxInfo() { return tax; }

    /**
     * Add a value to get or extract tax from. It must be called
     * before finalize.
     */
    public void add(double dValue) {
        subtotal += dValue;
    }

    /**
     * Apply a discount and round value. It must be called before finalize.
     */
    public void applyDiscount(double discountRate) {
        this.subtotal = Price.discount(this.subtotal, discountRate);
    }

    /**
     * Compute the final values. Once finalized, the getter can be called.
     * @param taxIncluded If the tax should be extracted from the sum or
     * computed from it.
     */
    public void finalize(boolean taxIncluded) {
        if (taxIncluded) {
            double tax = Price.extractTax(this.subtotal, this.tax.getRate());
            double base = Price.untax(this.subtotal, this.tax.getRate());
            this.subtotal = base;
            this.taxtotal = tax;
        } else {
            double tax = Price.getTax(this.subtotal, this.tax.getRate());
            //this.subtotal is already correct
            this.taxtotal = tax;
        }
    }
    public TaxInfo getTax() {
        return this.tax;
    }
    /** Get the base amount. Useable after finalize has been called. */
    public double getSubTotal() { return subtotal; }
    /** Get the tax amount. Useable after finalize has been called. */
    public double getTaxAmount() { return taxtotal; }
    /** @deprecated
     * Get the base + tax amount. Useable after finalize has been called.
     * Deprecated because meaningless. */
    public double getTotal() { return subtotal + taxtotal; }

    public String printSubTotal() {
        return Formats.CURRENCY.formatValue(Double.valueOf(this.getSubTotal()));
    }
    public String printTax() {
        return Formats.CURRENCY.formatValue(Double.valueOf(this.getTaxAmount()));
    }
    public String printTotal() {
        return Formats.CURRENCY.formatValue(Double.valueOf(this.getTotal()));
    }
}
