//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.io.*;
import fr.pasteque.pos.util.StringUtils;
import fr.pasteque.format.Formats;
import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.util.Price;
import java.util.Properties;
import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class TicketLineInfo implements Serializable
{
    private static final long serialVersionUID = 6608012948284450199L;

    private double multiply;
    /** Either taxed or untaxed unit price. */
    private double price;
    /** Flag to check if price is stored with or without taxes.
     * Only for internal price computation (see get/setPrice[Tax]). */
    private boolean taxIncluded;
    private double discountRate;
    private TaxInfo tax;
    private Properties attributes;
    private String productid;
    /** Unreliable Duplication of attributes.getProperty("product.name") */
    private String productLabel;
    private String attsetinstid;
    private boolean subproduct;

    private TicketLineInfo() {
        init(null, null, 0.0, 0.0, null, true, new Properties());
    }

    /** Create a line from a product.
     * @param product The product for the line.
     * @param dMultiply The quantity for the line.
     * @param dPrice The price for quantity = 1.
     * @param tax The tax to apply to the price.
     * @param taxIncluded Boolean to set if price is with or without tax.
     * @param attributes Standard properties are set automatically
     * from the product. For a custom product, use "product.name",
     * "product.com" and "product.scale". */
    public TicketLineInfo(ProductInfoExt product, double dMultiply,
            double dPrice, TaxInfo tax, boolean taxIncluded,
            Properties attributes) {
        String pid;
        if (product == null) {
            pid = null;
        } else {
            pid = product.getID();
            this.productLabel = product.getName();
            attributes.setProperty("product.name", product.getName());
            attributes.setProperty("product.com",
                    product.isCom() ? "true" : "false");
            attributes.setProperty("product.scale",
                    product.isScale() ? "true" : "false");
            if (product.getAttributeSetID() != null) {
                attributes.setProperty("product.attsetid",
                        product.getAttributeSetID());
            }
            attributes.setProperty("product.taxcategoryid",
                    product.getTaxCategoryID());
            if (product.getCategoryID() != null) {
                attributes.setProperty("product.categoryid",
                        product.getCategoryID());
            }
        }
        init(pid, null, dMultiply, dPrice, tax, taxIncluded, attributes);
    }

    public TicketLineInfo(TicketLineInfo line) {
        init(line.productid, line.attsetinstid, line.multiply, line.price,
                line.tax, line.taxIncluded,
                (Properties) line.attributes.clone());
        this.subproduct = line.isSubproduct();
        this.discountRate = line.discountRate;
    }

    private void init(String productid, String attsetinstid, double dMultiply,
            double dPrice, TaxInfo tax, boolean taxIncluded,
            Properties attributes) {
        this.productid = productid;
        this.productLabel = attributes.getProperty("product.name");
        this.attsetinstid = attsetinstid;
        this.multiply = dMultiply;
        this.price = dPrice;
        this.tax = tax;
        this.taxIncluded = taxIncluded;
        this.attributes = attributes;
        this.subproduct = false;
    }

    public JSONObject toJSON(int dispOrder) {
        JSONObject o = new JSONObject();
        o.put("dispOrder", dispOrder);
        o.put("productLabel", this.attributes.getProperty("product.name"));
        o.put("quantity", this.multiply);
        if (!this.taxIncluded) {
            o.put("unitPrice", this.price);
            o.put("taxedUnitPrice", JSONObject.NULL);
            o.put("price", this.getFullSubValue());
            o.put("taxedPrice", JSONObject.NULL);
            o.put("finalPrice", this.getSubValue());
            o.put("finalTaxedPrice", JSONObject.NULL);
        } else {
            o.put("unitPrice", JSONObject.NULL);
            o.put("taxedUnitPrice", this.price);
            o.put("price", JSONObject.NULL);
            o.put("taxedPrice", this.getFullTax());
            o.put("finalPrice", JSONObject.NULL);
            o.put("finalTaxedPrice", this.getValue());
        }
        o.put("taxRate", this.tax.getRate());
        o.put("discountRate", this.discountRate);
        if (this.productid != null) {
            o.put("product", this.productid);
        } else {
            o.put("product", JSONObject.NULL);
        }
        o.put("tax", this.tax.getId());
        return o;
    }

    public TicketLineInfo(JSONObject o) throws BasicException {
        ProductInfoExt product = null;
        DataLogicSales dlSales = new DataLogicSales();
        if (!o.isNull("product")) {
            this.productid = String.valueOf(o.getInt("product"));
            product = dlSales.getProductInfo(this.productid);
        }
        this.attributes = new Properties();
        if (product != null) {
            attributes.setProperty("product.name", product.getName());
            attributes.setProperty("product.com",
                    product.isCom() ? "true" : "false");
            attributes.setProperty("product.scale",
                    product.isScale() ? "true" : "false");
            attributes.setProperty("product.taxcategoryid",
                    product.getTaxCategoryID());
            if (product.getCategoryID() != null) {
                attributes.setProperty("product.categoryid",
                        product.getCategoryID());
            }
        } else {
            this.productLabel = o.getString("productLabel");
            attributes.setProperty("product.name", this.productLabel);
        }
        this.multiply = o.getDouble("quantity");
        if (o.isNull("unitPrice")) {
            this.taxIncluded = true;
            this.price = o.getDouble("taxedUnitPrice");
        } else {
            this.taxIncluded = false;
            this.price = o.getDouble("unitPrice");
        }
        this.tax = dlSales.getTax(String.valueOf(o.getInt("tax")));
        this.discountRate = o.getDouble("discountRate");
    }

    public TicketLineInfo copyTicketLine() {
        TicketLineInfo l = new TicketLineInfo();
        l.productid = this.productid;
        l.attsetinstid = this.attsetinstid;
        l.multiply = this.multiply;
        l.price = this.price;
        l.taxIncluded = this.taxIncluded;
        l.tax = this.tax;
        l.discountRate = this.discountRate;
        l.attributes = (Properties) this.attributes.clone();
        l.subproduct = this.subproduct;
        return l;
    }

    /** Merge the given ticket line with the current instance.
     * It does nothing if the lines are not mergeable.
     * @param ticketLine the ticketline to merge. */
    public void merge(TicketLineInfo ticketLine) {
        if (this.canMerge(ticketLine)) {
            this.adjustQuantity(ticketLine.getMultiply());
        }
    }

    /** Lines are mergeable if they have the same (sub)product,
     * same price and same discount. */
    public boolean canMerge(TicketLineInfo line) {
        if ((line.productid == null && this.productid != null)
                || line.productid != null && this.productid == null) {
            return false;
        }
        boolean samePrdId = ((line.productid == null && this.productid == null)
                || (line.productid.equals(this.productid)));
        boolean samePrdLabel = this.printName().equals(line.printName());
        return samePrdId && samePrdLabel && line.price == this.price
                && line.taxIncluded == this.taxIncluded
                && line.subproduct == this.subproduct
                && line.discountRate == this.discountRate;
    }

    /** Reset the price and tax to the one from the product. */
    private void resetPrice(CatalogDataSource cds) throws BasicException {
            ProductInfoExt p = cds.getProductInfo(this.getProductID());
            TaxInfo tax = cds.getTax(p.getTaxId());
            this.setTaxInfo(tax);
            this.setPrice(p.getPriceSell());
    }

    public void applyTariffArea(TariffInfo area, CatalogDataSource cds)
            throws BasicException {
        this.resetPrice(cds);
        if (area == null) {
            return;
        }
        TariffAreaPrice price = cds.getTariffAreaPrice(area.getID(),
                this.getProductID());
        if (price != null && (price.getTaxId() != null
                || price.getPrice() != null)) {
            if (price.getTaxId() != null) {
                double oldTaxedPrice = this.getPriceTax();
                this.setTaxInfo(cds.getTax(String.valueOf(price.getTaxId())));
                if (price.getPrice() != null) {
                    this.setPrice(price.getPrice());
                } else {
                    this.setPriceTax(oldTaxedPrice);
                }
            } else {
                this.setPrice(price.getPrice());
            }
        } else {
            this.resetPrice(cds);
        }
    }

    public String getProductID() { return productid; }
    public String getProductName() {
        return attributes.getProperty("product.name");
    }
    public String getProductAttSetId() {
        return attributes.getProperty("product.attsetid");
    }
    public String getProductAttSetInstDesc() {
        return attributes.getProperty("product.attsetdesc", "");
    }
    public void setProductAttSetInstDesc(String value) {
        if (value == null) {
            attributes.remove(value);
        } else {
            attributes.setProperty("product.attsetdesc", value);
        }
    }
    public String getProductAttSetInstId() { return attsetinstid; }
    public void setProductAttSetInstId(String value) {
        attsetinstid = value;
    }
    @Deprecated
    /** Use {@link #isSubproduct()} instead. */
    public boolean isProductCom() {
        return "true".equals(attributes.getProperty("product.com"));
    }
    public boolean isProductScale() {
        return "true".equals(attributes.getProperty("product.scale"));
    }
    public String getProductTaxCategoryID() {
        return (attributes.getProperty("product.taxcategoryid"));
    }
    public String getProductCategoryID() {
        return (attributes.getProperty("product.categoryid"));
    }

    public double getMultiply() {
        return this.multiply;
    }
    public void setMultiply(double dValue) {
        this.multiply = dValue;
    }
    /** Add or remove quantity. */
    public void adjustQuantity(double qty) {
        this.multiply += qty;
    }
    public double getDiscountRate() { return this.discountRate; }
    public void setDiscountRate(double rate) { this.discountRate = rate; }
    public boolean hasDiscount() { return this.discountRate > 0.0; }
    /** @deprecated
     * Get untaxed unit price without discount.
     * Not reliable. Deprecated until resurected by a B2B feature. */
    public double getFullPrice() {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.untax(this.price, this.getTaxRate());
        } else {
            return this.price;
        }
    }
    /** Get untaxed unit price with discount.
     * Not reliable, only informative. */
    public double getPrice() {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.discount(this.getFullPrice(), this.discountRate);
        } else {
            return Price.discount(this.price, this.discountRate);
        }
    }
    /** Set the untaxed unit price. */
    public void setPrice(double dValue) {
        if (this.taxIncluded && this.getTaxRate() != 0.0) {
            this.price = Price.addTax(dValue, this.getTaxRate());
        } else {
            this.price = dValue;
        }
    }
    /** Get taxed unit price without discount. */
    public double getFullPriceTax() {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            return this.price;
        } else {
            return Price.untax(this.price, this.getTaxRate());
        }
    }
    /** Get taxed unit price with discount. */
    public double getPriceTax() {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.discount(this.price, this.discountRate);
        } else {
            return Price.discount(this.getFullPriceTax(), this.discountRate);
        }
    }
    public void setPriceTax(double dValue) {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            this.price = dValue;
        } else {
            this.price = Price.untax(dValue, this.getTaxRate());
        }
    }
    public TaxInfo getTaxInfo() { return tax; }
    public void setTaxInfo(TaxInfo value) { tax = value; }
    public String getProperty(String key) {
        return attributes.getProperty(key);
    }
    public String getProperty(String key, String defaultvalue) {
        return attributes.getProperty(key, defaultvalue);
    }
    public void setProperty(String key, String value) {
        attributes.setProperty(key, value);
    }
    public Properties getProperties() { return attributes; }
    public double getTaxRate() { return tax == null ? 0.0 : tax.getRate(); }
    /** Get the untaxed price with quantity before discount.
     * This is the base value to apply discount when !taxIncluded.
     * Not reliable when taxIncluded, use getFullTax instead. */
    public double getFullSubValue() {
        if (!this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.inQuantity(this.price, this.multiply);
        } else {
            return Price.untax(this.getFullTax(), this.getTaxRate());
        }
    }
    /** Get the taxed price with quantity before discount.
     * This is the base value to apply discount when taxIncluded.
     * Not reliable when !taxIncluded, use getFullSubValue instead. */
    public double getFullTax() {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.inQuantity(this.price, this.multiply);
        } else {
            return Price.addTax(this.getFullSubValue(), this.getTaxRate());
        }
    }
    /** Get the final untaxed price after discount.
     * This is the final value to use when !taxIncluded.
     * Not reliable when taxIncluded, use getValue instead. */
    public double getSubValue() {
        if (!this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.discount(this.getFullSubValue(), this.discountRate);
        } else {
            return Price.untax(this.getValue(), this.getTaxRate());
        }
    }
    /** Get the final taxed price after discount.
     * This is the final value to use when taxIncluded.
     * Not reliable when !taxIncluded, use getSubValue instead. */
    public double getValue() {
        if (this.taxIncluded || this.getTaxRate() == 0.0) {
            return Price.discount(this.getFullTax(), this.discountRate);
        } else {
            return Price.addTax(this.getSubValue(), this.getTaxRate());
        }
    }
    /** @deprecated
     * Get total tax amount with discount. This is not reliable, taxes must be
     * computed from the sum of the bases to avoid rounding issues. */
    public double getTax() {
        if (this.getTaxRate() == 0.0) { return 0.0; }
        if (this.taxIncluded) {
            return Price.extractTax(this.getValue(), this.getTaxRate());
        } else {
            return Price.getTax(this.getSubValue(), this.getTaxRate());
        }
    }
    /** @deprecated
     * Get price with quantity and taxes (without discount).
     * This is an alias to getFullTax*/
    public double getFullValue() {
        return this.getFullTax();
    }

    public boolean isSubproduct() { return this.subproduct; }
    public void setSubproduct(boolean subproduct) {
        this.subproduct = subproduct;
    }

    public String printName() {
        return StringUtils.encodeXML(attributes.getProperty("product.name"));
    }

    public String printMultiply() {
        return Formats.DOUBLE.formatValue(multiply);
    }

    public String printFullPrice() {
        return Formats.CURRENCY.formatValue(this.getFullPrice());
    }
    public String printPrice() {
        return Formats.CURRENCY.formatValue(getPrice());
    }

    public String printFullPriceTax() {
        return Formats.CURRENCY.formatValue(this.getFullPriceTax());
    }
    public String printPriceTax() {
        return Formats.CURRENCY.formatValue(getPriceTax());
    }

    public String printFullTax() {
        return Formats.CURRENCY.formatValue(this.getFullTax());
    }
    public String printTax() {
        return Formats.CURRENCY.formatValue(getTax());
    }

    public String printTaxRate() {
        return Formats.PERCENT.formatValue(getTaxRate());
    }

    public String printFullSubValue() {
        return Formats.CURRENCY.formatValue(this.getFullSubValue());
    }
    public String printSubValue() {
        return Formats.CURRENCY.formatValue(getSubValue());
    }

    public String printFullValue() {
        return Formats.CURRENCY.formatValue(this.getFullValue());
    }
    public String printValue() {
        return Formats.CURRENCY.formatValue(getValue());
    }

    public String printDiscountRate() {
        return Formats.PERCENT.formatValue(this.discountRate);
    }

    public String printDiscountAmount() {
        double discount = 0.0;
        if (this.taxIncluded) {
            discount = Price.add(this.getValue(), -this.getFullTax());
        } else {
            discount = Price.add(this.getSubValue(), -this.getFullSubValue());
        }
        return Formats.CURRENCY.formatValue(discount);
    }
}
