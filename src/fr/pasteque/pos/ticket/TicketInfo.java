//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.LocalRes;
import fr.pasteque.format.DateUtils;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.forms.AppUser;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.payment.PaymentInfo;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.util.StringUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class TicketInfo implements Serializable
{
    private static final long serialVersionUID = 2765650092387265178L;

    public static final int RECEIPT_NORMAL = 0;
    public static final int RECEIPT_REFUND = 1;
    public static final int RECEIPT_PAYMENT = 2;

    private static final String[] TAX_CODES = {"a", "b", "c", "d", "e", "f", "g", "h",
        "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z"};

    /** Database ticket id. It is an int parsed to a string.
     * Used to check if the ticket was saved or not.
     * When the TicketInfo is an order, it may be null (new order)
     * or set to the order id.
     * When transforming into a Ticket through validate(), it is
     * set to null until it is set by the server. */
    private String m_sId;
    /** Flag to distinguish Ticket and Order. When true, this is a Ticket.
     * When false, this is an Order.
     * This is a hack until those two are distinct classes.
     * It is set on load or in validate(). */
    private boolean isTicket;
    private int tickettype;
    /** Cash register id. 0 when not set (order). */
    private int cashRegisterId;
    /** Cash session sequence. 0 when not set (order). */
    private int sequence;
    /** Ticket number. 0 when not set (order). */
    private int m_iTicketId;
    private java.util.Date m_dDate;
    /** @deprecated */
    private Properties attributes;
    private UserInfo m_User;
    private CustomerInfoExt m_Customer;
    /** Modification of the customer's balance with this ticket. */
    private double custBalance;
    private String m_sActiveCash;
    private List<TicketLineInfo> m_aLines;
    private List<PaymentInfo> payments;
    private List<TicketTaxInfo> taxes;
    private Integer customersCount;
    private Integer tariffAreaId;
    private Integer discountProfileId;
    private double discountRate;

    /** Create a new Order. */
    public TicketInfo() {
        this.isTicket = false;
        tickettype = RECEIPT_NORMAL;
        m_iTicketId = 0; // incrementamos
        m_dDate = new Date();
        attributes = new Properties();
        m_User = null;
        m_Customer = null;
        m_sActiveCash = null;
        m_aLines = new ArrayList<TicketLineInfo>(); // vacio de lineas

        payments = new ArrayList<PaymentInfo>();
        taxes = null;
    }

    /** Load an Order from server data. It is called only from
     * SharedTicketInfo constructors. */
    public static TicketInfo sharedTicketInfo(JSONObject o) throws BasicException {
        TicketInfo tkt = new TicketInfo();
        tkt.isTicket = false;
        if (!o.isNull("tariffArea")) {
            tkt.tariffAreaId = o.getInt("tariffArea");
        }
        if (!o.isNull("customer")) {
            String custId = o.getString("customer");
            DataLogicCustomers dlCust = new DataLogicCustomers();
            tkt.m_Customer = dlCust.getCustomer(custId);
        }
        if (!o.isNull("custCount")) {
            tkt.customersCount = o.getInt("custCount");
        }
        if (!o.isNull("discountProfile")) {
            tkt.discountProfileId = o.getInt("discountProfile");
        }
        tkt.discountRate = o.getDouble("discountRate");
        JSONArray jsLines = o.getJSONArray("lines");
        for (int i = 0; i < jsLines.length(); i++) {
            JSONObject jsLine = jsLines.getJSONObject(i);
            tkt.m_aLines.add(new TicketLineInfo(jsLine));
        }
        return tkt;
    }

    /** Load a Ticket from server. For Orders, see SharedTicketInfo
     * (and not TicketInfo.sharedTicketInfo). */
    public TicketInfo(JSONObject o) throws BasicException {
        this.m_sId = String.valueOf(o.getInt("id"));
        this.tickettype = RECEIPT_NORMAL;
        this.cashRegisterId = o.getInt("cashRegister");
        this.sequence = o.getInt("sequence");
        this.m_iTicketId = o.getInt("number");
        this.m_dDate = DateUtils.readSecTimestamp(o.getLong("date"));
        DataLogicSystem dlSystem = new DataLogicSystem();
        AppUser user = dlSystem.getPeople(String.valueOf(o.getInt("user")));
        this.m_User = new UserInfo(user.getId(), user.getName());
        if (!o.isNull("customer")) {
            DataLogicCustomers dlCust = new DataLogicCustomers();
            this.m_Customer = dlCust.getCustomer(String.valueOf(o.getInt("customer")));
        }
        if (!o.isNull("custBalance")) {
            this.custBalance = o.getDouble("custBalance");
        }
        if (!o.isNull("custCount")) {
            this.customersCount = o.getInt("custCount");
        }
        if (!o.isNull("tariffArea")) {
            this.tariffAreaId = o.getInt("tariffArea");
        }
        if (!o.isNull("discountProfile")) {
            this.discountProfileId = o.getInt("discountProfile");
        }
        this.discountRate = o.getDouble("discountRate");
        this.m_aLines = new ArrayList<TicketLineInfo>();
        JSONArray jsLines = o.getJSONArray("lines");
        for (int i = 0; i < jsLines.length(); i++) {
            JSONObject jsLine = jsLines.getJSONObject(i);
            this.m_aLines.add(new TicketLineInfo(jsLine));
        }
        this.payments = new ArrayList<PaymentInfo>();
        JSONArray jsPayments = o.getJSONArray("payments");
        for (int i = 0; i < jsPayments.length(); i++) {
            JSONObject jsPay = jsPayments.getJSONObject(i);
            this.payments.add(new PaymentInfo(jsPay));
        }
        this.taxes = new ArrayList<TicketTaxInfo>();
        JSONArray jsTaxes = o.getJSONArray("taxes");
        for (int i = 0; i < jsTaxes.length(); i++) {
            JSONObject jsTax = jsTaxes.getJSONObject(i);
            this.taxes.add(new TicketTaxInfo(jsTax));
        }
        this.attributes = new Properties();
    }

    /** Format a Ticket to send to the server.
     * For Orders see SharedTicketInfo. */
    public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        if (this.m_sId != null) {
            o.put("id", this.m_sId);
        }
        // These can be 0, in that case the server will reject the ticket
        o.put("cashRegister", this.cashRegisterId);
        o.put("sequence", this.sequence);
        o.put("number", this.m_iTicketId);
        o.put("date", DateUtils.toSecTimestamp(this.m_dDate));
        if (this.customersCount != null) {
            o.put("custCount", this.customersCount);
        } else {
            o.put("custCount", JSONObject.NULL);
        }
        o.put("price", this.getSubTotalWoDiscount());
        o.put("taxedPrice", this.getFullTotal());
        o.put("discountRate", this.discountRate);
        o.put("finalPrice", this.getSubTotal());
        o.put("finalTaxedPrice", this.getTotal());
        o.put("custBalance", this.custBalance);
        if (this.m_User != null) {
            o.put("user", Integer.parseInt(this.m_User.getId()));
        } else {
            // This will fail too
            o.put("user", JSONObject.NULL);
        }
        JSONArray lines = new JSONArray();
        int i = 0;
        for (TicketLineInfo l : this.m_aLines) {
            lines.put(l.toJSON(i));
            i++;
        }
        o.put("lines", lines);
        JSONArray taxes = new JSONArray();
        for (TicketTaxInfo t : this.taxes) {
            taxes.put(t.toJSON());
        }
        o.put("taxes", taxes);
        JSONArray payments = new JSONArray();
        i = 0;
        for (PaymentInfo p : this.payments) {
            JSONObject jsPay = p.toJSON();
            JSONObject jsBack = null;
            jsPay.put("dispOrder", i);
            if (!jsPay.isNull("back")) {
                jsBack = jsPay.getJSONObject("back");
                jsPay.remove("back");
            }
            payments.put(jsPay);
            i++;
            if (jsBack != null) {
                jsBack.put("dispOrder", i);
                payments.put(jsBack);
                i++;
            }
        }
        o.put("payments", payments);
        if (this.m_Customer != null) {
            o.put("customer", this.m_Customer.getId());
        } else {
            o.put("customer", JSONObject.NULL);
        }
        if (this.tariffAreaId != null) {
            o.put("tariffArea", this.tariffAreaId);
        } else {
            o.put("tariffArea", JSONObject.NULL);
        }
        if (this.discountProfileId != null) {
            o.put("discountProfile", this.discountProfileId);
        } else {
            o.put("discountProfile", JSONObject.NULL);
        }
        return o;
    }

    /** Serialize as shared ticket */
    public byte[] serialize() throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(m_sId);
        out.writeInt(tickettype);
        out.writeInt(m_iTicketId);
        out.writeObject(m_Customer);
        out.writeObject(m_dDate);
        out.writeObject(attributes);
        out.writeObject(m_aLines);
        out.writeObject(this.customersCount);
        out.writeObject(this.tariffAreaId);
        out.writeObject(this.discountProfileId);
        out.writeDouble(this.discountRate);
        out.flush();
        byte[] data = bos.toByteArray();
        out.close();
        return data;
    }

    /** Deserialize as shared ticket */
    public TicketInfo(byte[] data) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        ObjectInputStream in = new ObjectInputStream(bis);
        try {
            m_sId = (String) in.readObject();
            tickettype = in.readInt();
            m_iTicketId = in.readInt();
            m_Customer = (CustomerInfoExt) in.readObject();
            m_dDate = (Date) in.readObject();
            attributes = (Properties) in.readObject();
            m_aLines = (List<TicketLineInfo>) in.readObject();
            this.customersCount = (Integer) in.readObject();
            this.tariffAreaId = (Integer) in.readObject();
            this.discountProfileId = (Integer) in.readObject();
            this.discountRate = in.readDouble();
        } catch (ClassNotFoundException cnfe) {
            // Should never happen
            cnfe.printStackTrace();
        }
        in.close();
        m_User = null;
        m_sActiveCash = null;

        payments = new ArrayList<PaymentInfo>();
        taxes = null;
    }

    /** Copy an Order (used when splitting an Order) */
    public TicketInfo copyTicket() {
        TicketInfo t = new TicketInfo();
        if (this.m_sId != null) {
            t.m_sId = new String(this.m_sId);
        }
        t.isTicket = this.isTicket;
        t.tickettype = tickettype;
        t.m_iTicketId = m_iTicketId;
        t.m_dDate = m_dDate;
        t.m_sActiveCash = m_sActiveCash;
        t.attributes = (Properties) attributes.clone();
        t.m_User = m_User;
        t.m_Customer = m_Customer;
        if (this.customersCount != null) {
            t.customersCount = this.customersCount;
        }
        t.m_aLines = new ArrayList<TicketLineInfo>();
        for (TicketLineInfo l : m_aLines) {
            t.m_aLines.add(l.copyTicketLine());
        }

        t.payments = new LinkedList<PaymentInfo>();
        for (PaymentInfo p : payments) {
            t.payments.add(p.copyPayment());
        }
        if (this.tariffAreaId != null) {
            t.tariffAreaId = Integer.valueOf(this.tariffAreaId);
        }
        if (this.discountProfileId != null) {
            t.discountProfileId = Integer.valueOf(this.discountProfileId);
        }
        t.discountRate = this.discountRate;
        // taxes are not copied, must be calculated again.
        return t;
    }

    /** Check wether the Ticket or Order has been sent to the server
     * and was assigned an id. */
    public boolean isLocal() { return this.m_sId == null; }
    /** Get the database id assigned by server. */
    public String getId() { return m_sId; }
    /** Check wether this instance is a Ticket or an Order.
     * True = Ticket, False = Order. */
    public boolean isTicket() { return this.isTicket; }
    public int getTicketType() { return tickettype; }
    public void setTicketType(int tickettype) { this.tickettype = tickettype; }
    public int getCashRegisterId() { return this.cashRegisterId; }
    public int getSequence() { return this.sequence; }
    public int getNumber() { return m_iTicketId; }
    /** @deprecated */
    public int getTicketId() { return this.getNumber(); }

    /** @deprecated
     * This was what convert an order to a ticket. It sets the ticket number. */
    public void setTicketId(int iTicketId) { m_iTicketId = iTicketId; }

    /** Convert the order to a ticket. */
    public void validate(CashSession session, int number) {
        this.cashRegisterId = session.getCashRegisterId();
        this.sequence = session.getSequence();
        this.m_iTicketId = number;
        this.m_sId = null;
        this.isTicket = true;
    }
    /** Check if the ticket has been validated (has a fully defined id). */
    public boolean isValid() {
        return this.cashRegisterId != 0 && this.sequence != 0
                && this.m_iTicketId != 0;
    }

    /**
     * Check if the content of the order is empty (no data added)
     * @deprecated Use {@link fr.pasteque.pos.sales.SharedTicketInfo#isEmpty()} instead.
     * Will be removed in v9, to keep compatibility with custom scripts.
     * @return true when the ticket has no lines, no customer count or 0 and
     * is not assigned to a customer.
     */
    @Deprecated
    public boolean isOrderEmpty() {
        return this.getLinesCount() == 0
                && (!this.hasCustomersCount() || this.getCustomersCount() == 0)
                && this.getCustomer() == null;
    }

    /**
     * Get the name of the Order. It has no use for a Ticket but still returns
     * the ticket full number just in case.
     * @param info When not null, info is appended at the end. Otherwise, current
     * time or place is used instead.
     */
    public String getName(Object info) {
        if (this.isTicket) {
            return this.cashRegisterId + "-" + this.sequence
                    + "-" + this.m_iTicketId;
        }
        StringBuffer name = new StringBuffer();

        if (getCustomerId() != null) {
            name.append(m_Customer.toString());
            name.append(" - ");
        }

        if (info == null) {
            if (m_iTicketId == 0) {
                name.append("(" + Formats.TIME.formatValue(m_dDate) + ")");
            } else {
                name.append(Integer.toString(m_iTicketId));
            }
        } else {
            name.append(info.toString());
        }

        return name.toString();
    }

    public String getName() { return getName(null); }
    public java.util.Date getDate() { return m_dDate; }
    public void setDate(java.util.Date dDate) { m_dDate = dDate; }
    public UserInfo getUser() { return m_User; }
    public void setUser(UserInfo value) { m_User = value; }
    public CustomerInfoExt getCustomer() { return m_Customer; }
    public void setCustomer(CustomerInfoExt value) { m_Customer = value; }

    public String getCustomerId() {
        if (m_Customer == null) {
            return null;
        } else {
            return m_Customer.getId();
        }
    }

    /** Get the variation of the customer's balance of this ticket. */
    public double getCustBalance() { return this.custBalance; }
    /** Set the variation of the customer's balance of this ticket. */
    public void setCustBalance(double balance) { this.custBalance = balance; }

    /** @deprecated Not used since years. */
    public String getTransactionID(){
        return StringUtils.getCardNumber(); // random transaction ID
    }

    /** @deprecated Not used since years. */
    public String getReturnMessage(){
        return LocalRes.getIntString("button.ok");
    }

    public String getActiveCash() { return m_sActiveCash; }
    public String getProperty(String key) {
        return attributes.getProperty(key);
    }
    public String getProperty(String key, String defaultvalue) {
        return attributes.getProperty(key, defaultvalue);
    }
    public void setProperty(String key, String value) {
        attributes.setProperty(key, value);
    }
    public Properties getProperties() { return attributes; }
    public Integer getCustomersCount() { return this.customersCount; }
    public void setCustomersCount(Integer count) {
        if (count == null || count == 0) {
            this.customersCount = null;
        } else {
            this.customersCount = count;
        }
    }
    public void addCustomersCount(Integer count) {
        if (count == null) {
            return;
        }
        if (this.customersCount == null) {
            this.customersCount = count;
        } else {
            this.customersCount += count;
        }
    }
    public boolean hasCustomersCount() { return this.customersCount != null; }

    public Integer getTariffArea() { return this.tariffAreaId; }

    /**
     * Switch to an other tariff area and update the prices from the source.
     * @param area The new area.
     * @param cds The source to fetch prices from.
     * @throws BasicException When an error occurs reading prices from
     * the source.
     */
    public void setTariffArea(TariffInfo area, CatalogDataSource cds)
        throws BasicException {
        if ((this.tariffAreaId == null && area == null)
                || (area != null && this.tariffAreaId != null && area.getID() == this.tariffAreaId)) {
            return;
        }
        if (area == null) {
            this.tariffAreaId = null;
        } else {
            this.tariffAreaId = area.getID();
        }
        for (TicketLineInfo l : this.m_aLines) {
            if (!l.isProductCom()) { // Don't apply tariff area to composition lines
                l.applyTariffArea(area, cds);
            }
        }
    }

    public Integer getDiscountProfileId() { return this.discountProfileId; }
    public void setDiscountProfileId(Integer id) {
        this.discountProfileId = id;
    }
    public boolean hasDiscount() { return this.discountRate > 0.0; }
    public double getDiscountRate() { return this.discountRate; }
    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }
    public TicketLineInfo getLine(int index) { return m_aLines.get(index); }

    public void addLine(TicketLineInfo oLine) {
        m_aLines.add(oLine);
    }

    /** @deprecated
     * To be removed. Used only for product com (maybe compositions). */
    public void insertLine(int index, TicketLineInfo oLine) {
        m_aLines.add(index, oLine);
    }

    public void setLine(int index, TicketLineInfo oLine) {
        m_aLines.set(index, oLine);
    }

    public void removeLine(int index) {
        m_aLines.remove(index);
    }

    public int getLinesCount() {
        return m_aLines.size();
    }

    public double getArticlesCount() {
        double dArticles = 0.0;
        TicketLineInfo oLine;

        for (Iterator<TicketLineInfo> i = m_aLines.iterator(); i.hasNext();) {
            oLine = i.next();
            if (oLine.isProductScale()) {
                if (oLine.getPrice() >= 0) {
                    dArticles += 1;
                } else {
                    dArticles -= 1;
                }
            } else {
                if (oLine.getPrice() >= 0) {
                    dArticles += oLine.getMultiply();
                } else {
                    dArticles -= oLine.getMultiply();
                }
            }
        }

        return dArticles;
    }

    /** @deprecated
     * Get untaxed price without ticket discount.
     * Deprecated until resurrected by a B2B mode uses the untaxed prices
     * as a base instead of taxed prices. */
    public double getSubTotalWoDiscount() {
        double sum = 0.0;
        for (TicketLineInfo line : m_aLines) {
            sum = Price.add(sum, line.getSubValue());
        }
        return sum;
    }

    /** Get price without taxes and with discount. */
    public double getSubTotal() {
        double sum = 0.0;
        if (hasTaxesCalculated()) {
            // TODO: this assumes the B2C mode is used.
            for (TicketTaxInfo tax : this.taxes) {
                sum = Price.add(sum, tax.getSubTotal());
            }
            return sum;
        } else {
            // TODO: unaccurate, this shouldn't be used
            // Get the sum by tax rate
            for (TicketLineInfo line : m_aLines) {
                sum = Price.add(sum, line.getSubValue());
            }
            return Price.discount(sum, this.discountRate);
        }
    }

    /** Get tax amount with discount */
    public double getTax() {
        double sum = 0.0;
        if (hasTaxesCalculated()) {
            for (TicketTaxInfo tax : this.taxes) {
                sum = Price.add(sum, tax.getTaxAmount());
            }
            return sum;
        } else {
            // TODO: unaccurate, this shouldn't be used
            // Get the sums by tax rate
            for (TicketLineInfo line : m_aLines) {
                sum = Price.add(sum, line.getTax());
            }
            return Price.discount(sum, this.discountRate);
        }
    }

    /** Get taxed price before ticket discount. */
    public double getFullTotal() {
        double sum = 0.0;
        for (TicketLineInfo line : m_aLines) {
            // TODO: this assumes the B2C mode is used.
            sum = Price.add(sum, line.getValue());
        }
        return sum;
    }

    /** Get taxed price after discount */
    public double getTotal() {
        return Price.discount(getFullTotal(), this.discountRate);
    }

    public double getDiscountAmount() {
        return Price.add(getFullTotal(), -getTotal());
    }

    /** Get the amount effectively paid (excludes debt). */
    public double getTotalPaid() {
        double sum = 0.0;
        for (PaymentInfo p : payments) {
            if (!"debtpaid".equals(p.getMode().getCode())) {
                sum = Price.add(sum, p.getAmount());
            }
        }
        return sum;
    }

    public List<TicketLineInfo> getLines() { return m_aLines; }
    public void setLines(List<TicketLineInfo> l) { m_aLines = l; }
    public List<PaymentInfo> getPayments() { return payments; }
    public void setPayments(List<PaymentInfo> l) { payments = l; }
    public void resetPayments() { payments = new ArrayList<PaymentInfo>(); }
    public List<TicketTaxInfo> getTaxes() { return taxes; }
    public boolean hasTaxesCalculated() { return taxes != null; }
    public void setTaxes(List<TicketTaxInfo> l) { taxes = l; }
    public void resetTaxes() { taxes = null; }

    public TicketTaxInfo getTaxLine(TaxInfo tax) {
        for (TicketTaxInfo taxline : taxes) {
            if (tax.getId().equals(taxline.getTaxInfo().getId())) {
                return taxline;
            }
        }
        return new TicketTaxInfo(tax);
    }

    public TicketTaxInfo[] getTaxLines() {

        Map<String, TicketTaxInfo> m = new HashMap<String, TicketTaxInfo>();

        TicketLineInfo oLine;
        for (Iterator<TicketLineInfo> i = m_aLines.iterator(); i.hasNext();) {
            oLine = i.next();

            TicketTaxInfo t = m.get(oLine.getTaxInfo().getId());
            if (t == null) {
                t = new TicketTaxInfo(oLine.getTaxInfo());
                m.put(t.getTaxInfo().getId(), t);
            }
            t.add(oLine.getSubValue());
        }

        // return dSuma;
        Collection<TicketTaxInfo> avalues = m.values();
        return avalues.toArray(new TicketTaxInfo[avalues.size()]);
    }

    public String printId() {
        if (m_iTicketId > 0) {
            // valid ticket id
            return Formats.INT.formatValue(Integer.valueOf(m_iTicketId));
        } else {
            return "";
        }
    }

    public String printCashRegister() {
        DataLogicSystem dlSystem = new DataLogicSystem();
        try {
            CashRegisterInfo cr = dlSystem.getCashRegister(this.cashRegisterId);
            if (cr == null) {
                return Formats.INT.formatValue(Integer.valueOf(this.cashRegisterId));
            } else {
                return cr.getLabel();
            }
        } catch (BasicException e) {
            return Formats.INT.formatValue(Integer.valueOf(this.cashRegisterId));
        }
    }

    public String printDate() {
        return Formats.TIMESTAMP.formatValue(m_dDate);
    }

    public String printUser() {
        return m_User == null ? "" : m_User.getName();
    }

    public String printCustomer() {
        return m_Customer == null ? "" : m_Customer.getName();
    }

    public String printArticlesCount() {
        return Formats.DOUBLE.formatValue(Double.valueOf(getArticlesCount()));
    }

    public String printSubTotal() {
        return Formats.CURRENCY.formatValue(Double.valueOf(getSubTotal()));
    }

    public String printTax() {
        return Formats.CURRENCY.formatValue(Double.valueOf(getTax()));
    }

    public String printFullTotal() {
        return Formats.CURRENCY.formatValue(Double.valueOf(this.getFullTotal()));
    }
    public String printTotal() {
        return Formats.CURRENCY.formatValue(Double.valueOf(getTotal()));
    }

    public String printTotalPaid() {
        return Formats.CURRENCY.formatValue(Double.valueOf(getTotalPaid()));
    }

    public String printCustomersCount() {
        if (this.hasCustomersCount()) {
            return Formats.INT.formatValue(this.customersCount);
        } else {
            return "";
        }
    }

    public String printDiscountRate() {
        return Formats.PERCENT.formatValue(this.discountRate);
    }

    public String printDiscountAmount() {
        return Formats.CURRENCY.formatValue(Double.valueOf(-this.getDiscountAmount()));
    }

    public String printTaxCode(TicketLineInfo line) {
        TaxInfo tax = line.getTaxInfo();
        for (int i = 0; i < this.taxes.size(); i++) {
            TicketTaxInfo t = this.taxes.get(i);
            if (tax.equals(t.getTaxInfo())) {
                return TAX_CODES[i];
            }
        }
        return "?";
    }
    public String printTaxCode(TicketTaxInfo tax) {
        for (int i = 0; i < this.taxes.size(); i++) {
            TicketTaxInfo t = this.taxes.get(i);
            if (tax.getTaxInfo().equals(t.getTaxInfo())) {
                return TAX_CODES[i];
            }
        }
        return "?";
    }
}
