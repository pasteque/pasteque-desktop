//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.io.Serializable;
import org.json.JSONObject;

/** Model for Cash register config. */
@SuppressWarnings("serial")
public class CashRegisterInfo implements Serializable
{
    //TODO v9: add serialVersionUID not to mess with local caches
    //private static final long serialVersionUID = 3199568792999857484L;

    private int id;
    private String label;
    @SuppressWarnings("unused")
    //TODO v9: remove with serialVersionUID not to mess with local caches
    private String locationId;
    private int nextTicketId;
    private int nextSessionId;

    public CashRegisterInfo(String label, String locationId, int nextTicketId) {
        this.label = label;
        this.locationId = locationId;
        this.nextTicketId = nextTicketId;
    }

    public CashRegisterInfo(JSONObject o) {
        this.id = o.getInt("id");
        this.label = o.getString("label");
        this.locationId = "0"; // legacy, not used
        this.nextTicketId = o.getInt("nextTicketId");
        this.nextSessionId = o.getInt("nextSessionId");
    }

    public int getId() {
        return this.id;
    }

    public String getLabel() {
        return this.label;
    }

    public String getLocationId() {
        return "0";
    }

    public int getNextTicketId() {
        return this.nextTicketId;
    }

    public void incrementNextTicketId() {
        this.nextTicketId++;
    }

    public int getNextSessionId() {
        return this.nextSessionId;
    }

    public void incrementNextSessionId() {
        this.nextSessionId++;
    }
}
