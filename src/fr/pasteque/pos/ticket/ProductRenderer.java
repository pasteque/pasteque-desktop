//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.format.Formats;

/**
 * Search result list renderer for products
 * @author adrianromero
 *
 */
public class ProductRenderer extends DefaultListCellRenderer
{
    private static final long serialVersionUID = -4502643925509170561L;

    ThumbNailBuilder tnbprod;
    DataLogicSales dlSales;
    Map<String, CategoryInfo> catCache;

    /** Creates a new instance of ProductRenderer */
    public ProductRenderer() {
        tnbprod = new ThumbNailBuilder(32, 32, "product_default.png");
        dlSales = new DataLogicSales();
        catCache = new HashMap<String, CategoryInfo>();
    }

    public void clearCategoryCache() {
        this.catCache.clear();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, null, index, isSelected, cellHasFocus);

        ProductInfoExt prod = (ProductInfoExt) value;
        String catId = prod.getCategoryID();
        CategoryInfo cat = null;
        if (catId != null) {
            if (catCache.containsKey(catId)) {
                cat = catCache.get(catId);
            } else {
                try {
                    cat = dlSales.getCategory(catId);
                    catCache.put(catId, cat);
                } catch (BasicException e) {
                }
            }
        }
        String padding = "&nbsp;&nbsp;&nbsp;&nbsp;";
        String catLabel = "";
        if (cat != null) {
            catLabel = cat.getName();
        }
        if (prod != null) {
            String text = "<html>";
            if (!"".equals(prod.getName())) {
                text += prod.getName() + "<br>";
            }
            if (!"".equals(prod.getReference())) {
                text += padding + AppLocal.getIntString("label.prodrefabbrev")
                        + " " + prod.getReference() + "<br>";
            }
            if (!"".equals(catLabel)) {
                text += padding + AppLocal.getIntString("label.prodincat")
                        + " " + catLabel + "<br>";
            }
            text += padding + Formats.CURRENCY.formatValue(Double.valueOf(prod.getPriceSellTax()));
            setText(text);
            Image img = tnbprod.getThumbNail(prod.getImage());
            setIcon(img == null ? null :new ImageIcon(img));
        }
        return this;
    }
}
