//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import fr.pasteque.basic.BasicException;
import fr.pasteque.format.DateUtils;
import fr.pasteque.pos.caching.CashSessionsCache;
import fr.pasteque.pos.panels.PaymentsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class CashSession implements Serializable
{
    private static final long serialVersionUID = 2387629215096106232L;

    /** Close type for a daily close. See API. */
    public static final int CLOSE_SIMPLE = 0;
    /** Close type for a period close. See API. */
    public static final int CLOSE_PERIOD = 1;
    /** Close type for a fiscal year close. See API. */
    public static final int CLOSE_FYEAR = 2;

    /** @deprecated */
    private String id;
    private int cashRegisterId;
    private int sequence;
    private Date openDate;
    private Date closeDate;
    private Double openCash;
    private Double closeCash;
    private Double expectedCash;
    /** Close operation type. Set only on close. */
    private int closeType;
    /** Flag to detect if the local database was deleted (potentially with
     * unsaved tickets) between two runs. */
    private boolean continuous;
    /** CS sum of the period given by server and updated on close
     * with CS from ZTicket. Required for printing. */
    private double csPeriod;
    /** Backup for close rollback */
    private double csPeriodRollback;
    /** CS sum of the fiscal year given by server and updated on close
     * with CS from ZTicket. Required for printing. */
    private double csFYear;
    /** Backup for close rollback */
    private double csFYearRollback;
    /** Perpetual CS sum given by server and updated on close with CS
     * frow ZTicket. Required for printing. Null when the API doesn't manage
     * it yet.*/
    private Double csPerpetual;
    /** Local-only copy of the last closed coin count to be able to recall
     * when opening the next session. */
    private Map<Double, Integer> lastCloseCoinCount;
    /** Backup for close rollback */
    private Double csPerpetualRollback;
    private List<TaxSum> taxes;
    private List<TaxSum> taxesRollback;
    /** Flag to enable close rollback */
    private boolean closeRollbackEnabled;

    private CashSession() {
        this.taxes = new ArrayList<TaxSum>();
    }

    /** Create the next cash session */
    public CashSession next() {
        CashSession next = new CashSession();
        next.id = "0";
        next.cashRegisterId = this.cashRegisterId;
        next.sequence = this.sequence + 1;
        // Report tax sums and reset them according to close type
        if (this.closeType != CLOSE_FYEAR) {
            for (TaxSum s : this.taxes) {
                TaxSum newSum = new TaxSum(s, this.closeType);
                next.taxes.add(newSum);
            }
        }
        // Report cs sums and reset them according to close type
        next.csPeriod = this.csPeriod;
        next.csFYear = this.csFYear;
        next.csPerpetual = this.csPerpetual;
        switch (this.closeType) {
        case CLOSE_FYEAR: next.csFYear = 0.0; // nobreak;
        case CLOSE_PERIOD: next.csPeriod = 0.0; // nobreak;
        case CLOSE_SIMPLE: // reset nothing
        }
        next.lastCloseCoinCount = this.lastCloseCoinCount;
        return next;
    }

    public CashSession(JSONObject o) {
        this.id = "0";
        this.cashRegisterId = o.getInt("cashRegister");
        this.sequence = o.getInt("sequence");
        if (!o.isNull("openDate")) {
            this.openDate = DateUtils.readSecTimestamp(o.getLong("openDate"));
        }
        if (!o.isNull("closeDate")) {
            this.closeDate = DateUtils.readSecTimestamp(o.getLong("closeDate"));
        }
        if (!o.isNull("openCash")) {
            this.openCash = o.getDouble("openCash");
        }
        if (!o.isNull("closeCash")) {
            this.closeCash = o.getDouble("closeCash");
        }
        if (!o.isNull("expectedCash")) {
            this.expectedCash = o.getDouble("expectedCash");
        }
        this.csPeriod = o.getDouble("csPeriod");
        this.csFYear = o.getDouble("csFYear");
        if (o.has("csPerpetual")) {
            this.csPerpetual = o.getDouble("csPerpetual");
        } else {
            this.csPerpetual = null;
        }
        this.taxes = new ArrayList<TaxSum>();
        JSONArray taxes = o.getJSONArray("taxes");
        for (int i = 0; i < taxes.length(); i++) {
            this.taxes.add(new TaxSum(taxes.getJSONObject(i)));
        }
        // Continuous is managed locally
    }

    public JSONObject toJSON() { return this.toJSON(null); }
    public JSONObject toJSON(PaymentsModel z) {
        JSONObject o = new JSONObject();
        o.put("cashRegister", this.cashRegisterId);
        o.put("sequence", this.sequence);
        if (this.openDate == null) {
            o.put("openDate", JSONObject.NULL);
        } else {
            o.put("openDate", DateUtils.toSecTimestamp(this.openDate));
        }
        if (this.closeDate == null) {
            o.put("closeDate", JSONObject.NULL);
        } else {
            o.put("closeDate", DateUtils.toSecTimestamp(this.closeDate));
            o.put("closeType", this.closeType);
        }
        if (this.openCash == null) {
            o.put("openCash", JSONObject.NULL);
        } else {
            o.put("openCash", this.openCash);
        }
        if (this.closeCash == null) {
            o.put("closeCash", JSONObject.NULL);
        } else {
            o.put("closeCash", this.closeCash);
        }
        if (this.expectedCash == null) {
            o.put("expectedCash", JSONObject.NULL);
        } else {
            o.put("expectedCash", this.expectedCash);
        }
        JSONArray taxes = new JSONArray();
        for (TaxSum sum : this.taxes) {
            taxes.put(sum.toJSON());
        }
        o.put("taxes", taxes);
        o.put("continuous", this.continuous);
        if (z != null) {
            o.put("ticketCount", z.getSales());
            if (z.hasCustomersCount()) {
                o.put("custCount", z.getCustomersCount());
            } else {
                o.put("custCount", JSONObject.NULL);
            }
            o.put("cs", z.getSalesBase());
            o.put("csPeriod", this.csPeriod);
            o.put("csFYear", this.csFYear);
            if (this.csPerpetual != null) {
                o.put("csPerpetual", this.csPerpetual);
            }
            JSONArray pmts = new JSONArray();
            for (PaymentsModel.PaymentsLine l : z.getPaymentLines()) {
                JSONObject po = new JSONObject();
                po.put("desktop", true);
                po.put("type", l.getType());
                po.put("currency", l.getCurrency().getID());
                po.put("currencyAmount", l.getValue());
                po.put("amount", l.getCurrency().convertToMain(l.getValue()));
                pmts.put(po);
            }
            o.put("payments", pmts);
            JSONArray balances = new JSONArray();
            for (PaymentsModel.CustomerLine c : z.getCustLines()) {
                JSONObject co = new JSONObject();
                co.put("customer", c.getCustomerId());
                co.put("balance", c.getBalance());
                balances.put(co);
            }
            o.put("custBalances", balances);
            JSONArray catSales = new JSONArray();
            for (PaymentsModel.CategoryLine c : z.getCategoryLines()) {
                JSONObject co = new JSONObject();
                co.put("reference", c.getReference());
                co.put("label", c.getCategory());
                co.put("amount", c.getValue());
                catSales.put(co);
            }
            o.put("catSales", catSales);
            JSONArray catTaxes = new JSONArray();
            for (ZTicket.CategoryTax ct : z.getCatTaxes()) {
                JSONObject oct = new JSONObject();
                oct.put("reference", ct.getCatReference());
                oct.put("label", ct.getCatLabel());
                oct.put("tax", ct.getTaxId());
                oct.put("base", ct.getBase());
                oct.put("amount", ct.getAmount());
                catTaxes.put(oct);
            }
            o.put("catTaxes", catTaxes);
        }
        return o;
    }

    public String getId() { return this.id; }
    public int getCashRegisterId() { return this.cashRegisterId; }
    public int getSequence() { return this.sequence; }
    public void setSequence(int sequence) { this.sequence = sequence; }
    public Date getOpenDate() { return this.openDate; }
    public void open(Date d) { this.openDate = d; }
    /** Cancel open operation. It clears open date and open cash.
     * It should be used only as a rollback when open fails. */
    public void cancelOpen() {
        this.openDate = null;
        this.openCash = null;
    }
    public Date getCloseDate() { return this.closeDate; }
    public boolean hasLastCloseCoinCount() {
        return this.lastCloseCoinCount != null;
    }
    public Map<Double, Integer> getLastCloseCoinCount() {
        return this.lastCloseCoinCount;
    }
    /** Keep last close coin count from the last cached session.
     * It must be called before writing to cache.
     * @throws BasicException When there is an error while reading the cache. */
    private void updateLastCloseCoinCount() throws BasicException {
        CashSession cachedSession = CashSessionsCache.getCashSession(this.cashRegisterId, this.sequence);
        if (cachedSession == null) {
            // No cache for current session, check the previous one.
            cachedSession = CashSessionsCache.getCashSession(this.cashRegisterId, this.sequence - 1);
        }
        if (cachedSession != null) {
            if (cachedSession.hasLastCloseCoinCount()) {
                this.lastCloseCoinCount = new HashMap<Double, Integer>();
                for (Double key : cachedSession.lastCloseCoinCount.keySet()) {
                    Integer value = cachedSession.lastCloseCoinCount.get(key);
                    if (value != null) {
                        this.lastCloseCoinCount.put(Double.valueOf(key.doubleValue()), Integer.valueOf(value.intValue()));
                    }
                }
            } else {
                this.lastCloseCoinCount = null;
            }
            return;
        }
        // No cache at all
        this.lastCloseCoinCount = null;
    }

    /** Close the cash. Does nothing if the session is already closed.
     * @param d When the cash was closed. It sets closeDate.
     * @param closeType Close operation code. See constants.
     * @param z Z ticket to update the sums from.
     * @param coinCount Optional map of coin count.
     */
    public void close(Date d, int closeType, PaymentsModel z, Map<Double, Integer> coinCount) {
        if (this.isClosed()) { return; }
        // Save current values for rollback
        this.csPeriodRollback = this.csPeriod;
        this.csFYearRollback = this.csFYear;
        this.csPerpetualRollback = this.csPerpetual;
        this.taxesRollback = new ArrayList<TaxSum>();
        for (TaxSum tax : this.taxes) {
            TaxSum copy = new TaxSum(tax.toJSON());
            this.taxesRollback.add(copy);
        }
        this.closeRollbackEnabled = true;
        // Close
        this.closeDate = d;
        this.closeType = closeType;
        // Set CS and sums
        double cs = z.getSalesBase();
        this.csPeriod += cs;
        this.csFYear += cs;
        if (this.csPerpetual != null) {
            this.csPerpetual += cs;
        }
        // Set and update TaxSums
        for (PaymentsModel.SalesLine ztax : z.getSaleLines()) {
            boolean merged = false;
            for (TaxSum tax : this.taxes) {
                if (tax.getTaxId() == Integer.parseInt(ztax.getTaxId())) {
                    tax.set(ztax.getTaxBase(), ztax.getTaxes());
                    merged = true;
                }
            }
            if (!merged) {
                TaxSum tax = new TaxSum(Integer.parseInt(ztax.getTaxId()),
                        ztax.getTaxRate());
                tax.set(ztax.getTaxBase(), ztax.getTaxes());
                taxes.add(tax);
            }
        }
        // Store coin count
        if (coinCount != null) {
            this.lastCloseCoinCount = coinCount;
        } else {
            this.lastCloseCoinCount = null;
        }
    }
    /**
     * Revert the previous close operation. It should be used only
     * as a rollback when close fails.
     * Does nothing if close rollback was not enabled with close().
     */
    public void cancelClose() {
        if (!this.closeRollbackEnabled) {
            return;
        }
        this.csPeriod = this.csPeriodRollback;
        this.csFYear = this.csFYearRollback;
        this.csPerpetual = this.csPerpetualRollback;
        this.taxes = this.taxesRollback;
        this.taxesRollback = null;
        this.closeDate = null;
        this.lastCloseCoinCount = null;
        this.closeRollbackEnabled = false;
    }
    /**
     * Delete close rollback values.
     * Does nothing if close rollback was not enabled with close().
     */
    public void confirmClose() {
        if (!this.closeRollbackEnabled) {
            return;
        }
        this.csPeriodRollback = 0.0;
        this.csFYearRollback = 0.0;
        this.csPerpetualRollback = 0.0;
        this.taxesRollback = null;
        this.closeRollbackEnabled = false;
    }

    /** Cash is opened when usable (opened and not closed) */
    public boolean isOpened() {
        return this.openDate != null && this.closeDate == null;
    }
    public boolean wasOpened() { return this.openDate != null; }
    public boolean isClosed() { return this.closeDate != null; }
    public Double getOpenCash() { return this.openCash; }
    public void setOpenCash(Double amount) { this.openCash = amount; }
    public Double getCloseCash() { return this.closeCash; }
    public void setCloseCash(Double amount) { this.closeCash = amount; }
    public Double getExpectedCash() { return this.expectedCash; }
    public void setExpectedCash(Double amount) { this.expectedCash = amount; }

    public boolean isContinuous() { return this.continuous; }
    /** Automatically detect from cache if this cash session
     * is continuous or not. It must be called before writing to cache.
     * Then the session must be cached to prevent messing with the flag.
     * @throws BasicException When there is an error while reading the cache. */
    private void updateContinuous() throws BasicException {
        CashSession cachedSession = CashSessionsCache.getCashSession(this.cashRegisterId, this.sequence);
        if (cachedSession != null) {
            // This session is cached, keep the flag.
            this.continuous = cachedSession.continuous;
            return;
        }
        // No cache for current session, check the previous one.
        cachedSession = CashSessionsCache.getCashSession(this.cashRegisterId, this.sequence - 1);
        if (cachedSession != null) {
            // There was a previous session, it is continuous
            this.continuous = true;
            return;
        }
        // No cache at all, it is not continuous
        this.continuous = false;
    }

    public void updateFromPrevious() throws BasicException {
        this.updateContinuous();
        this.updateLastCloseCoinCount();
    }

    @SuppressWarnings("serial")
    public static class TaxSum implements Serializable
    {
        //TODO v9: add serialVersionUID not to mess with local caches
        //private static final long serialVersionUID = 6614737885582881349L;

        private int taxId;
        private double taxRate;
        private double base;
        private double amount;
        private double basePeriod;
        private double amountPeriod;
        private double baseFYear;
        private double amountFYear;

        public TaxSum(int taxId, double rate) {
            this.taxId = taxId;
            this.taxRate = rate;
        }
        /** Set the cumulative base and amount from a previous sum
         * according to the close type.
         * @param origin The previous sum.
         * @param closeType The close operation to know which sums
         * has to be kept and those that are reset. */
        public TaxSum(TaxSum origin, int closeType) {
            this.taxId = origin.taxId;
            this.taxRate = origin.taxRate;
            switch (closeType) {
            case CLOSE_SIMPLE: // Keep period
                this.basePeriod = origin.basePeriod;
                this.amountPeriod = origin.amountPeriod;
                // nobreak;
            case CLOSE_PERIOD: // Keep fiscal year
                this.baseFYear = origin.baseFYear;
                this.amountFYear = origin.amountFYear;
            }
        }
        public TaxSum(JSONObject o) {
            this.taxId = o.getInt("tax");
            this.taxRate = o.getDouble("taxRate");
            this.base = o.getDouble("base");
            this.amount = o.getDouble("amount");
            this.basePeriod = o.getDouble("basePeriod");
            this.amountPeriod = o.getDouble("amountPeriod");
            this.baseFYear = o.getDouble("baseFYear");
            this.amountFYear = o.getDouble("amountFYear");
        }
        public JSONObject toJSON() {
            JSONObject o = new JSONObject();
            o.put("tax", this.taxId);
            o.put("taxRate", this.taxRate);
            o.put("base", this.base);
            o.put("amount", this.amount);
            o.put("basePeriod", this.basePeriod);
            o.put("amountPeriod", this.amountPeriod);
            o.put("baseFYear", this.baseFYear);
            o.put("amountFYear", this.amountFYear);
            return o;
        }
        public int getTaxId() { return this.taxId; }
        public double getTaxRate() { return this.taxRate; }
        public double getBase() { return this.base; }
        public double getAmount() { return this.amount; }
        public double getBasePeriod() { return this.basePeriod; }
        public double getAmountPeriod() { return this.amountPeriod; }
        public double getBaseFYear() { return this.baseFYear; }
        public double getAmountFYear() { return this.amountFYear; }
        /** Set base and amount of tax if not already set.
         * It add those in period and fiscal year sums. */
        public void set(double base, double amount) {
            if (this.base > 0.005) { return; }
            this.base = base;
            this.amount = amount;
            this.basePeriod += base;
            this.amountPeriod += amount;
            this.baseFYear += base;
            this.amountFYear += amount;
        }
    }
}
