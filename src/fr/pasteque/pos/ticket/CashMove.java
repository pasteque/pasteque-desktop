//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class CashMove implements Serializable
{
    //TODO v9: add serialVersionUID not to mess with local caches
    //private static final long serialVersionUID = 6216375083792848739L;

    private int cashRegisterId;
    private int sequence;
    private Date date;
    private double amount;

    public CashMove(int cashId, int sequence, double amount) {
        this.cashRegisterId = cashId;
        this.sequence = sequence;
        this.date = new Date();
        this.amount = amount;
    }

    public int getCashRegisterId() {
        return this.cashRegisterId;
    }
    public int getSequence() {
        return this.sequence;
    }

    public Date getDate() {
        return this.date;
    }

    public double getAmount() {
        return this.amount;
    }
}
