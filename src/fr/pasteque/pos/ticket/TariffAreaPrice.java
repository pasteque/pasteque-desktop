//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008 Open Sistemas de Información Internet, S.L.
//    http://www.opensistemas.com
//    http://sourceforge.net/projects/openbravopos
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

package fr.pasteque.pos.ticket;

import org.json.JSONObject;

public class TariffAreaPrice
{
    private String productId;
    private Double price;
    private Integer taxId;

    public TariffAreaPrice(String prdId, Double price, Integer taxId) {
        this.productId = prdId;
        this.price = price;
        this.taxId = taxId;
    }

    public TariffAreaPrice(JSONObject o) {
        this.productId = String.valueOf(o.getInt("product"));
        if (!o.isNull("price")) {
            this.price = o.getDouble("price");
        }
        if (!o.isNull("tax")) {
            this.taxId = o.getInt("tax");
        }
    }

    public String getProductId() {
        return this.productId;
    }

    public Double getPrice() {
        return this.price;
    }

    public Integer getTaxId() {
        return this.taxId;
    }
}
