//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,

//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppProperties;
import fr.pasteque.pos.forms.AppUser;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/** Custom buttons and configuration that were loaded from a script before 8.8.
 * Handles Print/Open drawer buttons. */
public class JPanelButtons extends javax.swing.JPanel
{
    private static final long serialVersionUID = -9012971480802990685L;

    private JPanelTicket panelticket;

    /** Creates new form JPanelButtons */
    public JPanelButtons(AppProperties properties, JPanelTicket panelticket) {
        this.panelticket = panelticket;

        // Create printer/drawer buttons
        if (!"Not defined".equals(properties.getProperty("machine.printer.3"))
                && properties.getBoolean("machine.printer.3.filter.order")) {
            this.addPrintButton("button.print", "3", 3);
        }
        if (!"Not defined".equals(properties.getProperty("machine.printer.2"))
                && properties.getBoolean("machine.printer.2.filter.order")) {
            this.addPrintButton("button.print", "2", 2);
        }
        if (!"Not defined".equals(properties.getProperty("machine.printer"))
                && properties.getBoolean("machine.printer.filter.order")) {
            this.addPrintButton("button.print", null, 1);
            this.addOpenDrawerButton("button.opendrawer", AppLocal.getIntString("button.opendrawer"));
        }
    }

    public void setPermissions(AppUser user) {
        for (Component c : this.getComponents()) {
            String sKey = c.getName();
            if (sKey == null || sKey.equals("")) {
                c.setEnabled(true);
            } else {
                c.setEnabled(user.hasPermission(c.getName()));
            }
        }
    }

    private void addPrintButton(String key, String title, final int printerNum) {
        ImageIcon icon = ImageLoader.readImageIcon("tkt_print.png");
        JButton btn = WidgetsBuilder.createButton(icon, title,
                WidgetsBuilder.SIZE_MEDIUM);
        btn.setName(key);
        btn.setText(title);
        btn.setFocusPainted(false);
        btn.setFocusable(false);
        btn.setRequestFocusEnabled(false);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                panelticket.printOrder(printerNum);
            }
        });
        this.add(btn);
    }

    private void addOpenDrawerButton(String key, String title) {
        JButton btn = WidgetsBuilder.createButton(null, title,
                WidgetsBuilder.SIZE_MEDIUM);
        btn.setName(key);
        btn.setText(title);
        btn.setFocusPainted(false);
        btn.setFocusable(false);
        btn.setRequestFocusEnabled(false);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                panelticket.openDrawer();
            }
        });
        this.add(btn);
    }
}
