//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

/**
 * Interface to receive hi-level keypad events.
 */
public interface KeypadManagerListener
{
    public void onIncorrectQuantity(KeypadManager manager);
    public void onCustomerInput(KeypadManager manager, String customerCard);
    /**
     * Special handling of barcodes starting by "210". The first part of the
     * barcode holds the actual barcode, the last characters holds the quantity.
     * @param manager The KeypadManager that triggered the event.
     * @param quantity The quantity extracted from the complete barcode.
     * @param barcode The barcode of the product only, without the quantity.
     */
    public void onScaledBarcodeInput(KeypadManager manager,
            double quantity, String barcode);
    public void onEmptyInput(KeypadManager manager);
    public void onOtherInput(KeypadManager manager,
            double quantity, String barcode);
    /**
     * The checkout button was pressed. It has nothing to do with the keypad
     * status, but it is still handled there, because it is encapsulated within
     * it.
     */
    public void onCheckout(KeypadManager manager);
}
