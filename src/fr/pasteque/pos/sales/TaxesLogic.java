//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.ticket.TicketTaxInfo;
import fr.pasteque.pos.util.Price;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author adrianromero
 */
public class TaxesLogic
{
    private List<TaxInfo> taxlist;

    public TaxesLogic(List<TaxInfo> taxlist) {
        this.taxlist = taxlist;
    }

    public void calculateTaxes(TicketInfo ticket) {
        // TODO: this assumes taxIncluded computation mode.
        Map<String, TicketTaxInfo> ticketTaxes = new HashMap<String, TicketTaxInfo>();
        // Parse all lines and all taxed price to the TicketTaxInfo
        for (TicketLineInfo line: ticket.getLines()) {
            String taxId = line.getTaxInfo().getId();
            if (!ticketTaxes.containsKey(taxId)) {
                TicketTaxInfo tax = new TicketTaxInfo(line.getTaxInfo());
                ticketTaxes.put(taxId, tax);
            }
            ticketTaxes.get(taxId).add(line.getValue());
        }
        /*
         * Include ticket discount in taxes.
         * Compute the rounded values and rounding imprecision.
         * When there is a difference between the sum of rounded values
         * and the ticket total value (after discount), add this difference
         * in taxes.
         * Each 0.01 difference is added to the tax which is the nearest to
         * have its round change (only once per tax).
         */
        if (ticket.hasDiscount()) {
            // Apply discount to each tax and keep rounding imprecision
            double roundedTotal = 0.0;
            Map<String, Double> discountDiff = new HashMap<String, Double>();
            double discountRate = ticket.getDiscountRate();
            for (TicketTaxInfo t : ticketTaxes.values()) {
                double roundDiff = Price.roundDiff(t.getSubTotal() * (1.0 - discountRate));
                discountDiff.put(t.getTax().getId(), roundDiff);
                t.applyDiscount(discountRate);
                roundedTotal = Price.add(roundedTotal, t.getSubTotal());
            }
            // Check sum of rounded values and the total value
            double diff = Price.add(ticket.getTotal(), -roundedTotal);
            if (diff != 0.0) {
                boolean add = (diff > 0.005);
                // Update the set to the difference to the next rounded value
                for (String tId : discountDiff.keySet()) {
                    double roundDiff = discountDiff.get(tId);
                    roundDiff = add ?
                        ( 0.005 - roundDiff):
                        (-0.005 - roundDiff);
                    discountDiff.replace(tId, roundDiff);
                }
                /* Put 0.01 of the difference in the tax that require the less
                 * change to be rounded differently until it matches.
                 * There could be only taxes.length() / 2 loops.
                 */
                while (diff != 0.0) {
                    double leastDiff = 0.0;
                    String leastId = null;
                    for (String tId : discountDiff.keySet()) {
                        double roundDiff = discountDiff.get(tId);
                        if (leastId == null || (add && roundDiff < leastDiff)
                                || (!add && roundDiff > leastDiff)) {
                            leastDiff = roundDiff;
                            leastId = tId;
                        }
                    }
                    TicketTaxInfo tax = ticketTaxes.get(leastId);
                    tax.add(add ? 0.01 : -0.01);
                    discountDiff.remove(leastId);
                    diff = (add ?
                        Price.add(diff, -0.01):
                        Price.add(diff, 0.01));
                }
            }
        }
        // Finalize all TicketTaxInfo (set base and tax amounts)
        List<TicketTaxInfo> finalTaxes = new ArrayList<TicketTaxInfo>();
        for (String id : ticketTaxes.keySet()) {
            TicketTaxInfo tax = ticketTaxes.get(id);
            tax.finalize(true);
            finalTaxes.add(tax);
        }
        // Assign TicketTaxInfo to the ticket
        ticket.setTaxes(finalTaxes);
    }

    public double getTaxRate(String taxId) {
        return this.getTaxRate(taxId, null);
    }
    public double getTaxRate(String taxId, CustomerInfoExt customer) {
        if (taxId == null) {
            return 0.0;
        } else {
            TaxInfo tax = getTaxInfo(taxId, customer);
            if (tax == null) {
                return 0.0;
            } else {
                return tax.getRate();
            }
        }
    }

    public TaxInfo getTaxInfo(String taxId) {
        return this.getTaxInfo(taxId, null);
    }
    public TaxInfo getTaxInfo(String taxId, CustomerInfoExt customer) {
        TaxInfo ret = null;
        if (customer != null && customer.getTaxId() != null) {
            ret = this.searchTax(customer.getTaxId());
            if (ret != null) { return ret; }
        }
        ret = this.searchTax(taxId);
        return ret;
    }

    private TaxInfo searchTax(String id) {
        if (id == null) { return null; }
        for (TaxInfo tax : this.taxlist) {
            if (id.equals(tax.getId())) {
                return tax;
            }
        }
        return null;
    }
}
