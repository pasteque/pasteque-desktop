//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;

/** Wrapper for local Orders. It includes a TicketInfo. */
public class SharedTicketInfo implements Serializable
{
    private static final long serialVersionUID = 7640633837719L;
    private String id;
    /** The default and immutable name of the order. */
    private String name;
    /** A user-provided alternative label for the order. */
    private String customLabel;
    private TicketInfo ticket;

    /**
     * Creates a new instance of SharedTicketInfo. Id must always be set.
     * @param id For list mode it is generated with UUID.randomUUID().toString().
     * In restaurant mode it is the place id.
     * @param ticket The content of the order.
     */
    public SharedTicketInfo(String id, TicketInfo ticket) {
        this.id = id;
        if (ticket != null) { // Null from a deleted one in CallQueue
            this.name = ticket.getName();
        }
        this.ticket = ticket;
    }

    /** Load an Order from the server. It calls TicketInfo.sharedTicketInfo. */
    public SharedTicketInfo(JSONObject o) throws BasicException {
        this.id = String.valueOf(o.getInt("id"));
        this.name = o.getString("label");
        TicketInfo tkt = TicketInfo.sharedTicketInfo(o);
        this.ticket = tkt;
    }

    /** Send the underlying ticket in form of an Order. */
    public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("label", ticket.getName());
        o.put("discountRate", this.ticket.getDiscountRate());
        if (this.ticket.getCustomersCount() != null) {
            o.put("custCount", this.ticket.getCustomersCount());
        } else {
            o.put("custCount", JSONObject.NULL);
        }
        JSONArray lines = new JSONArray();
        int i = 0;
        for (TicketLineInfo l : this.ticket.getLines()) {
            lines.put(l.toJSON(i));
            i++;
        }
        o.put("lines", lines);
        if (this.ticket.getCustomerId() != null) {
            o.put("customer", this.ticket.getCustomerId());
        } else {
            o.put("customer", JSONObject.NULL);
        }
        if (this.ticket.getTariffArea() != null) {
            o.put("tariffArea", this.ticket.getTariffArea());
        } else {
            o.put("tariffArea", JSONObject.NULL);
        }
        if (this.ticket.getDiscountProfileId() != null) {
            o.put("discountProfile", this.ticket.getDiscountProfileId());
        } else {
            o.put("discountProfile", JSONObject.NULL);
        }
        return o;
    }

    public void updateContent(TicketInfo newContent) {
        this.ticket = newContent;
    }

    /** Update the id (use it to assign to an other place). */
    public void setId(String newId) {
        this.id = newId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    /**
     * Change the custom label of the order.
     * @param custLabel The new custom value. Can be null.
     */
    public void setCustomLabel(String custLabel) {
        this.customLabel = custLabel;
    }

    /**
     * Get the current custom label of the order.
     * @return The custom value. Can be null.
     */
    public String getCustomLabel() {
        return this.customLabel;
    }

    /**
     * Get a short name suitable for a title.
     * @return The custom label, customer's name or place name
     * or creation time and customer's count if set.
     */
    public String getShortLabel(Object orderExtra) {
        String label;
        if (this.customLabel != null) {
            label = this.customLabel;
        } else if (this.ticket.getCustomer() != null) {
            label = this.ticket.getCustomer().getName();
        } else {
            label = this.ticket.getName(orderExtra);
        }
        if (this.ticket.hasCustomersCount()) {
            label += " (" + this.ticket.getCustomersCount() + ")";
        }
        return label;
    }

    /**
     * Get an extensive name suitable for a place button.
     * @param defaultPlaceLabel The label to use when this order doesn't
     * override it.
     * @return The extensive name, with either the custom label,
     * the customer's name or the name of the order and the number
     * of customers if set.
     */
    public String getPlaceLabel(String defaultPlaceLabel) {
        if (this.customLabel == null
                && !this.ticket.hasCustomersCount()
                && this.ticket.getCustomer() == null) {
            return defaultPlaceLabel;
        }
        String label = defaultPlaceLabel;
        if (this.customLabel != null) {
            label = this.customLabel;
        } else if (this.ticket.getCustomer() != null) {
            label = this.ticket.getCustomer().getName();
        }
        if (this.ticket.hasCustomersCount()
                && this.ticket.getCustomersCount() > 0) {
            return label + " (" + this.ticket.getCustomersCount() + ")";
        } else {
            return label;
        }
    }

    /**
     * Get an extensive name suitable for listing.
     * @return The extensive name, with either the custom label,
     * the customer's name or the creation time, the translated number
     * of articles and the total amount.
     */
    public String getListLabel() {
        String idLbl;
        if (this.customLabel != null) {
            idLbl = this.customLabel;
        } else if (this.ticket.getCustomerId() != null) {
            idLbl = this.ticket.getCustomer().getName();
        } else if (this.ticket.getNumber() == 0) {
            idLbl = Formats.TIME.formatValue(this.ticket.getDate());
        } else {
            idLbl = Integer.toString(this.ticket.getNumber());
        }
        return idLbl + " - "
                + AppLocal.getIntString("Label.TicketArticleCount", this.ticket.getArticlesCount())
                + " - " + this.ticket.printTotal();
    }

    public TicketInfo getTicket() {
        return this.ticket;
    }

    /**
     * Check whether the order has any content or not.
     * @return True when the content is empty, there is no customer count or 0,
     * the order is not assigned to a customer and has no custom label.
     */
    @SuppressWarnings("deprecation")
    public boolean isEmpty() {
        /* ticket.isOrderEmpty() is deprecated and the method can be moved here
         * when it is removed. */
        if (this.ticket.isOrderEmpty()) {
            return this.customLabel == null;
        }
        return false;
    }
}
