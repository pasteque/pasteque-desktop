//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.catalog.JCatalogTab;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.JEditorDoublePositive;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** Popup to pick a discount profile. */
public class DiscountProfilePicker extends javax.swing.JDialog
implements ShortcutListener
{
    private static final long serialVersionUID = 3966059595155219437L;

    private DiscountProfile selectedProfile;
    /** The profile that was previously assigned to keep it on cancel. */
    private DiscountProfile originalProfile;
    private ThumbNailBuilder tnbbutton;

    private DiscountProfilePicker(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
    }

    private DiscountProfilePicker(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
    }

    /**
     * Show the popup and return the selected profile.
     * @return The selected profile on OK, the passed profile on cancel.
     */
    public static DiscountProfile show(Component parent,
            DiscountProfile profile) {
        Window window = getWindow(parent);
        DiscountProfilePicker myMsg;
        if (window instanceof Frame) {
            myMsg = new DiscountProfilePicker((Frame) window, true);
        } else {
            myMsg = new DiscountProfilePicker((Dialog) window, true);
        }
        return myMsg.init(profile);
    }

    private DiscountProfile init(DiscountProfile profile) {
        AppConfig cfg = AppConfig.loadedInstance;
        int widthCfg = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchhudgebtnminwidth")));
        int heightCfg = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchhudgebtnminheight")));
        this.tnbbutton = new ThumbNailBuilder(widthCfg, heightCfg,
                "discount_default.png");
        this.originalProfile = profile;
        initComponents();
        try {
            // Load profiles
            DataLogicCustomers dlCust = new DataLogicCustomers();
            java.util.List<DiscountProfile> profiles = dlCust.getDiscountProfiles();
            // Add buttons
            for (DiscountProfile dp : profiles) {
                this.profilesCatalog.addButton(new ImageIcon(tnbbutton.getThumbNailText(null, dp.getName())), dp);
            }
        } catch (BasicException eb) {
            eb.printStackTrace();
        }
        this.rateField.addEditorKeys(m_jKeys);
        this.rateField.activate();
        // Set shortcuts
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
        //show();
        setVisible(true);
        // Post edit
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
        return this.selectedProfile;
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                this.onOk();
                return true;
            case GENERAL_CANCEL:
                this.onCancel();
                return true;
            default: // Ignore
                return false;
        }
    }

    private static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window) parent;
        } else {
            return getWindow(parent.getParent());
        }
    }

    public DiscountProfile getSelectedProfile() {
        return this.selectedProfile;
    }

    protected class CatalogListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof DiscountProfile) {
                DiscountProfile profile = ((DiscountProfile) e.getSource());
                selectedProfile = profile;
                DiscountProfilePicker.this.dispose();
            }
        }
    }

    private void onOk() {
        Double rate = this.rateField.getDoubleValue();
        if (rate == null) {
            rate = 0.0;
        } else {
            rate = Math.abs(rate / 100);
        }
        if (rate > 1.0) {
            JMessageDialog.showMessage(this,
                    "Discount error",
                    AppLocal.tr("message.invaliddiscount"));
            this.rateField.activate();
            return;
        }
        this.selectedProfile = new DiscountProfile(rate);
        dispose();
    }

    private void onUnassign() {
        this.selectedProfile = null;
        dispose();
    }

    private void onCancel() {
        this.selectedProfile = this.originalProfile;
        dispose();
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        m_jButtonOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        unassignBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_generic.png"),
                AppLocal.getIntString("Button.Unassign"),
                WidgetsBuilder.SIZE_MEDIUM);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(AppLocal.getIntString("Form.DiscountProfile"));

        this.getContentPane().setLayout(new BorderLayout());
        JPanel container = new JPanel();
        container.setLayout(new GridBagLayout());
        this.getContentPane().add(container, BorderLayout.CENTER);
        GridBagConstraints cstr;

        this.rateField = new JEditorDoublePositive();
        this.rateField.setBounds(0.0, 100.0);
        JLabel rateLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.discountRate"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, 0);
        container.add(rateLabel, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.weightx = 1;
        cstr.insets = new Insets(btnSpacing, btnSpacing,
                btnSpacing, btnSpacing);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        JPanel ratePanel = new JPanel();
        ratePanel.setLayout(new BorderLayout());
        ratePanel.add(this.rateField, BorderLayout.CENTER);
        ratePanel.add(new JLabel("%"), BorderLayout.EAST);
        container.add(ratePanel, cstr);

        m_jKeys = new JEditorKeys();

        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 0;
        cstr.gridheight = 2;
        cstr.anchor = GridBagConstraints.CENTER;
        container.add(m_jKeys, cstr);

        this.catalogContainer = new JPanel();
        this.catalogContainer.setLayout(new java.awt.BorderLayout());
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.gridwidth = 2;
        cstr.weightx = 1;
        cstr.weighty = 1;
        cstr.insets = new Insets(0, btnSpacing, btnSpacing, btnSpacing);
        cstr.fill = GridBagConstraints.BOTH;
        container.add(this.catalogContainer, cstr);

        this.profilesCatalog = new JCatalogTab(new CatalogListener());
        this.profilesCatalog.applyComponentOrientation(getComponentOrientation());
        this.catalogContainer.add(this.profilesCatalog);

        JPanel buttonsContainer = new JPanel();
        m_jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onOk();
            }
        });
        buttonsContainer.add(m_jButtonOK);

        this.unassignBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onUnassign();
            }
        });
        if (this.originalProfile != null && this.originalProfile.getRate() > 0.0) {
            buttonsContainer.add(this.unassignBtn);
        }

        m_jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCancel();
            }
        });
        buttonsContainer.add(m_jButtonCancel);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 2;
        cstr.gridwidth = 3;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.anchor = GridBagConstraints.LINE_END;
        container.add(buttonsContainer, cstr);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-609)/2, (screenSize.height-388)/2, 609, 388);
    }

    private javax.swing.JButton m_jButtonCancel;
    private javax.swing.JButton m_jButtonOK;
    private javax.swing.JButton unassignBtn;
    private JPanel catalogContainer;
    private JEditorKeys m_jKeys;
    private JEditorDoublePositive rateField;
    private JCatalogTab profilesCatalog;
}
