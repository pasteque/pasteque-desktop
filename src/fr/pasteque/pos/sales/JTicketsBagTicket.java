//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.panels.JTicketsFinder;
import fr.pasteque.pos.printer.DevicePrinter;
import fr.pasteque.pos.printer.DeviceTicket;
import fr.pasteque.pos.printer.TicketPrinterException;
import fr.pasteque.pos.printer.document.TicketParser;
import fr.pasteque.pos.printer.document.TicketPrintScript;
import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import javax.swing.JComponent;

/** This is the screen to look for tickets and reprint them. */
public class JTicketsBagTicket extends JTicketsBag
{
    private static final long serialVersionUID = 7337367983525084955L;

    private DataLogicSystem m_dlSystem = null;
    protected DataLogicCustomers dlCustomers = null;

    private DeviceTicket m_TP;
    private TicketParser m_TTP;
    private TicketParser m_TTP2;
    private TicketPrintScript printScript;

    private TicketInfo m_ticket;
    private TicketInfo m_ticketCopy;

    private JTicketsBagTicketBag m_TicketsBagTicketBag;

    private JPanelTicketEdits m_panelticketedit;

    /** Creates new form JTicketsBagTicket */
    public JTicketsBagTicket(AppView app, JPanelTicketEdits panelticket) {

        super(app, panelticket);
        m_panelticketedit = panelticket;
        m_dlSystem = new DataLogicSystem();
        dlCustomers = new DataLogicCustomers();

        // Inicializo la impresora...
        m_TP = new DeviceTicket();

        // Setup ticket width from the first default printer
        int cpl = m_App.getDeviceTicket().getDevicePrinter("1").getCharsPerLine();
        m_TP.getDevicePrinter("1").setCharsPerLine(cpl);
         m_TP.getDevicePrinter("1").setDotsPerLine(m_App.getDeviceTicket().getDevicePrinter("1").getDotsPerLine());
        this.printScript = new TicketPrintScript(cpl);

        // Inicializo el parser de documentos de ticket
        m_TTP = new TicketParser(m_TP, m_dlSystem); // para visualizar el ticket
        m_TTP2 = new TicketParser(m_App.getDeviceTicket(), m_dlSystem); // para imprimir el ticket

        initComponents();

        m_TicketsBagTicketBag = new JTicketsBagTicketBag(this);

        m_jTicketEditor.addEditorKeys(m_jKeys);

        // Este deviceticket solo tiene una impresora, la de pantalla
        m_jPanelTicket.add(m_TP.getDevicePrinter("1").getPrinterComponent(), java.awt.BorderLayout.CENTER);
    }

    public void activate() {

        // precondicion es que no tenemos ticket activado ni ticket en el panel

        m_ticket = null;
        m_ticketCopy = null;

        printTicket();

        m_jTicketEditor.reset();
        m_jTicketEditor.activate();

        m_panelticketedit.setActiveOrder(null, null);

        m_jPrint.setVisible(m_App.getAppUserView().getUser().hasPermission("sales.PrintTicket"));
        m_jPrint2.setVisible(m_App.getAppUserView().getUser().hasPermission("sales.PrintTicket"));
        m_jPrint3.setVisible(m_App.getAppUserView().getUser().hasPermission("sales.PrintTicket"));
        // postcondicion es que tenemos ticket activado aqui y ticket en el panel
    }

    public boolean deactivate() {

        // precondicion es que tenemos ticket activado aqui y ticket en el panel
        m_ticket = null;
        m_ticketCopy = null;
        return true;
        // postcondicion es que no tenemos ticket activado ni ticket en el panel
    }

    public void refreshBagButtons() {}

    public void closeCurrentOrder() {
        if (m_ticketCopy != null) {
            try {
                m_dlSales.deleteTicket(m_ticketCopy);
            } catch (BasicException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE,
                        AppLocal.getIntString("message.nosaveticket"), e);
                msg.show(this);
            }
        }
        m_ticket = null;
        m_ticketCopy = null;
        resetToTicket();
    }

    public void canceleditionTicket() {

        m_ticketCopy = null;
        resetToTicket();
    }

    private void resetToTicket() {
        printTicket();
        m_jTicketEditor.reset();
        m_jTicketEditor.activate();
        m_panelticketedit.setActiveOrder(null, null);
    }

    protected JComponent getBagComponent() {
        return m_TicketsBagTicketBag;
    }

    protected JComponent getNullComponent() {
        return this;
    }

    /** Read ticket from ticket id in input */
    private void readTicket() {
        try {
            int tktId = m_jTicketEditor.getValueInteger();
            DataLogicSales dlSales = new DataLogicSales();
            java.util.List<TicketInfo> tkts = dlSales.searchTickets(tktId,
                    0, m_App.getCashRegister().getId(), null, null, null, null);
            if (tkts.size() > 0) {
                this.readTicket(tkts.get(0));
            } else {
                this.readTicket(null);
            }
        } catch (BasicException e) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotloadticket"), e);
            msg.show(this);
        }

        m_jTicketEditor.reset();
        m_jTicketEditor.activate();
    }
    private void readTicket(TicketInfo ticket) {
        if (ticket == null) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                    AppLocal.getIntString("message.notexiststicket"));
            msg.show(this);
        } else {
            m_ticket = ticket;
            m_ticketCopy = null; // se asigna al pulsar el boton de editar o devolver
            printTicket();
        }
    }

    private void printTicket() {
        // imprimo m_ticket
        m_jPrint.setEnabled(m_ticket != null);
        m_jPrint2.setEnabled(m_ticket != null);
        m_jPrint3.setEnabled(m_ticket != null);

        // Este deviceticket solo tiene una impresora, la de pantalla
        m_TP.getDevicePrinter("1").reset();

        if (m_ticket == null) {
            m_jTicketId.setText(null);
        } else {
            m_jTicketId.setText(m_ticket.getName());

            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                String sresource;
                if (m_ticket.getTicketType() == TicketInfo.RECEIPT_PAYMENT) {
                    sresource = m_dlSystem.getResourceAsXML("Printer.CustomerPaid");
                } else {
                    sresource = this.printScript.getTicketXml(m_ticket, 1, null, false);
                }
                script.put("ticket", m_ticket);
                m_TTP.printTicket(script.eval(sresource).toString());
            } catch (ScriptException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                msg.show(this);
            } catch (TicketPrinterException eTP) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), eTP);
                msg.show(this);
            }
        }
    }

    private void openSearch() {
        TicketInfo selectedTicket = JTicketsFinder.show(this, this.m_App);
        if (selectedTicket == null) {
            m_jTicketEditor.reset();
        } else {
            readTicket(selectedTicket);
        }
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        java.awt.GridBagConstraints gridBagConstraints;

        m_jOptions = new javax.swing.JPanel();
        m_jButtons = new javax.swing.JPanel();
        m_jTicketId = new javax.swing.JLabel();
        searchBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("search.png"),
                AppLocal.getIntString("label.search"),
                WidgetsBuilder.SIZE_MEDIUM);
        m_jPrint = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_print.png"),
                null,
                WidgetsBuilder.SIZE_MEDIUM);
        m_jPrint2 = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_print.png"),
                "2",
                WidgetsBuilder.SIZE_MEDIUM);
        m_jPrint3 = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_print.png"),
                "3",
                WidgetsBuilder.SIZE_MEDIUM);

        m_jPanelTicket = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        m_jKeys = new fr.pasteque.pos.widgets.JEditorKeys();
        jPanel5 = new javax.swing.JPanel();
        okBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"), WidgetsBuilder.SIZE_MEDIUM);

        m_jTicketEditor = new fr.pasteque.pos.widgets.JEditorIntegerPositive();
        tktTypeContainer = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        m_jOptions.setLayout(new java.awt.BorderLayout());

        m_jButtons.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        m_jTicketId.setBackground(java.awt.Color.white);
        m_jTicketId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        m_jTicketId.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jTicketId.setOpaque(true);
        m_jTicketId.setPreferredSize(new java.awt.Dimension(160, 25));
        m_jTicketId.setRequestFocusEnabled(false);
        m_jButtons.add(m_jTicketId);

        searchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openSearch();
            }
        });
        m_jButtons.add(searchBtn);

        m_jPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPrintActionPerformed(evt, 1);
            }
        });
        m_jPrint2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPrintActionPerformed(evt, 2);
            }
        });
        m_jPrint3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPrintActionPerformed(evt, 3);
            }
        });
        if (!"Not defined".equals(cfg.getProperty("machine.printer.3"))
                && cfg.getBoolean("machine.printer.3.filter.ticket")) {
            m_jButtons.add(m_jPrint3);
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer.2"))
                && cfg.getBoolean("machine.printer.2.filter.ticket")) {
            m_jButtons.add(m_jPrint2);
        }
        if (!"Not defined".equals(cfg.getProperty("machine.printer"))
                && cfg.getBoolean("machine.printer.filter.ticket")) {
            m_jButtons.add(m_jPrint);
        }
        m_jOptions.add(m_jButtons, java.awt.BorderLayout.WEST);

        add(m_jOptions, java.awt.BorderLayout.NORTH);

        m_jPanelTicket.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        m_jPanelTicket.setLayout(new java.awt.BorderLayout());
        add(m_jPanelTicket, java.awt.BorderLayout.CENTER);

        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.Y_AXIS));

        m_jKeys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jKeysActionPerformed(evt);
            }
        });
        jPanel4.add(m_jKeys);

        jPanel5.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okBtnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel5.add(okBtn, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(m_jTicketEditor, gridBagConstraints);

        jPanel4.add(jPanel5);

        jPanel3.add(jPanel4, java.awt.BorderLayout.NORTH);

        jPanel3.add(tktTypeContainer, java.awt.BorderLayout.CENTER);

        add(jPanel3, java.awt.BorderLayout.EAST);
    }

    private void m_jPrintActionPerformed(java.awt.event.ActionEvent evt, int printerNum) {
        if (m_ticket != null) {
            DevicePrinter printer = m_App.getDeviceTicket().getDevicePrinter(String.valueOf(printerNum));
            TicketPrintScript printScript = new TicketPrintScript(printer.getCharsPerLine());
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                script.put("ticket", m_ticket);
                m_TTP2.printTicket(script.eval(printScript.getReprintTicketXml(m_ticket, printerNum)).toString());
            } catch (ScriptException e) {
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotprint"), e));
            } catch (TicketPrinterException e) {
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotprint"), e));
            }
        }
    }

    private void okBtnActionPerformed(java.awt.event.ActionEvent evt) {
        readTicket();
    }

    private void m_jKeysActionPerformed(java.awt.event.ActionEvent evt) {
        readTicket();
    }

    private javax.swing.JButton okBtn;
    private javax.swing.JButton searchBtn;
    private javax.swing.JPanel tktTypeContainer;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel m_jButtons;
    private fr.pasteque.pos.widgets.JEditorKeys m_jKeys;
    private javax.swing.JPanel m_jOptions;
    private javax.swing.JPanel m_jPanelTicket;
    private javax.swing.JButton m_jPrint;
    private javax.swing.JButton m_jPrint2;
    private javax.swing.JButton m_jPrint3;
    private fr.pasteque.pos.widgets.JEditorIntegerPositive m_jTicketEditor;
    private javax.swing.JLabel m_jTicketId;
}
