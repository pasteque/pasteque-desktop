//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales.shared;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.sales.SharedTicketInfo;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;

/** Dialog to select an order from pending orders. */
public class JTicketsBagSharedList extends javax.swing.JDialog
implements ShortcutListener
{
    private static final long serialVersionUID = -8376409553199741787L;

    private SharedTicketInfo selectedOrder;

    /** Creates new form JTicketsBagSharedList */
    private JTicketsBagSharedList(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
    }
    /** Creates new form JTicketsBagSharedList */
    private JTicketsBagSharedList(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
    }

    public SharedTicketInfo showTicketsList(List<SharedTicketInfo> atickets,
            String currentOrderId) {
        for (int i = 0; i < atickets.size(); i++) {
            SharedTicketInfo order = atickets.get(i);
            if (currentOrderId == null || !currentOrderId.equals(order.getId())) {
                m_jtickets.add(new JButtonTicket(order));
            }
        }
        this.pack();
        this.setLocationRelativeTo(getOwner());
        this.selectedOrder = null;
        // Set shortcuts
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
        //show();
        setVisible(true);
        // Post edit
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
        return this.selectedOrder;
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_CANCEL:
                this.onCancel();
                return true;
            default: // Ignore
                return false;
        }
    }

    public static JTicketsBagSharedList newJDialog(JTicketsBagShared ticketsbagshared) {

        Window window = getWindow(ticketsbagshared);
        JTicketsBagSharedList mydialog;
        if (window instanceof Frame) {
            mydialog = new JTicketsBagSharedList((Frame) window, true);
        } else {
            mydialog = new JTicketsBagSharedList((Dialog) window, true);
        }

        mydialog.initComponents();

        mydialog.jScrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));

        return mydialog;
    }

    private static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window) parent;
        } else {
            return getWindow(parent.getParent());
        }
    }

    private class JButtonTicket extends JButton
    {
        private static final long serialVersionUID = -7666764671518767155L;

        private SharedTicketInfo m_Ticket;

        public JButtonTicket(SharedTicketInfo ticket) {

            super();

            m_Ticket = ticket;
            setFocusPainted(false);
            setFocusable(false);
            setRequestFocusEnabled(false);
            setMargin(new Insets(8, 14, 8, 14));
            //setFont(new java.awt.Font ("Dialog", 1, 24));
            //setBackground(new java.awt.Color (220, 220, 220));
            addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {

                    // Selecciono el ticket
                    selectedOrder = m_Ticket;

                    // y oculto la ventana
                    JTicketsBagSharedList.this.setVisible(false);
                }
            });

            setText(ticket.getListLabel());
        }
    }

    private void onCancel() {
        dispose();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        m_jtickets = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        m_jButtonCancel = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);

        setTitle(AppLocal.getIntString("caption.tickets")); // NOI18N
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.BorderLayout());

        m_jtickets.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        m_jtickets.setLayout(new java.awt.GridLayout(0, 1, 5, 5));
        jPanel2.add(m_jtickets, java.awt.BorderLayout.NORTH);

        jScrollPane1.setViewportView(jPanel2);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));
        jPanel3.add(jPanel4);

        m_jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCancel();
            }
        });
        jPanel3.add(m_jButtonCancel);

        getContentPane().add(jPanel3, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton m_jButtonCancel;
    private javax.swing.JPanel m_jtickets;
    // End of variables declaration//GEN-END:variables
}
