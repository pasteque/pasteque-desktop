//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales.shared;

import fr.pasteque.pos.ticket.TicketInfo;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.pos.sales.*;
import fr.pasteque.pos.forms.*;
import fr.pasteque.pos.widgets.WidgetsBuilder;

/**
 * Order management for the standard mode.
 * It handles the local cache and the new and switch buttons.
 * It is coupled to a JTicketsBagShaledList to display the list of pending
 * orders when switching.
 */
public class JTicketsBagShared extends JTicketsBag
{
    private static final long serialVersionUID = 5817671402263561170L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.sales.shared.JTicketsBagShared");

    private boolean createAfterClose;

    /** Creates new form JTicketsBagShared */
    public JTicketsBagShared(AppView app, TicketsEditor panelticket) {
        super(app, panelticket);
        this.createAfterClose = "new".equals(AppConfig.loadedInstance.getProperty("ui.orderautoswitch"));
        initComponents();
    }

    public void activate() {
        selectFirstOrder();
        refreshBagButtons();
    }

    public boolean deactivate() {
        // Unselect and persist
        m_panelticket.setActiveOrder(null, null);
        return true;
    }

    public void refreshBagButtons() {
        try {
            boolean canSwitch = m_panelticket.getController().hasPendingOrders();
            this.m_jListTickets.setEnabled(canSwitch);
        } catch (BasicException e) {
            logger.log(Level.WARNING, "Unable to list orders.");
        }
        SharedTicketInfo currentOrder = m_panelticket.getActiveOrder();
        this.m_jNewTicket.setEnabled(currentOrder == null
                || !currentOrder.isEmpty());
    }

    /**
     * Delete the current order and create a new one or switch to the first
     * pending one according to the configuration.
     */
    public void closeCurrentOrder() {
        this.deleteCurrentOrder();
        if (this.createAfterClose) {
            selectNewOrder();
        } else {
            selectFirstOrder();
        }
    }

    protected JComponent getBagComponent() {
        return this;
    }

    protected JComponent getNullComponent() {
        return new JPanel();
    }

    /**
     * Drop the current order from the local cache.
     * it does not switch the current order and does nothing if there is no
     * current order.
     */
    protected void deleteCurrentOrder() {
        if (m_panelticket.getActiveOrder() != null) {
            try {
                m_panelticket.getController().deleteCurrentOrder();
            } catch (BasicException e) {
                logger.log(Level.WARNING, "Unable to delete order.", e);
            }
        }
    }

    /** Proxy to switchOrder(order, false), select an existing order */
    private void switchOrder(SharedTicketInfo order) throws BasicException {
        switchOrder(order, false);
    }

    /**
     * Switch to an other order, the TicketEditor will handle persisting or
     * deleting the previous one.
     * @param order The order to select.
     * @param isNew Save the order after selecting it.
     */
    private void switchOrder(SharedTicketInfo order, boolean isNew)
    throws BasicException {
        if (order == null)  {
            // Does not exists ???
            throw new BasicException(AppLocal.getIntString("message.noticket"));
        }
        // Switch to the other order
        m_panelticket.setActiveOrder(order, null);
        if (isNew) {
            // Save the new order so it is present in cache
            try {
                m_panelticket.getController().saveCurrentOrder();
            } catch (BasicException e) {
                new MessageInf(e).show(this);
            }
        }
        refreshBagButtons();
    }

    /** Select the first available order, create one if none are available. */
    private void selectFirstOrder() {
        try {
            List<SharedTicketInfo> l = m_panelticket.getController().getOrderDataSource().getOrderList();
            if (l == null) {
                logger.log(Level.WARNING, "Unable to get shared tickets. List is null.");
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER, "Unable to get shared tickets", "List is null"));
                selectNewOrder();
            } else if (l.size() == 0) {
                selectNewOrder();
            } else {
                switchOrder(l.get(0));
            }
        } catch (BasicException e) {
            new MessageInf(e).show(this);
            selectNewOrder();
        }
    }

    /**
     * Create a new order and switch to it.
     */
    private void selectNewOrder() {
        SharedTicketInfo order = new SharedTicketInfo(
                UUID.randomUUID().toString(), new TicketInfo());
        try {
            switchOrder(order, true);
        } catch (BasicException e) {
            // Should never happen
            logger.log(Level.WARNING, "New order not found.");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jPanel1 = new javax.swing.JPanel();
        m_jNewTicket = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_new.png"),
                AppLocal.getIntString("Button.m_jNewTicket.toolTip"));
        m_jListTickets = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_list.png"),
                AppLocal.getIntString("Button.m_jListTickets.toolTip"));

        setLayout(new java.awt.BorderLayout());

        m_jNewTicket.setFocusPainted(false);
        m_jNewTicket.setFocusable(false);
        m_jNewTicket.setRequestFocusEnabled(false);
        m_jNewTicket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jNewTicketActionPerformed(evt);
            }
        });

        jPanel1.add(m_jNewTicket);

        m_jListTickets.setFocusPainted(false);
        m_jListTickets.setFocusable(false);
        m_jListTickets.setRequestFocusEnabled(false);
        m_jListTickets.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jListTicketsActionPerformed(evt);
            }
        });

        jPanel1.add(m_jListTickets);

        add(jPanel1, java.awt.BorderLayout.WEST);

    }// </editor-fold>//GEN-END:initComponents

    private void m_jListTicketsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jListTicketsActionPerformed
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    List<SharedTicketInfo> l = m_panelticket.getController().getOrderDataSource().getOrderList();
                    JTicketsBagSharedList listDialog = JTicketsBagSharedList.newJDialog(JTicketsBagShared.this);
                    SharedTicketInfo order = listDialog.showTicketsList(
                            l, m_panelticket.getActiveOrder().getId());
                    if (order != null) {
                        switchOrder(order);
                    }
                } catch (BasicException e) {
                    new MessageInf(e).show(JTicketsBagShared.this);
                    selectNewOrder();
                }
            }
        });
    }//GEN-LAST:event_m_jListTicketsActionPerformed

    private void m_jNewTicketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jNewTicketActionPerformed
        selectNewOrder();
    }//GEN-LAST:event_m_jNewTicketActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton m_jListTickets;
    private javax.swing.JButton m_jNewTicket;
    // End of variables declaration//GEN-END:variables
}
