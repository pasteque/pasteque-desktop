//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.ListKeyed;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.caching.CashRegistersCache;
import fr.pasteque.pos.caching.CustomersCache;
import fr.pasteque.pos.catalog.CatalogSelector;
import fr.pasteque.pos.catalog.JCatalogSubgroups;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.customers.JCustomerFinder;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.BeanFactoryApp;
import fr.pasteque.pos.forms.BeanFactoryException;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.forms.JPanelView;
import fr.pasteque.pos.forms.shortcuts.CustomShortcut;
import fr.pasteque.pos.forms.shortcuts.CustomShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.panels.JProductFinder;
import fr.pasteque.pos.payment.JPaymentSelect;
import fr.pasteque.pos.payment.PaymentInfo;
import fr.pasteque.pos.payment.PaymentMode;
import fr.pasteque.pos.printer.TicketPrinterException;
import fr.pasteque.pos.printer.document.OpenDrawerPrintScript;
import fr.pasteque.pos.printer.document.TicketParser;
import fr.pasteque.pos.printer.document.TicketPrintScript;
import fr.pasteque.pos.sales.restaurant.JTicketsBagRestaurant;
import fr.pasteque.pos.sales.restaurant.JTicketsBagRestaurantMap;
import fr.pasteque.pos.sales.shared.JTicketsBagShared;
import fr.pasteque.pos.scale.ScaleDialog;
import fr.pasteque.pos.scale.ScaleException;
import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.util.Alarm;
import fr.pasteque.pos.util.Price;
import fr.pasteque.pos.widgets.JLabelClock;
import fr.pasteque.pos.widgets.JNumberKeys;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author adrianromero
 */
public abstract class JPanelTicket extends JPanel
implements JPanelView, BeanFactoryApp, TicketsEditor,
DataLogicCustomers.CustomerListener, KeypadManagerListener, ShortcutListener,
CustomShortcutListener
{
    private static final long serialVersionUID = 5787045683499307342L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.sales.JPanelTicket");

    protected JTicketLines m_ticketlines;

    // private Template m_tempLine;
    private TicketParser m_TTP;

    /** The catalog currently displayed (for compositions) */
    protected Component m_catalog;
    protected CatalogSelector m_cat;
    /** Keypad display state */
    protected boolean keypadCollapsed = false;

    private JTicketsBag m_ticketsbag;
    private KeypadManager keypadManager;
    protected SalesController controller;

    private ListKeyed<TaxInfo> taxcollection;

    private TaxesLogic taxeslogic;

    private List<TariffInfo> m_TariffList;

    protected JPanelButtons m_jbtnconfig;

    protected AppView m_App;
    protected DataLogicSystem dlSystem;
    protected DataLogicSales dlSales;
    protected DataLogicCustomers dlCustomers;

    private JPaymentSelect paymentdialog;

    private Alarm alarm;

    /** Creates new form JTicketView */
    public JPanelTicket() {
        initComponents ();
        this.keypadManager = new KeypadManager(m_jNumberKeys, m_jPrice,
                m_jEnter, this);
    }

    public void init(AppView app) throws BeanFactoryException {
        m_App = app;
        this.dlSystem = new DataLogicSystem();
        this.dlSales = new DataLogicSales();
        this.dlCustomers = new DataLogicCustomers();
        try {
            this.controller = new SalesController(new OrderManager(m_App),
                    new DataLogicReceipts(), this.dlSales);
        } catch (BasicException e) {
            JMessageDialog.showMessage(this,
                    new MessageInf(MessageInf.SGN_DANGER,
                    "Unable to access local cache, please restart and reload"));
            return;
        }

        m_ticketsbag = getJTicketsBag();
        m_jPanelBag.add(m_ticketsbag.getBagComponent(), BorderLayout.LINE_START);
        add(m_ticketsbag.getNullComponent(), "null");

        m_ticketlines.init(dlSystem.getResourceAsXML("Ticket.Line"));

        m_TTP = new TicketParser(m_App.getDeviceTicket(), dlSystem);

        // Print/Open drawer buttons
        m_jbtnconfig = new JPanelButtons(app.getProperties(), this);
        m_jButtonsExt.add(m_jbtnconfig);

        // El panel de los productos o de las lineas...
        m_catalog = getSouthComponent();
        catcontainer.add(getSouthComponent(), BorderLayout.CENTER);

        // ponemos a cero el estado
        this.keypadManager.reset();

        this.alarm = new Alarm("audio/beep.aiff");
        this.updateStackToggleState();
    }

    public Object getBean() {
        return this;
    }

    /**
     * Access to the controller. This should be used to ease transition
     * to SalesController but it shouldn’t be used when the code is clean.
     */
    public SalesController getController() {
        return this.controller;
    }

    protected Component getSouthAuxComponent() {
        // Get the flags to display the price along the products
        AppConfig cfg = AppConfig.loadedInstance;
        String priceVisible = cfg.getProperty("ui.pricevisible");
        boolean displayPrice = !"none".equals(priceVisible);
        boolean includeTaxes = "taxed".equals(priceVisible);
        m_cat = new JCatalogSubgroups(dlSales, displayPrice, includeTaxes,
                Integer.parseInt(cfg.getProperty("ui.buttons.prdwidth")),
                Integer.parseInt(cfg.getProperty("ui.buttons.prdheight")));
        m_cat.getComponent().setPreferredSize(new Dimension(
                0,
                Integer.parseInt(cfg.getProperty("ui.buttons.catheight"))));
        m_cat.addActionListener(new CatalogListener());
        return m_cat.getComponent();
    }

    public JComponent getComponent() {
        return this;
    }

    public void activate() throws BasicException {

        paymentdialog = JPaymentSelect.getDialog(this);
        paymentdialog.init(m_App);

        // Inicializamos el combo de los impuestos.
        java.util.List<TaxInfo> taxlist = this.dlSales.getTaxList();
        taxcollection = new ListKeyed<TaxInfo>(taxlist);

        taxeslogic = new TaxesLogic(taxlist);

        // Initialize tariff area combobox
        m_TariffList = this.dlSales.getTariffAreaList();
        TariffInfo defaultArea = new TariffInfo(-1,
                AppLocal.getIntString("Label.DefaultTariffArea"));
        m_TariffList.add(0, defaultArea);
        m_jTariff.removeAllItems();
        m_jTariff.addItems(m_TariffList);
        if (m_TariffList.size() > 1) {
            this.updateTariffCombo();
            m_jTariff.setVisible(true);
        } else {
            m_jTariff.setVisible(false);
        }

        // Authorization for buttons
        btnSplit.setEnabled(m_App.getAppUserView().getUser().hasPermission("sales.Total"));
        m_jDelete.setEnabled(m_App.getAppUserView().getUser().hasPermission("sales.EditLines"));
        m_jNumberKeys.setMinusEnabled(m_App.getAppUserView().getUser().hasPermission("sales.EditLines"));
        m_jNumberKeys.setEqualsEnabled(m_App.getAppUserView().getUser().hasPermission("sales.Total"));
        m_jbtnconfig.setPermissions(m_App.getAppUserView().getUser());

        AppConfig cfg = AppConfig.loadedInstance;
        boolean autoCollapse = "0".equals(cfg.getProperty("ui.showkeypad"));
        if (autoCollapse != this.keypadCollapsed) {
            this.collapseKeypad(autoCollapse);
        }
        m_ticketsbag.activate();

        this.clock.activate();
        // Set shortcuts
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this, ShortcutSections.SALES, this);
        config.addCustomListener(CustomShortcut.TYPE_PRODUCT, this, this);
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        if (this.controller.getOrderManager().getOrder() == null) { // No active order (restaurant map)
            return false;
        }
        switch (s) {
            case ORDER_UP:
                m_jUp.doClick(Shortcuts.CLICK_TIME);
                return true;
            case ORDER_DOWN:
                m_jDown.doClick(Shortcuts.CLICK_TIME);
                return true;
            case ORDER_INCLINE:
                m_jPlus.doClick(Shortcuts.CLICK_TIME);
                return true;
            case ORDER_DECLINE:
                m_jMinus.doClick(Shortcuts.CLICK_TIME);
                return true;
            case ORDER_DELLINE:
                m_jDelete.doClick(Shortcuts.CLICK_TIME);
                return true;
            case ORDER_EDITLINE:
                m_jEditLine.doClick(Shortcuts.CLICK_TIME);
                return true;
            case SALES_CATUP:
                m_cat.selectItemAt(m_cat.getSelectedIndex() - 1);
                return true;
            case SALES_CATDOWN:
                m_cat.selectItemAt(m_cat.getSelectedIndex() + 1);
                return true;
            case SALES_RELOAD:
                btnCustomerReload.doClick(Shortcuts.CLICK_TIME);
                return true;
            case SALES_CUSTOMER:
                btnCustomer.doClick(Shortcuts.CLICK_TIME);
                return true;
            case SALES_DISCOUNT:
                if(btnTicketDiscount.isVisible()){
                    btnTicketDiscount.doClick(Shortcuts.CLICK_TIME);
                }
                return true;
            case SALES_SPLIT:
                btnSplit.doClick(Shortcuts.CLICK_TIME);
                return true;
            case SALES_NEWORDER:
                // TODO TicketBag button shortcuts
                return true;
            case SALES_ORDERS:
                // TODO TicketBag button shortcuts
                return true;
            case SALES_MAP:
                // TODO TicketBag button shortcuts
                return true;
            case SALES_SEARCH:
                m_jList.doClick(Shortcuts.CLICK_TIME);
                return true;
            case SALES_BARCODE:
                // hardcoded to enter because of barcode readers.
                return false;
            case SALES_PAY:
                this.payTicket();
                return true;
            default: // Ignore
                return false;
        }
    }

    @Override
    public void customShortcutPressed(CustomShortcut cs) {
        // From CustomShortcutListener
        for (char c : cs.getShortcut().toCharArray()) {
            keypadManager.inputChar(c);
        }
        keypadManager.inputChar(KeypadManager.KEY_ENTER);
    }

    public boolean deactivate() {
        this.clock.deactivate();
        ShortcutConfig shortcuts = ShortcutConfig.getInstance();
        shortcuts.removeListener(this, ShortcutSections.SALES, this);
        shortcuts.removeCustomListener(CustomShortcut.TYPE_PRODUCT, this, this);
        return m_ticketsbag.deactivate();
    }

    protected abstract JTicketsBag getJTicketsBag();
    protected abstract Component getSouthComponent();
    protected abstract void resetSouthComponent();

    public void setActiveOrder(SharedTicketInfo order, Object oTicketExt) {
        try {
            this.controller.setOrder(order, oTicketExt);
        } catch (BasicException e) {
            new MessageInf(e).show(this);
            this.controller.forceSkipPersistence();
            try {
                this.controller.setOrder(order, oTicketExt);
            } catch (BasicException e2) {
                // Should not happen
                new MessageInf(e).show(this);
                return;
            }
        }
        refreshTicket();
        this.updateTariffCombo();
        this.messageBox.updateOrderMessage(this.controller.getOrderManager());
        // Show customers count if not set in restaurant mode
        // and only when opening a new table
        if (this.m_ticketsbag instanceof JTicketsBagRestaurantMap
                && order != null && order.isEmpty()
                && !AppConfig.loadedInstance.getProperty("ui.autodisplaycustcount").equals("0")) {
            ((JTicketsBagRestaurant)this.m_ticketsbag.getBagComponent()).custCountBtnActionPerformed(null);
        }
    }

    public SharedTicketInfo getActiveOrder() {
        return this.controller.getOrderManager().getOrder();
    }

    /**
     * Assign or remove a customer to the current order.
     * @param customer The customer to assign, null to unassign.
     */
    protected void assignCustomer(CustomerInfoExt customer) {
        this.controller.assignCustomer(customer);
        this.messageBox.updateOrderMessage(this.controller.getCurrentOrder().getTicket());
        this.updateTariffCombo();
        this.nextCustomer(true);
        refreshTicket();
    }

    public void setCustomersCount(int count) {
        this.controller.setCustomersCount(count);
        this.updateOrderLabel();
    }

    protected void updateOrderLabel() {
        if (this.controller.getCurrentOrder() == null) {
            this.orderLabel.update(null, null);
        } else {
            this.orderLabel.update(this.controller.getCurrentOrder(),
                    this.controller.getCurrentOrderExtra());
        }
    }

    private void refreshTicket() {
        CardLayout cl = (CardLayout)(getLayout());
        this.updateOrderLabel();
        SharedTicketInfo order = this.controller.getCurrentOrder();
        if (order == null) {
            m_ticketlines.clearTicketLines();
            m_jTotalEuros.setText(null);
            this.discountLabel.setText(null);

            this.keypadManager.reset();

            // Muestro el panel de nulos.
            cl.show(this, "null");
            resetSouthComponent();

        } else {
            TicketInfo orderContent = order.getTicket();
            if (orderContent.getTicketType() == TicketInfo.RECEIPT_REFUND) {
                //Make disable Search and Edit Buttons
                m_jEditLine.setVisible(false);
                m_jList.setVisible(false);
            }
            // Limpiamos todas las filas y anadimos las del ticket actual
            m_ticketlines.clearTicketLines();

            for (int i = 0; i < orderContent.getLinesCount(); i++) {
                m_ticketlines.addTicketLine(orderContent.getLine(i));
            }
            printPartialTotals();
            this.keypadManager.reset();

            // Muestro el panel de tickets.
            cl.show(this, "ticket");
            resetSouthComponent();

            // activo el tecleador...
            m_jKeyFactory.setText(null);
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    m_jKeyFactory.requestFocus();
                }
            });

            if (this.m_ticketsbag instanceof JTicketsBagShared) {
                // Update button to switch order
                ((JTicketsBagShared)this.m_ticketsbag).refreshBagButtons();
            }
        }
    }

    /** Update tariff area combo box to select the area of the ticket. */
    protected void updateTariffCombo() {
        TariffInfo currentTA = this.controller.getCurrentTariffArea();
        // No selected tariff area
        if (currentTA == null) {
            m_jTariff.setSelectedIndex(0);
            return;
        }
        // Select the current tariff area
        TicketInfo ticket = this.controller.getCurrentOrder().getTicket();
        for (int i= 0; i < m_jTariff.getItemCount(); i++) {
            try {
                TariffInfo tariff = m_jTariff.getItemAt(i).getItem();
                if (tariff != null) {
                   if (Integer.valueOf(tariff.getID()).equals(ticket.getTariffArea())) {
                        m_jTariff.setSelectedIndex(i);
                        return;
                    }
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
        // Tariff area not found, deselect
        m_jTariff.setSelectedIndex(0);
    }

    private void switchTariffArea(TariffInfo area) {
        try {
            this.controller.switchTariffArea(area);
        } catch (BasicException e) {
            new MessageInf(e).show(this);
        }
        this.updateTariffCombo();
        this.refreshTicket();
    }

    private void printPartialTotals() {
        TicketInfo order = this.controller.getCurrentOrder().getTicket();
        m_jTotalEuros.setText(order.printTotal());
        if (order.getDiscountRate() > 0.0) {
            this.discountLabel.setText(AppLocal.tr("label.totalDiscount",
                    order.printDiscountRate(), order.printFullTotal()));
        } else {
            this.discountLabel.setText(null);
        }
    }

    /**
     * Reset the customer display and info message when starting
     * to edit a new order.
     * Does nothing when the order is already being edited.
     * @param resetCustDisplay Whether or not reset the customer display.
     * Set to false when setting an other message right after that.
     */
    private void nextCustomer(boolean resetCustDisplay) {
        if (!this.btnNext.isEnabled()) {
            return;
        }
        this.btnNext.setEnabled(false);
        if (resetCustDisplay) {
            // Reset the customer display to the welcome message
            String sresource = dlSystem.getResourceAsXML("Printer.Start");
            if (sresource == null) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                        AppLocal.tr("message.cannotprintticket"));
                msg.show(JPanelTicket.this);
            } else {
                try {
                    ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                    m_TTP.printTicket(script.eval(sresource).toString());
                } catch (ScriptException e) {
                    MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                            AppLocal.tr("message.cannotprintticket"), e);
                    msg.show(JPanelTicket.this);
                } catch (TicketPrinterException e) {
                    MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                            AppLocal.tr("message.cannotprintticket"), e);
                    msg.show(JPanelTicket.this);
                }
            }
        }
        // Reset the information message
        this.messageBox.setInfoMessage("");
    }

    /**
     * Refresh the display of the ticket line and select it.
     * @param index The index of the line.
     */
    private void paintTicketLine(int index){
        TicketLineInfo line = this.controller.getCurrentOrder().getTicket().getLine(index);
        if (index == m_ticketlines.getLineCount()) {
            m_ticketlines.addTicketLine(line);
        } else {
            m_ticketlines.setTicketLine(index, line);
        }
        m_ticketlines.setSelectedIndex(index);
        visorTicketLine(line); // Y al visor tambien...
        printPartialTotals();
        this.keypadManager.reset();
    }

    /**
     * Update the GUI after an OrderManager operation.
     * @param r The result of the operation.
     */
    private void onOrderManagerRet(OrderManager.Ret r) {
        boolean linesCountChanged = false;
        switch (r.getOperation()) {
            case OrderManager.OP_ADD:
                TicketLineInfo l = this.controller.getCurrentOrder().getTicket().getLine(r.i());
                m_ticketlines.insertTicketLine(r.i(), l);
                visorTicketLine(l);
                printPartialTotals();
                linesCountChanged = true;
                break;
            case OrderManager.OP_UPD:
                for (int upd = 0; upd < r.n(); upd++) {
                    this.paintTicketLine(r.i() + upd);
                }
                printPartialTotals();
                break;
            case OrderManager.OP_RM:
                m_ticketlines.removeTicketLine(r.i(), r.n());
                visorTicketLine(null); // borro el visor
                printPartialTotals(); // pinto los totales parciales...
                linesCountChanged = true;
                break;
            case OrderManager.OP_ERR:
                Toolkit.getDefaultToolkit().beep();
                if (!"0".equals(AppConfig.loadedInstance.getProperty("ui.beepline"))) {
                    this.alarm.soundplay();
                }
                break;
        }
        this.keypadManager.reset();
        if (linesCountChanged && (this.m_ticketsbag instanceof JTicketsBagShared)) {
            // Update button to create a new order
            ((JTicketsBagShared)this.m_ticketsbag).refreshBagButtons();
        }
    }

    protected void addLine(TicketLineInfo oLine) {
        if (oLine.isProductCom()) {
            this.onOrderManagerRet(this.controller.getOrderManager().addSubline(oLine,
                    m_ticketlines.getSelectedIndex()));
        } else {
            this.onOrderManagerRet(this.controller.getOrderManager().addLine(oLine));
        }
        this.nextCustomer(false);
    }

    private void removeTicketLine(int i){
        this.onOrderManagerRet(this.controller.removeLine(i));
        this.nextCustomer(false);
    }

    /** Adjust the quantity of the selected line and refresh it. */
    private void adjustSelectedLineQuantity(double quantity) {
        int i = m_ticketlines.getSelectedIndex();
        this.onOrderManagerRet(this.controller.adjustLineQuantity(i, quantity));
        this.nextCustomer(false);
    }

    @Override
    public void onIncorrectQuantity(KeypadManager manager) {
        Toolkit.getDefaultToolkit().beep();
        new MessageInf(MessageInf.SGN_WARNING,
                AppLocal.getIntString("Message.InvalidQuantity",
                        manager.getInput())).show(this);
    }

    @Override
    public void onCustomerInput(KeypadManager manager, String customerCard) {
        try {
            CustomerInfoExt newcustomer =
                    this.dlCustomers.getCustomerByCard(customerCard);
            if (newcustomer == null) {
                // No customer, beep and show message
                Toolkit.getDefaultToolkit().beep();
                new MessageInf(MessageInf.SGN_WARNING,
                        AppLocal.getIntString("message.nocustomer")).show(this);
            } else {
                this.assignCustomer(newcustomer);
            }
        } catch (BasicException e) {
            // ERROR
            Toolkit.getDefaultToolkit().beep();
            new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.nocustomer"), e).show(this);
        }
    }

    @Override
    public void onScaledBarcodeInput(KeypadManager manager, double quantity,
            String barcode) {
        incProductByCode(quantity, barcode);
    }

    @Override
    public void onEmptyInput(KeypadManager manager) {
        Toolkit.getDefaultToolkit().beep();
    }

    @Override
    public void onOtherInput(KeypadManager manager, double quantity,
            String barcode) {
        this.incProductByCode(quantity, barcode);
    }

    @Override
    public void onCheckout(KeypadManager manager) {
        this.payTicket();
    }

    /**
     * Add a product from its barcode, show an error when no product is found
     * with this code. Calls incProduct.
     */
    private void incProductByCode(double quantity, String sCode) {
    // precondicion: sCode != null
        try {
            ProductInfoExt oProduct = dlSales.getProductInfoByCode(sCode);
            if (oProduct == null) {
                Toolkit.getDefaultToolkit().beep();
                new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.noproduct")).show(this);
            } else {
                incProduct(quantity, oProduct);
            }
        } catch (BasicException eData) {
            new MessageInf(eData).show(this);
        }
    }

    /** Add a product, ask for quantity when it is scaled. Calls incProduct. */
    private void incProduct(ProductInfoExt prod) {
        if (prod.isScale()) {
            try {
                ScaleDialog scaleDialog = new ScaleDialog(this, m_App.getProperties());
                Double value = scaleDialog.readWeight(prod);
                if (value != null) {
                    incProduct(value.doubleValue(), prod);
                }
            } catch (ScaleException e) {
                Toolkit.getDefaultToolkit().beep();
                new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.noweight"), e).show(this);
            }
        } else {
            // No es un producto que se pese o no hay balanza
            incProduct(1.0, prod);
        }
    }

    /** Add a product to the order with quantity. */
    private void incProduct(double quantity, ProductInfoExt product) {
        try {
            OrderManager.Ret r = this.controller.pickProduct(quantity, product);
            this.nextCustomer(false);
            this.onOrderManagerRet(r);
        } catch (BasicException e) {
            new MessageInf(e).show(this);
        }
    }

    /** Switch catalog according to composition state */
    public void changeCatalog() {
        catcontainer.removeAll();
        setSubgroupMode(this.controller.isCompositing());
        if (this.controller.isCompositing()) {
            // Set composition catalog
            m_catalog = getSouthAuxComponent();
        } else {
            // Set default catalog
            m_catalog = getSouthComponent();
        }
        catcontainer.add(m_catalog, BorderLayout.CENTER);
        catcontainer.updateUI();
    }

    /** Activate or deactivate input component for subgroups (or not) */
    private void setSubgroupMode(boolean value) {
        this.lineBtnsContainer.setEnabled(!value);
        enableComponents(this.lineBtnsContainer, !value);
        enableComponents(m_jPanEntries, !value);
        if (!value) {
            m_jKeyFactory.requestFocusInWindow();
        }
    }

    private void enableComponents(Container cont, boolean value) {
        for (Component c: cont.getComponents()) {
            try {
                c.setEnabled(value);
                enableComponents((Container) c, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handle the click on a product button.
     * @param prod The product associated with the button.
     */
    protected void buttonTransition(ProductInfoExt prod) {
        // Get the quantity from the text input
        Double input = null;
        try {
            input = this.keypadManager.getInputNumber();
        } catch (NumberFormatException e) {
            this.onIncorrectQuantity(this.keypadManager);
            this.keypadManager.reset();
            return;
        }
        double quantity = (input == null) ? 1.0 : input;
        // precondicion: prod != null
        // Add the line
        if (prod.isScale() && input == null) {
            this.incProduct(prod);
        } else {
            this.incProduct(quantity, prod);
        }
    }

    /** Go to the payment window and register/cancel the sale (blocking method). */
    private void payTicket() {
        if (this.controller.getOrderManager().getOrderContent().getLinesCount() > 0) {
            if (this.closeTicket(this.controller.getOrderManager().getOrderContent(),
                    this.controller.getOrderManager().getOrderExtra())) {
                // Ends edition of current receipt
                m_ticketsbag.closeCurrentOrder();
            } else {
                // repaint current ticket
                refreshTicket();
            }
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    private boolean closeTicket(TicketInfo ticket, Object ticketext) {

        boolean resultok = false;

        if (m_App.getAppUserView().getUser().hasPermission("sales.Total")) {
            // Check customer for refill if any
            for (int i = 0; i < ticket.getLinesCount(); i++) {
                TicketLineInfo line = ticket.getLine(i);
                if (line.getProductID() != null) {
                    try {
                        ProductInfoExt product = dlSales.getProductInfo(line.getProductID());
                        if(product.isPrepay() && ticket.getCustomer() == null) {
                            JMessageDialog.showMessage(this,
                                    AppLocal.tr("Message.NoCustomerPrepayRefillTitle"),
                                    AppLocal.tr("Message.NoCustomerPrepayRefill"));
                            return false;
                        }
                    } catch (BasicException e) {
                        logger.log(Level.WARNING, "Error while loading product from cache " + line.getProductID() + ": " + e.getMessage());
                    // Assume this is not a prepayment
                    }
                }
            }
            // reset the payment info
            taxeslogic.calculateTaxes(ticket);
            if (ticket.getTotal()>=0.0){
                ticket.resetPayments(); //Only reset if is sale
            }

            // Muestro el total
            printTicket("Printer.TicketTotal", ticket, ticketext);


            // Select the Payments information
            boolean autoPrint;
            autoPrint = AppConfig.loadedInstance.getBoolean("ui.printticketbydefault");
            paymentdialog.setPrintSelected(autoPrint);

            paymentdialog.setTransactionID(ticket.getTransactionID());

            if (paymentdialog.showDialog(ticket.getTotal(), ticket.getCustomer())) {

                // assign the payments selected and calculate taxes.
                ticket.setPayments(paymentdialog.getSelectedPayments());
                // Get the customer balance update
                double custBalance = 0.0;
                if (ticket.getCustomer() != null) {
                    CustomerInfoExt cust = ticket.getCustomer();
                    // Prepaid collection
                    for (TicketLineInfo line : ticket.getLines()) {
                        if (line.getProductID() != null) {
                            try {
                                ProductInfoExt prd = dlSales.getProductInfo(line.getProductID());
                                if (prd.isPrepay()) {
                                    custBalance = Price.add(custBalance,
                                            line.getFullSubValue());
                                }
                            } catch (BasicException e) {
                                JMessageDialog.showMessage(JPanelTicket.this,
                                        new MessageInf(MessageInf.SGN_DANGER,
                                                "Unable to find product in ticket. Prepayment is not updated localy.", e.getMessage()));
                                logger.log(Level.SEVERE, "No product found while saving ticket " + line.getProductID() + ": " + e.getMessage());
                            }
                        }
                    }
                    // Prepaid and debt use
                    for (PaymentInfo pmt : ticket.getPayments()) {
                        double amount = pmt.getAmount();
                        PaymentMode pm = pmt.getMode();
                        if (pm.isDebt() || pm.isPrepaid()) {
                            custBalance = Price.add(custBalance, -amount);
                        }
                    }
                    // Assign to ticket and update Customer locally before printing
                    if (custBalance > 0.005 || custBalance < 0.005) {
                        ticket.setCustBalance(custBalance);
                        cust.updateBalance(custBalance);
                    }
                }
                // Asigno los valores definitivos del ticket...
                ticket.setUser(m_App.getAppUserView().getUser().getUserInfo()); // El usuario que lo cobra
                ticket.setDate(new Date()); // Le pongo la fecha de cobro
                ticket.validate(m_App.getActiveCashSession(),
                        m_App.getCashRegister().getNextTicketId());
                // Save the receipt and assign a receipt number
                try {
                    dlSales.saveTicket(ticket);
                } catch (BasicException eData) {
                    MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.nosaveticket"), eData);
                    msg.show(this);
                }
                // Update the customer's balance locally
                if (custBalance > 0.005 || custBalance < -0.005) {
                    try {
                        // It was already updated before, just save it.
                        CustomerInfoExt cust = ticket.getCustomer();
                        CustomersCache.refreshCustomer(cust);
                    } catch (BasicException e) {
                        JMessageDialog.showMessage(JPanelTicket.this,
                                new MessageInf(MessageInf.SGN_DANGER,
                                        "Unable to update customer in cache. Prepayment and debt are not updated localy.", e.getMessage()));
                        logger.log(Level.SEVERE, "Unable to update customer in cache on ticket save. " + e.getMessage());
                    }
                }

                // Print receipt.
                if (paymentdialog.isPrintSelected()) {
                    int printCount = paymentdialog.getPrintCount();
                    for (int printNum = 0; printNum < JPaymentSelect.PRINTER_COUNT; printNum++) {
                        if (paymentdialog.isPrintSelected(printNum)) {
                            for (int i = 0; i < printCount; i++) {
                                printTicket("Printer.Ticket", ticket, ticketext, printNum + 1);
                            }
                        }
                    }
                } else {
                    printTicket("Printer.Ticket2", ticket, ticketext);
                }
                resultok = true;

                // Update message box
                String lastGiveBack = null;
                for (PaymentInfo p : paymentdialog.getSelectedPayments()) {
                    PaymentInfo back = p.getBackPayment();
                    if (back != null) {
                        lastGiveBack = back.printPaid();
                        if (lastGiveBack.charAt(0) == '-') {
                            lastGiveBack = lastGiveBack.substring(1);
                        }
                    }
                }
                if (lastGiveBack == null) {
                    this.messageBox.setInfoMessage(AppLocal.tr("MsgBox.LastCheckout",
                            ticket.printTotal(), this.clock.getClock()));
                } else {
                    this.messageBox.setInfoMessage(AppLocal.tr("MsgBox.LastCheckoutWChange",
                            ticket.printTotal(), this.clock.getClock(), lastGiveBack));
                }
                // Increment next ticket id
                m_App.getCashRegister().incrementNextTicketId();
                try {
                    CashRegistersCache.saveCashRegister(m_App.getCashRegister());
                } catch (BasicException be) {
                    JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER,
                            "Unable to save cash session",
                            "Please do not restart Pasteque and reload data."));
                    logger.log(Level.SEVERE, "Unable to save cash session", be);
                    be.printStackTrace();
                }
                // Enable "Next client" button
                this.btnNext.setEnabled(true);
            }

            // reset the payment info
            this.controller.getOrderManager().getOrderContent().resetTaxes();
            this.controller.getOrderManager().getOrderContent().resetPayments();
        }

        // cancelled the ticket.total script
        // or canceled the payment dialog
        // or canceled the ticket.close script
        return resultok;
    }

    private void printTicket(String sresourcename, TicketInfo ticket, Object ticketext) {
        this.printTicket(sresourcename, ticket, ticketext, 1);
    }
    private void printTicket(String sresourcename, TicketInfo ticket, Object ticketext, int printerNum) {
        String sresource;
        boolean printTicket = !"Printer.OpenDrawer".equals(sresourcename);
        String cplKey = (printerNum == 1) ?
                "machine.printer.cpl":
                "machine.printer." + printerNum + ".cpl";
        Double cpl = AppConfig.loadedInstance.getNumber(cplKey);
        if (cpl == null) {
            cpl = Double.valueOf(48.0);
        }
        TicketPrintScript printScript = new TicketPrintScript(cpl.intValue());
        if ("Printer.Ticket".equals(sresourcename)) {
            String placeName = null;
            if (ticketext != null && (ticketext instanceof String)) {
                placeName = (String) ticketext;
            }
            sresource = printScript.getTicketXml(ticket, printerNum, placeName, true);
        } else if ("Printer.TicketPreview".equals(sresourcename)) {
            boolean forCustomer = printerNum == 1;
            String placeName = null;
            if (ticketext != null && (ticketext instanceof String)) {
                placeName = (String) ticketext;
            }
            sresource = printScript.getOrderXml(ticket, printerNum, placeName, forCustomer);
        } else if ("Printer.OpenDrawer".equals(sresourcename)) {
            sresource = OpenDrawerPrintScript.getOpenDrawerXml();
        } else {
            sresource = dlSystem.getResourceAsXML(sresourcename);
        }
        if (printTicket) {
            taxeslogic.calculateTaxes(ticket);
        }
        if (sresource == null) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"));
            msg.show(JPanelTicket.this);
        } else {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                if (printTicket) {
                    script.put("taxes", taxcollection);
                    script.put("taxeslogic", taxeslogic);
                    script.put("ticket", ticket);
                    script.put("place", ticketext);
                }
                m_TTP.printTicket(script.eval(sresource).toString());
            } catch (ScriptException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                msg.show(JPanelTicket.this);
            } catch (TicketPrinterException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
                msg.show(JPanelTicket.this);
            }
        }
    }

    private void visorTicketLine(TicketLineInfo oLine){
        if (oLine == null) {
             m_App.getDeviceTicket().getDeviceDisplay().clearVisor();
        } else {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
                script.put("ticketline", oLine);
                m_TTP.printTicket(script.eval(dlSystem.getResourceAsXML("Printer.TicketLine")).toString());
            } catch (ScriptException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintline"), e);
                msg.show(JPanelTicket.this);
            } catch (TicketPrinterException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintline"), e);
                msg.show(JPanelTicket.this);
            }
        }
    }


    private Object evalScript(ScriptObject scr, String resource, ScriptArg... args) {

        // resource here is guaratied to be not null
         try {
            scr.setSelectedIndex(m_ticketlines.getSelectedIndex());
            return scr.evalScript(dlSystem.getResourceAsXML(resource), args);
        } catch (ScriptException e) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotexecute"), e);
            msg.show(this);
            return msg;
        }
    }

    public void evalScriptAndRefresh(String resource, ScriptArg... args) {

        if (resource == null) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotexecute"));
            msg.show(this);
        } else {
            ScriptObject scr = new ScriptObject(this.controller.getOrderManager().getOrderContent(),
                    this.controller.getOrderManager().getOrderExtra());
            scr.setSelectedIndex(m_ticketlines.getSelectedIndex());
            evalScript(scr, resource, args);
            refreshTicket();
            setSelectedIndex(scr.getSelectedIndex());
        }
    }

    public void printTicket(String resource) {
        printTicket(resource, this.controller.getOrderManager().getOrderContent(),
                this.controller.getOrderManager().getOrderExtra());
    }

    public void printOrder(int printerNum) {
       this.printTicket("Printer.TicketPreview", this.controller.getOrderManager().getOrderContent(), this.controller.getOrderManager().getOrderExtra(), printerNum);
    }

    public void openDrawer() {
        this.printTicket("Printer.OpenDrawer", this.controller.getOrderManager().getOrderContent(), this.controller.getOrderManager().getOrderExtra());
    }

    public String getResourceAsXML(String sresourcename) {
        return dlSystem.getResourceAsXML(sresourcename);
    }

    public BufferedImage getResourceAsImage(String sresourcename) {
        return dlSystem.getResourceAsImage(sresourcename);
    }

    private void setSelectedIndex(int i) {

        if (i >= 0 && i < this.controller.getOrderManager().getOrderContent().getLinesCount()) {
            m_ticketlines.setSelectedIndex(i);
        } else if (this.controller.getOrderManager().getOrderContent().getLinesCount() > 0) {
            m_ticketlines.setSelectedIndex(this.controller.getOrderManager().getOrderContent().getLinesCount() - 1);
        }
    }

    public static class ScriptArg {
        private String key;
        private Object value;

        public ScriptArg(String key, Object value) {
            this.key = key;
            this.value = value;
        }
        public String getKey() {
            return key;
        }
        public Object getValue() {
            return value;
        }
    }

    public class ScriptObject {

        private TicketInfo ticket;
        private Object ticketext;

        private int selectedindex;

        private ScriptObject(TicketInfo ticket, Object ticketext) {
            this.ticket = ticket;
            this.ticketext = ticketext;
        }

        public double getInputValue() {
            Double input = JPanelTicket.this.keypadManager.getInputNumber();
            return (input == null) ? 1.0 : input;
        }

        public int getSelectedIndex() {
            return selectedindex;
        }

        public void setSelectedIndex(int i) {
            selectedindex = i;
        }

        public void printTicket(String sresourcename) {
            JPanelTicket.this.printTicket(sresourcename, ticket, ticketext);
        }

        public Object evalScript(String code, ScriptArg... args) throws ScriptException {

            ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.BEANSHELL);
            script.put("ticket", ticket);
            script.put("place", ticketext);
            script.put("taxes", taxcollection);
            script.put("taxeslogic", taxeslogic);
            script.put("user", m_App.getAppUserView().getUser());
            script.put("sales", this);

            // more arguments
            for(ScriptArg arg : args) {
                script.put(arg.getKey(), arg.getValue());
            }

            return script.eval(code);
        }
    }

    protected class CatalogListener implements ActionListener {
        private void reloadCatalog () {
            changeCatalog();
            try {
                m_cat.loadCatalog();
            } catch (BasicException e) {
                e.printStackTrace();
            }
        }

        public void actionPerformed(ActionEvent e) {
            if ( (e.getSource()).getClass().equals(ProductInfoExt.class) ) {
                // Clicked on a product
                ProductInfoExt prod = ((ProductInfoExt) e.getSource());
                if (prod.isCompo()) {
                    // Clicked on a composition
                    buttonTransition(prod);
                    reloadCatalog();
                    m_cat.showCatalogPanel(prod.getID());
                } else {
                    // Clicked on a regular product
                    boolean compositing = controller.isCompositing();
                    buttonTransition(prod);
                    if (compositing && !controller.isCompositing()) {
                        // End of composition
                        reloadCatalog();
                    }
                }
            } else {
                // Si se ha seleccionado cualquier otra cosa...
                // Si es una orden de cancelar la venta de una composición
                if ( e.getActionCommand().equals("cancelSubgroupSale")){
                    onOrderManagerRet(controller.cancelComposition());
                    reloadCatalog();
                }
            }
        }

    }

    protected class CatalogSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {

            if (!e.getValueIsAdjusting()) {
                int i = m_ticketlines.getSelectedIndex();

                // Buscamos el primer producto no Auxiliar.
                TicketInfo content = controller.getOrderManager().getOrderContent();
                if (content == null) {
                    return;
                }
                if (i >= content.getLinesCount()) {
                    i = content.getLinesCount() - 1;
                } else {
                    while (i >= 0 && (content.getLine(i).isProductCom() || content.getLine(i).isSubproduct())) {
                        i--;
                    }
                }
                // Mostramos el panel de catalogo adecuado...
                if (i >= 0) {
                    m_cat.showCatalogPanel(controller.getOrderManager().getOrderContent().getLine(i).getProductID());
                } else {
                    m_cat.showCatalogPanel(null);
                }
            }
        }
    }

    public void customerLoaded(CustomerInfoExt customer) {
        if (this.controller.refreshCustomer(customer)) {
            // Loading went well and the customer is still the one on the ticket
            logger.log(Level.INFO, "Customer refreshed from server.");
            this.messageBox.updateOrderMessage(this.controller.getOrderManager());
        } else {
            logger.log(Level.INFO,
                    "Customer refresh failed or customer changed.");
        }
    }

    private void updateStackToggleState() {
        this.controller.getOrderManager().setArticleStacking(m_jbtnStackToggle.isSelected());
        String res = (m_jbtnStackToggle.isSelected()) ?
                "tkt_stack_add.png" : "tkt_stack_new.png";
        String tooltip = (m_jbtnStackToggle.isSelected()) ?
                "Button.StackProductsOn.ToolTip" : "Button.StackProductsOff.ToolTip";
        m_jbtnStackToggle.setIcon(ImageLoader.readImageIcon(res));
        m_jbtnStackToggle.setToolTipText(AppLocal.getIntString(tooltip));
    }

    private void collapseKeypad(boolean collapsed) {
        this.keypadCollapsed = collapsed;
        if (this.keypadCollapsed) {
            this.btnKeypadCollapse.setIcon(ImageLoader.readImageIcon("menu-up.png"));
        } else {
            this.btnKeypadCollapse.setIcon(ImageLoader.readImageIcon("menu-down.png"));
        }
        this.m_jNumberKeys.setCollapsed(this.keypadCollapsed);
        this.m_jNumberKeys.getParent().validate();
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnspacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        JPanel m_jPanContainer = new JPanel(); // The main container
        m_jButtonsExt = new JPanel();
        m_jPanelBag = new JPanel();
        lineBtnsContainer = new JPanel();
        m_jUp = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_up.png"), AppLocal.getIntString("Button.m_jUpSales.toolTip"));
        m_jDown = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_down.png"), AppLocal.getIntString("Button.m_jDownSales.toolTip"));
        m_jPlus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_plus.png"), AppLocal.getIntString("Button.m_jPlus.toolTip"));
        m_jMinus = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_minus.png"), AppLocal.getIntString("Button.m_jMinus.toolTip"));
        m_jDelete = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_delete.png"), AppLocal.getIntString("Button.m_jDelete.toolTip"));
        m_jList = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_search.png"), AppLocal.getIntString("Button.m_jList.toolTip"));
        m_jEditLine = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_edit.png"), AppLocal.getIntString("Button.m_jEditLine.toolTip"));
        m_jbtnLineDiscount = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_discount.png"), AppLocal.getIntString("Button.m_jbtnLineDiscount.toolTip"));
        this.btnKeypadCollapse = WidgetsBuilder.createButton(ImageLoader.readImageIcon("menu-down.png"));
        m_jPanTotals = new JPanel();
        m_jTotalEuros = WidgetsBuilder.createImportantLabel();
        this.discountLabel = WidgetsBuilder.createLabel();
        m_jPanEntries = new JPanel();
        m_jNumberKeys = new JNumberKeys();
        m_jPrice = WidgetsBuilder.createLabel();
        m_jEnter = WidgetsBuilder.createButton(ImageLoader.readImageIcon("barcode.png"),AppLocal.getIntString("Button.m_jEnter.toolTip"));
        m_jKeyFactory = new JTextField();
        catcontainer = new JPanel();
        m_jInputContainer = new JPanel();
        m_jTariff = WidgetsBuilder.createComboBoxVal();

        this.setBackground(new Color(255, 204, 153));
        this.setLayout(new CardLayout());


        m_jPanContainer.setLayout(new GridBagLayout());
        GridBagConstraints cstr = null;

        // Main container is broken into 4 vertical parts
        // Brand header
        // Ticket header
        // Input
        // Footer

        // Brand header
        ///////////////
        JPanel brandHeader = new JPanel();
        brandHeader.setLayout(new GridBagLayout());
        ImageIcon brand = ImageLoader.readImageIcon("logo_flat.png");
        JLabel brandLabel = new JLabel(brand);
        this.clock = new JLabelClock();
        this.messageBox = new fr.pasteque.pos.widgets.JSalesInfoBox();
        this.btnNext = WidgetsBuilder.createButton(ImageLoader.readImageIcon("right_once.png"), AppLocal.getIntString("Button.btnNext.toolTip"));
        this.btnNext.setEnabled(false);
        this.btnNext.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    nextCustomer(true);
                }
        });

        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        brandHeader.add(brandLabel, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.weightx = 1.0;
        cstr.insets = new Insets(5, 10, 5, 10);
        brandHeader.add(this.messageBox, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        brandHeader.add(this.clock, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, 10, 0, 0);
        brandHeader.add(this.btnNext, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(5, 5, 5, 5);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        m_jPanContainer.add(brandHeader, cstr);

        // Ticket info/buttons
        //////////////////////
        JPanel ticketHeader = new JPanel();
        ticketHeader.setLayout(new GridBagLayout());
        // Ticket id
        javax.swing.JPanel orderLblContainer = new javax.swing.JPanel();
        orderLblContainer.setLayout(new GridBagLayout());
        this.orderLabel = new fr.pasteque.pos.widgets.JOrderLabel();
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        orderLblContainer.add(this.orderLabel, cstr);
        // Rename order button
        javax.swing.JButton btnRenameOrder = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_rename.png"),
                AppLocal.tr("Button.BtnRenameOrder.ToolTip"));
        btnRenameOrder.setFocusPainted(false);
        btnRenameOrder.setFocusable(false);
        btnRenameOrder.setRequestFocusEnabled(false);
        btnRenameOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRenameOrderActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, btnspacing, 0, 5);
        orderLblContainer.add(btnRenameOrder, cstr);
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.NONE;
        ticketHeader.add(orderLblContainer, cstr);
        // Customer reload button
        btnCustomerReload = WidgetsBuilder.createButton(ImageLoader.readImageIcon("reload.png"), AppLocal.getIntString("Button.btnCustomerReload.toolTip"));
        btnCustomerReload.setFocusPainted(false);
        btnCustomerReload.setFocusable(false);
        btnCustomerReload.setRequestFocusEnabled(false);
        btnCustomerReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerReloadActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, btnspacing, 0, btnspacing);
        ticketHeader.add(btnCustomerReload, cstr);
        // Customer button
        btnCustomer = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_assign_customer.png"), AppLocal.getIntString("Button.btnCustomer.toolTip"));
        btnCustomer.setFocusPainted(false);
        btnCustomer.setFocusable(false);
        btnCustomer.setRequestFocusEnabled(false);
        btnCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, btnspacing, 0, btnspacing);
        ticketHeader.add(btnCustomer, cstr);
        // Ticket discount
        btnTicketDiscount = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_discount.png"), AppLocal.getIntString("Button.btnTicketDiscount.toolTip"));
        btnTicketDiscount.setFocusPainted(false);
        btnTicketDiscount.setFocusable(false);
        btnTicketDiscount.setRequestFocusEnabled(false);
        btnTicketDiscount.setVisible(cfg.getBoolean("ui.enablediscounts"));
        btnTicketDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketDiscountActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, btnspacing, 0, btnspacing);
        ticketHeader.add(btnTicketDiscount, cstr);
        // Split button
        btnSplit = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_split.png"),AppLocal.getIntString("Button.btnSplit.toolTip"));
        btnSplit.setFocusPainted(false);
        btnSplit.setFocusable(false);
        btnSplit.setRequestFocusEnabled(false);
        btnSplit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSplitActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, 0, 0, btnspacing);
        ticketHeader.add(btnSplit, cstr);
        // Custom product button
        btnCustomProduct = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_customproduct.png"),AppLocal.getIntString("Button.BtnCustomProduct.ToolTip"));
        btnCustomProduct.setFocusPainted(false);
        btnCustomProduct.setFocusable(false);
        btnCustomProduct.setRequestFocusEnabled(false);
        btnCustomProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomProductActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, btnspacing, 0, btnspacing);
        ticketHeader.add(btnCustomProduct, cstr);
        // Stack toggle button
        m_jbtnStackToggle = new javax.swing.JToggleButton();
        m_jbtnStackToggle.setSelected(true);
        m_jbtnStackToggle.setFocusPainted(false);
        m_jbtnStackToggle.setFocusable(false);
        m_jbtnStackToggle.setRequestFocusEnabled(false);
        WidgetsBuilder.adaptSize(m_jbtnStackToggle, WidgetsBuilder.SIZE_MEDIUM);
        m_jbtnStackToggle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateStackToggleState();
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.insets = new Insets(0, 0, 0, btnspacing);
        ticketHeader.add(m_jbtnStackToggle, cstr);

        // Ticket bag extra buttons
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        ticketHeader.add(m_jPanelBag, cstr);
        // Script extra buttons
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        ticketHeader.add(m_jButtonsExt, cstr);
        // Add container
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        m_jPanContainer.add(ticketHeader, cstr);

        // Main zone
        ////////////
        JPanel mainZone = new JPanel();
        mainZone.setLayout(new GridBagLayout());
        // Catalog
        catcontainer.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        catcontainer.setLayout(new java.awt.BorderLayout());
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.gridheight = 3;
        cstr.weightx = 1.0;
        cstr.weighty = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        mainZone.add(catcontainer, cstr);
        // Ticket zone
        JPanel ticketZone = new JPanel();
        ticketZone.setLayout(new GridBagLayout());
        // Tariff area
        m_jTariff.setFocusable(false);
        m_jTariff.setRequestFocusEnabled(false);
        m_jTariff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jTariffActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.gridwidth = 2;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.insets = new Insets(btnspacing, btnspacing,
                btnspacing, btnspacing);
        ticketZone.add(m_jTariff, cstr);
        // Ticket lines
        m_ticketlines = new JTicketLines();
        m_ticketlines.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.weightx = 1.0;
        cstr.weighty = 1.0;
        ticketZone.add(m_ticketlines, cstr);

        JPanel lineEditBtns = new JPanel();
        lineEditBtns.setLayout(new GridBagLayout());
        // Up/down buttons
        // Set listeners even if they are not visible for keyboard shortcuts
        m_jUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jUpActionPerformed(evt);
            }
        });
        m_jDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jDownActionPerformed(evt);
            }
        });
        if (cfg == null
                || cfg.getProperty("ui.showupdownbuttons").equals("1")) {
            m_jUp.setFocusPainted(false);
            m_jUp.setFocusable(false);
            m_jUp.setRequestFocusEnabled(false);
            cstr = new GridBagConstraints();
            cstr.gridx = 0;
            cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
            lineEditBtns.add(m_jUp, cstr);
            m_jDown.setFocusPainted(false);
            m_jDown.setFocusable(false);
            m_jDown.setRequestFocusEnabled(false);
            cstr = new GridBagConstraints();
            cstr.gridx = 0;
            cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
            lineEditBtns.add(m_jDown, cstr);
        }
        // Increase line quantity
        m_jPlus.setFocusPainted(false);
        m_jPlus.setFocusable(false);
        m_jPlus.setRequestFocusEnabled(false);
        m_jPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jPlusActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
        lineEditBtns.add(m_jPlus, cstr);
        // Decrease line quantity
        m_jMinus.setFocusPainted(false);
        m_jMinus.setFocusable(false);
        m_jMinus.setRequestFocusEnabled(false);
        m_jMinus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jMinusActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
        lineEditBtns.add(m_jMinus, cstr);
        // Delete line
        m_jDelete.setFocusPainted(false);
        m_jDelete.setFocusable(false);
        m_jDelete.setRequestFocusEnabled(false);
        m_jDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jDeleteActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
        lineEditBtns.add(m_jDelete, cstr);
        // Find product
        m_jList.setFocusPainted(false);
        m_jList.setFocusable(false);
        m_jList.setRequestFocusEnabled(false);
        m_jList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jListActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
        lineEditBtns.add(m_jList, cstr);
        // Edit line
        m_jEditLine.setFocusPainted(false);
        m_jEditLine.setFocusable(false);
        m_jEditLine.setRequestFocusEnabled(false);
        m_jEditLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jEditLineActionPerformed(evt);
            }
        });
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
        lineEditBtns.add(m_jEditLine, cstr);


        m_jbtnLineDiscount.setFocusPainted(false);
        m_jbtnLineDiscount.setFocusable(false);
        m_jbtnLineDiscount.setRequestFocusEnabled(false);
        m_jbtnLineDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jbtnLineDiscountActionPerformed();
            }
        });
        m_jbtnLineDiscount.setVisible(cfg.getBoolean("ui.enablediscounts"));
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
        lineEditBtns.add(m_jbtnLineDiscount, cstr);



        // Add line edit container
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 1;
        cstr.weighty = 1.0;
        cstr.fill = GridBagConstraints.VERTICAL;
        ticketZone.add(lineEditBtns, cstr);
        // Total zone
        JPanel totalZone = new JPanel();
        totalZone.setLayout(new GridBagLayout());
        // Discount
        this.discountLabel.setRequestFocusEnabled(false);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.anchor = GridBagConstraints.CENTER;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        totalZone.add(this.discountLabel, cstr);
        // Total
        m_jTotalEuros.setRequestFocusEnabled(false);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 0;
        cstr.anchor = GridBagConstraints.FIRST_LINE_END;
        cstr.weightx = 1.0;
        //cstr.fill = GridBagConstraints.HORIZONTAL;
        totalZone.add(m_jTotalEuros, cstr);
        // Add total zone
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 2;
        cstr.gridwidth = 2;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        ticketZone.add(totalZone, cstr);
        // Add ticket zone
        cstr = new GridBagConstraints();
        cstr.gridy = 0;
        cstr.gridx = 1;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.weighty = 1.0;
        mainZone.add(ticketZone, cstr);
        // Barcode and manual input zone
        if (cfg == null || cfg.getProperty("ui.showbarcode").equals("1")) {
            JPanel barcodeZone = new JPanel();
            barcodeZone.setLayout(new GridBagLayout());
            btnKeypadCollapse.setFocusPainted(false);
            btnKeypadCollapse.setFocusable(false);
            btnKeypadCollapse.setRequestFocusEnabled(false);
            btnKeypadCollapse.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnKeypadCollapseActionPerformed(evt);
                }
            });
            cstr = new java.awt.GridBagConstraints();
            cstr.gridx = 0;
            cstr.gridy = 0;
            barcodeZone.add(btnKeypadCollapse, cstr);
            m_jPrice.setBackground(java.awt.Color.white);
            m_jPrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            m_jPrice.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
            m_jPrice.setOpaque(true);
            m_jPrice.setPreferredSize(new java.awt.Dimension(260, 22));
            m_jPrice.setRequestFocusEnabled(false);
            cstr = new java.awt.GridBagConstraints();
            cstr.gridx = 1;
            cstr.gridy = 0;
            cstr.insets = new Insets(btnspacing, btnspacing, btnspacing, btnspacing);
            cstr.fill = java.awt.GridBagConstraints.BOTH;
            cstr.weightx = 1.0;
            cstr.weighty = 1.0;
            barcodeZone.add(m_jPrice, cstr);
            m_jEnter.setFocusPainted(false);
            m_jEnter.setFocusable(false);
            m_jEnter.setRequestFocusEnabled(false);
            cstr = new java.awt.GridBagConstraints();
            cstr.gridx = 3;
            cstr.gridy = 0;
            cstr.insets = new java.awt.Insets(btnspacing, btnspacing,
                    btnspacing, btnspacing);
            barcodeZone.add(m_jEnter, cstr);
            cstr = new GridBagConstraints();
            cstr.gridx = 1;
            cstr.gridy = 1;
            cstr.fill = GridBagConstraints.HORIZONTAL;
            mainZone.add(barcodeZone, cstr);
        }
        // Numpad zone
        JPanel numpadZone = new JPanel();
        numpadZone.setLayout(new GridBagLayout());
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = 2;
        mainZone.add(m_jNumberKeys, cstr);
        // Add main zone container
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.weightx = 1.0;
        cstr.weighty = 1.0;
        cstr.fill = GridBagConstraints.BOTH;
        m_jPanContainer.add(mainZone, cstr);

        // Footer line
        //////////////

        m_jPanTotals.setLayout(new java.awt.GridBagLayout());

        m_jPanEntries.setLayout(new javax.swing.BoxLayout(m_jPanEntries, javax.swing.BoxLayout.Y_AXIS));

        m_jKeyFactory.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
        m_jKeyFactory.setForeground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
        m_jKeyFactory.setBorder(null);
        m_jKeyFactory.setCaretColor(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
        m_jKeyFactory.setPreferredSize(new java.awt.Dimension(1, 1));
        m_jKeyFactory.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                m_jKeyFactoryKeyTyped(evt);
            }
        });
        m_jPanEntries.add(m_jKeyFactory);






        cstr = new GridBagConstraints();
        cstr.gridx = 2;
        cstr.gridy = 0;
        cstr.gridheight = 2;
        m_jInputContainer.add(m_jPanEntries, cstr);


        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        cstr.weightx = 1.0;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        m_jPanContainer.add(m_jInputContainer, cstr);

        this.add(m_jPanContainer, "ticket");
    }

    private void m_jEditLineActionPerformed(java.awt.event.ActionEvent evt) {

        int i = m_ticketlines.getSelectedIndex();
        if (i < 0){
            Toolkit.getDefaultToolkit().beep(); // no line selected
        } else {
            try {
                TicketLineInfo newline = JProductLineEdit.showMessage(this, m_App, this.controller.getOrderManager().getOrderContent().getLine(i));
                if (newline != null) {
                    // line has been modified
                    this.controller.replaceLine(i, newline);
                    paintTicketLine(i);
                }
            } catch (BasicException e) {
                new MessageInf(e).show(this);
            }
        }
    }

    private void m_jbtnLineDiscountActionPerformed() {
        Double discountRate = null;
        try {
            discountRate = this.keypadManager.getInputNumber();
        } catch (NumberFormatException e) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                    AppLocal.getIntString("message.invaliddiscount"));
            msg.show(this);
            return;
        }
        if (discountRate == null) {
            // No rate
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                    AppLocal.getIntString("message.selectratefordiscount"));
            msg.show(this);
            return;
        }
        discountRate = Math.abs(discountRate / 100.0);
        if (discountRate > 1.0) {
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                    AppLocal.getIntString("message.invaliddiscount"));
            msg.show(this);
            java.awt.Toolkit.getDefaultToolkit().beep();
            this.keypadManager.reset();
            return;
        }
        int index = m_ticketlines.getSelectedIndex();
        if (index >= 0) {
            this.controller.applyLineDiscount(index, discountRate);
            this.keypadManager.reset();
            this.refreshTicket();
        } else {
            // No item or discount selected
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                    AppLocal.getIntString("message.selectlinefordiscount"));
            msg.show(this);
            java.awt.Toolkit.getDefaultToolkit().beep();
        }
    }

    private void btnTicketDiscountActionPerformed(ActionEvent evt) {
        DiscountProfile current = null;
        if (this.controller.getOrderManager().getOrderContent().getDiscountProfileId() != null) {
            try {
                current = dlCustomers.getDiscountProfile(this.controller.getOrderManager().getOrderContent().getDiscountProfileId());
            } catch (BasicException e) {}
        } else {
            current = new DiscountProfile(this.controller.getOrderManager().getOrderContent().getDiscountRate());
        }
        DiscountProfile profile = DiscountProfilePicker.show(this, current);
        this.controller.getOrderManager().assignDiscountProfile(profile);
        refreshTicket();
    }

    private void m_jTariffActionPerformed(java.awt.event.ActionEvent evt) {
        try{
            TariffInfo tariff = m_jTariff.getSelectedValue();
            if(tariff != null) {
                this.switchTariffArea(tariff);
            }
        } catch(java.lang.ClassCastException e){

        }
    }

    private void m_jKeyFactoryKeyTyped(java.awt.event.KeyEvent evt) {
        if(evt.isAltDown() || evt.isAltGraphDown() || evt.isControlDown() || evt.isMetaDown()){
            // ignore keypress if it is a shortcut key
            return;
        }
        m_jKeyFactory.setText(null);
        this.keypadManager.inputChar(evt.getKeyChar());
    }

    private void m_jPlusActionPerformed(java.awt.event.ActionEvent evt) {
        this.adjustSelectedLineQuantity(1.0);
    }
    private void m_jMinusActionPerformed(java.awt.event.ActionEvent evt) {
        this.adjustSelectedLineQuantity(-1.0);
    }

    private void m_jDeleteActionPerformed(java.awt.event.ActionEvent evt) {

        int i = m_ticketlines.getSelectedIndex();
        if (i < 0){
            Toolkit.getDefaultToolkit().beep(); // No hay ninguna seleccionada
        } else {
            removeTicketLine(i); // elimino la linea
        }

    }

    private void m_jUpActionPerformed(java.awt.event.ActionEvent evt) {

        m_ticketlines.selectionUp();

    }

    private void m_jDownActionPerformed(java.awt.event.ActionEvent evt) {

        m_ticketlines.selectionDown();

    }

    private void m_jListActionPerformed(java.awt.event.ActionEvent evt) {

        ProductInfoExt prod = JProductFinder.showMessage(JPanelTicket.this, dlSales);
        if (prod != null) {
            buttonTransition(prod);
        }
    }

    private void btnCustomerReloadActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            this.dlCustomers.preloadCustomers();
        } catch (BasicException e) {
            logger.log(Level.WARNING,
                    "Unable to reload customers.", e);
            MessageInf msg = new MessageInf(MessageInf.SGN_WARNING,
                    "Unable to reload customers" , e);
            msg.show(this);
            java.awt.Toolkit.getDefaultToolkit().beep();
        }
    }

    private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {
        CustomerInfoExt customer = JCustomerFinder.show(this,
                dlCustomers, this.controller.getOrderManager().getOrderContent().getCustomer());
        if (customer == this.controller.getOrderManager().getOrderContent().getCustomer()) {
            // No change/cancelled
            return;
        }
        this.assignCustomer(customer);
    }

    private void btnSplitActionPerformed(java.awt.event.ActionEvent evt) {

        if (this.controller.getOrderManager().getOrderContent().getLinesCount() > 0) {
            ReceiptSplit splitdialog = ReceiptSplit.getDialog(this, dlSystem.getResourceAsXML("Ticket.Line"), dlSales, dlCustomers, taxeslogic);

            TicketInfo ticket1 = this.controller.getOrderManager().getOrderContent().copyTicket();
            TicketInfo ticket2 = new TicketInfo();
            ticket2.setCustomer(this.controller.getOrderManager().getOrderContent().getCustomer());

            if (splitdialog.showDialog(ticket1, ticket2, this.controller.getOrderManager().getOrderExtra())) {
                if (closeTicket(ticket2, this.controller.getOrderManager().getOrderExtra())) { // already checked  that number of lines > 0
                    this.controller.getOrderManager().getOrder().updateContent(ticket1);
                    setActiveOrder(this.controller.getOrderManager().getOrder(), this.controller.getOrderManager().getOrderExtra());// set result ticket
                }
            }
        }
    }

    private void btnCustomProductActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            TicketLineInfo newline = JProductLineEdit.showMessage(this, m_App);
            if (newline != null) {
                // line has been modified
                this.addLine(newline);
            }
        } catch (BasicException e) {
            new MessageInf(e).show(this);
        }
    }

    private void btnRenameOrderActionPerformed(java.awt.event.ActionEvent evt) {
        SharedTicketInfo order = this.controller.getOrderManager().getOrder();
        String newName = JOrderRenameDialog.showMessage(this, order.getCustomLabel());
        order.setCustomLabel(newName);
        this.orderLabel.update(order, this.controller.getOrderManager().getOrderExtra());
        if (this.m_ticketsbag instanceof JTicketsBagShared) {
            // Update button to switch order
            ((JTicketsBagShared)this.m_ticketsbag).refreshBagButtons();
        }
    }

    private void btnKeypadCollapseActionPerformed(java.awt.event.ActionEvent evt) {
        this.collapseKeypad(!this.keypadCollapsed);
    }

    private javax.swing.JButton btnCustomer;
    private javax.swing.JButton btnCustomerReload;
    private javax.swing.JButton btnSplit;
    private javax.swing.JButton btnCustomProduct;
    private javax.swing.JPanel catcontainer;
    private javax.swing.JPanel lineBtnsContainer;
    private javax.swing.JPanel m_jButtonsExt;
    private javax.swing.JButton m_jDelete;
    private javax.swing.JButton m_jDown;
    private javax.swing.JButton m_jPlus;
    private javax.swing.JButton m_jMinus;
    private javax.swing.JButton m_jEditLine;
    private javax.swing.JButton m_jbtnLineDiscount;
    private javax.swing.JToggleButton m_jbtnStackToggle;

    private javax.swing.JButton m_jEnter;
    private javax.swing.JTextField m_jKeyFactory;
    private javax.swing.JButton m_jList;
    private JNumberKeys m_jNumberKeys;
    private javax.swing.JPanel m_jPanEntries;
    private javax.swing.JPanel m_jPanTotals;
    private javax.swing.JPanel m_jPanelBag;
    private javax.swing.JLabel m_jPrice;
    private fr.pasteque.pos.widgets.JOrderLabel orderLabel;
    private javax.swing.JLabel m_jTotalEuros;
    private javax.swing.JLabel discountLabel;
    private javax.swing.JButton m_jUp;
    private javax.swing.JButton btnTicketDiscount;
    private javax.swing.JPanel m_jInputContainer;
    private JComboBoxVal<TariffInfo> m_jTariff;
    private JLabelClock clock;
    private fr.pasteque.pos.widgets.JSalesInfoBox messageBox;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnKeypadCollapse;
}
