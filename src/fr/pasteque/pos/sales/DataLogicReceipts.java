//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import java.util.List;
import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.datasource.OrderDataSource;
import fr.pasteque.pos.caching.TicketsCache;

/**
 * Management of SharedTickets. They are all local (so not shared).
 * Because a shared ticket is a hell of concurrent access with disconnections.
 * And most users have a bad connection that lead to blocked tickets.
 * It is then just a proxy to TicketsCache until something more is inserted.
 */
public class DataLogicReceipts implements OrderDataSource
{
    /** Creates a new instance of DataLogicReceipts */
    public DataLogicReceipts() {
    }

    @Override /* from OrderDataSource */
    public final SharedTicketInfo getOrder(String id) throws BasicException {
        return TicketsCache.getTicket(id);
    }

    @Override /* from OrderDataSource */
    public final List<SharedTicketInfo> getOrderList() throws BasicException {
        return TicketsCache.getAllTickets();
    }

    @Override /* from OrderDataSource */
    public final void saveOrder(SharedTicketInfo order) throws BasicException {
        //SharedTicketInfo stkt = new SharedTicketInfo(id, ticket);
        TicketsCache.saveTicket(order);
    }

    @Override /* from OrderDataSource */
    public final void deleteOrder(final String id) throws BasicException {
        TicketsCache.deleteTicket(id);
    }
}
