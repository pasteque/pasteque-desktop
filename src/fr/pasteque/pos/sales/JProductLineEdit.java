//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JComboBoxVal;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.shortcuts.ShortcutConfig;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.forms.shortcuts.ShortcutSections;
import fr.pasteque.pos.forms.shortcuts.Shortcuts;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.widgets.JEditorCurrency;
import fr.pasteque.pos.widgets.JEditorDouble;
import fr.pasteque.pos.widgets.JEditorDoublePositive;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Properties;
import javax.swing.JFrame;

/**
 *
 * @author adrianromero
 */
public class JProductLineEdit extends javax.swing.JDialog
implements ActionListener, ShortcutListener
{
    private static final long serialVersionUID = -5573225942446058984L;

    private TicketLineInfo returnLine;
    private TicketLineInfo m_oLine;
    private boolean m_bnameok;
    private boolean m_bunitsok;
    private boolean m_bpriceok;
    private List<CurrencyInfo> currencies;
    private CurrencyInfo selectedCurrency;
    private List<TaxInfo> taxes;
    private TaxInfo selectedTax;

    /** Creates new form JProductLineEdit */
    private JProductLineEdit(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
    }
    /** Creates new form JProductLineEdit */
    private JProductLineEdit(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
    }

    /** Init to create a new line. */
    private TicketLineInfo init(AppView app) throws BasicException {
        DataLogicSales dl = new DataLogicSales();
        this.currencies = dl.getCurrenciesList();
        this.taxes = dl.getTaxList();

        // Inicializo los componentes
        initComponents();
        currencyCbx.addItems(this.currencies);
        currencyCbx.setSelectedIndex(0);
        this.selectedCurrency = this.currencies.get(0);
        currencyCbx.addActionListener(this);

        m_jTax.addItems(this.taxes);
        m_jTax.setSelectedIndex(0);
        this.selectedTax = this.taxes.get(0);
        m_jTax.addActionListener(this);
        m_jTax.setEnabled(true);

        m_oLine = new TicketLineInfo(null, 1.0, 0.0, this.selectedTax, true, new Properties());
        m_bnameok = false;
        m_bunitsok = true;
        m_bpriceok = true;

        m_jName.setEnabled(true);
        m_jPrice.setEnabled(false); // for B2C mode
        m_jPriceTax.setEnabled(true);

        m_jName.setText("");
        m_jUnits.setDoubleValue(m_oLine.getMultiply());
        m_jPrice.setDoubleValue(m_oLine.getPrice());
        m_jPriceTax.setDoubleValue(m_oLine.getPriceTax());
        m_jDiscount.setDoubleValue(m_oLine.getDiscountRate() * 100.0);

        m_jName.addPropertyChangeListener("Edition", new RecalculateName());
        m_jUnits.addPropertyChangeListener("Edition", new RecalculateUnits());
        m_jPrice.addPropertyChangeListener("Edition", new RecalculatePrice());
        m_jPriceTax.addPropertyChangeListener("Edition", new RecalculatePriceTax());
        m_jDiscount.addPropertyChangeListener("Edition", new RecalculateDiscount());

        m_jName.addEditorKeys(m_jKeys);
        m_jUnits.addEditorKeys(m_jKeys);
        m_jPrice.addEditorKeys(m_jKeys);
        m_jPriceTax.addEditorKeys(m_jKeys);
        m_jDiscount.addEditorKeys(m_jKeys);

        if (m_jName.isEnabled()) {
            m_jName.activate();
        } else {
            m_jUnits.activate();
        }

        printTotals();

        // Set shortcuts
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
        // Show
        returnLine = null;
        m_jButtonOK.setEnabled(this.canValidate());
        setVisible(true);
        // Post edit
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
        return returnLine;
    }

    /** Init to edit an existing line. */
    private TicketLineInfo init(AppView app, TicketLineInfo oLine) throws BasicException {
        DataLogicSales dl = new DataLogicSales();
        this.currencies = dl.getCurrenciesList();
        this.taxes = dl.getTaxList();

        // Inicializo los componentes
        initComponents();
        currencyCbx.addItems(this.currencies);
        currencyCbx.setSelectedIndex(0);
        this.selectedCurrency = this.currencies.get(0);
        currencyCbx.addActionListener(this);

        if (oLine.getTaxInfo() == null) {
            throw new BasicException(AppLocal.getIntString("message.cannotcalculatetaxes"));
        }
        m_jTax.addItems(this.taxes);
        m_jTax.setSelectedValue(oLine.getTaxInfo());
        this.selectedTax = oLine.getTaxInfo();
        m_jTax.setEnabled(app.getAppUserView().getUser().hasPermission("fr.pasteque.pos.sales.JPanelTicketEdits"));
        m_jTax.addActionListener(this);

        m_oLine = new TicketLineInfo(oLine);
        m_bnameok = true;
        m_bunitsok = true;
        m_bpriceok = true;

        m_jName.setEnabled(m_oLine.getProductID() == null && app.getAppUserView().getUser().hasPermission("fr.pasteque.pos.sales.JPanelTicketEdits"));
        m_jPrice.setEnabled(false); // for B2C mode
        m_jPriceTax.setEnabled(app.getAppUserView().getUser().hasPermission("fr.pasteque.pos.sales.JPanelTicketEdits"));

        m_jName.setText(m_oLine.getProperty("product.name"));
        m_jUnits.setDoubleValue(oLine.getMultiply());
        m_jPrice.setDoubleValue(oLine.getPrice());
        m_jPriceTax.setDoubleValue(oLine.getPriceTax());
        m_jDiscount.setDoubleValue(oLine.getDiscountRate() * 100.0);

        m_jName.addPropertyChangeListener("Edition", new RecalculateName());
        m_jUnits.addPropertyChangeListener("Edition", new RecalculateUnits());
        m_jPrice.addPropertyChangeListener("Edition", new RecalculatePrice());
        m_jPriceTax.addPropertyChangeListener("Edition", new RecalculatePriceTax());
        m_jDiscount.addPropertyChangeListener("Edition", new RecalculateDiscount());

        m_jName.addEditorKeys(m_jKeys);
        m_jUnits.addEditorKeys(m_jKeys);
        m_jPrice.addEditorKeys(m_jKeys);
        m_jPriceTax.addEditorKeys(m_jKeys);
        m_jDiscount.addEditorKeys(m_jKeys);

        if (m_jName.isEnabled()) {
            m_jName.activate();
        } else {
            m_jUnits.activate();
        }

        printTotals();

        // Set shortcuts
        ShortcutConfig config = ShortcutConfig.getInstance();
        config.addListener(this.rootPane, ShortcutSections.GENERAL, this);
        // Show
        returnLine = null;
        m_jButtonOK.setEnabled(this.canValidate());
        setVisible(true);
        // Post edit
        config.removeListener(this.rootPane, ShortcutSections.GENERAL, this);
        return returnLine;
    }

    @Override // from ShortcutListener
    public boolean shortcutPressed(Shortcuts s) {
        switch (s) {
            case GENERAL_VALIDATE:
                this.onOk();
                return true;
            case GENERAL_CANCEL:
                this.onCancel();
                return true;
            default: // Ignore
                return false;
        }
    }

    private void switchCurrency(CurrencyInfo currency) {
        this.selectedCurrency = currency;
        // Update display
        m_jPriceTax.setAltCurrency(this.selectedCurrency);
        m_jPrice.setAltCurrency(this.selectedCurrency);
        m_jPrice.setDoubleValue(this.selectedCurrency.convertFromMain(m_oLine.getPrice()));
        m_jPriceTax.setDoubleValue(this.selectedCurrency.convertFromMain(m_oLine.getPriceTax()));
        this.printTotals();
    }

    private void switchTax(TaxInfo t) {
        this.selectedTax = t;
        m_oLine.setTaxInfo(this.selectedTax);
        recalculatePriceTax();
    }

    private boolean canValidate() {
        return m_bunitsok && m_bpriceok && m_bnameok;
    }

    private void printTotals() {
        if (m_bunitsok && m_bpriceok) {
            m_jSubtotal.setDoubleValue(m_oLine.getSubValue());
            m_jTotal.setDoubleValue(m_oLine.getValue());
        } else {
            m_jSubtotal.setDoubleValue(null);
            m_jTotal.setDoubleValue(null);
        }
        m_jButtonOK.setEnabled(this.canValidate());
    }

    private class RecalculateUnits implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            Double value = m_jUnits.getDoubleValue();
            if (value == null || value == 0.0) {
                m_bunitsok = false;
            } else {
                m_oLine.setMultiply(value);
                m_bunitsok = true;
            }

            printTotals();
        }
    }

    private class RecalculatePrice implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            recalculatePrice();
        }
    }
    private void recalculatePrice() {
        Double value = m_jPrice.getDoubleValue();
        if (value == null) {
            m_bpriceok = false;
        } else {
            m_oLine.setPrice(selectedCurrency.convertToMain(value));
            m_jPriceTax.setDoubleValue(selectedCurrency.convertFromMain(m_oLine.getPriceTax()));
            m_bpriceok = true;
        }
        printTotals();
    }

    private class RecalculatePriceTax implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            recalculatePriceTax();
        }
    }
    private void recalculatePriceTax() {
        Double value = m_jPriceTax.getDoubleValue();
        if (value == null) {
            m_bpriceok = false;
        } else {
            m_oLine.setPriceTax(selectedCurrency.convertToMain(value));
            m_jPrice.setDoubleValue(selectedCurrency.convertFromMain(m_oLine.getPrice()));
            m_bpriceok = true;
        }
        printTotals();
    }

    private class RecalculateDiscount implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            Double value = m_jDiscount.getDoubleValue();
            double rate = 0.0;
            if (value != null) {
                rate = value / 100.0;
            }
            m_oLine.setDiscountRate(rate);
            printTotals();
        }
    }

    private class RecalculateName implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            String name = m_jName.getText();
            m_oLine.setProperty("product.name", name);
            m_bnameok = !"".equals(name);
            m_jButtonOK.setEnabled(canValidate());
        }
    }

    private static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window)parent;
        } else {
            return getWindow(parent.getParent());
        }
    }

    /** Create and show a line edit dialog for an existing ticket line. */
    public static TicketLineInfo showMessage(Component parent, AppView app, TicketLineInfo oLine) throws BasicException {
        Window window = getWindow(parent);
        JProductLineEdit myMsg;
        if (window instanceof Frame) {
            myMsg = new JProductLineEdit((Frame) window, true);
        } else {
            myMsg = new JProductLineEdit((Dialog) window, true);
        }
        return myMsg.init(app, oLine);
    }

    /** Create an show a line edit dialog to create a custom product. */
    public static TicketLineInfo showMessage(Component parent, AppView app) throws BasicException {
        Window window = getWindow(parent);
        JProductLineEdit myMsg;
        if (window instanceof Frame) {
            myMsg = new JProductLineEdit((Frame) window, true);
        } else {
            myMsg = new JProductLineEdit((Dialog) window, true);
        }
        return myMsg.init(app);
    }

    public void actionPerformed(java.awt.event.ActionEvent evt) {
        CurrencyInfo c = this.currencies.get(currencyCbx.getSelectedIndex());
        if (!c.equals(this.selectedCurrency)) {
            this.switchCurrency(c);
        }
        TaxInfo t = this.taxes.get(m_jTax.getSelectedIndex());
        if (!t.equals(this.selectedTax)) {
            this.switchTax(t);
        }
    }

    protected void onOk() {
        if (!this.canValidate()) {
            return;
        }
        returnLine = m_oLine;
        dispose();
    }

    protected void onCancel() {
        dispose();
    }

    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnspacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;

        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        currencyLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Currency"));
        priceLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.price"));
        unitsLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.units"));
        discountLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.discountRate"));
        pricetaxLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.pricetax"));
        itemLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.item"));
        currencyCbx = WidgetsBuilder.createComboBoxVal();
        m_jName = new JEditorString();
        m_jUnits = new JEditorDouble();
        m_jPrice = new JEditorCurrency();
        m_jPriceTax = new JEditorCurrency();
        m_jDiscount = new JEditorDoublePositive();
        m_jDiscount.setBounds(0.0, 100.00);
        m_jTax = WidgetsBuilder.createComboBoxVal();
        taxLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.tax"));
        totalLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.totalcash"));
        m_jTotal = new JEditorCurrency();
        m_jTotal.setEnabled(false);
        subtotalLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.subtotalcash"));
        m_jSubtotal = new JEditorCurrency();
        m_jSubtotal.setEnabled(false);
        jPanel1 = new javax.swing.JPanel();
        m_jButtonOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                                                  AppLocal.getIntString("Button.OK"),
                                                  WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                                                      AppLocal.getIntString("Button.Cancel"),
                                                      WidgetsBuilder.SIZE_MEDIUM);
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        m_jKeys = new JEditorKeys();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(AppLocal.getIntString("label.editline")); // NOI18N

        jPanel5.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new GridBagLayout());

        GridBagConstraints cstr = null;
        int y = 0;
        // Currency
        if (this.currencies.size() > 1) {
            cstr = new GridBagConstraints();
            cstr.gridx = 0;
            cstr.gridy = y;
            cstr.anchor = GridBagConstraints.LINE_START;
            cstr.insets = new Insets(marginInset, marginInset, 0, marginInset);
            jPanel2.add(currencyLbl, cstr);
            cstr = new GridBagConstraints();
            cstr.gridx = 1;
            cstr.gridy = y;
            cstr.insets = new Insets(marginInset, 0, btnspacing, marginInset);
            cstr.fill = GridBagConstraints.HORIZONTAL;
            cstr.weightx = 0.6;
            jPanel2.add(currencyCbx, cstr);
            y++;
        }
        // Product name
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.anchor = GridBagConstraints.LINE_START;
        cstr.insets = new Insets(btnspacing, marginInset, btnspacing, marginInset);
        jPanel2.add(itemLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.insets = new Insets(marginInset, 0, btnspacing, marginInset);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        jPanel2.add(m_jName, cstr);
        y++;
        // Quantity
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(unitsLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        jPanel2.add(m_jUnits, cstr);
        y++;
        // Price
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(priceLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        jPanel2.add(m_jPrice, cstr);
        y++;
        // Taxed price
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(pricetaxLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        jPanel2.add(m_jPriceTax, cstr);
        y++;
        // Discount
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(discountLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        jPanel2.add(m_jDiscount, cstr);
        y++;
        // Tax
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(taxLbl, cstr);
        m_jTax.setOpaque(true);
        m_jTax.setPreferredSize(new java.awt.Dimension(150, 25));
        m_jTax.setRequestFocusEnabled(false);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        jPanel2.add(m_jTax, cstr);
        y++;
        // Subtotal
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(subtotalLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        cstr.insets = new Insets(0, 0, btnspacing, marginInset);
        jPanel2.add(m_jSubtotal, cstr);
        y++;
        // Total
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = y;
        cstr.insets = new Insets(0, marginInset, marginInset, marginInset);
        cstr.anchor = GridBagConstraints.LINE_START;
        jPanel2.add(totalLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 1;
        cstr.gridy = y;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.weightx = 0.6;
        cstr.insets = new Insets(0, 0, marginInset, marginInset);
        jPanel2.add(m_jTotal, cstr);
        y++;

        jPanel5.add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        m_jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onOk();
            }
        });
        jPanel1.add(m_jButtonOK);

        m_jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCancel();
            }
        });
        jPanel1.add(m_jButtonCancel);

        jPanel5.add(jPanel1, java.awt.BorderLayout.SOUTH);

        getContentPane().add(jPanel5, java.awt.BorderLayout.CENTER);

        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.Y_AXIS));
        jPanel4.add(m_jKeys);

        jPanel3.add(jPanel4, java.awt.BorderLayout.NORTH);

        getContentPane().add(jPanel3, java.awt.BorderLayout.EAST);

        pack();
        setLocationRelativeTo(getOwner());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel currencyLbl;
    private javax.swing.JLabel priceLbl;
    private javax.swing.JLabel unitsLbl;
    private javax.swing.JLabel pricetaxLbl;
    private javax.swing.JLabel itemLbl;
    private javax.swing.JLabel taxLbl;
    private javax.swing.JLabel discountLbl;
    private javax.swing.JLabel totalLbl;
    private javax.swing.JLabel subtotalLbl;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JButton m_jButtonCancel;
    private javax.swing.JButton m_jButtonOK;
    private JComboBoxVal<CurrencyInfo> currencyCbx;
    private JEditorKeys m_jKeys;
    private JEditorString m_jName;
    private JEditorCurrency m_jPrice;
    private JEditorCurrency m_jPriceTax;
    private JEditorDoublePositive m_jDiscount;
    private JEditorCurrency m_jSubtotal;
    private JComboBoxVal<TaxInfo> m_jTax;
    private JEditorCurrency m_jTotal;
    private JEditorDouble m_jUnits;
    // End of variables declaration//GEN-END:variables
}
