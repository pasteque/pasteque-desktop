//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Manager for composition state and selection.
 * The manager is ready when {@link start()} is called and until
 * it is canceled with {@link cancel()}.
 */
public class CompositionManager
{
    /** The main product. */
    private ProductInfoExt composition;
    /** The list of subgroups from the composition. */
    private List<SubgroupInfo> subgroups;
    /**
     * The list of selected subproducts (components), in the same order
     * as subgroups. It is initialized with nulls.
     */
    private List<ProductInfoExt> components;
    /**
     * The manager that will update the current order.
     * Note: this should be removed once the composition is not added
     * in real time and only returned to the sales controller when finished.
     */
    private OrderManager orderManager;
    /** Index of the currently selected subgroup. */
    private int currentSubgroup;
    /** The quantity to apply to each. */
    private double quantity;
    /** The index of the line of the order holding the composition. */
    private int mainLineIndex;
    /** The product was added to the order and components can be picked */
    private boolean started;
    /** Previous state of the OrderManager. */
    private boolean wasStacking;

    /**
     * Create a manager to update the current order. Use {@link start()}
     * to effectively add the product to the OrderManager.
     * @param composition The composition that was selected.
     * @param subgroups The subgroups of the composition.
     * @param manager The manager that will update the selected order.
     * @param quantity The quantity of composition to add.
     */
    public CompositionManager(ProductInfoExt composition,
            List<SubgroupInfo> subgroups, OrderManager manager,
            double quantity) {
        this.composition = composition;
        this.orderManager = manager;
        this.quantity = quantity;
        this.subgroups = subgroups;
        this.components = new ArrayList<ProductInfoExt>(this.subgroups.size());
        for (int i = 0; i < this.subgroups.size(); i++) {
            this.components.add(null);
        }
        this.started = false;
    }

    /**
     * Change the currently selected subgroup for {@link addSubproduct(component)}.
     * @param index The index of the subgroup. Does nothing if out of bounds.
     */
    public void selectSubgroup(int index) {
        if (index > 0 && index < this.subgroups.size()) {
            this.currentSubgroup = index;
        }
    }

    /**
     * Add the main product to the current order.
     * @return The result of adding the main product to the current order.
     */
    public OrderManager.Ret start() {
        this.wasStacking = this.orderManager.doesStackArticles();
        this.orderManager.setArticleStacking(false);
        OrderManager.Ret r = this.orderManager.addProduct(this.composition, this.quantity);
        this.mainLineIndex = r.i();
        this.currentSubgroup = 0;
        this.started = true;
        return r;
    }

    /**
     * Interal call to reset the OrderManager to its previous state
     * and update the own state of itself.
     * It is not automatic to be able to confirm when all components
     * are selected.
     */
    /* package */ void endComposition() {
        /* package scope to allow SalesController to call it.
         * This should be handled privately either by finishing or cancelling
         * when the code is refactored enough. */
        this.orderManager.setArticleStacking(this.wasStacking);
        this.started = false;
    }

    /**
     * Assign a product to a subgroup.
     * @param subgroupIndex The index of the subgroup from the composition.
     * @param component The selected component.
     * @return The result of adding the component to the current order.
     * Returns an error if the composition manager is not ready.
     */
    public OrderManager.Ret pickProduct(int subgroupIndex, ProductInfoExt component) {
        if (!this.started) {
            return OrderManager.Ret.Err();
        }
        this.components.set(subgroupIndex, component);
        return this.orderManager.addSubproduct(component, this.mainLineIndex);
    }

    /**
     * Assign a product to the first non-selected subgroup.
     * @param component The selected component.
     * @return The result of adding the component to the current order.
     * Returns an error if the composition manager is not ready.
     */
    public OrderManager.Ret addSubproduct(ProductInfoExt component) {
        if (!this.started) {
            return OrderManager.Ret.Err();
        }
        this.components.set(this.currentSubgroup, component);
        this.currentSubgroup++;
        return this.orderManager.addSubproduct(component, this.mainLineIndex);
    }

    /**
     * Check if each components were picked.
     * @return True when a component was picked for every subgroup. False otherwise.
     */
    public boolean compositionCompleted() {
        if (!this.started) {
            return false;
        }
        for (ProductInfoExt component : this.components) {
            if (component == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Remove the uncomplete composition from the current order and reset.
     * @return The result of removing the composition to from current order.
     * Returns an error if the composition manager is not ready.
     */
    public OrderManager.Ret cancel() {
        if (!this.started) {
            return OrderManager.Ret.Err();
        }
        OrderManager.Ret ret = orderManager.removeLine(this.mainLineIndex);
        this.endComposition();
        return ret;
    }

    /**
     * Get the main product.
     */
    public ProductInfoExt getCompositionProduct() {
        return this.composition;
    }

    /**
     * Get the selected components, in the same order as the subgroups order.
     * When a product was not picked for a subgroup, it is present in the list
     * as null.
     * When {@link compositionCompleted()} is true, all products are set.
     */
    public List<ProductInfoExt> getComponents() {
        return this.components;
    }

    /**
     * Check if the manager is useable.
     * @return True when the manager was started and not canceled.
     */
    public boolean isCompositing() {
        return this.started;
    }
}
