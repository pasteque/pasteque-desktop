//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.datasource.CustomerDataSource;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manage and edit orders. This class holds the workflow of updating an order
 * from high-level events. Like adding or removing a product.
 * Other classes should uses a manager to edit an order instead of applying
 * changes directly to the order.
 */
public class OrderManager
{
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.sales.OrderManager");

    /** The operation could not succeed, nothing was done. */
    public static final int OP_ERR = -1;
    /** One or more new lines were added. */
    public static final int OP_ADD = 0;
    /** One or more lines were edited. */
    public static final int OP_UPD = 1;
    /** One or more lines were removed. */
    public static final int OP_RM = 2;

    /** A hacky shortcut to get the current application user. */
    protected AppView app;
    /** Where to find alternative prices from. */
    protected CatalogDataSource catDs;
    /** Get discount profile info from the data source. */
    protected CustomerDataSource custDs;
    /** Get alternative tax rates from the data source. */
    protected TaxesLogic taxesLogic;
    /** The managed order */
    protected SharedTicketInfo order;
    /** Extra information about the order, like the name of the place. */
    protected Object orderExtra;
    /** Cache of the selected tariff area, because order holds only the id. */
    protected TariffInfo orderTariffArea;
    /**
     * When true, same articles are stacked in a single line.
     * Otherwise they are put in distinct lines.
     */
    private boolean stackArticles;

    /**
     * Create a manager by expliciting sources.
     * @param catDs The source of catalog data.
     * @param custDs The source of customers’ data.
     */
    public OrderManager(CatalogDataSource catDs, CustomerDataSource custDs) throws BasicException {
        this.catDs = catDs;
        this.custDs = custDs;
        this.taxesLogic = new TaxesLogic(catDs.getTaxList());
    }

    /**
     * Creates a manager without managing an order with the default sources.
     * @param app The application context.
     * @throws BasicException When the local cache cannot be accessed.
     */
    public OrderManager(AppView app) throws BasicException {
        this.init(app);
    }

    /**
     * Create a manager and link it to an existing order with
     * the default sources.
     * @param app The application context.
     * @param o The order to manage.
     * @throws BasicException When the local cache cannot be accessed.
     */
    public OrderManager(AppView app, SharedTicketInfo o) throws BasicException {
        this.init(app);
        this.setOrder(o);
    }

    protected void init(AppView app) throws BasicException {
        this.app = app;
        DataLogicSales dlSales = new DataLogicSales();
        this.catDs = dlSales;
        this.custDs = new DataLogicCustomers();
        this.taxesLogic = new TaxesLogic(dlSales.getTaxList());
    }

    /** Change the currently managed order without extra information. */
    public void setOrder(SharedTicketInfo o) {
        this.setOrder(o, null);
    }

    /**
     * Change the currently managed order with extra information.
     * @param o The order to switch to, null to manage no order.
     * @param extra Extra order information, like the name of the place.
     */
    public void setOrder(SharedTicketInfo o, Object extra) {
        this.order = o;
        this.orderExtra = extra;
        if (this.order == null) {
            this.orderTariffArea = null;
            return;
        }
        // Asign preliminary properties to the order
        if (this.order.getTicket().getUser() == null && this.app != null) {
            this.order.getTicket().setUser(this.app.getAppUserView().getUser().getUserInfo());
        }
        this.orderTariffArea = null;
        if (this.order.getTicket().getTariffArea() != null) {
            try {
                for (TariffInfo ta : this.catDs.getTariffAreaList()) {
                    if (ta.getID() == this.order.getTicket().getTariffArea()) {
                        this.orderTariffArea = ta;
                        break;
                    }
                }
            } catch (BasicException e) {
                logger.log(Level.SEVERE, "Read cache error for tariff area list", e);
                e.printStackTrace();
            }
        }
    }

    /** Get the currently managed order. */
    public SharedTicketInfo getOrder() {
        return this.order;
    }

    /** 
     * Get the currently managed TicketInfo.
     * Shortcut to getOrder().getTicket() with null check.
     */
    public fr.pasteque.pos.ticket.TicketInfo getOrderContent() {
        if (this.order == null) {
            return null;
        }
        return this.order.getTicket();
    }

    public Object getOrderExtra() {
        return this.orderExtra;
    }

    /**
     * Activate or deactivate same article stacking.
     * @param stack When true, adding identical articles will stack them on
     * existing lines when they exist. Otherwise they are always put in on
     * distinct new line.
     */
    public void setArticleStacking(boolean stack) {
        this.stackArticles = stack;
    }

    /** Get article stacking state. */
    public boolean doesStackArticles() {
        return this.stackArticles;
    }

    /**
     * Set the customer count for the order.
     * @param count The new count. Null to remove it.
     * @return True when the count is changed, false if it was already
     * set to the same value or not currently managing an order.
     */
    public boolean setCustomersCount(Integer count) {
        if (this.order == null) {
            return false;
        }
        if (this.order.getTicket().getCustomersCount() != count) {
            this.order.getTicket().setCustomersCount(count);
            return true;
        }
        return false;
    }

    /**
     * Assign or remove a customer to the current order. Also set or reset
     * discount.
     * @param customer The customer to assign, null to unassign.
     */
    public void assignCustomer(CustomerInfoExt customer) {
        if (this.order == null) {
            return;
        }
        this.order.getTicket().setCustomer(customer);
        if (customer != null && customer.getDiscountProfileId() != null) {
            // Set discount profile and rate to ticket
            try {
                DiscountProfile profile = this.custDs.getDiscountProfile(customer.getDiscountProfileId());
                this.order.getTicket().setDiscountRate(profile.getRate());
                this.order.getTicket().setDiscountProfileId(profile.getId());
            } catch (BasicException e) {
                e.printStackTrace();
            }
        } else {
            // Reset discount profile and rate
            this.order.getTicket().setDiscountProfileId(null);
            this.order.getTicket().setDiscountRate(0.0);
        }
        if (customer != null && customer.getTariffAreaId() != null) {
            // Set tariff area
            try {
                TariffInfo area = this.catDs.getTariffArea(customer.getTariffAreaId());
                this.setTariffArea(area);
            } catch (BasicException e) {
                e.printStackTrace();
                logger.log(Level.WARNING, "Could not load TariffArea", e);
            }
        } else {
            // Reset tarif area
            try {
                this.setTariffArea(null);
            } catch (BasicException e) {
                logger.log(Level.WARNING, "Could not unset TariffArea", e);
                e.printStackTrace();
            }
        }
    }

    /**
     * Assign a discount profile to the current order.
     * @param profile The new discount profile to assign,
     * or null to remove the current one.
     */
    public void assignDiscountProfile(DiscountProfile profile) {
        if (profile != null) {
            // Set discount profile and rate to ticket
            this.order.getTicket().setDiscountRate(profile.getRate());
            this.order.getTicket().setDiscountProfileId(profile.getId());
        } else {
            // Reset discount profile and rate
            this.order.getTicket().setDiscountProfileId(null);
            this.order.getTicket().setDiscountRate(0.0);
        }
    }

    /**
     * Assign the current order to a place. This methods only update the
     * current order without managing changes from or to a pool.
     * @param p Assign the order to this place.
     */
    public void assignToPlace(fr.pasteque.pos.sales.restaurant.Place p) {
        this.order.setId(p.getId());
    }

    /**
     * Change the current tariff area for the managed order, and update
     * prices.
     * @param ta The new tariff area, use null to set the default one.
     * @throws BasicException When the area cannot be found in local cache.
     */
    public void setTariffArea(TariffInfo ta) throws BasicException {
        if (this.order == null) {
            return;
        }
        this.order.getTicket().setTariffArea(ta, this.catDs);
        this.orderTariffArea = ta;
    }

    public TariffInfo getTariffArea() {
        return this.orderTariffArea;
    }

    private TicketLineInfo prepareLine(ProductInfoExt product, double quantity) {
        TaxInfo tax = this.taxesLogic.getTaxInfo(product.getTaxId(),
                this.order.getTicket().getCustomer());
        if (tax == null) {
            String strPrdId = (product.getID() == null) ?
                    "null" : product.getID();
            String strCustId = (order.getTicket().getCustomer().getId() == null) ?
                     "null" : order.getTicket().getCustomer().getId();
            logger.log(Level.SEVERE, "No tax found for product "
                    + strPrdId + " customer " + strCustId);
            return null;
        }
        TicketLineInfo line = new TicketLineInfo(product, quantity,
                product.getPriceSellTax(), tax, true,
                (java.util.Properties) (product.getProperties().clone()));
        // Apply the automatic discount if there is any
        if (product.isDiscountEnabled()) {
            double rate = product.getDiscountRate();
            if (rate > 0.005) {
                line.setDiscountRate(rate);
            }
        }
        return line;
    }

    /**
     * Add some quantity of a product to the managed order.
     * @param product The product to add.
     * @param quantity The quantity to add.
     * @return Same as {@link #addLine}
     */
    public Ret addProduct(ProductInfoExt product, double quantity) {
        if (this.order == null) {
            return Ret.Err();
        }
        TicketLineInfo line = this.prepareLine(product, quantity);
        if (line == null) {
            return Ret.Err();
        }
        line.setSubproduct(false);
        return this.addLine(line);
    }

    /**
     * Add some quantity of a product to the managed order as a subproduct.
     * @param product The product to add. Its price is set to 0.
     * @param parentLineIndex The index of the line containing the main product
     * (the line to which this product is attached to).
     * @return Same as {@link #addSubline}.
     */
    public Ret addSubproduct(ProductInfoExt product, int parentLineIndex) {
        if (this.order == null) {
            return Ret.Err();
        }
        // Set component product price to 0
        product.setPriceSell(0.0);
        product.setPriceSellTax(0.0);
        TicketLineInfo line = this.prepareLine(product, 1.0);
        if (line == null) {
            return Ret.Err();
        }
        line.setDiscountRate(0.0);
        line.setSubproduct(true);
        return this.addSubline(line, parentLineIndex);
    }

    /**
     * Add a new line to the order.
     * @param line The line to add. The price is updated from current tariff
     * area.
     * @return An Ret with either OP_ADD, OP_UPD when
     * stacking, OP_RM when stacking to quantity 0.0 or
     * OP_ERR when there is no order or when the line is a subproduct
     * one.
     */
    public Ret addLine(TicketLineInfo line) {
        if (this.order == null || line.isSubproduct()) {
            return Ret.Err();
        }
        // Update price from tariff area if needed
        if (this.orderTariffArea != null && !line.isSubproduct()
                && line.getProductID() != null) {
            try {
                line.applyTariffArea(this.orderTariffArea, this.catDs);
            } catch (BasicException e) {
                logger.log(Level.SEVERE, "Read cache error for tariff area " + this.orderTariffArea, e);
                e.printStackTrace();
            }
        }
        // Regular product, try to stack
        if (this.stackArticles) {
            for (int i = 0; i < this.order.getTicket().getLines().size(); i++) {
                TicketLineInfo l = this.order.getTicket().getLine(i);
                if (l.canMerge(line)) {
                    l.merge(line);
                    if (l.getMultiply() == 0.0) {
                       return this.removeLine(i);
                    }
                    return new Ret(OP_UPD, i, 1);
                }
            }
        }
        this.order.getTicket().addLine(line);
        return new Ret(OP_ADD,
                this.order.getTicket().getLinesCount() - 1, 1);
    }

    /**
     * Add a line to the order as a subline of an other, those never stack.
     * @param newLine The line to add. It's quantity is set to the parent one.
     * @param parentLineIndex The index of the line containing the main product
     * (the line to which this line is attached to).
     * @return An Ret with either OP_ADD, or OP_ERR
     * when there is no order, when the line is a not a subproduct one or when
     * parent line is a subproduct line.
     */
    public Ret addSubline(TicketLineInfo newLine, int parentLineIndex) {
        if (this.order == null || !newLine.isSubproduct()) {
            return Ret.Err();
        }
        /* This should be completely refactored. Auxiliary lines are too losely
         * bound to their parent. */
        TicketLineInfo parentLine = this.order.getTicket().getLine(parentLineIndex);
        if (parentLine.isSubproduct()) {
            return Ret.Err();
        }
        // Get down until a non aux line is found to insert there
        int i = parentLineIndex + 1;
        while (i < this.order.getTicket().getLines().size()) {
            if (!this.order.getTicket().getLine(i).isSubproduct()) {
                break;
            }
            i++;
        }
        newLine.setMultiply(parentLine.getMultiply());
        this.order.getTicket().insertLine(i, newLine);
        return new Ret(OP_ADD, i, 1);
    }

    /** Get the main index of a subline. */
    private int getMainIndex(int i) {
        if (i == 0) {
            return 0;
        }
        TicketLineInfo l = this.order.getTicket().getLine(--i);
        while (i > 0 && l.isSubproduct()) {
            l = this.order.getTicket().getLine(--i);
        }
        return i;
    }

    /**
     * Remove an existing line by it's index. When removing a subline or a
     * composition, the main line and all associated sublines are removed.
     * @param i The index of the line to remove.
     * @return Ret with OP_RM when the line could be removed,
     * OP_ERR when there is no order or index is out of bounds.
     */
    public Ret removeLine(int i) {
        if (this.order == null || i < 0 || i >= this.order.getTicket().getLinesCount()) {
            return Ret.Err();
        }
        boolean isSubline = this.order.getTicket().getLine(i).isSubproduct();
        boolean isCompo = !isSubline
                && this.order.getTicket().getLinesCount() > (i + 1)
                && this.order.getTicket().getLine(i + 1).isSubproduct();
        if (!isCompo && !isSubline) {
            this.order.getTicket().removeLine(i);
            return new Ret(OP_RM, i, 1);
        }
        // Remove the main line and all auxillary following lines
        // mainLine points to the next one when one is removed
        int mainLine = (isCompo) ? i : this.getMainIndex(i);
        this.order.getTicket().removeLine(mainLine);
        int count = 1;
        while(mainLine < this.order.getTicket().getLinesCount()
                && this.order.getTicket().getLine(mainLine).isSubproduct()) {
            this.order.getTicket().removeLine(mainLine);
            count++;
        }
        return new Ret(OP_RM, mainLine, count);
    }

    /**
     * Replace the content of a line by an other.
     * @param index The number of the line to replace.
     * @param line The new line that replaces the current one.
     */
    public void replaceLine(int index, TicketLineInfo line) {
        if (this.order == null) {
            return;
        }
        this.order.getTicket().setLine(index, line);
    }

    /**
     * Adjust the quantity of the desired line, remove it when reaching 0.
     * This removes the line when 0 is reached instead of going from positive
     * to negative or vice-versa.
     * When adjusting a subline, it adjust the main line and all its sublines.
     * @return The number of removed lines when negative, the number of updated
     * lines when positive.
     */
    public Ret adjustLineQuantity(int i, double quantity) {
        if (this.order == null) {
            return Ret.Err();
        }
        TicketLineInfo l = this.order.getTicket().getLine(i);
        if (l.isSubproduct()) {
            i = this.getMainIndex(i);
        }
        TicketLineInfo newLine = new TicketLineInfo(this.order.getTicket().getLine(i));
        double oldQuantity = newLine.getMultiply();
        double newQuantity = oldQuantity + quantity;
        if ((oldQuantity < 0.0 && newQuantity >= 0.0)
                || (oldQuantity > 0.0 && newQuantity <= 0.0)) {
            // Remove the line
            return this.removeLine(i);
        } else {
            // Adjust quantity
            newLine.setMultiply(newQuantity);
            this.replaceLine(i, newLine);
            int updated = 1;
            // Update following aux lines
            while (i + updated < this.order.getTicket().getLines().size()) {
                if (this.order.getTicket().getLine(i + updated).isSubproduct()) {
                    this.order.getTicket().getLine(i + updated).setMultiply(newQuantity);
                    updated++;
                } else {
                    break;
                }
            }
            return new Ret(OP_UPD, i, updated);
        }
    }

    public boolean applyLineDiscount(int line, double rate) {
        if (this.order == null) {
            return false;
        }
        TicketLineInfo l = this.order.getTicket().getLine(line);
        if (l.isSubproduct()) {
            l = this.order.getTicket().getLine(this.getMainIndex(line));
        }
        l.setDiscountRate(rate);
        return true;
    }

    /**
     * Return object for order manipulation with the kind of operation, the
     * index of the first edited line and the number of successive lines that
     * were edited.
     */
    public static class Ret
    {
        private int operation;
        private int startingLine;
        private int lineCount;

        /** Short for new Ret(OP_ERR, -1, -1). */
        public static Ret Err() {
            return new Ret(OP_ERR, -1, -1);
        }

        public Ret(int op, int start, int count) {
            this.operation = op;
            this.startingLine = start;
            this.lineCount = count;
        }

        /** Short for {@link #getOperation}. */
        public int op() {
            return this.operation;
        }

        /** What the operation was. See {@link OrderManager} constants. */
        public int getOperation() {
            return this.operation;
        }

        /** Short for {@link getStartIndex}. -1 in case of error. */
        public int i() {
            return this.startingLine;
        }

        /**
         * Get the first line index this operation modified. With
         * OP_RM this is the index of where the removed line was. -1 in case
         * of error.
         */
        public int getStartIndex() {
            return this.startingLine;
        }

        /** Short for {@link #getCount}. */
        public int n() {
            return this.lineCount;
        }

        /** Get the number of successive lines the operation modified. */
        public int getCount() {
            return this.lineCount;
        }

        @Override
        public int hashCode() {
            return this.operation + this.startingLine * 10
                    + this.lineCount * 1000;
        }

        @Override
        /** Check content equality, mostly for testing. */
        public boolean equals(Object o) {
            return (o instanceof Ret) && ((Ret)o).operation == this.operation
                    && ((Ret)o).startingLine == this.startingLine
                    && ((Ret)o).lineCount == this.lineCount;
        }

        @Override
        public String toString() {
            String op = "";
            switch (this.operation) {
            case OP_ERR: op = "ERR"; break;
            case OP_ADD: op = "ADD"; break;
            case OP_UPD: op = "UPD"; break;
            case OP_RM:  op = "RM"; break;
            default:     op = "???"; break;
            }
            return op + ":" + this.startingLine + "," + this.lineCount;
        }
    }
}
