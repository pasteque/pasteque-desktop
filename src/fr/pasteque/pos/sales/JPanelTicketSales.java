//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;
import java.awt.Component;
import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.catalog.JCatalog;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppView;
import java.awt.Dimension;

public class JPanelTicketSales extends JPanelTicket
{
    private static final long serialVersionUID = 1691953239005025734L;

    /** Creates a new instance of JPanelTicketSales */
    public JPanelTicketSales() {
    }

    @Override
    public void init(AppView app) {
        super.init(app);
        m_ticketlines.addListSelectionListener(new CatalogSelectionListener());
    }

    public String getTitle() {
        return null;
    }

    public boolean requiresOpenedCash() {
        return true;
    }

    protected Component getSouthComponent() {
        // Get the flags to display the price along the products
        AppConfig cfg = AppConfig.loadedInstance;
        String priceVisible = cfg.getProperty("ui.pricevisible");
        boolean displayPrice = !"none".equals(priceVisible);
        boolean includeTaxes = "taxed".equals(priceVisible);
        m_cat = new JCatalog(dlSales, displayPrice, includeTaxes,
                Integer.parseInt(cfg.getProperty("ui.catalog.catlistwidth")));
        m_cat.addActionListener(new CatalogListener());
        m_cat.getComponent().setPreferredSize(new Dimension(
                0,
                Integer.parseInt(cfg.getProperty("ui.buttons.catheight"))));
        return m_cat.getComponent();
    }

    protected void resetSouthComponent() {
        m_cat.showCatalogPanel(null);
    }

    protected JTicketsBag getJTicketsBag() {
        return JTicketsBag.createTicketsBag(m_App.getProperties().getProperty("machine.ticketsbag"), m_App, this);
    }

    @Override
    public void activate() throws BasicException {
        super.activate();
        m_cat.loadCatalog();
    }
}
