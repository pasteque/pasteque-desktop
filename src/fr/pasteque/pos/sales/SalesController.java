//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.datasource.CatalogDataSource;
import fr.pasteque.pos.datasource.OrderDataSource;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import java.util.List;

/**
 * Manages creating, persisting and switching orders and converting
 * them to tickets.
 */
public class SalesController
{
    /** Picked regular products. */
    public static final int PRODUCT_SINGLE = 0;
    /** Picked a composition product. */
    public static final int PRODUCT_COMPOSITION = 1;
    /** Picked a composition component. */
    public static final int PRODUCT_SUBGROUP = 2;


    protected OrderManager orderManager;
    protected OrderDataSource orderDs;
    protected CatalogDataSource catalogDs;
    /**
     * Internal manager to handle composition
     */
    protected CompositionManager compoManager;
    private boolean skipPersistence;

    /**
     * Create a controller to manage orders and persistence.
     * @param manager The manager that will handle editing an order.
     * @param source The data source that will handle persistence.
     * @param catalog The data source to handle compositions.
     */
    public SalesController(OrderManager manager, OrderDataSource source,
            CatalogDataSource catalog) {
        this.orderManager = manager;
        this.catalogDs = catalog;
        this.orderDs = source;
        this.skipPersistence = false;
    }

    /**
     * Get the underlying order manager. This should be used
     * to ease transition to SalesController but it shouldn’t be used
     * when the code is clean.
     */
    public OrderManager getOrderManager() {
        return this.orderManager;
    }
    /**
     * Get the underlying order data source. This should be used
     * to ease transition to SalesController but it shouldn’t be used
     * when the code is clean.
     */
    public OrderDataSource getOrderDataSource() {
        return this.orderDs;
    }

    /**
     * Check from the order data source if there are unselected orders.
     * It doesn’t checks if the orders are empty or not.
     * @return True there is more orders from the source than selected.
     * @throws BasicException If an error occurs while reading the source.
     */
    public boolean hasPendingOrders() throws BasicException {
        java.util.List<SharedTicketInfo> l = this.orderDs.getOrderList();
        if (this.orderManager.getOrder() == null) {
            return l.size() > 0;
        } else {
            // Consider the manager uses orders from the source
            // TODO add more checks about the currently selected order
            return l.size() > 1;
        }
    }

    /**
     * Switch to an other order. If the current order is empty,
     * it is discarded, otherwise it is persisted. Unless it was already
     * deleted with {@link #deleteCurrentOrder()}.
     * @param o The order to select.
     * @param extra Extra information (mostly place name).
     * @throws BasicException When an error occurs while persisting
     * the current order. It is not switched then.
     * Use {@link #forceSkipPersistence} to bypass the error.
     */
    public void setOrder(SharedTicketInfo o, Object extra) throws BasicException {
        // Save or delete the previous order
        SharedTicketInfo currentOrder = this.getCurrentOrder();
        if (currentOrder != null && !this.skipPersistence) {
            if (currentOrder.isEmpty()) {
                this.deleteCurrentOrder();
            } else {
                this.saveCurrentOrder();
            }
        }
        // Switch
        this.skipPersistence = false;
        this.orderManager.setOrder(o, extra);
    }

    /**
     * Merge the current order in the target and select it.
     * The new state is saved into the data source.
     * If both orders already have a customer, the one from the target is kept.
     * @param target The new order to select.
     * @param extraTarget The target extra data. If null the current extra
     * is kept.
     * @throws BasicException When the data source cannot be updated.
     */
    public void mergeOrderInto(SharedTicketInfo target, Object extraTarget)
            throws BasicException{
        Object newExtra = (extraTarget == null) ?
                this.orderManager.getOrderExtra() : extraTarget;
        if (this.orderManager.getOrder() == null) {
            this.setOrder(target, newExtra);
            this.saveCurrentOrder();
            return;
        }
        if (target == null) {
            return;
        }
        SharedTicketInfo current = this.orderManager.getOrder();
        fr.pasteque.pos.ticket.TicketInfo targetTicket = target.getTicket();
        if (targetTicket.getCustomer() == null) {
            targetTicket.setCustomer(current.getTicket().getCustomer());
        }
        targetTicket.addCustomersCount(current.getTicket().getCustomersCount());
        for(TicketLineInfo line : current.getTicket().getLines()) {
            targetTicket.addLine(line);
        }
        // Save and select target
        this.deleteCurrentOrder();
        this.setOrder(target, newExtra);
        this.saveCurrentOrder();
    }

    public SharedTicketInfo getCurrentOrder() {
        return this.orderManager.getOrder();
    }

    public Object getCurrentOrderExtra() {
        return this.orderManager.getOrderExtra();
    }

    public void assignCustomer(CustomerInfoExt customer) {
        this.orderManager.assignCustomer(customer);
    }

    public boolean refreshCustomer(CustomerInfoExt customer) {
        if (this.orderManager.getOrder() == null || customer == null) {
            return false;
        }
        CustomerInfoExt currentCustomer = this.orderManager.getOrderContent().getCustomer();
        if (currentCustomer == null
                || !currentCustomer.getId().equals(customer.getId())) {
            return false;
        }
        this.orderManager.assignCustomer(customer);
        return true;
    }

    public void setCustomersCount(int count) {
        this.orderManager.setCustomersCount(count);    
    }

    public TariffInfo getCurrentTariffArea() {
        SharedTicketInfo order = this.orderManager.getOrder();
        if (order == null) {
            return null;
        }
        return this.orderManager.getTariffArea();
    }

    /**
     * Set a tariff area to the current order.
     * @param area The new area, can be null to unset.
     * An area which id is -1 will also deselect it.
     * @throws BasicException When updating the prices fails.
     */
    public void switchTariffArea(TariffInfo area) throws BasicException {
        if (area == null || area.getID() == -1) {
            this.orderManager.setTariffArea(null);
        } else {
            this.orderManager.setTariffArea(area);
        }
    }

    /**
     * Check if the product selection is about adding to the order
     * or adding components for a composition.
     * @return True when a composition is being edited.
     */
    public boolean isCompositing() {
        return this.compoManager != null && this.compoManager.isCompositing();
    }

    /**
     * Add a product to the current order. It will handle the composition
     * status.
     * @param quantity The quantity to add.
     * @param product The product to add.
     * @return The result of adding the product to the current order.
     * @throws BasicException When initalizing a composition fails.
     */
    public OrderManager.Ret pickProduct(double quantity,
            ProductInfoExt product) throws BasicException {
        if (this.isCompositing()) {
            // Currently picking composition components
            OrderManager.Ret ret = this.compoManager.addSubproduct(product);
            // Automatically end the composition once all components
            // are selected without confirmation.
            if (this.compoManager.compositionCompleted()) {
                this.endComposition();
            }
            return ret;
        }
        // Picking a composition
        if (product.isCompo()) {
            return this.startComposition(quantity, product);
        }
        // Regular pick
        return this.orderManager.addProduct(product, quantity);
    }

    /**
     * Add a composition product and set the compositing state.
     * @param quantity The quantity of the composition.
     * @param product The composition (unchecked).
     * @return The result of adding the composition to the current order.
     * @throws BasicException When the CompositionManager cannot be initialized.
     */
    private OrderManager.Ret startComposition(double quantity,
            ProductInfoExt product) throws BasicException {
        List<SubgroupInfo> subgroups = this.catalogDs.getSubgroups(product.getID());
        this.compoManager = new CompositionManager(product, subgroups,
                this.orderManager, quantity);
        return this.compoManager.start();
    }

    /**
     * Indicate that composing is finished. This is there to ease transition
     * to SalesController and should be private once the code is clean.
     */
    public void endComposition() {
        if (this.compoManager != null && this.compoManager.isCompositing()) {
            this.compoManager.endComposition();
            this.compoManager = null;
        }
    }

    /**
     * Cancel the current composition, remove the composition and all the
     * subproducts.
     * @return The result of removing the composition from the order.
     * Return an error when not currently compositing.
     */
    public OrderManager.Ret cancelComposition() {
        if (this.compoManager == null) {
            return OrderManager.Ret.Err();
        }
        return this.compoManager.cancel();
    }

    public OrderManager.Ret removeLine(int i) {
        return this.orderManager.removeLine(i);
    }

    public void replaceLine(int index, TicketLineInfo newLine) {
        this.orderManager.replaceLine(index, newLine);
    }

    /**
     * Adjust the quantity of the given line.
     */
    public OrderManager.Ret adjustLineQuantity(int line, double quantity) {
        return this.orderManager.adjustLineQuantity(line, quantity);
    }

    public void applyLineDiscount(int line, double rate) {
        this.orderManager.applyLineDiscount(line, rate);
    }

    public OrderManager.Ret addProduct(ProductInfoExt product, double quantity) {
        return this.orderManager.addProduct(product, quantity);
    }

    /**
     * Persist the current order to the data source.
     * If the order was deleted with {@link #deleteCurrentOrder},
     * it will be persisted again with {@link #setOrder(SharedTicketInfo, Object)}.
     * @throws BasicException When an error occurs while persisting.
     */
    public void saveCurrentOrder() throws BasicException {
        if (this.orderManager.getOrder() == null) {
            return;
        }
        this.skipPersistence = false;
        this.orderDs.saveOrder(this.orderManager.getOrder());
    }

    /**
     * Remove the current order from the data source.
     * The order is left untouched but will not be persisted anymore
     * unless explicitely requested with {@link #saveCurrentOrder()}.
     * @throws BasicException When an error occurs while persisting.
     */
    public void deleteCurrentOrder() throws BasicException {
        if (this.orderManager.getOrder() == null) {
            return;
        }
        this.skipPersistence = true;
        this.orderDs.deleteOrder(this.orderManager.getOrder().getId());
    }

    /**
     * Skip persistence when switching to an other order.
     * Use it when the persistence fails but you need to continue
     * anyway.
     * Persistence will be automatically reenabled after switching to an other
     * order successfully.
     */
    public void forceSkipPersistence() {
        this.skipPersistence = true;
    }
}
