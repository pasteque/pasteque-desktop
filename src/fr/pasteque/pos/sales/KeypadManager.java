//    Pastèque
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                  2012-2015 Scil (http://scil.coop)
//              Cédric Houbart, Pierre Ducroquet, Philippe Pary
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.pos.util.LimitChecker;
import fr.pasteque.pos.widgets.JNumberEvent;
import fr.pasteque.pos.widgets.JNumberEventListener;
import fr.pasteque.pos.widgets.JNumberKeys;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * Controller for the virtual keypad. See {@link KeypadManagerListener}
 * to receive hi-level events from the manager when confirming input.
 * '*' in input is read as quantity delimiter, quantity first, then barcode.
 * Barcode starting with 'c' are considered customer cards.
 * Barcode of 13 characters starting with '210' are considered scaled barcodes,
 * with quantity read from the last 6 characters divided by 100.
 */
public class KeypadManager
{
    /** Character code for backspace/remove last character. */
    public static final char KEY_BACK = '\u0008';
    /** Character code for delete/reset. */
    public static final char KEY_DEL = '\u007f';
    /** Character code for enter/process input. */
    public static final char KEY_ENTER = '\n';
    /**
     * The checkout button. It has nothing to do with the keypad state but
     * is required to pick the keypad input.
     */
    public static final char KEY_CHECKOUT = '=';

    private JLabel inputLabel;
    private JButton enterButton;
    private JNumberKeys keypad;
    private KeypadManagerListener listener;

    /**
     * Create a manager that will read and set input to the given label.
     * @param keypad The virtual keypad widget that can trigger input.
     * @param inputLabel The label that displays the currrent input.
     * @param enterButton The button that acts as enter.
     * @param listener The listener that will receive the events.
     */
    public KeypadManager(JNumberKeys keypad, JLabel inputLabel,
            JButton enterButton, KeypadManagerListener listener) {
        this.keypad = keypad;
        this.inputLabel = inputLabel;
        this.enterButton = enterButton;
        this.listener = listener;
        if (this.keypad != null) {
            this.bindKeys();
        }
    }

    private void bindKeys() {
        if (this.keypad != null) {
            this.keypad.addJNumberEventListener(new JNumberEventListener() {
                public void keyPerformed(JNumberEvent evt) {
                    KeypadManager.this.inputChar(evt.getKey());
            }
            });
        }
        if (this.enterButton != null) {
            this.enterButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    KeypadManager.this.inputChar(KEY_ENTER);
                }
            });
        }
    }

    /**
     * Get the raw input string.
     */
    public String getInput() {
        return this.inputLabel.getText();
    }

    /**
     * Get the numpad input as a number.
     * @return Null when no input is provided.
     * The quantity from a "qty*code" input or the input when no code is
     * provided ("quantity" without '*').
     * @throws NumberFormatException When the input is non-numeric. The message
     * holds the unprocessed input.
     */
    public Double getInputNumber() throws NumberFormatException {
        if ("".equals(this.inputLabel.getText().trim())) {
            return null;
        }
        CompositeInput cmpInput = new CompositeInput(this.inputLabel.getText(), true);
        return cmpInput.getQuantity();
    }

    /** Clear input and reset the internal state. */
    public void reset() {
        this.inputLabel.setText("");
    }

    /**
     * Chooses what to do when a specific key is typed
     * @param c contains the key typed
     */
    public void inputChar(char c) {
        if (c == KEY_CHECKOUT) {
            if (this.listener != null) {
                this.listener.onCheckout(this);
            }
            return;
        }
        // barcode when enter is typed
        if (c == KEY_ENTER) {
            this.processInput();
        }
        // Other character
        else {
            // goes to the fonction processPrice if the key typed matches the key condition and
            // if the input is made in quantity's label
            if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4'
                    || c == '5' || c == '6' || c == '7' || c == '8'
                    || c == '9' || c == '.' || c == KEY_BACK || c == KEY_DEL
                    || c == '/') {
                processKeypadInput(c);
            } else if (c == '+') {
                // + cancel a -
                String label = this.inputLabel.getText();
                if (label.startsWith("-")) {
                    this.inputLabel.setText(label.substring(1));
                }
            } else if (c == '-') {
                // - set negative quantity
                String label = this.inputLabel.getText();
                if (!label.startsWith("-")) {
                    this.inputLabel.setText("-" + label);
                }
            } else if (c == '*') {
                String label = this.inputLabel.getText();
                if (label.indexOf('*') == -1) {
                    this.inputLabel.setText(label + "*");
                }
            } else {
                // Any other character
                String label = this.inputLabel.getText();
                this.inputLabel.setText(label + c);
            }
        }
    }

    /**
     * Parse input and trigger event according to its content.
     * @param reset When true, the input is reset after processing.
     */
    public void processInput(boolean reset) {
        CompositeInput input = null;
        try {
            input = new CompositeInput(this.inputLabel.getText(), false);
        } catch (NumberFormatException e) {
            if (this.listener != null) {
                this.listener.onIncorrectQuantity(this);
            }
            if (reset) {
                this.reset();
            }
            return;
        }
        String sCode = input.getBarcode();
        double quantity = input.getQuantity();
        if (sCode.length() > 0) {
            if (sCode.startsWith("c")) {
                // Barcode of a customers card
                if (this.listener != null) {
                    this.listener.onCustomerInput(this, sCode);
                }
            } else if (sCode.length() == 13 && sCode.startsWith("210")) {
                // Weighted barcode input
                if (this.listener != null) {
                    this.listener.onScaledBarcodeInput(this,
                            Double.parseDouble(sCode.substring(7, 13)) / 100,
                            sCode.substring(0, 7));
                }
            } else {
                // Regular input
                if (this.listener != null) {
                    if (LimitChecker.getInstance().checkLimits(quantity)) {
                        this.listener.onOtherInput(this, quantity, sCode);
                    } else {
                        this.listener.onIncorrectQuantity(this);
                    }
                }
            }
        } else {
            // Empty input
            if (this.listener != null) {
                this.listener.onEmptyInput(this);
            }
        }
        if (reset) {
            this.reset();
        }
    }

    /** Parse input and trigger event according to its content and reset it. */
    public void processInput() {
        this.processInput(true);
    }

    /**
     * Update the input from a typed character.
     * @param c contains the key typed
     */
    protected void processKeypadInput(char c){
        String label = this.inputLabel.getText();
        switch (c) {
        case '0': case '1': case '2': case '3': case'4': case '5': case '6':
        case '7': case '8': case '9':
            // Number entered
            this.inputLabel.setText(this.inputLabel.getText() + c);
            break;
        case '.':
            if ("".equals(label) || "0".equals(label)) {
                // . over empty, replace by "0."
                this.inputLabel.setText("0.");
            } else if (label.indexOf('.') == -1
                    || label.indexOf('.') < label.indexOf('*')) {
                // No . in input or after *, add it
                this.inputLabel.setText(this.inputLabel.getText() + c);
            }
            break;
        case KEY_DEL: case '/':
            // Reset
            this.reset();
            break;
        case KEY_BACK:
            // Erase numbers one by one
            if (label.length() > 0) {
                this.inputLabel.setText(label.substring(0, label.length() - 1));
            }
            break;
        }
    }

    private class CompositeInput
    {
        double quantity;
        /** Input barcode, use "" instead of null. */
        String barcode;

        /**
         * Read a composite input from a String.
         * @param input The input string to parse.
         * @param asNumber If the input should be parsed as a number when no
         * quantity is explicitely set, instead of reading a barcode only.
         * @throws NumberFormatException When the quantity is not a numeric
         * value. The message holds the quantity from the input.
         */
        public CompositeInput(String input, boolean asNumber) throws NumberFormatException {
            if (input.indexOf('*') == -1) {
                if (asNumber) {
                    if ("-".equals(input)) {
                        this.quantity = -1.0;
                    } else {
                        this.quantity = Double.parseDouble(input);
                    }
                    this.barcode = "";
                } else {
                    this.quantity = 1.0;
                    this.barcode = input;
                }
                return;
            } else {
                String[] split = input.split("\\*");
                if (!"".equals(split[0])) {
                    if ("-".equals(split[0])) {
                        this.quantity = -1.0;
                        this.barcode = split[1];
                        return;
                    }
                    try {
                        this.quantity = Double.parseDouble(split[0]);
                        this.barcode = split[1];
                        return;
                    } catch (NumberFormatException e) {
                        throw new NumberFormatException(split[0]);
                    }
                } else {
                    this.quantity = 1.0;
                    this.barcode = split[1];
                    return;
                }
            }
        }

        public double getQuantity() {
            return this.quantity;
        }

        /** Get the barcode read from input. It is never null. */
        public String getBarcode() {
            return this.barcode;
        }
    }
}
