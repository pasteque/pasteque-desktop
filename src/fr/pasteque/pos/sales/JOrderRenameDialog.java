//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.data.gui.JAbstractChoiceDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.shortcuts.ShortcutListener;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JPanel;

/**
 * Popup to rename an order.
 */
public class JOrderRenameDialog extends JAbstractChoiceDialog<String>
implements ShortcutListener
{
    private static final long serialVersionUID = 7341969040054196719L;

    private JOrderRenameDialog(java.awt.Component parent,
            String currentCustom) {
        super(parent, null);
        this.originalChoice = currentCustom;
        if (currentCustom != null) {
            this.labelInput.setText(currentCustom);
        }
        getRootPane().setDefaultButton(okBtn);
    }
    /**
     * All-in-one function to create the dialog and return the selected value.
     * @param parent The parent component to attach the dialog to.
     * @param currentCustom The predefined value.
     */
    public static String showMessage(Component parent, String currentCustom) {
        JOrderRenameDialog dialog = new JOrderRenameDialog(parent, currentCustom);
        return dialog.renameOrder();
    }

    /**
     * Open the dialog and return the selected value.
     * @return The new value, null or a non-empty string.
     */
    public String renameOrder() {
        this.labelInput.activate();
        this.open();
        return this.getChoice();
    }

    /**
     * Close the dialog and set the selected value.
     */
    protected void confirm() {
        String choice = this.labelInput.getText();
        if (choice.isEmpty()) {
            choice = null;
        }
        this.setChoice(choice);
        super.confirm();
    }

    protected void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnspacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
        int marginInset = 10;
        this.keyPad = new JEditorKeys();
        this.labelInput = new JEditorString();
        this.labelInput.addEditorKeys(this.keyPad);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle(AppLocal.tr("Form.RenameOrder.Title"));
        this.setLayout(new GridBagLayout());

        okBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);
        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirm();
            }
        });
        cancelBtn = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close();
            }
        });

        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(marginInset, marginInset, btnspacing, marginInset);
        this.add(this.labelInput, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.insets = new Insets(0, marginInset, btnspacing, marginInset);
        this.add(this.keyPad, cstr);
        // Bottom part: buttons
        javax.swing.JPanel buttonLine = new JPanel();
        buttonLine.setLayout(new FlowLayout(FlowLayout.RIGHT, marginInset, btnspacing));
        buttonLine.add(okBtn);
        buttonLine.add(cancelBtn);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        this.add(buttonLine, cstr);
    }

    private JEditorString labelInput;
    private JEditorKeys keyPad;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton okBtn;
}
