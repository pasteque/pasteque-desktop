//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.customers.JCustomerFinder;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author  adrian
 */
public class SimpleReceipt extends javax.swing.JPanel
{
    private static final long serialVersionUID = 7325686896418984340L;

    protected DataLogicCustomers dlCustomers;
    protected DataLogicSales dlSales;
    protected TaxesLogic taxeslogic;

    private JTicketLines ticketlines;
    private TicketInfo ticket;
    private Object ticketext;

    /** Creates new form SimpleReceipt */
    public SimpleReceipt(String ticketline, DataLogicSales dlSales, DataLogicCustomers dlCustomers, TaxesLogic taxeslogic) {

        initComponents();

        // dlSystem.getResourceAsXML("Ticket.Line")
        ticketlines = new JTicketLines();
        ticketlines.init(ticketline);
        this.dlCustomers = dlCustomers;
        this.dlSales = dlSales;
        this.taxeslogic = taxeslogic;

        jPanel2.add(ticketlines, BorderLayout.CENTER);
    }

    public void setCustomerEnabled(boolean value) {
        btnCustomer.setEnabled(value);
    }

    public void setTicket(TicketInfo ticket, Object ticketext) {

        this.ticket = ticket;
        this.ticketext = ticketext;

        // The ticket name
        m_jTicketId.setText(ticket.getName(ticketext));

        ticketlines.clearTicketLines();
        for (int i = 0; i < ticket.getLinesCount(); i++) {
            ticketlines.addTicketLine(ticket.getLine(i));
        }

        if (ticket.getLinesCount() > 0) {
            ticketlines.setSelectedIndex(0);
        }

        printTotals();

    }

    private void refreshTicketTaxes() {

        for (TicketLineInfo line : ticket.getLines()) {
            line.setTaxInfo(taxeslogic.getTaxInfo(line.getProductTaxCategoryID(), ticket.getCustomer()));
        }
    }

    private void printTotals() {

        if (ticket.getLinesCount() == 0) {
            m_jSubtotalEuros.setText(null);
            m_jTaxesEuros.setText(null);
            m_jTotalEuros.setText(null);
        } else {
            m_jSubtotalEuros.setText(ticket.printSubTotal());
            m_jTaxesEuros.setText(ticket.printTax());
            m_jTotalEuros.setText(ticket.printTotal());
        }
    }

    public TicketInfo getTicket()  {
        return ticket;
    }

    /** Find the index of the mother line of selected line.
     * If the selected line is a subproduct, it will return the index of the
     * composition. Otherwise it returns the selected line index. */
    private int findFirstNonAuxiliarLine() {
        int i = ticketlines.getSelectedIndex();
        while (i >= 0 && ticket.getLine(i).isProductCom()) {
            i--;
        }
        return i;
    }

    /** Extract the selected line, or all the lines of a composition.
     * They are removed from the ticket before returning them. */
    public TicketLineInfo[] getSelectedLines() {
        int i = findFirstNonAuxiliarLine();
        if (i >= 0) {
            // Pick the line
            List<TicketLineInfo> l = new ArrayList<TicketLineInfo>();
            TicketLineInfo line = this.ticket.getLine(i);
            l.add(line);
            // Remove them
            ticket.removeLine(i);
            ticketlines.removeTicketLine(i);
            // add also auxiliars
            while (i < ticket.getLinesCount() && ticket.getLine(i).isProductCom()) {
                l.add(ticket.getLine(i));
                ticket.removeLine(i);
                ticketlines.removeTicketLine(i);
            }
            // Update display and return the lines
            printTotals();
            return l.toArray(new TicketLineInfo[l.size()]);
        } else {
            return null;
        }
    }

    /** Extract one unit from the selected line, or all the lines of a composition.
     * They are removed from the ticket before returning them.
     * It does nothing if there are less that 1.0 unit before extraction. */
    public TicketLineInfo[] getSelectedLinesUnit() {
        int i = findFirstNonAuxiliarLine();
        if (i >= 0) {
            TicketLineInfo line = ticket.getLine(i);
            if (line.getMultiply() >= 1.0) {
                // Extract 1.0 to quantity and update the lines
                List<TicketLineInfo> l = new ArrayList<TicketLineInfo>();
                if (line.getMultiply() > 1.0) {
                    line.setMultiply(line.getMultiply() -1.0);
                    ticketlines.setTicketLine(i, line);
                    line = line.copyTicketLine();
                    line.setMultiply(1.0);
                    l.add(line);
                    i++;
                } else { // == 1.0
                    l.add(line);
                    ticket.removeLine(i);
                    ticketlines.removeTicketLine(i);
                }
                // add also auxiliars
                while (i < ticket.getLinesCount() && ticket.getLine(i).isProductCom()) {
                    l.add(ticket.getLine(i));
                    ticket.removeLine(i);
                    ticketlines.removeTicketLine(i);
                }
                printTotals();
                return l.toArray(new TicketLineInfo[l.size()]);
            } else { // < 1.0
                return null;
            }
        } else {
            return null;
        }
    }

    /** Inject the lines into the ticket. */
    public void addSelectedLines(TicketLineInfo[] lines) {
        if (lines.length == 1) {
            // Single line means a regular product. Try to merge with
            // an existing line.
            TicketLineInfo line = lines[0];
            boolean merged = false;
            for (int i = 0; i < this.ticket.getLines().size(); i++) {
                if (this.ticket.getLines().get(i).canMerge(line)) {
                    this.ticket.getLines().get(i).merge(line);
                    this.ticketlines.setTicketLine(i, this.ticket.getLines().get(i));
                    this.ticketlines.setSelectedIndex(i);
                    merged = true;
                    break;
                }
            }
            if (!merged) {
                // Add it to the end
                int lastIndex = this.ticket.getLinesCount();
                this.ticket.insertLine(lastIndex, line);
                this.ticketlines.insertTicketLine(lastIndex, line);
                this.ticketlines.setSelectedIndex(lastIndex);
            }
        } else {
            // Composition, add all to the end
            for (TicketLineInfo line : lines) {
                int lastIndex = this.ticket.getLinesCount();
                this.ticket.insertLine(lastIndex, line);
                this.ticketlines.insertTicketLine(lastIndex, line);
                this.ticketlines.setSelectedIndex(lastIndex);
            }
        }
        printTotals();
    }

    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        JPanel jPanel1 = new javax.swing.JPanel();
        JPanel m_jPanTotals = new javax.swing.JPanel();
        m_jTotalEuros = WidgetsBuilder.createImportantLabel();
        JLabel m_jLblTotalEuros1 = WidgetsBuilder.createImportantLabel();
        m_jSubtotalEuros = WidgetsBuilder.createLabel();
        m_jTaxesEuros = WidgetsBuilder.createLabel();
        JLabel m_jLblTotalEuros2 = WidgetsBuilder.createLabel();
        JLabel m_jLblTotalEuros3 = WidgetsBuilder.createLabel();
        JPanel m_jButtons = new javax.swing.JPanel();
        m_jTicketId = WidgetsBuilder.createLabel();
        btnCustomer = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_assign_customer.png"));
        jPanel2 = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel1.setLayout(new java.awt.BorderLayout());

        m_jPanTotals.setLayout(new java.awt.GridBagLayout());

        m_jTotalEuros.setBackground(java.awt.Color.white);
        m_jTotalEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jTotalEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jTotalEuros.setOpaque(true);
        m_jTotalEuros.setPreferredSize(new java.awt.Dimension(150, 25));
        m_jTotalEuros.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        m_jPanTotals.add(m_jTotalEuros, gridBagConstraints);

        m_jLblTotalEuros1.setText(AppLocal.getIntString("label.totalcash")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        m_jPanTotals.add(m_jLblTotalEuros1, gridBagConstraints);

        m_jSubtotalEuros.setBackground(java.awt.Color.white);
        m_jSubtotalEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jSubtotalEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jSubtotalEuros.setOpaque(true);
        m_jSubtotalEuros.setPreferredSize(new java.awt.Dimension(150, 25));
        m_jSubtotalEuros.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        m_jPanTotals.add(m_jSubtotalEuros, gridBagConstraints);

        m_jTaxesEuros.setBackground(java.awt.Color.white);
        m_jTaxesEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jTaxesEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jTaxesEuros.setOpaque(true);
        m_jTaxesEuros.setPreferredSize(new java.awt.Dimension(150, 25));
        m_jTaxesEuros.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        m_jPanTotals.add(m_jTaxesEuros, gridBagConstraints);

        m_jLblTotalEuros2.setText(AppLocal.getIntString("label.taxcash")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        m_jPanTotals.add(m_jLblTotalEuros2, gridBagConstraints);

        m_jLblTotalEuros3.setText(AppLocal.getIntString("label.subtotalcash")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        m_jPanTotals.add(m_jLblTotalEuros3, gridBagConstraints);

        jPanel1.add(m_jPanTotals, java.awt.BorderLayout.EAST);

        add(jPanel1, java.awt.BorderLayout.SOUTH);

        m_jButtons.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        m_jTicketId.setBackground(java.awt.Color.white);
        m_jTicketId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        m_jTicketId.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jTicketId.setOpaque(true);
        m_jTicketId.setPreferredSize(new java.awt.Dimension(160, 25));
        m_jTicketId.setRequestFocusEnabled(false);
        m_jButtons.add(m_jTicketId);

        btnCustomer.setFocusPainted(false);
        btnCustomer.setFocusable(false);
        btnCustomer.setRequestFocusEnabled(false);
        btnCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerActionPerformed(evt);
            }
        });
        m_jButtons.add(btnCustomer);

        add(m_jButtons, java.awt.BorderLayout.NORTH);

        jPanel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel2.setLayout(new java.awt.BorderLayout());
        add(jPanel2, java.awt.BorderLayout.CENTER);
    }

    private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {
        ticket.setCustomer(JCustomerFinder.show(this, dlCustomers,
                ticket.getCustomer()));
        // The ticket name
        m_jTicketId.setText(ticket.getName(ticketext));
        refreshTicketTaxes();
        // refresh the receipt....
        setTicket(ticket, ticketext);
    }

    private javax.swing.JButton btnCustomer;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel m_jSubtotalEuros;
    private javax.swing.JLabel m_jTaxesEuros;
    private javax.swing.JLabel m_jTicketId;
    private javax.swing.JLabel m_jTotalEuros;
}
