//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales.restaurant;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.widgets.JNumberDialog;
import fr.pasteque.pos.widgets.WidgetsBuilder;

/** The order selector for restaurant mode.
 * It is coupled to a JTicketsBagRestaurantMap to display the map and listen
 * to selection events.
 * The bag itselft holds only the move and return to map buttons.
 */
public class JTicketsBagRestaurant extends javax.swing.JPanel
{
    private static final long serialVersionUID = -794030245890015572L;

    private JTicketsBagRestaurantMap m_restaurant;

    /** Creates new form JTicketsBagRestaurantMap */
    public JTicketsBagRestaurant(AppView app, JTicketsBagRestaurantMap restaurant) {

        m_restaurant = restaurant;

        initComponents();
    }

    public void activate() {
    }

    private void initComponents() {

        moveTicketBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_move.png"),
                AppLocal.getIntString("Button.MoveTable.Tooltip"));
        backBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_room.png"),
                AppLocal.getIntString("Button.RestaurantMap.Tooltip"));
        javax.swing.JButton custCountBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_places.png"),
                AppLocal.getIntString("Button.CustCount.Tooltip"));

        setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        moveTicketBtn.setFocusPainted(false);
        moveTicketBtn.setFocusable(false);
        moveTicketBtn.setRequestFocusEnabled(false);
        moveTicketBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveTicketBtnActionPerformed(evt);
            }
        });
        add(moveTicketBtn);

        backBtn.setFocusPainted(false);
        backBtn.setFocusable(false);
        backBtn.setRequestFocusEnabled(false);
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        add(backBtn);

        custCountBtn.setFocusPainted(false);
        custCountBtn.setFocusable(false);
        custCountBtn.setRequestFocusEnabled(false);
        custCountBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                custCountBtnActionPerformed(evt);
            }
        });
        add(custCountBtn);

    }

    private void moveTicketBtnActionPerformed(java.awt.event.ActionEvent evt) {
        m_restaurant.moveTicket();
        m_restaurant.loadTickets();
    }

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {
        m_restaurant.deactivateCurrentPlace();
        m_restaurant.loadTickets();
    }

    // TODO: use of public UI event is an awfully dirty way to show popup
    public void custCountBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Double dblcount = JNumberDialog.showEditNumber(this, JNumberDialog.INT_POSITIVE, AppLocal.getIntString("Label.CustCount"), AppLocal.getIntString("Label.CustCountInput"), ImageLoader.readImageIcon("tkt_places.png"));
        if (dblcount != null) {
            int count = (int) dblcount.doubleValue();
            m_restaurant.setCustomersCount(count);
        }
    }

    private javax.swing.JButton backBtn;
    private javax.swing.JButton moveTicketBtn;
}

