//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales.restaurant;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JConfirmDialog;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.caching.FloorsCache;
import fr.pasteque.pos.customers.CustomerInfo;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.sales.JTicketsBag;
import fr.pasteque.pos.sales.SharedTicketInfo;
import fr.pasteque.pos.sales.TicketsEditor;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.widgets.JFloorPanel;
import fr.pasteque.pos.widgets.JPlaceButton;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;

/**
 * Restaurant map and UI and logic.
 * Manages places and click on them as well as saving pending orders.
 */
public class JTicketsBagRestaurantMap extends JTicketsBag
{
    private static final long serialVersionUID = 4243747553366407025L;
    private static Logger logger = Logger.getLogger("fr.pasteque.pos.sales.restaurant.JTicketsBagRestaurantMap");

    /** Map of places indexed by floor id */
    private Map<String, List<Place>> places;
    /** Map of place buttons indexed by place id. */
    private Map<String, JPlaceButton> placeBtns;
    private List<Floor> floors;

    private JTicketsBagRestaurant m_restaurantmap;
    private JTicketsBagRestaurantRes m_jreservations;

    private Place m_PlaceCurrent;

    /**
     * The clipboard to hold the reference of the original place
     * when moving it.
     */
    private Place m_PlaceClipboard;
    private CustomerInfo customer;

    /** Creates new form JTicketsBagRestaurant */
    public JTicketsBagRestaurantMap(AppView app, TicketsEditor panelticket) {
        super(app, panelticket);
        m_restaurantmap = new JTicketsBagRestaurant(app, this);
        m_PlaceCurrent = null;
        m_PlaceClipboard = null;
        customer = null;
        this.floors = new ArrayList<Floor>();
        this.places = new HashMap<String, List<Place>>();
        this.placeBtns = new HashMap<String, JPlaceButton>();

        AppConfig cfg = AppConfig.loadedInstance;
        int widthCfg = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchhudgebtnminwidth")));
        int heightCfg = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchhudgebtnminheight")));
        ThumbNailBuilder tnbPlace = new ThumbNailBuilder(widthCfg, heightCfg);
        try {
            this.floors = FloorsCache.getFloors();
            for (Floor f : this.floors) {
                List<Place> places = FloorsCache.getPlaces(f.getID());
                this.places.put(f.getID(), places);
                for (Place p : places) {
                    this.placeBtns.put(p.getId(), new JPlaceButton(p, tnbPlace));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        initComponents();

        // add the Floors containers
        if (this.floors.size() > 1) {
            // A tab container for 2 or more floors
            JTabbedPane jTabFloors = new JTabbedPane();
            jTabFloors.applyComponentOrientation(getComponentOrientation());
            jTabFloors.setBorder(new javax.swing.border.EmptyBorder(new Insets(5, 5, 5, 5)));
            jTabFloors.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
            jTabFloors.setFocusable(false);
            jTabFloors.setRequestFocusEnabled(false);
            m_jPanelMap.add(jTabFloors, BorderLayout.CENTER);
            int minTabWidth = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnminwidth")));
            int minTabHeight = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnminheight")));
            for (Floor f : this.floors) {
                JFloorPanel floorPanel = this.createFloorPanel(f, cfg, null);
                ThumbNailBuilder tnbcat = new ThumbNailBuilder(minTabWidth, minTabHeight, JFloorPanel.defaultImage());
                ImageIcon icon = new ImageIcon(tnbcat.getThumbNail(f.getImage()));
                jTabFloors.addTab(f.getName(), icon, floorPanel);
            }
        } else if (this.floors.size() == 1) {
            // Just a frame for 1 floor
            Floor f = this.floors.get(0);
            JFloorPanel floorPanel = this.createFloorPanel(f, cfg, f.getName());
            m_jPanelMap.add(floorPanel, BorderLayout.CENTER);
        }

        // Add the reservations panel
        m_jreservations = new JTicketsBagRestaurantRes(app, this);
        add(m_jreservations, "res");
    }

    /**
     * Create a floor panel and bind all place buttons.
     * this.places and this.placeBtns must have been populated before.
     */
    private JFloorPanel createFloorPanel(Floor f, AppConfig cfg, String title) {
        JFloorPanel floorPanel = new JFloorPanel(cfg, title);
        // Bind all table buttons.
        List<Place> places = this.places.get(f.getID());
        for (Place pl : places) {
            JPlaceButton btn = this.placeBtns.get(pl.getId());
            floorPanel.addPlace(btn);
            btn.addActionListener(new MyActionListener(pl));
            btn.update();
        }
        return floorPanel;
    }

    public void activate() {
        // precondicion es que no tenemos ticket activado ni ticket en el panel
        m_PlaceClipboard = null;
        customer = null;
        loadTickets();
        this.releasePlace();
        m_restaurantmap.activate();
        showView("map"); // arrancamos en la vista de las mesas.
        // postcondicion es que tenemos ticket activado aqui y ticket en el panel
        m_jbtnOpenDrawer.setEnabled(m_App.getAppUserView().getUser().hasPermission("button.opendrawer"));
    }

    public boolean deactivate() {
        this.deactivateCurrentPlace();
        // precondicion es que tenemos ticket activado aqui y ticket en el panel
        if (viewTables()) {
            // borramos el clipboard
            m_PlaceClipboard = null;
            customer = null;
            this.saveCurrentOrder();
            this.releasePlace();
            return true;
        } else {
            return false;
        }
        // postcondicion es que no tenemos ticket activado
    }

    private void saveCurrentOrder() {
        if (m_PlaceCurrent != null) {
            try {
                m_panelticket.getController().saveCurrentOrder();
            } catch (BasicException e) {
                new MessageInf(e).show(this);
            }
        }
    }

    public void refreshBagButtons() {}

    private List<Place> getAllPlaces() {
        List<Place> all = new ArrayList<Place>();
        for (Floor f : this.floors) {
            all.addAll(this.places.get(f.getID()));
        }
        return all;
    }

    protected JComponent getBagComponent() {
        return m_restaurantmap;
    }
    protected JComponent getNullComponent() {
        return this;
    }

    /**
     * Deselect the current place if any.
     */
     protected void releasePlace() {
        m_PlaceCurrent = null;
        printState();
        m_panelticket.setActiveOrder(null, null);
    }

    /** Not moveTicket but prepareToMoveTicket.
     * Save the current ticket at its current place, put it in clipboard
     * and release it.
     * It will be moved on next click on a place as the clipboard is set. */
    public void moveTicket() {
        if (m_PlaceCurrent != null) {
            this.saveCurrentOrder();
            m_PlaceClipboard = m_PlaceCurrent;
            customer = null;
        }
        this.releasePlace();
    }

    public void setCustomersCount(int count) {
        if (m_PlaceCurrent != null) {
             m_panelticket.setCustomersCount(count);
        }
    }

    public boolean viewTables(CustomerInfo c) {
        // deberiamos comprobar si estamos en reservations o en tables...
        if (m_jreservations.deactivate()) {
            showView("map"); // arrancamos en la vista de las mesas.
            m_PlaceClipboard = null;
            customer = c;
            printState();
            return true;
        } else {
            return false;
        }
    }

    public boolean viewTables() {
        return viewTables(null);
    }

    /**
     * Deselect the current place.
     * Persistence will be managed by the TicketsEditor.
     */
    public void deactivateCurrentPlace() {
        this.releasePlace();
    }

    public void closeCurrentOrder() {
        if (m_PlaceCurrent != null) {
            try {
                m_panelticket.getController().deleteCurrentOrder();
            } catch (BasicException e) {
                new MessageInf(e).show(this);
            }
            JPlaceButton btn = this.placeBtns.get(m_PlaceCurrent.getId());
            btn.setOccupied(false);
            btn.update();
        }
        this.releasePlace();
    }

    public void loadTickets() {
        Map<String, SharedTicketInfo> sharedTickets = new HashMap<String, SharedTicketInfo>();
        try {
            java.util.List<SharedTicketInfo> l = m_panelticket.getController().getOrderDataSource().getOrderList();
            if (l == null) {
                logger.log(Level.WARNING, "Unable to get shared tickets. List is null.");
                JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_DANGER, "Unable to get shared tickets", "List is null"));
            }
            // Put all tickets in the map indexed by id
            for (SharedTicketInfo ticket : l) {
                sharedTickets.put(ticket.getId(), ticket);
            }
            for (Place table : this.getAllPlaces()) {
                SharedTicketInfo order = sharedTickets.get(table.getId());
                JPlaceButton btn = this.placeBtns.get(table.getId());
                btn.setState(order);
                btn.update();
            }
        } catch (BasicException e) {
            new MessageInf(e).show(this);
        }
    }

    private void printState() {
        if (m_PlaceClipboard == null) {
            if (customer == null) {
                // Select a table
                m_jText.setText(null);
                // Enable all tables
                for (Place place : this.getAllPlaces()) {
                    JPlaceButton btn = this.placeBtns.get(place.getId());
                    btn.setEnabled(true);
                    btn.update();
                }
                m_jbtnReservations.setEnabled(true);
            } else {
                // receive a customer
                m_jText.setText(AppLocal.getIntString("label.restaurantcustomer", new Object[] {customer.getName()}));
                // Enable all tables
                for (Place place : this.getAllPlaces()) {
                    JPlaceButton btn = this.placeBtns.get(place.getId());
                    btn.setEnabled(!btn.isOccupied());
                    btn.update();
                }
                m_jbtnReservations.setEnabled(false);
            }
        } else {
            // Moving or merging the receipt to another table
            m_jText.setText(AppLocal.getIntString("label.restaurantmove", new Object[] {m_PlaceClipboard.getName()}));
            // Enable all empty tables and origin table.
            for (Place place : this.getAllPlaces()) {
                JPlaceButton btn = this.placeBtns.get(place.getId());
                btn.setEnabled(true);
                btn.update();
            }
            m_jbtnReservations.setEnabled(false);
        }
    }

    private SharedTicketInfo getOrder(Place place) {
        try {
            return m_panelticket.getController().getOrderDataSource().getOrder(place.getId());
        } catch (BasicException e) {
            new MessageInf(e).show(JTicketsBagRestaurantMap.this);
            return null;
        }
    }

    private TicketInfo getTicketInfo(Place place) {
        SharedTicketInfo order = this.getOrder(place);
        return (order != null) ? order.getTicket() : null;
    }

    private void setActivePlace(Place place, SharedTicketInfo order) {
        m_PlaceCurrent = place;
        m_panelticket.setActiveOrder(order, m_PlaceCurrent.getName());
    }

    private void showView(String view) {
        CardLayout cl = (CardLayout)(getLayout());
        cl.show(this, view);
    }

    /**
     * Select a table. Open the current order or create a new one.
     * @param targetPlace The place to put a new order on.
     */
    private void openTable(Place targetPlace) {
        TicketInfo ticket = getTicketInfo(targetPlace);
        JPlaceButton targetBtn = this.placeBtns.get(targetPlace.getId());
        if (ticket == null) {
            // Clicked on an empty table
            if (!targetBtn.isOccupied()) {
                // Open an empty table
                ticket = new TicketInfo();
                SharedTicketInfo order = new SharedTicketInfo(targetPlace.getId(), ticket);
                targetBtn.setState(order);
                setActivePlace(targetPlace, order);
            } else {
                // Inconsistency on m_place, empty it to fix
                new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.tableempty")).show(JTicketsBagRestaurantMap.this);
                targetBtn.setOccupied(false); // fixed
            }
            targetBtn.update();
        } else {
            // Clicked on an active table
            if (targetBtn.isOccupied()) {
                setActivePlace(targetPlace, getOrder(targetPlace));
            } else {
                // Inconsistency on m_place, make it active to fix
                new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.tablefull")).show(JTicketsBagRestaurantMap.this);
                if (targetBtn.setState(getOrder(targetPlace))) {
                    targetBtn.update();
                }
            }
        }
    }

    /**
     * Open an empty order to the target place for a customer and select it.
     * @param targetPlace The place to put a new order on.
     * @param customer The customer that reserved the place.
     */
    private void openTableForCustomer(Place targetPlace, CustomerInfo customer) {
        // check if the sharedticket is the same
        TicketInfo ticket = getTicketInfo(targetPlace);
        JPlaceButton targetBtn = this.placeBtns.get(targetPlace.getId());
        if (ticket == null) {
            // receive the customer
            // table occupied
            SharedTicketInfo order = new SharedTicketInfo(targetPlace.getId(),
                    new TicketInfo());
            try {
                DataLogicCustomers dlCust = new DataLogicCustomers();
                order.getTicket().setCustomer(customer.getId() == null
                        ? null
                        : dlCust.getCustomer(customer.getId()));
            } catch (BasicException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotfindcustomer"), e);
                msg.show(JTicketsBagRestaurantMap.this);
            }
            try {
                m_panelticket.getController().getOrderDataSource().saveOrder(order);
            } catch (BasicException e) {
                new MessageInf(e).show(JTicketsBagRestaurantMap.this); // Glup. But It was empty.
            }
            targetBtn.setState(order);
            m_PlaceClipboard = null;
            customer = null;
            setActivePlace(targetPlace, order);
        } else {
            // TODO: msg: The table is now full
            new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.tablefull")).show(JTicketsBagRestaurantMap.this);
            targetBtn.setOccupied(true);
            targetBtn.setEnabled(false);
        }
        targetBtn.update();
    }

    /**
     * Move the place from the clipboald (m_PlaceClipboard) to the target,
     * merge it if required, then select it.
     * @param targetPlace Where to move the clipboard to.
     */
    private void moveClipboard(Place targetPlace) {
        SharedTicketInfo orderClip = getOrder(m_PlaceClipboard);
        JPlaceButton clipBtn = this.placeBtns.get(m_PlaceClipboard.getId());
        if (orderClip == null) {
            // Clipboard is empty, error
            new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.tableempty")).show(JTicketsBagRestaurantMap.this);
            if (clipBtn.setState(null)) {
                clipBtn.update();
            }
            m_PlaceClipboard = null;
            customer = null;
            printState();
            return;
        }
        if (m_PlaceClipboard == targetPlace) {
            // Moving to the same place, cancel
            Place placeclip = m_PlaceClipboard;
            m_PlaceClipboard = null;
            customer = null;
            printState();
            setActivePlace(placeclip, orderClip);
            return;
        }
        SharedTicketInfo targetOrder = getOrder(targetPlace);
        JPlaceButton targetBtn = this.placeBtns.get(targetPlace.getId());
        if (targetOrder == null || targetOrder.isEmpty()) {
            // Moving the receipt to an empty table
            orderClip.setId(targetPlace.getId());
            try {
                // Save the ticket on the new table
                m_panelticket.getController().getOrderDataSource().saveOrder(orderClip);
                targetBtn.setState(orderClip);
                // Remove the ticket from the old table
                m_panelticket.getController().getOrderDataSource().deleteOrder(m_PlaceClipboard.getId());
                clipBtn.setState(null);
            } catch (BasicException e) {
                new MessageInf(e).show(JTicketsBagRestaurantMap.this); // Glup. But It was empty.
            }
            targetBtn.update();
            clipBtn.update();
            // Empty the clipboard and select the target table
            m_PlaceClipboard = null;
            customer = null;
            printState();
            setActivePlace(targetPlace, orderClip);
        } else {
            // Merging with an existing order
            //asks if you want to merge tables
            if (JConfirmDialog.showConfirm(JTicketsBagRestaurantMap.this,
                    AppLocal.tr("Message.MergeTable"),
                    AppLocal.tr("Message.MergeTableQuestion"))) {
                try {
                    // Merge clip into target
                    /* At that time the controller isn’t managing any order
                     * reselect the place in the clipboard to merge. */
                    m_panelticket.getController().setOrder(orderClip, m_PlaceClipboard.getName());
                    m_panelticket.getController().mergeOrderInto(targetOrder, targetPlace.getName());
                } catch (BasicException e) {
                    new MessageInf(e).show(JTicketsBagRestaurantMap.this); // Glup. But It was empty.
                }
                // Update table buttons
                targetBtn.setState(targetOrder);
                clipBtn.setState(null);
                targetBtn.update();
                clipBtn.update();
                // Empty the clipboard and select the target table
                m_PlaceClipboard = null;
                customer = null;
                printState();
                setActivePlace(targetPlace, targetOrder);
            } else {
                // Merge canceled
                Place placeclip = m_PlaceClipboard;
                m_PlaceClipboard = null;
                customer = null;
                printState();
                setActivePlace(placeclip, orderClip);
            }
        }
    }

    /** Action listener to react to clicks on places. */
    private class MyActionListener implements ActionListener {
        private Place m_place;

        public MyActionListener(Place place) {
            m_place = place;
        }

        public void actionPerformed(ActionEvent evt) {
            if (m_PlaceClipboard == null) {
                if (customer == null) {
                    openTable(m_place);
                } else {
                    // receiving customer.
                    openTableForCustomer(m_place, customer);
                }
            } else {
                // Moving a table to a new place
                moveClipboard(m_place);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;

        m_jPanelMap = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        m_jbtnReservations = new javax.swing.JButton();
        m_jbtnRefresh = WidgetsBuilder.createButton(
                ImageLoader.readImageIcon("reload.png"),
                AppLocal.getIntString("button.reloadticket"),
                WidgetsBuilder.SIZE_MEDIUM);
        m_jbtnOpenDrawer = WidgetsBuilder.createButton(
                null,
                AppLocal.getIntString("button.opendrawer"),
                WidgetsBuilder.SIZE_MEDIUM);
        m_jText = new javax.swing.JLabel();

        setLayout(new java.awt.CardLayout());

        m_jPanelMap.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        m_jbtnReservations.setIcon(ImageLoader.readImageIcon("booking.png"));
        m_jbtnReservations.setText(AppLocal.getIntString("button.reservations"));
        m_jbtnReservations.setFocusPainted(false);
        m_jbtnReservations.setFocusable(false);
        m_jbtnReservations.setMargin(new java.awt.Insets(8, 14, 8, 14));
        m_jbtnReservations.setRequestFocusEnabled(false);
        m_jbtnReservations.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jbtnReservationsActionPerformed(evt);
            }
        });
        //jPanel2.add(m_jbtnReservations);

        m_jbtnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jbtnRefreshActionPerformed(evt);
            }
        });
        jPanel2.add(m_jbtnRefresh);
        if (!"Not defined".equals(cfg.getProperty("machine.printer"))) {
            m_jbtnOpenDrawer.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    m_panelticket.openDrawer();
                }
            });
            jPanel2.add(m_jbtnOpenDrawer);
        }
        jPanel2.add(m_jText);

        jPanel1.add(jPanel2, java.awt.BorderLayout.LINE_START);

        m_jPanelMap.add(jPanel1, java.awt.BorderLayout.NORTH);

        add(m_jPanelMap, "map");
    }// </editor-fold>//GEN-END:initComponents

    private void m_jbtnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jbtnRefreshActionPerformed

        m_PlaceClipboard = null;
        customer = null;
        loadTickets();
        printState();

    }//GEN-LAST:event_m_jbtnRefreshActionPerformed

    private void m_jbtnReservationsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jbtnReservationsActionPerformed

        showView("res");
        m_jreservations.activate();

    }//GEN-LAST:event_m_jbtnReservationsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel m_jPanelMap;
    private javax.swing.JLabel m_jText;
    private javax.swing.JButton m_jbtnRefresh;
    private javax.swing.JButton m_jbtnOpenDrawer;
    private javax.swing.JButton m_jbtnReservations;
    // End of variables declaration//GEN-END:variables
}
