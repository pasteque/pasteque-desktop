//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales.restaurant;

import java.awt.Container;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import javax.swing.JPanel;
import org.json.JSONObject;

public class Floor implements Serializable
{
    private static final long serialVersionUID = 8694154682897L;
    private String m_sID;
    private String m_sName;
    @SuppressWarnings("unused")
    /**
     * @deprecated
     * Not used anymore, GUI was moved to {@link fr.pasteque.pos.widgets.JFloorPanel}.
     * Still there to keep serialization compatibility.
     */
    private Container m_container;
    private BufferedImage m_img;

    public Floor(JSONObject o) {
        this.m_sID = String.valueOf(o.getInt("id"));
        this.m_sName = o.getString("label");
        this.m_container = null;
    }

    public String getID() {
        return m_sID;
    }
    public String getName() {
        return m_sName;
    }
    public BufferedImage getImage() {
        return m_img;
    }
    public void setImage(BufferedImage img) {
        m_img = img;
    }

    @SuppressWarnings("unused")
    /**
     * @deprecated
     * Not used anymore, GUI was moved to {@link fr.pasteque.pos.widgets.JFloorPanel}.
     * Still there to keep serialization compatibility.
     */
    private static class JPanelDrawing extends JPanel
    {
        private static final long serialVersionUID = 5484799891136585001L;

        private java.awt.Image img;

        public JPanelDrawing(java.awt.Image img) {
            this.img = img;
            setLayout(null);
        }
    }
}
