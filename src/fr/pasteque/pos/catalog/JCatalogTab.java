//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.catalog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import fr.pasteque.beans.JFlowPanel;

/**
 * A lower-level widget that displays a list of buttons in a FlowLayout with
 * a vertical scrollbar.
 * Use addButton functions to populate the widget.
 * When a button is clicked, an ActionEvent is sent to the listener with
 * its source set to the selected item, id is ACTION_PERFORMED and command
 * is "selected".
 * @author adrianromero
 */
public class JCatalogTab extends javax.swing.JPanel
{
    private static final long serialVersionUID = 1476995489485125983L;

    private JFlowPanel flowpanel;
    private ActionListener listener;

    /**
     * Creates an empty JCatalogTab.
     * @param listener The listener that will receive button click events.
     */
    public JCatalogTab(ActionListener listener) {
        initComponents();
        this.listener = listener;
    }

    @Override // From JPanel
    public void setEnabled(boolean value) {
        flowpanel.setEnabled(value);
        super.setEnabled(value);
    }

    /**
     * Add a textless button at the end of the list.
     * @param ico The icon of the button.
     * @param item The object that will be transfered as the
     * {@link ActionEvent} source on click.
     */
    public void addButton(Icon ico, Object item) {
        JButton btn = new JButton();
        btn.applyComponentOrientation(getComponentOrientation());
        btn.setIcon(ico);
        btn.setFocusPainted(false);
        btn.setFocusable(false);
        btn.setRequestFocusEnabled(false);
        btn.setHorizontalTextPosition(SwingConstants.CENTER);
        btn.setVerticalTextPosition(SwingConstants.BOTTOM);
        btn.setMargin(new Insets(2, 2, 2, 2));
        btn.addActionListener(new SelectedAction(item));
        flowpanel.add(btn);
    }

    /**
     * Add a button at the end of the list. If a button exists with
     * the same label, it does nothing.
     * @param ico The icon of the button.
     * @param id The id of the button, also used as its label.
     * @param item The object that will be transfered as the
     * {@link ActionEvent} source on click.
     */
    public void addButton(Icon ico, String id, Object item) {
        JButton btn = checkButton(id);
        if (btn == null) {
            btn = new JButton();
            btn.setIcon(ico);
            btn.setName(id);
            btn.setFocusPainted(false);
            btn.setFocusable(false);
            btn.setRequestFocusEnabled(false);
            btn.setHorizontalTextPosition(SwingConstants.CENTER);
            btn.setVerticalTextPosition(SwingConstants.BOTTOM);
            btn.setMargin(new Insets(2, 2, 2, 2));
            btn.addActionListener(new SelectedAction(item));
            flowpanel.add(btn);
        }
    }

    /**
     * Remove a labeled button.
     * @param id The id of the button, also used as its label.
     */
    public void delButton(String id) {
        JButton btn = checkButton(id);
        if (btn != null)
            flowpanel.remove(btn);
    }

    /**
     * Get the already existing button with the given id.
     * @param id The id of the button.
     * @return The existing button, null if not already added.
     */
    private JButton checkButton(String id) {
        int count = flowpanel.getComponentCount();
        JButton btn = null;

        for (int i = 0; i < count; i++) {
            if ( ((JButton)flowpanel.getComponent(i)).getName().equals(id) )
                btn = (JButton)flowpanel.getComponent(i);
        }
        return btn;
    }

    /**
     * Send an ActionEvent to the listener upon selection.
     * @param item The item that was selected and is passed
     * to the listener as the {@link ActionEvent} source.
     */
    private void fireSelection(Object item) {
        if (this.listener != null) {
            ActionEvent e = new ActionEvent(item,
                    ActionEvent.ACTION_PERFORMED,
                    "selected");
            this.listener.actionPerformed(e);
        }
    }

    private void initComponents() {
        setLayout(new java.awt.BorderLayout());
        flowpanel = new JFlowPanel();
        JScrollPane scroll = new JScrollPane(flowpanel);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));
        add(scroll, BorderLayout.CENTER);
    }

    /**
     * Convert the button event to item selection event.
     */
    private class SelectedAction implements ActionListener {
        private Object item;
        public SelectedAction(Object item) {
            this.item = item;
        }
        public void actionPerformed(ActionEvent e) {
            fireSelection(item);
        }
    }
}
