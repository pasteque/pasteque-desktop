//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008 Open Sistemas de Información Internet, S.L.
//    http://www.opensistemas.com
//    http://sourceforge.net/projects/openbravopos
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

package fr.pasteque.pos.catalog;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.gui.JMessageDialog;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.sales.TaxesLogic;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.util.ThumbNailBuilder;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * <p>The composition selector widget, showing subgroups and picking a product
 * for each subgroup.</p>
 * <p>The listeners will receive {@link ActionEvent} with ACTION_PERFORMED id:</p>
 * <ul>
 * <li>Upon selection, with the selected {@link ProductInfoExt} as source
 * and product id as command.</li>
 * <li>Upon cancel, with `this` as source and "cancelSubgroupSale" as command.</li>
 * </ul>
 * @author  Luis Ig. Bacas Riveiro	lbacas@opensistemas.com
 * @author  Pablo J. Urbano Santos	purbano@opensistemas.com
 */
public class JCatalogSubgroups extends JPanel
implements ListSelectionListener, CatalogSelector
{
    private static final long serialVersionUID = -1954265654849992292L;

    protected EventListenerList listeners = new EventListenerList();
    private DataLogicSales m_dlSales;
    private TaxesLogic taxeslogic;

    private boolean pricevisible;
    private boolean taxesincluded;

    // Set of Categoriespanels
    private Set<Integer> m_subgroupsset = new HashSet<Integer>();

    private ThumbNailBuilder tnbbutton;
    private ThumbNailBuilder tnbcat;

    /**
     * Create a catalog from the given source, without showing prices.
     * @param dlSales The category and product source.
     */
    public JCatalogSubgroups(DataLogicSales dlSales) {
        this(dlSales, false, false, 64, 54);
    }

    /**
     * Create a JCatalog with detailed options.
     * @param dlSales The category and product source.
     * @param pricevisible If price should be included in product labels.
     * @param taxesincluded If the price should include taxes or not.
     */
    public JCatalogSubgroups(DataLogicSales dlSales, boolean pricevisible, boolean taxesincluded, int width, int height) {
        m_dlSales = dlSales;
        this.pricevisible = pricevisible;
        this.taxesincluded = taxesincluded;

        initComponents();

        m_jListSubgroups.addListSelectionListener(this);
        m_jscrollsubgr.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));
        m_jListSubgroups.setEnabled(false);
        m_jscrollsubgr.setEnabled(false);

//TODO Cambiar imagenes por defecto si hay otras. Cambiar tb en SubgroupEditor
        tnbcat = new ThumbNailBuilder(32, 32, "category_default.png");
        tnbbutton = new ThumbNailBuilder(width, height, "product_default.png");
    }

    /**
     * Get `this`.
     * @return this.
     */
    @Override // From CatalogSelector
    public Component getComponent() {
        return this;
    }

    /**
     * Update subgroups from the given composition and select the first one.
     * @param id The product id. If the product is not a composition, no
     * subgroups are shown.
     */
    @Override // From CatalogSelector
    public void showCatalogPanel(String id) {
        if (id == null) {
            showRootSubgroupsPanel();
        } else {
            try {
                java.util.List<SubgroupInfo> subgroups = m_dlSales.getSubgroups(id);
                m_jListSubgroups.setModel(new SubgroupsListModel<SubgroupInfo>(subgroups));
            } catch (BasicException e) {
                e.printStackTrace();
            }
            if (m_jListSubgroups.getModel().getSize() > 0) {
                m_jListSubgroups.setSelectedIndex(0);
            }
            //showProductPanel(id);
            showRootSubgroupsPanel();
        }
    }

    /**
     * Clear content, the actual loading is done within
     * {@link #showCatalogPanel(String)}.
     * @throws BasicException Never.
     */
    @Override // From CatalogSelector
    public void loadCatalog() throws BasicException {
        taxeslogic = new TaxesLogic(m_dlSales.getTaxList());
        // delete all categories panel
        m_jProducts.removeAll();
        m_subgroupsset.clear();
        // Select the first category
        m_jListSubgroups.setCellRenderer(new SmallSubgroupRenderer());
        // Display catalog panel
        showRootSubgroupsPanel();
    }

    @Override // From CatalogSelector
    public void setComponentEnabled(boolean value) {
        m_jListSubgroups.setEnabled(value);
        m_jscrollsubgr.setEnabled(value);
        m_lblIndicator.setEnabled(value);
        m_btnBack.setEnabled(value);
        m_jProducts.setEnabled(value);
        synchronized (m_jProducts.getTreeLock()) {
            int compCount = m_jProducts.getComponentCount();
            for (int i = 0 ; i < compCount ; i++) {
                m_jProducts.getComponent(i).setEnabled(value);
            }
        }
        this.setEnabled(value);
    }

    /**
     * Does nothing, this widget doesn't support selection.
     * @return 0
     */
    @Override // From CatalogSelector
    public int getSelectedIndex() {
        // TODO: JCatalogSubgroups does not support selection
	return 0;
    }

    /**
     * Does nothing.
     * @return 0
     */
    @Override // From CatalogSelector
    public int getItemCount() {
        // TODO: JCatalogSubgroups does not support selection
	return 0;
    }

    /**
     * Does nothing, this widgets doesn't support selection.
     * @param index Unused.
     */
    @Override // From CatalogSelector
    public void selectItemAt(int index) {
        // TODO: JCatalogSubgroups does not support selection
    }

    @Override // From CatalogSelector
    public void addActionListener(ActionListener l) {
        listeners.add(ActionListener.class, l);
    }
    @Override // From CatalogSelector
    public void removeActionListener(ActionListener l) {
        listeners.remove(ActionListener.class, l);
    }

    /**
     * React to a subgroup selection, do not call it.
     * @param evt The triggered list event.
     */
    public void valueChanged(ListSelectionEvent evt) {
        if (!evt.getValueIsAdjusting()) {
            int i = m_jListSubgroups.getSelectedIndex();
            if (i >= 0) {
                // Lo hago visible...
                Rectangle oRect = m_jListSubgroups.getCellBounds(i, i);
                m_jListSubgroups.scrollRectToVisible(oRect);
            }
        }
    }

    /**
     * <p>Send an {@link ActionEvent} to the listeners when a product is picked.
     * The ActionEvent has prod as source, ACTION_PERFORMED as id and product
     * id as command.</p>
     * <p>Also move to the next subgroup when in guided mode.
     * When the last subgroup is picked then an other ActionEvent is sent,
     * with prod as source, ACTION_PERFORMED as id and "-1" as command.</p>
     * @param prod The product that was picked.
     */
    protected void fireSelectedProduct(ProductInfoExt prod) {
        EventListener[] l = listeners.getListeners(ActionListener.class);
        ActionEvent e = null;

        for (int i = 0; i < l.length; i++) {
            if (e == null) {
                e = new ActionEvent(prod, ActionEvent.ACTION_PERFORMED, prod.getID());
            }
            ((ActionListener) l[i]).actionPerformed(e);
        }

        int index = m_jListSubgroups.getSelectedIndex();
        if (m_jListSubgroups.getModel().getSize() > index) {
            m_jListSubgroups.setSelectedIndex(index + 1);
        }
    }

    /**
     * Call for cancelling the subgroup sale. It sends an {@link ActionEvent}
     * with `this` as source, ACTION_PERFORMED as id and "cancelSubgroupSale"
     * as command.
     */
    private void cancelSubgroupSale() {
        EventListener[] l = listeners.getListeners(ActionListener.class);
        ActionEvent e = null;

        for (int i = 0; i < l.length; i++) {
            if (e == null) {
                e = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "cancelSubgroupSale");
            }
            ((ActionListener) l[i]).actionPerformed(e);
        }

    }

    /**
     * Display the {@link JCatalogTab} for the given subgroup. Build and
     * populate the widget if it is not already cached.
     * Shows an error if the subgroup content cannot be loaded.
     * @param sid The subgroup id.
     */
    private void selectSubgroupPanel(Integer sid) {

        try {
            // Load subgroups panel if not exists
            if (!m_subgroupsset.contains(sid)) {

                JCatalogTab jcurrTab = new JCatalogTab(new SelectedAction());
                m_jProducts.add(jcurrTab, String.valueOf(sid));
                m_subgroupsset.add(sid);

                // Add products
                java.util.List<ProductInfoExt> products = m_dlSales.getSubgroupCatalog(sid);
                for (ProductInfoExt prod : products) {
                    jcurrTab.addButton(new ImageIcon(tnbbutton.getThumbNailText(prod.getImage(), getProductLabel(prod))), prod);
                }
            }

            // Show categories panel
            CardLayout cl = (CardLayout)(m_jProducts.getLayout());
            cl.show(m_jProducts, String.valueOf(sid));
        } catch (BasicException e) {
            JMessageDialog.showMessage(this, new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.notactive"), e));
        }
    }

    private String getProductLabel(ProductInfoExt product) {
    	AppConfig cfg = AppConfig.loadedInstance;
        String prodText = null;
        if(cfg.getProperty("ui.buttons.prodbyref").equals("1")) {
                prodText = product.getReference();
        }
        else {
                prodText = product.getName();
        }
        if (pricevisible) {
            if (taxesincluded) {
                TaxInfo tax = taxeslogic.getTaxInfo(product.getTaxId());
                return prodText + " (" + product.printPriceSellTax(tax) + ")";
            } else {
                return prodText + " (" + product.printPriceSell() + ")";
            }
        } else {
            return prodText;
        }
    }

    private void selectIndicatorSubgroups() {
        // Show root categories panel
        CardLayout cl = (CardLayout)(m_jCategories.getLayout());
        cl.show(m_jCategories, "rootcategories");
    }

    /**
     * Switch to the currently selected subgroup.
     */
    private void showRootSubgroupsPanel() {
        selectIndicatorSubgroups();
        // Show selected root category
        SubgroupInfo s = (SubgroupInfo) m_jListSubgroups.getSelectedValue();
        if (s != null) {
            selectSubgroupPanel(s.getID());
        }
    }

    private class SelectedAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof ProductInfoExt) {
                fireSelectedProduct((ProductInfoExt) e.getSource());
            }
        }
    }

    private class SubgroupsListModel<T> extends AbstractListModel<T>
    {
        private static final long serialVersionUID = 5242013593312007034L;
        private java.util.List<T> m_aSubgroups;
        public SubgroupsListModel(java.util.List<T> aSubgroups) {
            m_aSubgroups = aSubgroups;
        }
        public int getSize() {
            return m_aSubgroups.size();
        }
        public T getElementAt(int i) {
            return m_aSubgroups.get(i);
        }
    }

    private class SmallSubgroupRenderer extends DefaultListCellRenderer
    {
        private static final long serialVersionUID = -8954324981555791019L;
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, null, index, isSelected, cellHasFocus);
            SubgroupInfo s = (SubgroupInfo) value;
            setText(s.getName());
            setIcon(new ImageIcon(tnbcat.getThumbNail(s.getImage())));
            return this;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        m_jCategories = new javax.swing.JPanel();
        m_jRootCategories = new javax.swing.JPanel();
        m_jscrollsubgr = new javax.swing.JScrollPane();
        m_jListSubgroups = new javax.swing.JList<SubgroupInfo>();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        m_jCancel = new javax.swing.JButton();
        m_jSubCategories = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        m_lblIndicator = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        m_btnBack = new javax.swing.JButton();
        m_jProducts = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        m_jCategories.setLayout(new java.awt.CardLayout());

        m_jRootCategories.setLayout(new java.awt.BorderLayout());

        m_jscrollsubgr.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        m_jscrollsubgr.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        m_jscrollsubgr.setPreferredSize(new java.awt.Dimension(210, 0));
        m_jListSubgroups.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        m_jListSubgroups.setFocusable(false);
        m_jListSubgroups.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                m_jListSubgroupsValueChanged(evt);
            }
        });

        m_jscrollsubgr.setViewportView(m_jListSubgroups);

        m_jRootCategories.add(m_jscrollsubgr, java.awt.BorderLayout.WEST);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel3.setLayout(new java.awt.GridLayout(0, 1, 0, 5));

        jPanel3.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 5, 0, 5));
        m_jCancel.setIcon(ImageLoader.readImageIcon("button_cancel.png"));
        m_jCancel.setFocusPainted(false);
        m_jCancel.setFocusable(false);
        m_jCancel.setMargin(new java.awt.Insets(8, 14, 8, 14));
        m_jCancel.setRequestFocusEnabled(false);
        m_jCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jCancelActionPerformed(evt);
            }
        });

        jPanel3.add(m_jCancel);

        jPanel2.add(jPanel3, java.awt.BorderLayout.NORTH);

        m_jRootCategories.add(jPanel2, java.awt.BorderLayout.CENTER);

        m_jCategories.add(m_jRootCategories, "rootcategories");

        m_jSubCategories.setLayout(new java.awt.BorderLayout());

        jPanel4.setLayout(new java.awt.BorderLayout());

        m_lblIndicator.setText("jLabel1");
        jPanel4.add(m_lblIndicator, java.awt.BorderLayout.NORTH);

        m_jSubCategories.add(jPanel4, java.awt.BorderLayout.CENTER);

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel5.setLayout(new java.awt.GridLayout(0, 1, 0, 5));

        jPanel5.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 5, 0, 5));
    m_btnBack.setIcon(ImageLoader.readImageIcon("up_twice.png"));
        m_btnBack.setFocusPainted(false);
        m_btnBack.setFocusable(false);
        m_btnBack.setMargin(new java.awt.Insets(8, 14, 8, 14));
        m_btnBack.setRequestFocusEnabled(false);
        m_btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_btnBackActionPerformed(evt);
            }
        });

        jPanel5.add(m_btnBack);

        jPanel1.add(jPanel5, java.awt.BorderLayout.NORTH);

        m_jSubCategories.add(jPanel1, java.awt.BorderLayout.EAST);

        m_jCategories.add(m_jSubCategories, "subcategories");

        add(m_jCategories, java.awt.BorderLayout.WEST);

        m_jProducts.setLayout(new java.awt.CardLayout());

        m_jProducts.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 5, 0, 0));
        add(m_jProducts, java.awt.BorderLayout.CENTER);

    }// </editor-fold>//GEN-END:initComponents

    private void m_jCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_jCancelActionPerformed
        cancelSubgroupSale();
    }//GEN-LAST:event_m_jCancelActionPerformed

    private void m_btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_btnBackActionPerformed

        showRootSubgroupsPanel();

    }//GEN-LAST:event_m_btnBackActionPerformed

    private void m_jListSubgroupsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_m_jListSubgroupsValueChanged

        if (!evt.getValueIsAdjusting()) {
            SubgroupInfo s = (SubgroupInfo) m_jListSubgroups.getSelectedValue();
            if (s != null) {
                selectSubgroupPanel(s.getID());
            }
        }

}//GEN-LAST:event_m_jListSubgroupsValueChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JButton m_btnBack;
    private javax.swing.JButton m_jCancel;
    private javax.swing.JPanel m_jCategories;
    private javax.swing.JList<SubgroupInfo> m_jListSubgroups;
    private javax.swing.JPanel m_jProducts;
    private javax.swing.JPanel m_jRootCategories;
    private javax.swing.JPanel m_jSubCategories;
    private javax.swing.JScrollPane m_jscrollsubgr;
    private javax.swing.JLabel m_lblIndicator;
    // End of variables declaration//GEN-END:variables

}
