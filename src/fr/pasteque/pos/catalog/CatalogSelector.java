//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.catalog;

import java.awt.Component;
import java.awt.event.ActionListener;
import fr.pasteque.basic.BasicException;

/**
 * Common iterface for item selection, using
 * {@link java.awt.event.ActionListener} for callbacks.
 * @author adrianromero
 */
public interface CatalogSelector
{
    /** Populate the catalog from it's associated source. */
    public void loadCatalog() throws BasicException;
    /**
     * Switch panel to a given id for catalogs with multiple states.
     * @param id State id, can be null.
     */
    public void showCatalogPanel(String id);
    /**
     * Enable or disable all underlying components.
     * @see javax.swing.JComponent#setEnabled(boolean)
     * @param value True if this component should be enabled, false otherwise.
     */
    public void setComponentEnabled(boolean value);
    /**
     * Get the component used to present items for selection.
     * This component must be added to the GUI explicitely.
     * @return The component to include in a GUI.
     */
    public Component getComponent();

    /** Get the index of the currently selected item in the main list. */
    public int getSelectedIndex();
    /** Get the count of items in the main list. */
    public int getItemCount();
    /** Set the selection of the item at the given index in the main list.
     * If index is out of bounds it should do nothing.
     * Listeners will be notified. */
    public void selectItemAt(int index);

    /**
     * Add a listener to respond to selection events.
     */
    public void addActionListener(ActionListener l);
    /**
     * Remove an event listener.
     */
    public void removeActionListener(ActionListener l);
}
