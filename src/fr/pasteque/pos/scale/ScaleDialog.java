//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.scale;

import fr.pasteque.data.gui.JAbstractChoiceDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppProperties;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorNumber;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.awt.Component;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;

/**
 * GUI to enter weight, either manually or from a device.
 */
public class ScaleDialog extends JAbstractChoiceDialog<Double>
{
    private static final long serialVersionUID = 4666774172777585090L;

    @SuppressWarnings("unused") /* Entry point when connecting devices */
    private List<ScaleDevice> devices;

    public ScaleDialog (Component parent, AppProperties config) {
        super(parent, null);
        this.devices = ScaleDeviceFactory.getFromConfig(config);
    }

    /** Create a dialog with the given devices. */
    public ScaleDialog(Component parent, List<ScaleDevice> devices) {
        super(parent, null);
        this.devices = devices;
    }

    /** Show the popup to read weight and return the value. Null if canceled. */
    public Double readWeight(ProductInfoExt prd) throws ScaleException {
        this.productLbl.setText(prd.getName());
        this.keyPadValue.activate();
        this.open();
        return this.getChoice();
    }

    protected void confirm() {
        this.setChoice(keyPadValue.getDoubleValue());
        super.confirm();
    }

    protected void initComponents() {
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        setTitle(AppLocal.getIntString("label.scale"));
        titleLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("label.scaleinput"));
        titleLbl.setIcon(ImageLoader.readImageIcon("scale.png"));
        titleLbl.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, java.awt.Color.darkGray), javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        productLbl = WidgetsBuilder.createLabel("product"); // Will be changed
        okBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"), WidgetsBuilder.SIZE_MEDIUM);
        cancelBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"), WidgetsBuilder.SIZE_MEDIUM);
        keyPad = new JEditorKeys();
        keyPadValue = new fr.pasteque.pos.widgets.JEditorDoublePositive();
        keyPadValue.addEditorKeys(keyPad);

        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirm();
            }
        });
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close();
            }
        });

        javax.swing.JPanel btnContainer = new javax.swing.JPanel();
        btnContainer.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));
        btnContainer.add(okBtn);
        btnContainer.add(cancelBtn);

        // Top part: title
        getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 0;
        cstr.gridwidth = 1;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.insets = new Insets(0, 0, btnSpacing, 0);
        getContentPane().add(titleLbl, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 1;
        getContentPane().add(productLbl, cstr);
        // Left part: keypad and value
        javax.swing.JPanel leftContainer = new javax.swing.JPanel();
        leftContainer.setLayout(new GridBagLayout());
        int yLeft = 0;
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = yLeft;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        leftContainer.add(keyPad, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = ++yLeft;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        leftContainer.add(keyPadValue, cstr);
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 2;
        cstr.fill = GridBagConstraints.BOTH;
        cstr.weightx = 1;
        cstr.weighty = 1;
        getContentPane().add(leftContainer, cstr);
        // Bottom part: buttons
        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 3;
        cstr.gridwidth = 1;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(btnContainer, cstr);
    }

    private javax.swing.JLabel titleLbl;
    private javax.swing.JLabel productLbl;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton okBtn;
    private JEditorKeys keyPad;
    private JEditorNumber keyPadValue;
}
