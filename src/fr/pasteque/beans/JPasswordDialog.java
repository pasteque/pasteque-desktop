//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.beans;

import fr.pasteque.data.gui.JAbstractChoiceDialog;
import fr.pasteque.data.loader.ImageLoader;

import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.widgets.WidgetsBuilder;
import fr.pasteque.pos.forms.AppLocal;

import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.Icon;

/**
 * Dialog to enter a password.
 * It will return null when no password is entered (cancelling).
 */
public class JPasswordDialog extends JAbstractChoiceDialog<String>
{
    private static final long serialVersionUID = -6574820302695169592L;
    private static LocaleResources m_resources;

    public JPasswordDialog(java.awt.Component parent,
            String title, String message, Icon icon) {
        super(parent, null);
        setTitle(title);
        m_lblMessage.setText(message);
        m_lblMessage.setIcon(icon);
    }

    /**
     * All in one function to show the configured dialog.
     * @param parent The component to attach the popup to.
     * @param title The title of the dialog.
     * @param message The prompt for the dialog.
     * @param icon The icon to show along the prompt.
     * @return The entered password, null when canceled.
     */
    public static String showEditPassword(java.awt.Component parent,
            String title, String message, Icon icon) {
        JPasswordDialog pwdDial = new JPasswordDialog(parent,
                title, message, icon);
        pwdDial.open();
        return pwdDial.getChoice();
    }

    protected void confirm() {
        this.setChoice(m_jpassword.getPassword());
        super.confirm();
    }

    protected void initComponents() {
        // load translations
        if (m_resources == null) {
            m_resources = new LocaleResources();
            m_resources.addBundleName("beans_messages");
        }
        // Setup gui components
        AppConfig cfg = AppConfig.loadedInstance;
        int btnSpacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));

        jcmdOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"), WidgetsBuilder.SIZE_MEDIUM);
        jcmdCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        jcmdOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirm();
            }
        });
        jcmdCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close();
            }
        });
        m_jKeys = new fr.pasteque.pos.widgets.JEditorKeys();
        m_jpassword = new fr.pasteque.pos.widgets.JEditorPassword();
        m_jPanelTitle = new javax.swing.JPanel();
        m_lblMessage = WidgetsBuilder.createLabel();

        getContentPane().setLayout(new GridBagLayout());

        int y = 0; m_lblMessage.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, java.awt.Color.darkGray), javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0; c.gridy = y++;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        getContentPane().add(m_lblMessage, c);

        c = new GridBagConstraints();
        c.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        c.gridx = 0; c.gridy = y++;
        getContentPane().add(m_jKeys, c);

        c = new GridBagConstraints();
        c.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        c.gridx = 0; c.gridy = y++;
        c.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(m_jpassword, c);

        javax.swing.JPanel btnsContainer = new javax.swing.JPanel();
        btnsContainer.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, btnSpacing, btnSpacing));
        btnsContainer.add(jcmdOK);
        btnsContainer.add(jcmdCancel);
        c = new GridBagConstraints();
        c.gridx = 0; c.gridy = y++;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(btnSpacing, btnSpacing, btnSpacing, btnSpacing);
        getContentPane().add(btnsContainer, c);

        getRootPane().setDefaultButton(jcmdOK);
        
        m_jpassword.addEditorKeys(m_jKeys);
        m_jpassword.reset();
        m_jpassword.activate();
        
        m_jPanelTitle.setBorder(RoundedBorder.createGradientBorder());
    }

    private javax.swing.JButton jcmdCancel;
    private javax.swing.JButton jcmdOK;
    private fr.pasteque.pos.widgets.JEditorKeys m_jKeys;
    private javax.swing.JPanel m_jPanelTitle;
    private fr.pasteque.pos.widgets.JEditorPassword m_jpassword;
    private javax.swing.JLabel m_lblMessage;
}
