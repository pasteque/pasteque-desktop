# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The semantic of version number is 'Level.Version'. Level is for compatibility between sofwares and Version is the release number.

## [8.24] - 2024-10-21

### Added
- Button to recall the last close cash count when opening the next session (saved locally)
- Print tickets (after payment) and open/close (z) and partial cash session on 3 printers
- Filters in configuration for printer to accept printing orders, tickets or open/close (z)

### Changed
- Merge all search fields in customer finder, search for card and display name

### Removed
- Search by customer number (deprecated field)


## [8.23] - 2024-05-23

### Added
- Show customer's maximum debt when exceeded on payment

### Changed
- Block payment instead of showing an error popup on overly excessive payments

### Fixed
- Switching tariff area (introduced in 8.21)
- Rounding issue with customer's maximum debt on payment
- Payment tab size when using a custom image with zoom
- Floor tab size with zoom
- Category size on sales screen with zoom


## [8.22] - 2024-02-20

### Added
- Rename orders with a custom name
- Backspace button on virtual keypads
- Hidden `ui.catalog.catlistwidth` config option to resize the list of categories
- Missing serialversionUID for some widgets

### Changed
- Use product-like buttons for places with a dedicated widget
- Automatically resize a floor to fit ant fill its container
- Default screen density raised from 72 to 100, renamed to "zoom"
- Switch from TicketInfo to SharedTicketInfo to manage orders
- Refactored JPanelTicket to move order logic and persistence to SalesController
- `machine.uniqueinstance` is enabled by default (only for fresh installations)

### Removed
- ScaleDialog.onManualInput
- 32 bit Windows launcher (use start.bat from the standalone release instead)

### Fixed
- Customer's name is shown on restaurant map
- Picking a composition, using a new CompositionManager
- Excessive payment detection for negative amounts
- Single instance detection before the window is ready
- Warnings about unused variables, imports


## [8.21] - 2023-11-07

### Added
- CustomerDataSource for testing purposes
- Quantity limits on sales screen
- Customizable customer's contact fields from Option `customer.customFields`
- Missing serialversionUID for non-cached classes

### Changed
- Merged ComboBoxValModel and JCobBoxString into JComboBoxVal
- `play` tag in resource uses Alarm instead of AudioClip
- JConfirmDialog was moved from fr.pasteque.pos.widgets to fr.pasteque.data.gui
- Default value for `paper.receipt.y` switched from 287 to 10
- DiscountProfilePicker uses JCatalogTab directly instead of a wierd CatalogSelector
- Dialogs use a multiline text widgets with word wraps and '\n' instead of HTML

### Removed
- Unused and undocumented function or classes

### Fixed
- Searching a ticket by its number
- Default option for confirmation dialog is cancel for excessive payments
- Keyboard shortcuts are not propagated to underlying windows/popups (sales, payments, confirmation)
- Popups adjusts themselves from screen density (close period, confirm dialogs and warnings)
- Silent exception when increasing/decreasing quantity with no selected order line or empty discount rate
- Large blank above ticket header with the default printing configuration (see `paper.receipt.y` change)
- `ui.buttons.prodbyref` hidden setting
- A lot of warnings (generic types, close short lived streams, valueOf instead of deprecated constructors, javadoc errors and a few warnings)


## [8.20] - 2023-02-02

### Added
- Min/max values for numeric input field set in `ui.editornumber.maxvalue` (default 1000000).
- Min/max excess value for payment set in `ticket.maxmanualpayment` (default 500).
- Support for TCP/IP ESCPOS printers.
- 32 bit Windows launcher.
- Tariff area, discount profile and alternative tax on customer's profile form.
- Customer's tariff area is selected when assigned to an order.
- Allow to edit a customer's profile (cannot edit their balance).

### Changed
- Ticket and Z ticket print scripts adpat their content according to characters per line.
- Updated Launch4j to version 3.50.

### Fixed
- False error logged on first startup.
- Loading sound when not run from the base directory.
- Change password, product finder and scale dialog are resized correctly.
- Text in bold is displayed at the correct size for the screen printer.
- Creating a new customer with accented characters.
- Apply customer's discount profile on assignment (introduced in 8.19).
- Print duplicate, place name, customer's balance again on tickets.
- First name/Last name inverted in French on customer's profile form.
- Reseting the checkout info when editing the next order.
- Order discount input is restricted between 0 and 100%.


## [8.19] - 2022-08-25

### Added
- Configuration option to automatically reload data on startup.
- Show last checkout time, amount and change in info box after checking out.
- Show `(expired)` after customer's name in finder.
- Options from API are stored locally, add link to the back office set in `backoffice.url` option.
- Fullscreen window display mode.
- Citizen printer setting for ESCPOS printing.
- Javadoc ant task.
- Basic linting at compile time.

### Changed
- Refactored order management, keypad controller and clock in dedicated classes.
- Fullscreen window is now the default display mode.
- Direct printing (ESCPOS) cut paper and open drawer deprecated codes.
- ESCPOS image printing center the image from dot per line setting.

### Removed
- Unused deprecated hardcoded payment modes.
- User home directory detection for Linux installer.

### Fixed
- GUI after assigning an order to a customer from their card.
- Picking items for compositions.
- Accept transparent pixels with ESCPOS image printing.
- Allow to enter negative payment values when negative amounts are requested.


## [8.18] - 2022-06-09

### Added
- Print the total before discount on the ticket.
- Print tax codes on tickets to link ticket lines to a tax rate.
- JUnit ant tasks, tests for price and tax computations.

### Changed
- Rounding approximation is placed in the tax that introduces the least change when it appears with a discount (the sum of tax bases and amount always equals the discounted total).

### Fixed
- Broken label on startup and cash movement from 8.17.
- Backspace works for numeric input fields, empty fields don't shrink vertically.
- Prevent negative discount rates in line edit.
- Line editing cannot be confirmed without a label and a price.
- JWT is refreshed from the response of an API call.
- About popup size


## [8.17] - 2022-04-07

### Added
- Print count and toggle on open/close cash session.
- Windows executable to start Pasteque, Windows executable wrapper for Izpack.
- Autolaunch option for Windows for Izpack installer.
- Pending order warning on close cash session.

### Changed
- Readme switched to markdown syntax, installer readme merged in dev readme.
- launchers and static icons/shortcuts/user readme moved into /extra.
- Izpack welcome screen, with installation notice. Removed windows registeries because of instability.
- Distribution file names

### Fixed
- Configuration values are now translated.
- Printer selection popup is sized correctly.
- Keyboard shortcuts correctly handled after logout.
- Sync without images doesn't load images in background.
- Free a table when switching screen from the menu when the order is empty.


## [8.16] - 2022-03-09

### Added
- Cash count is saved in memory when counting to close the cash session unless canceled.
- Button to collapse/uncollapse the virtual keypad on sales screen, with default visibility in settings.
- Setting to switch to a pending order or create a new one after checkout in standard mode.
- Add GUI setting to ask for customer count in restaurant mode (was only available in the configuration file).

### Changed
- Default behaviour after checking out an order in standard mode is creating a new one.
- Order saving in local cache was updated, empty orders are discarded (may fix non-empty orders being lost).

### Fixed
- Similar custom products with different labels are not merged anymore.
- Customer count is only asked once when canceled from automatic popup, and only when opening a new table.


## [8.15] - 2022-01-13

### Added
- ui.enablediscounts as hidden option to disable discounts.
- Top 10 customers button in customer finder to restore the default search.
- The number of pending tickets is shown on home screen, with a rewritten text.

### Changed
- Pending tickets are sent by batch of 10 instead of everything at once, with a 1s delay between batches.
- Switch order and new order buttons in standard mode are inactive when there is only one and when the current order is empty respectively.
- Orders label follows the hh:mm:ss format. The number of items and the total value is shown in the switching popup. The customer's name still replaces the time when assigned.
- Customer finder popup layout: the list is displayed in full height on the right, the keypad is under the search fields.

### Fixed
- Opening the drawer with embedded printer driver.
- Cannot assign a discount more than 100%.
- Can enter a negative discount rate, assumed to be a positive one (-10% discount means 10%).
- Remove discount line when entering 0%.
- Pending tickets are displayed in decreasing number in the ticket finder, as it is for sent tickets.
- Change password popup size with various screen density.
- Switch order popup now adjusts its size according to its content


## [8.14] - 2021-10-04

### Added
- Support for expire date for customers (requires to reload the data).
- The label of the product is displayed on the scale dialog.

### Changed
- Product search was redesigned to show the price with taxes and the category and display on multiple lines.
- Added a button to unassign a customer or discount profile instead of using the cancel button.

### Fixed
- Pressing enter on a search popup (product/customer) runs the search instead of closing it.
- Negative quantity with keypad input (-x*barcode).
- Increase the visual feedback of button pressing with keyboard shortcut.
- Error when not setting a value for text fields.
- Disallow a custom product without a label instead of producing an error.
- Note for customers are correctly loaded.
- Ok/Cancel keyboard shortcuts for customer finder popup, ticket search, product search, discount profile search, splitting an order, switching to an other pending order, payment popup.
- Remove a line with a quantity of 0 when updated outside + and - buttons.
- Allow "-" as "-1" for quantity from keypad input.


## [8.13] - 2021-07-07

### Added
- Toggle button on sales screen to stack products on the same line or separate in different lines.

### Fixed
- Physical keyboard input on coin count (introduced in 8.12).
- OK/Cancel keyboard shortcuts on scale dialog, information popup and line edit dialog.
- Prevent opening and closing cash locally on network failure.


## [8.12] - 2021-06-21

### Added
- Allow to change tax when editing an order line.
- Add a custom product to an order.
- Add a button to reload data without reloading images (must be enabled in configuration).
- Show the error amount when counting cash while closing the session.

### Changed
- The default configuration now uses the default printer of the system and has no API url.

### Removed
- Unused configuration entries (report printer, margin type, title/footer bar display, scanpal/scanner).
- Standard screen mode (always use touchscreen-compatible GUI).
- Some unused translations.

### Fixed
- Don't reset payment mode selection when switching currency or adding a payment.
- Print currency name and amount in main currency on tickets for payments.
- Currency conversion when editing an order line.


## [8.11] - 2021-02-04

### Added
- Support for tax rate change with a tariff area.

### Fixed
- Customers count is moved along the order when moving from a table to an other and sumed up when merging tables.
- The customers count is deleted when set to 0.
- French translation for the confirmation message when merging tables.
- Tariff area price is computed correctly.
- Prices in order are updated when switching between tariff areas.
- Do not crash with a currency format with an empty string as separator.
- Paying with an alternate currency.
- Inactive payment modes are not shown.


## [8.10] - 2020-10-15

### Added
- Button to open the cash drawer on the restaurant map.
- Previous cash movements are listed on the cash movement panel.

### Changed
- Read data from cache on startup, reloading is manual (open and close session still require connection).
- Cash session ticket printing is now hardcoded, like tickets (Printer.OpenCash, Printer.CloseCash and Printer.PartialCash are unused).
- Printing an order on the main printer includes logo and total price, but not taxes (for the customers before paying).

### Fixed
- Configuration launchers (configure.bat/sh).
- A resource that was deleted is effectively deleted when refreshed.


## [8.9] - 2020-05-25

### Added
- Perpetual CS in cash session.
- Discount button for line discount is back, discount can still be changed with line edition.
- Ask for confirmation when entering an excessive payment. Can be configured: always, without return mode (default) or never.

### Fixed
- Ask for the quantity when adding a scaled product by it's code.
- Show an error when entering an invalid quantity with the numpad.
- Tables are released when the order is empty and no customer is assigned.
- Restored payment mode images and payment mode value images.
- The cash session cannot be closed while some tickets are waiting to be sent.
- Errors with order-related shortcuts on the restaurant map.
- Full payment when the ticket payment is split in parts.


## [8.8] - 2020-01-23

### Added
- Changelog.
- Buttons to print an order to any configured printer along the one to open the cash drawer.
- Buttons to print the ticket one or multiple times.
- Show the customer's current balance on payments with debt or prepayment.

### Changed
- Moved the machine name configuration entry into the connection subpanel. Wording to match with Android version.
- The size of the product and category buttons is now in the local configuration instead of in a resource.
- Peripheral configuration options are moved into a dedicated subpanel.

### Removed
- The resource `Ticket.Buttons` is now unused. The custom buttons are removed.
- Unused mostly invisible and empty buttons to add taxes to custom lines.
- Link to the back-office that pointed to nothing.

### Fixed
- Print full price on a discounted line to be consistent with the following discount line.
- Crash when selecting a debt payment mode with a positive balance or prepayment with negative balance.


## [8.7] - 2019-08-29

### Added
- New resources: `Printer.Ticket.Header` and `Printer.Ticket.Footer`. Those are plain text.

### Changed
- Payment modes are now read from the API instead of using hardcoded ones.
- Ticket format is simplified, using plain text `Printer.Ticket.Header` and `Printer.Ticket.Footer` resources. The body is now hardcoded (like in pasteque-android).

### Removed
- The resource `Printer.Ticket` is now unused. Use `Printer.Ticket.Header` and `Printer.Ticket.Footer` instead.

### Fixed
- Clicking on a button when counting cash when a value was entered manually now increase the value instead of reseting it.
- Show different error messages on startup when connection failed or when versions are incompatible instead of always incompatible version.


## [8.6] - 2019-02-13

### Added
- New configuration entries `ui.pricevisible` and `ui.taxincluded` and GUI entry to show price on the buttons of the products.
- Keyboard shortcuts.
- Form to add a customer.

### Changed
- The label of the product on its button is now on a solid background to be more readable.
- The default size of the log is now on 5 files of about 512KB instead of 1 of about 50KB.

### Fixed
- Use a connection timeout to prevent blocked requests.
- Prevent the local cache to block the main thread when adding a ticket while the local tickets are being sent.
- Fixed .classpath for Eclipse
- Quantity is read when adding a product from the keypad with its barcode instead of always adding 1.
- The display of untaxed price from taxed price is now correct.


## [8.5] - 2018-08-23

### Added
- Local cash movements.

### Fixed
- Compatibility with Java 11
- Do not crash when computing ZTickets with arbitrary products.
- Floating point arithmetic on counted cash and expected cash is fixed. Counted cash was sometime marked as unmatching even when correct.


## [8.4] - 2018-02-02

### Changed
- Products now stack on the same line when ordered multiple times instead of adding multiple lines.
- Order lines are mutable again, plus and minus buttons are restored.


## [8.3] - 2018-01-23

### Changed
- Default configuration has no customer display.
- The "Next" button to clear the customer display does not show when no customer display is set.

### Fixed
- Ticket line numbers are computed on the fly instead of recomputed after adding a line.


## [8.2] - 2018-01-11

### Added
- Now embeds two fixed fonts for printer and customer display.
- Add a button to manually clear the customer display after payment.
- Can now force to select a payment mode (select none by default), `ui.useemptypayment` in config file and GUI option.

### Changed
- Canceled order lines are showed as a thin empty line.
- Canceled order lines are not printed on the ticket, but are still recorded in the fiscal ticket.
- The display height of order lines when splitting is reverted to the previous value.
- Text in green on black for the customer display.
- Changed the customer display font to 5x7 dot matrix.
- Clear the customer display when closing the application.
- Tickets rejected from the API are now discarded as they are registered server-side.

### Fixed
- Weight/volume is now asked again when clicking on the button of a product.
- Composition products.
- Splitting an order.
- Payment amount when previewing a previous ticket.
- Discount is taken in account on the customer display.
- Fix flickering on the customer display.
- Fix expected cash amount with cash refunds.
- Prevent splitting a canceled order line.
- Discarding rejected tickets prevents the faulty tickets to stay in the local cache forever.


## [8.1] - 2017-12-21

### Added
- Negative quantity can be set with barcode/manual input.

### Changed
- Click on the Edit Line button will edit the next entered order line instead of the selected one.
- Discount is now located in the edit line form instead of having a dedicated button.
- ui.showbarcode is now enabled by default (it shows the manual barcode/quantity input).
- The quantity and barcode inputs are merged into a single one.
- Deleting an order line cancels it instead of removing it completely.
- Order lines are immutable, plus and minus buttons are removed.

### Fixed
- Subcategories are shown under their parent instead of at root level.
- Rounding errors are fixed within customer's balance.
- Screen printer does not clip the ticket on display.


## [8.0] - 2017-12-13

### Added
- Choose the period to close (day, month, year) when closing the cash session. Add sums for CS and taxes for each period.
- Informative taxes by category are shown on the Z ticket.
- Include the total change in customer balances on the Z ticket.
- Continuous cash session flag.

### Changed
- Connects to Version 8 API.
- Use taxed prices to compute the sums (B2C mode), it prevents inconsistencies between totals and sum of each lines.
- Edit sales is now named Check sales (sales are not editable anymore).
- Left-side menu is now hardcoded instead of using a resource.
- Debt and prepayment uses the same balance. Debt is a negative prepayment.
- The customer data is not refreshed when saving a ticket to prevent inconsistencies.
- Cannot delete previous tickets.

### Removed
- Pricebuy is not used and is removed from the client.
- Product attributes.
- Special barcodes prefixed by 250 that crashed the client.
- Refund screen.
- Resource based custom code are removed.
- Simple ticket mode is removed. The default one is now Standard.
- Orders are not shared anymore between cashes and server. They are local only.

### Fixed
- Switching to level 8 fixed a few issues when loading/reloading data. A lot of undead code was shot down but some may have been summoned.
- Prepaid and debt is updated locally when saving a ticket (prevents inconsistencies with offline mode).
- The configuration panel does not crash anymore when no data are loaded.


## [7.1] - 2017-05-23

### Added
- Dot per line and Dot per character in printer configuration.
- Start in fullscreen mode when resulution is 1024x768 or lower.

### Fixed
- Cash sequence number is correctly set when opening the cash session.
- Fix rounding issues on the discount amount of a ticket.
- Fix tax amount on a ticket with discount.
- Updated RXTX lib to fix some printing issues.
- The size of the logo is printed with the correct size.
- ESCPos printing codes size x2 and x4.
- Delete a failing shared ticket update instead of blocking updates forever.
- Save customer count in shared tickets.


## [7.0] - 2017-02-14

### Added
- Allow to delete a failing ticket that cannot be saved on the server.
- Tickets waiting to be saved on the server are shown in the ticket finder.
- Emit a sound on alerts.
- New icons.

### Changed
- Requires version 7 of the server.
- The default server is now https://my.pasteque.org, with no demo user and password. Open the configuration screen when no user is set.
- About: updated website and removed references of Scil.
- Compositions can be in any category instead of a fixed one grouping them all.
- Save every ticket locally to save them asynchronously instead of trying to save them synchronously and lock the GUI.
- Restaurant map is cached locally to prevent reloading it everytime from the server.
- Use JWT token for authentication.
- Load all data with a single sync API call.
- Product and category images are loaded with dedicated API calls.

### Removed
- Cannot edit a previous ticket because it was crashing.

### Fixed
- Allow to find a customer which have a null value in a field.


## [6.0.2]
Same as 6.0.1


## [6.0.1] - 2015-10-26

### Added
- Property to show or hide barcode input.
- Button to reload the customers.

### Changed
- Don't show barcode input by default.
- Default configuration values for the demo account.
- Removed execution rights on linux launch scripts.

### Fixed
- Startup on Windows with the local cache DB (same as for 5.0.3).
- Do not crash when category display mode is not set (set ui.button.catbyref to 0 by default).


## [6.0.0] - 2015-09-15

Includes changes from 1.4.x and up to 2.0-alpha8 but not 5.0.x versions.

### Added
- Show reference or label for products and categories (ui.buttons.prodbyref and ui.buttons.catbyref configuration entries, no configuration GUI).

### Changed
- Requires Java 1.7 instead of 1.6.
- Default locale is French.
- Faster startup with multiple threads.
- Shared tickets are transfered in JSON format instead of binary.
- Website url.

### Fixed
- Fix crash when loading a category with it's reference set to null.
- Size of the discount button.


## [5.0.4] - 2015-01-08

### Changed
- Larger and configurable order display zone. (ui.ticketlineminwidth and ui.ticketlineminheight configuration entries, no configuration GUI).
- Website url.

### Fixed
- Try to send pending tickets on startup.
- Correctly detect if there are pending tickets.
- Allow alphanumeric customer number.


## [5.0.3] - 2014-10-21

### Changed
- The default logging level is WARNING (instead of INFO).

### Fixed
- H2 library link for Windows.


## [5.0.2] - 2014-09-24

### Fixed
- Cash sequence number (was always 0).
- Loading images through the image API.


## [5.0.1] - 2014-08-28

### Changed
- Logo and icons.


## [5.0.0] - 2014-08-06

Diverged from master.

### Added
- Option to hide barcode input (ui.showbarcode, no configuration GUI).
- Alert when using the demo account, button to open the browser to create an account.

### Changed
- Use API Level as major version number. Client and server are compatible only with the same major version number.
- New scrollbar for categories.
- Moved ticket discount button.
- New icon for the checkout button.


### [2.0 alpha8] - 2014-06-03

Includes changes from 1.4.x.

### Added
- Restored the ability to delete a ticket.

### Changed
- Reset coin count when entering open/close cash screen.

### Fixed
- Open drawer on cash open/close.
- Size of buttons in ticket edit (Edit, Delete).
- Enhanced discount profile picker layout.
- Web and close icons in about.


## [1.4.2] - 2015-05-28

### Fixed
- Restore focus on barcode input after picking a composition or refunding.


## [1.4.1] - 2014-04-17

### Added
- Show subtotal on tax report.

### Changed
- Show vat prices and sort by date on customer's diary.

### Fixed
- Disable restore configuration when no restoration file is present.


## [2.0 alpha7] - 2014-04-09

### Added
- Discount profiles
- Show expected cash along closed cash.
- Print a ticket when opening a cash session
- Print coin count on open/close ticket.
- Show customer's prepaid and debt amount in the message box when selected (and restore message box).

### Changed
- Don't print open/close coin counts when the count is not set or 0.
- The configuration file was moved from ~/.pasteque.properties to ~/.pasteque/config.properties.

### Fixed
- Disable restore configuration when no restoration file is present.


## [2.0 alpha6] - 2014-04-06

### Added
- Local queue for offline mode.
- Local cache for shared tickets and cash session, predict next ticket number.
- Clock on sales screen.

### Removed
- Reservations button.
- Attributes.

### Changed
- Hide unused message box.
- Sort product and categories by name when they have the same dispOrder.
- Switched from MessageFormat to gettext.

### Fixed
- Fix ticket search with arbitrary product.
- Caching a category which has null as dispOrder.
- Customer payments is shown correctly in ticket edit.
- Refresh customer after recovering debt.
- Set coin count to 1 after clicking on it after clearing the count.
- Hide products not sold in catalog.


## [2.0 alpha5] - 2014-03-21

### Added
- Preset starting value for Z ticket search to 7 days before.
- Browse and reprint Z tickets.
- Allow to print tax base and rate on tickets.
- User images and visible/invisible state.
- Huge button size for products.

### Removed
- Title and footer bars.

### Changed
- Load all shared tickets in restaurant mode at once instead of one by one.
- Default logging to a file in ~/.pasteque/
- Hide up/down buttons by default.
- Numpad layout, ticket lines in sales screen.

### Fixed
- Refresh the customer after saving a ticket.
- Ticket search by number.


## [2.0 alpha4] - 2014-03-18

### Added
- Translation for cash register not found error.
- Restored barcode input.
- Load compositions and product and category images from the server.
- Discount printing on ticket.
- Restored line discount, restored product search, tariff areas, cash movements and customer's payments.
- Coin buttons on cash movements, open the drawer and show confirmation when saved.
- Open the drawer on open/close cash session.
- Local database cache for currencies, categories, taxes and products.
- Search ticket by customer or user.
- DispOrder on products.

### Removed
- Installer top header images.

### Changed
- Ignore other search criterias when searching a ticket by it's number.
- Search tickets only from opened session when editing.

### Fixed
- Login through API.
- Discount on a refund.
- Include cash movements when checking cash on close.
- User name display on tickets.
- Button size on ticket edit.
- Uninstaller.


## [1.4.0] - 2014-02-12

### Added
- Installer.
- Screen density dependent button sizes.
- Website in about.
- Some icons in the admin menu.

### Removed
- Useless default Guest role and user for new databases.

### Changed
- Some wording on customer reports.
- Restaurant map is loaded from the server instead of directly from the database.
- Tickets and shared tickets are saved through the API instead of directly to the database.
- Default VAT rate for France.


## [2.0 alpha3] - 2014-02-05

### Added
- Binary cache for customers, roles, users and resources, preloaded on startup.
- Popup to change user's password.


## [2.0 alpha2] - 2014-01-24

### Added
- Count cash on open and close, print open/close/expected cash.
- Read discount from customer's discount profile.

### Fixed
- Can open cash whis disabled counting.
- Reading multiple shared tickets.
- Subtotal label on close cash screen.


## [2.0 alpha1] - 2014-01-20

### Added
- Localized splash screen.

### Removed
- Search products, Attributes and line discount until reenabled.
- Title and date in brand header.

### Changed
- Package name from com.openbravo to fr.pasteque.
- Source files renamed from src-pos to src.
- Use API calls instead of retreiving data directly from the database.

### Fixed
- Missing prepaid payment icon.


## [1.4 alpha1] - 2014-01-03

### Added
- Default value for ticket search to match the cash session.
- Display the number of customers on a table on the restaurant map.
- Automatically ask for the number of customers when opening a table (remove with ui.autodisplaycustcount="0" in configuration).
- Display subtotal on close screen.
- Some icons in the left menu.

### Changed
- Wording on catalog report, label for parent category in category edit.

### Fixed
- Customer automatic number above 10.


## [1.3.2] - 2013-12-06

### Fixed
- dist ant task.
- Derby and Postgresql database upgrade.
- Payment total label on close cash.
- Set default prepaid amount to 0 for new customers.
- Set default search key for customers when not manually assigned.
- Stock management buy and sell prices.


## [1.3.1] - 2013-10-15

### Added
- Glancetron customer display compatibility.

### Fixed
- Display taxed price on customer's display.
- Aggregated sales report with Derby database.
- 2,1% French tax name on database creation.
- Price unrounding when editing a product.
- Windows icon file.
- Postgresql database.
- Product labels with altenative themes.


## [1.3.0] - 2013-08-06

### Added
- Order field on composition subgroups.

### Fixed
- Show categories and subgroups in order on sales screen.


## [1.3 beta6] - 2013-07-20

### Fixed
- Crash with +/- buttons on composition edit when no product is selected.
- Don't show compositions on the product panel.


## [1.3 beta5] - 2013-07-18

### Fixed
- Ticket editing.
- 2,1% French tax rate on database creation.
- Embbed mysql connector.


## [1.3 beta4] - 2013-07-17

### Fixed
- Don't show compositions and prepayment in inventory.
- Free payment.
- Consolidated sales with Derby database.


## [1.3 beta3] - 2013-07-15

### Added
- About screen.

### Changed
- Don't show version in the window title.

### Fixed
- Set taxes when reprinting a ticket.


## [1.3 beta2] - 2013-06-27

### Added
- Date on inventory report.

### Fixed
- Cash movement with the main currency.


## [1.3 beta1] - 2013-06-20

### Added
- Select a tariff area on sales screen.
- Pick a composition on sales screen.
- Prepayment payment mode and refill.

### Fixed
- Show untaxed price in consolidated sales.
- Price display in catalog.
- Disable buttons when picking a composition.
- Currency amount in close cash.
- Cropped +/- buttons on composition edit.
- Derby database compatibility.


## [1.3 alpha2] - 2013-05-31

### Added
- Reset stock operation.

### Fixed
- Payment format in alternative currency on payment screen.
- Refresh prices when selecting a tariff area.
- Database scripts for compositions.
- French wording on consolidated sales report.


## [1.3 alpha1] - 2013-05-29

### Added
- Tariff areas.
- Compositions.
- Prepayments.
- Currencies.
- Import stock movement from a csv file.
- Automatically assign customer's number and card on creation.
- Raw sales and consolidated sales report.
- Automatically get the last 10 tickets in ticket search.
- Option in the configuration to print the ticket by default (ui.printticketbydefault with GUI).
- Show database codename in version.
- Allow to delete (disable) products when they were already sold.

### Fixed
- Get product by reference.
- Product dispOrder can be null (empty value).


## [1.2.1] - 2013-05-17

### Added
- Print taxed price by default on tickets.


## [1.2.0] - 2013-03-28

### Added
- Some Pasteque logos.
- Linux and Windows launcher scripts.

### Changed
- Default data are now in French.

### Fixed
- Use hi-rez default icons for product and categories, fix image thumbnailer.
- Include customers without sales in top 10.


## [1.2 beta3] - 2013-03-17

### Removed
- References to Openbravo POS in default templates.

### Fixed
- Set quantity with numpad input and clicking on a product.


## [1.2 beta2] - 2013-03-04

### Added
- Norwegian translation.

### Fixed
- Crash on product attributes.


## [1.2 beta1] - 2013-02-26

### Added
- Margin around validation buttons in configuration panel.
- Print sales by category by default on close cash ticket.


## [1.2 alpha3] - 2013-02-22

### Added
- Aggregated close cash report.
- Sales by category on close cash.
- Some button tooltips.

### Changed
- Can close the cash session when no ticket is registered.

### Fixed
- Screen overflow on close cash on small displays.
- Compatibility with Java 1.7.
- Closing cash with tickets without customers count.


## [1.2 alpha2] - 2013-02-08

Changes from 1.1.X and 1.2.

### Added
- Display version on startup screen.
- Print customer count on ticket.
- Pressing `/` on the keyboard now clear the keypad.
- Print the number of customers in restaurant mode and the average on Z ticket.
- Print the average price per customer in restaurant mode on Z ticket.
- `README DEV` for compiling and sending patches with git instructions.
- Button spacing configuration entry.
- Reset on the configuration screen reloads the configuration from `.restore` files if they exists.
- `debug` target for ant to recompile only changes.
- Support for Mettler Toledo 8217 scales.
- Tooltips for buttons on sale screen.
- `Page up` and `Page down` select the next/previous line in the current order.

### Changed
- Use the default category icon for subcategories.
- All source files are now located under `src-pos`, launcher scripts in `launchers`.
- Order date is not shown in name when a customer is assigned. It shows in different color when one is assigned.
- The customer selection popup shows the top 10 customers by default instead of nothing.
- Attribute selection popup shows automatically when adding a product with an attribute set.

### Removed
- (Online?) payment configuration is removed.

### Fixed
- Do not print inventory when no resource `Printer.Inventory` is set.
- `start.sh` is executable.
- Sales by customer French translation.


## [1.1.4] - 2013-01-03

### Changed
- The pattern `* X -` with the keypad adds negative quantity instead of one unit with negative price.
- Screen density is taken in account for keypads in sales screen, order line edit and search products.

### Fixed
- `'` does not shows as `&apos` (apos entity is no supported).
- Does not crash on startup when no database is configured.
- Keep focus on payment amount when paying with debt.


## [1.1.3] - 2012-12-18

### Fixed
- Refund display.


## [1.1.2] - 2012-09-28

### Fixed
- Customer barcode generation.


## [1.1.1] - 2012-09-20
From version 1.1.0.

### Fixed
- Fix dividing payments in cash not allowing to validate when all parts are paid (floating point arithmetic).


## [1.2 alpha1] 2012-09-14

### Added
- Apply automatic discounts when adding a product.
- Count customers on a table in restaurant mode.
- Sales by customer report.

### Changed
- Use percent for product discount instead of rate.
- Order line display shows only article, quantity and total price by default.
- Screen density is taken in account on scale popup.

### Fixed
- Can now upgrade from multiple versions at once.


## [1.1.0]

Nothing changed from 1.1.0 beta2.


## [1.1.0 beta2] - 2012-07-23

### Added
- Sales by customer report.
- Automatic discount for products.

### Changed
- Screen density is taken in account on ticket splitting and customer assignment.
- Products sold by scale now count as 1 article instead of it's weight.

### Fixed
- Discounts are not count as articles.
- A few bugs in sales screen.
- Margin rate computation.


## [1.1.0 beta1] - 2012-07-03

### Added
- New configuration options: screen density, show title and/or footer bars, auto-hide menu.
- Divide payments in equal parts.
- French starting data.
- Select which users are selectable on startup.
- Show margin as percent or ratio.
- GNU/Linux install script.

### Changed
- Buy price for product is not mandatory.
- Cash session has to be opened manually.
- Taxes and stocks are managed in a dedicated panel.
- Customer tax configuration is accessible from taxes and customers panels.
- Taxed price are rounded
- Icons for attribute panels are different from one to another.

### Fixed
- Derby and Postgresql creation and update scripts.


## [1.0.0] 2012-04-16

### Added
- DB_VERSION for compatibility.
- Splash screen.
- French translation.

### Changed
- Changed name and logo to POS-Tech. Using Major.minor.patch versionning. Major is the compatibility number (DB_VERSION), minor is for new features, patch is for fixes.
- Changed APP_NAME and APP_ID to `POS-Tech` and `postech` respectively.
- Increased the default size of the button of product.
- Rewrited README completely.

### Removed
- Welcome message from Openbravo POS.
- HSQLDB and Oracle database supports.


## [2.30.2.sc3]


## [2.30.2.sc2] - 2012-03-28


## [2.30.2.sc1] - 2012-02-23

### Added
- Openbravo POS Version 2.30.2.
- Printer margin.
