Hack font, from https://github.com/source-foundry/Hack

Hack work is © 2018 Source Foundry Authors. MIT License

Bitstream Vera Sans Mono © 2003 Bitstream, Inc. (with Reserved Font Names Bitstream and Vera). Bitstream Vera License.

The font binaries are released under a license that permits unlimited print, desktop, web, and software embedding use for commercial and non-commercial applications.

See hack-licence.txt for the full texts of the licenses.
